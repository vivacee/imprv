/*******************************************************************************
 * Copyright (c) 2015 Bosch Software Innovations GmbH. All rights reserved.
 ******************************************************************************/
angular.module("app.controllers.administration", ['app.controllers.base', 'dndLists'])
    .controller(
        "AdministrationCtrl",
        [
            "$rootScope",
            "$scope",
            "$controller",
            "$location",
            "$state",
            "$q",
            "growl",
            "UserService",
            "UsersAdministrationService",
            "UserGroupsAdministrationService",
            "RolesAdministrationService",
            "PermissionsService",
            function($rootScope, $scope, $controller, $location, $state, $q, growl, UserService,
                     UsersAdministrationService, UserGroupsAdministrationService,
                     RolesAdministrationService, PermissionsService) {

                angular.extend(this, $controller('BaseCtrl', {
                    $scope: $scope
                }));

                $scope.usersAdminControls = {
                    resourceView: 'usersView',
                    selectionView: ''
                }

                $scope.clearControls = function() {
                    $scope.buttons = {
                        collapsed: true,
                        confirm: false
                    }
                    $scope.errors = {};
                    $scope.newObject = {};
                }
                $scope.clearControls();

                $scope.firstSelected = false;
                $scope.firstSelectedRow = {};

                $scope.init = function(supressEvents) {
                    if ($location.$$search.resourceView) {
                        $scope.usersAdminControls.resourceView = $location.$$search.resourceView;
                        if ($location.$$search.uuid) {
                            switch ($location.$$search.resourceView) {
                                case 'usersView':
                                    $scope.loadAllUsers(function() {
                                        for (var i = 0; i < $scope.data.users.length; ++i) {
                                            if ($scope.data.users[i].uuid == $location.$$search.uuid)
                                                $scope.selectInitialRow($scope.data.users[i], supressEvents);
                                        }
                                    });
                                    break;
                                case 'userGroupsView':
                                    $scope.loadAllUserGroups(function() {
                                        for (var i = 0; i < $scope.data.userGroups.length; ++i) {
                                            if ($scope.data.userGroups[i].uuid == $location.$$search.uuid)
                                                $scope.selectInitialRow($scope.data.userGroups[i], supressEvents);
                                        }
                                    });
                                    break;
                                case 'rolesView':
                                    $scope.loadAllRoles(function() {
                                        for (var i = 0; i < $scope.data.roles.length; ++i) {
                                            if ($scope.data.roles[i].uuid == $location.$$search.uuid)
                                                $scope.selectInitialRow($scope.data.roles[i], supressEvents);
                                        }
                                    });
                                    break;
                            }
                        }
                    }
                }

                $scope.addUser = function() {
                    var user = {
                        username: $scope.newObject.username,
                        email: $scope.newObject.email,
                        firstName: $scope.newObject.firstName,
                        lastName: $scope.newObject.lastName,
                        organization: {
                            uuid: $scope.currentUser.organization.uuid
                        },
                        password: $scope.newObject.password,
                        userGroups: [],
                        roles: [],
                        permissions: [],
                        contacts: []
                    };
                    UsersAdministrationService.addUser(user).then(function(newUser) {
                        $scope.deselectAll(function() {
                            $location.search('uuid', newUser.uuid);
                            $scope.init();
                        });
                    });
                }

                $scope.addUserGroup = function() {
                    var userGroup = {
                        displayName: $scope.newObject.displayName,
                        name: $scope.newObject.displayName,
                        description: $scope.newObject.description,
                        organization: {
                            uuid: $scope.currentUser.organization.uuid
                        }
                    };
                    UserGroupsAdministrationService.addUserGroup(userGroup).then(
                        function(newUserGroup) {
                            $scope.deselectAll(function() {
                                $location.search('uuid', newUserGroup.uuid);
                                $scope.init();
                            });
                        });
                }

                $scope.addRole = function() {
                    var role = {
                        displayName: $scope.newObject.displayName,
                        name: $scope.newObject.displayName,
                        description: $scope.newObject.description,
                        organization: {
                            uuid: $scope.currentUser.organization.uuid
                        }
                    };
                    RolesAdministrationService.addRole(role).then(function(newRole) {
                        $scope.deselectAll(function() {
                            $location.search('uuid', newRole.uuid);
                            $scope.init();
                        });
                    });
                }

                $scope.removeUsers = function() {
                    for (var i = 0; i < $scope.data.selectedUsers.length; i++) {
                        $scope.removeUser($scope.data.selectedUsers[i]);
                    }
                }

                $scope.removeUser = function(user) {
                    UsersAdministrationService.deleteUser(user.uuid).then(function() {
                        $scope.$emit("elementDeSelectedEvent", user.uuid, user);
                        var index = $scope.data.users.indexOf(user);
                        $scope.data.users.splice(index, 1);
                    })
                }

                $scope.removeUserGroups = function() {
                    for (var i = 0; i < $scope.data.selectedUserGroups.length; i++) {
                        $scope.removeUserGroup($scope.data.selectedUserGroups[i]);
                    }
                }

                $scope.removeUserGroup = function(userGroup) {
                    UserGroupsAdministrationService.deleteUserGroup(userGroup.uuid).then(function() {
                        $scope.$emit("elementDeSelectedEvent", userGroup.uuid, userGroup);
                        var index = $scope.data.userGroups.indexOf(userGroup);
                        $scope.data.userGroups.splice(index, 1);
                    })
                }

                $scope.removeRoles = function() {
                    for (var i = 0; i < $scope.data.selectedRoles.length; i++) {
                        $scope.removeRole($scope.data.selectedRoles[i]);
                    }
                }

                $scope.removeRole = function(role) {
                    RolesAdministrationService.deleteRole(role.uuid).then(function() {
                        $scope.$emit("elementDeSelectedEvent", role.uuid, role);
                        var index = $scope.data.roles.indexOf(role);
                        $scope.data.roles.splice(index, 1);
                    })
                }

                $scope.$watch('newObject.password', function() {
                    if (!$scope.errors) return;
                    if (!$scope.newObject.password) return;
                    $scope.errors.password = [];
                    if (/[a-z]/.test($scope.newObject.password) == false) {
                        $scope.errors.password.push('Enter at least one lower case letter.');
                    }
                    if (/[A-Z]/.test($scope.newObject.password) == false) {
                        $scope.errors.password.push('Enter at least one upper case letter.');
                    }
                    if (/\d/.test($scope.newObject.password) == false) {
                        $scope.errors.password.push('Enter at least one number.');
                    }
                    if (/\W+/.test($scope.newObject.password) == false) {
                        $scope.errors.password.push('Enter at least one special character.');
                    }
                })

                $scope.$watch('newObject.password2', function() {
                    if (!$scope.errors) return;
                    if (!$scope.newObject.password2) return;
                    $scope.errors.password2 = [];
                    if ($scope.newObject.password2 !== $scope.newObject.password) {
                        $scope.errors.password2.push('Passwords should match');
                    }
                })

                $scope.$watch('usersAdminControls.resourceView', function(newView, oldView) {
                    if (newView !== oldView) $scope.deselectAll(function() {
                    });
                    $location.search('resourceView', newView);
                    switch (newView) {
                        case 'usersView':
                            if (!$location.$$search.uuid) {
                                $scope.loadAllUsers(function() {
                                    $scope.usersAdminControls.selectionView = 'userGroupsView';
                                    if ($scope.data.users.length > 0)
                                        $scope.selectInitialRow($scope.data.users[0]);
                                });
                            } else
                                $scope.usersAdminControls.selectionView = 'userGroupsView';
                            break;
                        case 'userGroupsView':
                            if (!$location.$$search.uuid) {
                                $scope.loadAllUserGroups(function() {
                                    $scope.usersAdminControls.selectionView = 'usersView';
                                    if ($scope.data.userGroups.length > 0)
                                        $scope.selectInitialRow($scope.data.userGroups[0]);
                                });
                            } else
                                $scope.usersAdminControls.selectionView = 'usersView';
                            break;
                        case 'rolesView':
                            if (!$location.$$search.uuid) {
                                $scope.loadAllRoles(function() {
                                    $scope.usersAdminControls.selectionView = 'permissionsView';
                                    if ($scope.data.roles.length > 0)
                                        $scope.selectInitialRow($scope.data.roles[0]);
                                });
                            } else
                                $scope.usersAdminControls.selectionView = 'permissionsView';
                            break;
                    }
                    $scope.firstSelected = true;
                }, true);

                $scope.$watch('usersAdminControls.selectionView', function(newView) {
                    if (newView && !jQuery.isEmptyObject($scope.currentSelection)) {
                        $scope.refreshDisplay(newView);
                    }
                }, true);

                $scope.loadAllUsers = function(callback) {
                    UsersAdministrationService.getUsers().then(function(users) {
                        $scope.data.users = users.plain();
                        if (callback) callback();
                    });
                }

                $scope.loadAllRoles = function(callback) {
                    RolesAdministrationService.getRoles().then(function(roles) {
                        $scope.data.roles = roles.plain();
                        if (callback) callback();
                    });
                }

                $scope.loadAllUserGroups = function(callback) {
                    UserGroupsAdministrationService.getUserGroups().then(function(userGroups) {
                        $scope.data.userGroups = userGroups.plain();
                        if (callback) callback();
                    });
                }

                $scope.loadAllPermissions = function(callback) {
                    PermissionsService.getPermissions().then(function(permissions) {
                        $scope.data.permissions = permissions.plain();
                        if (callback) callback();
                    });
                    /*
                     * TODO: Use when Permission Service is ready
                     * PermissionsAdministrationService.getPermissions().then(function(permissions) {
                     * $scope.data.permissions = permissions.plain(); callback();
                     * });
                     */
                }

                $scope.data = {
                    users: [],
                    roles: [],
                    userGroups: []
                };

                // Will be more useful if/when we enable multiple selections
                $scope.allSelected = {
                    users: [],
                    roles: [],
                    userGroups: []
                };

                $scope.currentSelection = {};

                $scope.existsSelection = false; // Used in administration.html to
                // update selection display

                $scope.updateSelectionFlag = function() {
                    if ($scope.allSelected.users.length > 0 || $scope.allSelected.roles.length > 0
                        || $scope.allSelected.userGroups.length > 0) {
                        $scope.existsSelection = true;
                    } else {
                        $scope.existsSelection = false;
                        $location.search('uuid', null);
                    }
                }

                $scope.selectInitialRow = function(row, supressEvents) {
                    row.isSelected = true;
                    $scope.firstSelectedRow = row;
                    if (supressEvents == undefined || supressEvents == false)
                        $scope.$emit("elementSelectedEvent", row.uuid, row);
                }

                $scope.deselectAll = function(callback) {
                    for (var i = 0; i < $scope.allSelected.users.length; ++i) {
                        $scope.allSelected.users[i].isSelected = false;
                    }
                    for (var i = 0; i < $scope.allSelected.roles.length; ++i) {
                        $scope.allSelected.roles[i].isSelected = false;
                    }
                    for (var i = 0; i < $scope.allSelected.userGroups.length; ++i) {
                        $scope.allSelected.userGroups[i].isSelected = false;
                    }
                    $scope.allSelected.users = [];
                    $scope.allSelected.roles = [];
                    $scope.allSelected.userGroups = [];
                    $scope.currentSelection = {};
                    $scope.clearControls();
                    $scope.updateSelectionFlag();
                    callback();
                }

                $scope.$on("elementSelectedEvent", function(e, uuid, row) {
                    // Must deselect initially selected row (if applicable)
                    if ($scope.firstSelected && $scope.firstSelectedRow !== row) {
                        $scope.firstSelectedRow.isSelected = false;
                        $scope.$emit("elementDeSelectedEvent", $scope.firstSelectedRow.uuid,
                            $scope.firstSelectedRow);
                        $scope.firstSelected = false;
                    }
                    $location.search('uuid', uuid);
                    switch (row.className) {
                        case 'User':
                            $scope.allSelected.users.push(row);
                            $scope.currentSelection = row;
                            break;
                        case 'UserGroup':
                            $scope.allSelected.userGroups.push(row);
                            $scope.currentSelection = row;
                            break;
                        case 'Role':
                            $scope.allSelected.roles.push(row);
                            $scope.currentSelection = row;
                            break;
                    }
                    $scope.refreshDisplay($scope.usersAdminControls.selectionView);
                    $scope.updateSelectionFlag();
                })

                $scope.$on("elementDeSelectedEvent", function(e, uuid, row) {
                    switch (row.className) {
                        case 'User':
                            var index = $scope.allSelected.users.indexOf(row);
                            $scope.allSelected.users.splice(index, 1);
                            break;
                        case 'UserGroup':
                            var index = $scope.allSelected.userGroups.indexOf(row);
                            $scope.allSelected.userGroups.splice(index, 1);
                            break;
                        case 'Role':
                            var index = $scope.allSelected.roles.indexOf(row);
                            $scope.allSelected.roles.splice(index, 1);
                            break;
                    }
                    $scope.updateSelectionFlag();
                });

                $scope.models = [{
                    listName: "Available",
                    items: [],
                    dragging: false,
                    style: {
                        height: null
                    }
                }, {
                    listName: "Current",
                    items: [],
                    dragging: false,
                    style: {
                        height: null
                    }
                }];

                $scope.refreshDisplay = function(view) {
                    switch (view) {
                        case 'usersView':
                            $scope.loadAllUsers(function() {
                                // $scope.updateSelectionDisplay($scope.data.users,
                                // $scope.currentSelection.users);

                                UsersAdministrationService.getUserGroupUsers($scope.currentSelection.uuid)
                                    .then(function(users) {
                                        $scope.updateSelectionDisplay($scope.data.users, users.plain());
                                    })

                            });
                            break;
                        case 'userGroupsView':
                            $scope.loadAllUserGroups(function() {
                                $scope.updateSelectionDisplay($scope.data.userGroups,
                                    $scope.currentSelection.userGroups);
                            });
                            break;
                        case 'rolesView':
                            $scope
                                .loadAllRoles(function() {
                                    $scope.updateSelectionDisplay($scope.data.roles,
                                        $scope.currentSelection.roles);
                                });
                            break;
                        case 'permissionsView':
                            $scope.loadAllPermissions(function() {
                                $scope.updateSelectionDisplay($scope.data.permissions,
                                    $scope.currentSelection.permissions);
                            });
                            break;
                    }
                }

                $scope.ArrayContainsUuid = function(array, uuid) {
                    for (var i = 0; i < array.length; ++i) {
                        if (array[i].uuid == uuid) return true;
                    }
                    return false;
                }

                $scope.updateSelectionDisplay = function(allItems, currentItems) {
                    $scope.models[0].items = [];
                    $scope.models[1].items = [];
                    for (var i = 0; i < allItems.length; ++i) {
                        if (!$scope.ArrayContainsUuid(currentItems, allItems[i].uuid)) {
                            $scope.models[0].items.push(allItems[i]);
                        }
                    }
                    for (var i = 0; i < currentItems.length; ++i) {
                        $scope.models[1].items.push(currentItems[i]);
                    }
                }

                /**
                 * dnd-dragging determines what data gets serialized and send to
                 * the receiver of the drop. While we usually just send a single
                 * object, we send the array of all selected items here.
                 */
                $scope.getSelectedItemsIncluding = function(list, item) {
                    item.selected = true;
                    return list.items.filter(function(item) {
                        return item.selected;
                    });
                };

                /**
                 * We set the list into dragging state, meaning the items that are
                 * being dragged are hidden. We also use the HTML5 API directly to
                 * set a custom image, since otherwise only the one item that the
                 * user actually dragged would be shown as drag image.
                 */
                $scope.onDragstart = function(list, event) {
                    list.dragging = true;
                    if (event.dataTransfer.setDragImage) {
                        var img = new Image();
                        // img.src =
                        // 'framework/vendor/ic_content_copy_black_24dp_2x.png';
                        event.dataTransfer.setDragImage(img, 0, 0);
                    }
                };

                /**
                 * In the dnd-drop callback, we now have to handle the data array
                 * that we sent above. We handle the insertion into the list
                 * ourselves. By returning true, the dnd-list directive won't do
                 * the insertion itself.
                 */
                $scope.onDrop = function(list, items, index) {
                    angular.forEach(items, function(item) {
                        item.selected = false;
                    });
                    list.items = list.items.slice(0, index).concat(items).concat(
                        list.items.slice(index));
                    return true;
                }

                $scope.assignItems = function(className, items) {
                    var defer = $q.defer();
                    switch ($scope.currentSelection.className) {
                        case 'User':
                            if (className == 'UserGroup')
                                for (var i = 0; i < items.length; ++i) {
                                    UserGroupsAdministrationService.assignUsers(items[i],
                                        $scope.currentSelection.uuid).then(function() {
                                        defer.resolve('true');
                                    });
                                }
                            if (className == 'Role')
                                UsersAdministrationService.assignRoles($scope.currentSelection.uuid, items)
                                    .then(function() {
                                        defer.resolve('true');
                                    });
                            if (className == 'Permission')
                                UsersAdministrationService.assignPermissions($scope.currentSelection.uuid,
                                    items).then(function() {
                                    defer.resolve('true');
                                });
                            break
                        case 'UserGroup':
                            if (className == 'User')
                                UserGroupsAdministrationService
                                    .assignUsers($scope.currentSelection.uuid, items).then(function() {
                                    defer.resolve('true');
                                });
                            if (className == 'Role')
                                UserGroupsAdministrationService
                                    .assignRoles($scope.currentSelection.uuid, items).then(function() {
                                    defer.resolve('true');
                                });
                            if (className == 'Permission')
                                UserGroupsAdministrationService.assignPermissions($scope.currentSelection.uuid,
                                    items).then(function() {
                                    defer.resolve('true');
                                });
                            break
                        case 'Role':
                            if (className == 'Permission')
                                RolesAdministrationService.assignPermissions($scope.currentSelection.uuid,
                                    items).then(function() {
                                    defer.resolve('true');
                                });
                            break
                    }
                    return defer.promise;
                }

                $scope.unassignItems = function(className, items) {
                    var defer = $q.defer();
                    switch ($scope.currentSelection.className) {
                        case 'User':
                            if (className == 'UserGroup')
                                for (var i = 0; i < items.length; ++i) {
                                    UserGroupsAdministrationService.unassignUsers(items[i],
                                        $scope.currentSelection.uuid).then(function() {
                                        defer.resolve('true');
                                    });
                                }
                            if (className == 'Role')
                                UsersAdministrationService.unassignRoles($scope.currentSelection.uuid, items)
                                    .then(function() {
                                        defer.resolve('true');
                                    });

                            if (className == 'Permission')
                                UsersAdministrationService.unassignPermissions($scope.currentSelection.uuid,
                                    items).then(function() {
                                    defer.resolve('true');
                                });
                            break
                        case 'UserGroup':
                            if (className == 'User')
                                UserGroupsAdministrationService.unassignUsers($scope.currentSelection.uuid,
                                    items).then(function() {
                                    defer.resolve('true');
                                });
                            if (className == 'Role')
                                UserGroupsAdministrationService.unassignRoles($scope.currentSelection.uuid,
                                    items).then(function() {
                                    defer.resolve('true');
                                });
                            if (className == 'Permission')
                                UserGroupsAdministrationService.unassignPermissions(
                                    $scope.currentSelection.uuid, items).then(function() {
                                    defer.resolve('true');
                                });
                            break
                        case 'Role':
                            if (className == 'Permission')
                                RolesAdministrationService.unassignPermissions($scope.currentSelection.uuid,
                                    items).then(function() {
                                    defer.resolve('true');
                                });
                            break
                    }
                    return defer.promise;
                }

                /**
                 * Last but not least, we have to remove the previously dragged
                 * items in the dnd-moved callback.
                 */
                $scope.onMoved = function(list) {
                    var movedItems = list.items.filter(function(item) {
                        return item.selected;
                    });

                    list.items = list.items.filter(function(item) {
                        return !item.selected;
                    })

                    var promises = [];

                    // Make sure items have actually been moved across lists
                    if (movedItems.length > 0
                        && !$scope.ArrayContainsUuid(list.items, movedItems[0].uuid)) {
                        var uuids = [];
                        var movedClass = '';
                        if (movedItems.length > 0) movedClass = movedItems[0].className;
                        for (var i = 0; i < movedItems.length; ++i) {
                            uuids.push(movedItems[i].uuid);
                            movedItems[i].selected = false;
                            if (list.listName == "Available") {
                                growl.success(movedItems[i].displayName + " added to "
                                    + $scope.currentSelection.displayName, {
                                    title: 'Updated Instance'
                                });
                            } else if (list.listName == "Current") {
                                growl.success(movedItems[i].displayName + " removed from "
                                    + $scope.currentSelection.displayName, {
                                    title: 'Updated Instance'
                                });
                            }
                        }
                        if (list.listName == "Available")
                            promises.push($scope.assignItems(movedClass, uuids));
                        if (list.listName == "Current")
                            promises.push($scope.unassignItems(movedClass, uuids));
                    }

                    $q.all(promises).then(function() {
                        $scope.init(true);
                    })
                };

                // $scope.checkInitialPermissions = function() {
                // if (!PermissionsService.checkPermissions(['VIEW_USERS']))
                // $location.path('/dashboards');
                // }
                // $scope.checkInitialPermissions();
            }]);
/*******************************************************************************
 * Copyright (c) 2015 Bosch Software Innovations GmbH. All rights reserved.
 ******************************************************************************/
angular.module('app.controllers.profile', ['ngFileUpload', 'angular-international'])//, 'angular-stripe'])
//.config(['stripeProvider', function (stripeProvider) {
//    stripeProvider.setPublishableKey('pk_test_KU2W7iv2ofe5ufG7VMa5n8O0');
//}])
.controller(
    "ProfileCtrl",
    [
        "$http",
        "$scope",
        "$rootScope",
        "$log",
        "growl",
        "countries",
        "languages",
        "ConfigurationService",
        "ProfileService",
        "Upload",
        "TranslationService",
        '$uibModal',
        '$modalStack',
        //'PaymentService',
        function($http, $scope, $rootScope, $log, growl, countries, languages,
            ConfigurationService, ProfileService, Upload, TranslationService,
                 $uibModal) {//, PaymentService) {

          console.log("Profile State");

          $scope.images = {};
          $scope.data = {};
          $scope.controls = {};
          $scope.payment = {};
          $scope.data = {
        		  show: false,
        		  exec: false,
        		  word: false,
        		  metric: false,
        		  corr: false,
        		  disable: false
          };
          $scope.add = {};

          $scope.getCountries = function() {
            return countries;
          }

          $scope.getLanguages = function() {
            return languages;
          }

          $scope.saveProfile = function() {
            $scope.saveProfilePicture();
            $scope.saveUserSettings();
          }

          $scope.saveUserSettings = function() {
            ConfigurationService.updateConfiguration($scope.user.configuration,
                $scope.currentUser.uuid).then(function(configuration) {
              $rootScope.user.configuration = configuration;
              growl.success("PROFILE_HAS_BEEN_SUCCESSFULLY_SAVED", {
                title: 'SUCCESS'
              });
              $scope.controls.countryEdit = false;
            });
          }

          $scope.saveProfilePicture = function() {
            if (!$scope.picFile) return;
            var fd = new FormData();
            fd.append('file', $scope.picFile);
            $http.post("api/v1/media", fd, {
              transformRequest: angular.identity,
              headers: {
                'Content-Type': undefined,
                'name': 'user-profile'
              }
            }).success(function(result) {
              growl.success("PROFILE_HAS_BEEN_SUCCESSFULLY_SAVED", {
                title: 'SUCCESS'
              });
              $log.info("Profile picture successfully saved")
            }).error(function(err) {
              growl.error("There is been a problem updating your profile picture", {
                title: 'ERROR'
              });
              console.log(err);
            });
          }

          var exception = "com.bosch.im.authentication.AuthenticationDeniedRuntimeException";

          $scope.savePassword = function() {
            if ($scope.password.newPassErrors.length == 0
                && $scope.password.newPass2Errors.length == 0) {
              ProfileService.changePassword($scope.password.current, $scope.password.newPass).then(
                  function(result) {
                    $scope.passwordEdit = false;
                    growl.success("Successfully changed password", {
                      title: "SUCCESS"
                    });
                  }, function(error) {
                    if (error.data.exception == exception) {
                      growl.warning("Current password incorrect", {
                        title: 'ERROR'
                      });
                    }
                  });
            }
          }

          $http.get("api/v1/media", {
            responseType: "blob",
            params: {
              'name': 'user-profile'
            }
          }).success(function(data, status, headers, config) {
            var fr = new FileReader();
            fr.onload = function(e) {
              $scope.updateProfilePicture(e.target.result.split(',')[1]);
            };
            fr.readAsDataURL(data);
          }).error(function(data, status, headers, config) {
            console.log(data);
          });

          $scope.updateProfilePicture = function(data) {
            $scope.data.profilePic = 'data:image/jpeg;base64,' + data;
            $scope.$apply();
          }

          $scope.$watch('password.newPass', function() {
            if (!$scope.password) return;
            $scope.password.newPassErrors = [];

            if (/[a-z]/.test($scope.password.newPass) == false) {
              $scope.password.newPassErrors
                  .push('Your password should have at least one lower case letter.');
            }
            if (/[A-Z]/.test($scope.password.newPass) == false) {
              $scope.password.newPassErrors
                  .push('Your password should have at least one upper case letter.');
            }
            if (/\d/.test($scope.password.newPass) == false) {
              $scope.password.newPassErrors.push('Your password should have at least one number.');
            }
            if (/\W+/.test($scope.password.newPass) == false) {
              $scope.password.newPassErrors
                  .push('Your password should have at least one special character.');
            }

          })

          $scope.$watch('password.newPass2', function() {
            if (!$scope.password) return;
            $scope.password.newPass2Errors = [];
            if ($scope.password.newPass2 !== $scope.password.newPass) {
              $scope.password.newPass2Errors.push('Passwords should match');
            }
          })

        }]);

/*******************************************************************************
 * Copyright (c) 2015 Bosch Software Innovations GmbH. All rights reserved.
 ******************************************************************************/
angular.module('app.controllers.links', []).controller(
    "LinksCtrl",
    [
        "$filter",
        "$location",
        "$rootScope",
        "$scope",
        "$state",
        "$stateParams",
        "$translate",
        "$http",
        "$interval",
        "$window",
        "growl",
        "Configuration",
        "Idle",
        "PermissionsService",
        "TranslationService",
        function($filter, $location, $rootScope, $scope, $state, $stateParams, $translate, $http,
                 $interval, $window, growl, Configuration, Idle,
                 PermissionsService, TranslationService) {

            /**
             * Links functionality
             */
            $scope.selectState = function(state, param1, param2, param3, param4, param5, param6) {
                if (!validate(state)) return;
                if (param5) {
                    $location.url($location.path());
                    $location.path(state).search(param1, param2).search(param3, param4).search(param5, param6);
                } else if (param3) {
                    $location.url($location.path());
                    $location.path(state).search(param1, param2).search(param3, param4);
                } else if (param1) {
                    $location.url($location.path());
                    $location.path(state).search(param1, param2);
                } else {
                    $location.url(state);
                }
            };

            var validate = function(state) {
                if (state === '/dashboards') {
                    if ($scope.dashboardEditMode) {
                        growl.warning("Please, save or discard the changes before changing dashboards", {
                            title: 'Invalid Action'
                        });
                        return false;
                    }
                } else {
                    $scope.dashboardEditMode = false;
                }
                return true;
            }

            $scope.$on('urlChange', function(event, msg) {
                $scope.selectState(msg.path, msg.key1, msg.value1, msg.key2, msg.value2);
            });

            $scope.getState = function() {
                return $location.$$path;
            }

            $scope.getUuid = function() {
                return $location.$$search.uuid;
            }

            $scope.getType = function() {
                return $location.$$search.type;
            }

            $scope.getClass = function() {
                return $location.$$search.clazz;
            }

            /**
             * User display
             */
            $scope.isAdmin = false;
            $scope.$watch('currentUser', function() {
                if ($rootScope.currentUser) {
                    $scope.username = $rootScope.currentUser.username;
                    $scope.isAdmin = $.inArray("ROLE_ADMIN", $rootScope.currentUser.authorities) > -1;
                }
            });

            $scope.$on('profilePictureLoaded', function() {
                $scope.$apply();
            })

            /**
             * Automatic logout
             */
            $scope.$on('IdleTimeout', function() {
                console.log("IdleTimeout");
                $window.location.href = 'logout';
            });

            /**
             * Language Options
             */
            var availableLanguages = TranslationService.getAvailableLocales();

            $scope.getLanguages = function() {
                return $.grep(availableLanguages, function(language) {
                    return language.name != $scope.activeLanguage.name;
                })
            }

            $scope.selectLanguage = function(language) {
                var previousLanguage = angular.copy($scope.activeLanguage);
                $scope.activeLanguage = language;
                $translate.use(language.name).then(function() {
                    if (previousLanguage) {
                        $window.sessionStorage.locale = language.name;
                        // $window.location.reload();
                    }
                });
            }

            var language = $window.navigator.userLanguage || $window.navigator.language;

            if ($window.sessionStorage.locale) language = $window.sessionStorage.locale;

            var languages = $.grep(availableLanguages, function(lang) {
                return lang.name.indexOf(language) === 0;
            })

            if (languages.length > 0) {
                $scope.selectLanguage(languages[0]);
            } else {
                $scope.selectLanguage(availableLanguages[0]);
            }

        }]);

/*******************************************************************************
 * Copyright (c) 2015 Bosch Software Innovations GmbH. All rights reserved.
 ******************************************************************************/
angular.module('app.controllers.links').controller(
    "LinksExtCtrl",
    [
        "$rootScope",
        "$controller",
        "$filter",
        "$log",
        "$interval",
        "$scope",
        "$window",
        "$state",
        "growl",
        "ConfigurationService",
        "DatasourceService",
        "AuthService",
        "AdminUsersService",
        "AdminOrganizationsService",
        function($rootScope, $controller, $filter, $log, $interval, $scope, $window, $state, growl,
                 ConfigurationService, DatasourceService, AuthService, AdminUsersService, AdminOrganizationsService) {

            $scope.data = {};

            $rootScope.selections = {
                datasource: {}
            }

            $scope.logout = function() {
                AuthService.revokeToken();
                $rootScope.currentUser = null;
                $scope.selectState('/public');
            }

            $scope.$watch('user.configuration.content.selection', function(newObj, oldObj) {
                if (!newObj) return;
                $rootScope.selections.datasource = newObj;
            })

            $scope.$watch('selections.datasource', function(newObj, oldObj) {
                if (!newObj) return;
                $rootScope.$broadcast('datasource.selected', newObj);
                if (newObj.uuid && newObj.uuid !== oldObj.uuid) {
                    if (!$rootScope.user.configuration) $rootScope.user.configuration = {};
                    if (!$rootScope.user.configuration.content)
                        $rootScope.user.configuration.content = {};
                    $rootScope.user.configuration.content.selection = newObj;
                    ConfigurationService.updateConfiguration($scope.user.configuration,
                        $scope.currentUser.uuid).then(function(configuration) {
                        $rootScope.user.configuration = configuration;
                    });
                }
            })

            angular.extend(this, $controller('LinksCtrl', {
                $scope: $scope
            }));

            $scope.init = function() {
                // DatasourceService.getDatasources().then(function(datasources) {
                //     $scope.data.datasources = datasources;
                // })
            }

            $scope.$on('datasource.added', function(event, datasource) {
                $scope.data.datasources.push(datasource);
            })

            $scope.$on('datasource.removed', function(event, datasource) {
                $scope.data.datasources = $.grep($scope.data.datasources, function(item) {
                    return item.uuid !== datasource.uuid;
                })
                if (datasource.uuid === $rootScope.selections.datasource.uuid) {
                    $rootScope.selections.datasource = {};
                    $rootScope.user.configuration.content.selection = {};
                    ConfigurationService.updateConfiguration($scope.user.configuration,
                        $scope.currentUser.uuid).then(function(configuration) {
                        $rootScope.user.configuration = configuration;
                    });
                }
            })


            $scope.getBadgeClass = function(role) {
                if (!role) return;
                switch (role.name) {
                    case 'SuperAdmin':
                        return 'label-inverse';
                        break;
                    case 'Admin':
                        return 'label-default';
                        break;
                    case 'General Manager':
                    case 'GeneralManager':
                        return 'label-success';
                        break;
                    case 'Plant Manager':
                    case 'PlantManager':
                        return 'label-primary';
                        break;
                    case 'Production Manager':
                    case 'ProductionManager':
                        return 'label-warning';
                        break;
                    case 'Operator':
                        return 'label-info';
                        break;
                    default:
                        break;
                }
            };

            function getOrganizationConfiguration(uuid) {
                AdminOrganizationsService.getOrganizationConfiguration(uuid).then(function(orgConfiguration) {
                    if (orgConfiguration && orgConfiguration.conf.numShifts) {
                        $rootScope.org.configuration = orgConfiguration.plain();
                    } else {
                        $rootScope.org.configuration = {
                            organizationUuid: $rootScope.currentUser.organization.uuid,
                            conf: {
                                numShifts: 3
                            }
                        }
                    }
                })
            }

            $scope.selectOrganization = function(organization) {
                if (organization.uuid !== $rootScope.currentUser.organization.uuid) {
                    AdminUsersService.updateUserOrganization($rootScope.currentUser, organization.uuid).then(function(user) {
                        $rootScope.currentUser = user.plain();
                        getOrganizationConfiguration(user.organization.uuid)
                        if ($scope.getState().indexOf('/admin/plants/') > -1)
                            $scope.selectState('/admin/plants')
                        else if ($scope.getState().indexOf('/admin/schedules/') > -1)
                            $scope.selectState('/admin/schedules')
                        else if ($scope.getState().indexOf('/admin/organizations/') > -1)
                            $scope.selectState('/admin/organizations/' + $rootScope.currentUser.organization.uuid)
                        else
                            $state.reload(true);
                    });
                }
            };

            $scope.init();

        }]);

angular.module('app.controllers.services.consulting', []).controller("ConsultingServicesCtrl",
    ["$rootScope", "$scope", function($rootScope, $scope) {
        $rootScope.state = 'Consulting';
    }]);
//import "./consulting.ctrl.js";

var servicesModule = angular.module('app.controllers.services', ['app.controllers.services.consulting']).controller("ServicesCtrl",
    ["$scope", "$controller", "$state", function($scope, $controller, $state) {


        angular.extend(this, $controller('DataFilterCtrl', {
            $scope: $scope
        }));


    }]);

servicesModule.directive('slideShow', function() {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            var nice = $(element).find(".slider-show").niceScroll({cursorcolor: "#d9e0e7", touchbehavior: true});
            $(element).find(".slider-show").scroll(function() {
                $(element).find(".slider-show").getNiceScroll().resize();
            });
        }
    };
});
/*******************************************************************************
 * Copyright (c) 2015 Bosch Software Innovations GmbH. All rights reserved.
 ******************************************************************************/

'use strict';

var multimetric = angular.module('improvidus-filters', []);

multimetric
    .constant('FIELD', {
        Date: 0,
        Time: 1,
        Shift: 2,
        Plant: 3,
        CostCenter: 4,
        WorkCenter: 5,
        Deviation1Level: 6,
        Deviation2Level: 7,
        Deviation3Level: 8,
        Deviation4Level: 9,
        Duration: 10,
        Amount: 11,
        Partnumber: 12,
        Comment: 13
    })
    .controller(
        'FiltersCtrl',
        [
            '$q',
            '$rootScope',
            '$scope',
            '$http',
            '$timeout',
            '$location',
            '$stateParams',
            'AnalysisResultService',
            'CurrentAnalysisGroupService',
            'DatasourceService',
            'DeviationEntryService',
            'DisplayService',
            'usSpinnerService',
            function ($q, $rootScope, $scope, $http, $timeout, $location, $stateParams, AnalysisResultService, CurrentAnalysisGroupService, DatasourceService, DeviationEntryService,
                      DisplayService, usSpinnerService) {

                // if ($stateParams.uuid)
                //     $location.search("uuid", $location.$$search.uuid);

                $scope.flag = false;
                // $scope.$on('datasource.selected', function (event, datasource) {
                //     if (!$scope.flag && datasource) {
                //         $scope.load(datasource);
                //         $scope.flag = true;
                //     } else {
                //         $scope.flag = false;
                //     }
                // })

                $scope.name = {};
                //
                // $scope.load = function (datasource) {
                //     DatasourceService.getDatasourceMedia(datasource.uuid, {
                //         type: "mmparetos"
                //     }).then(
                //         function (data) {
                //             $scope.rawData = data.slice(1, data.length);
                //             $scope.date.date = {
                //                 startDate: moment($scope.rawData[0][0] + " " + $scope.rawData[0][1],
                //                     "M/D/YYYY h:mm:ss a", true),
                //                 endDate: moment()
                //             }
                //             $scope.init($scope.rawData);
                //         }, function (err) {
                //             usSpinnerService.stop('spinner-notifications');
                //         })
                // }

                $scope.controls = {};
                $scope.chartOptions = {};

                var FIELD = {
                    Date: 0,
                    Time: 1,
                    Shift: 2,
                    Plant: 3,
                    CostCenter: 4,
                    WorkCenter: 5,
                    Deviation1Level: 6,
                    Deviation2Level: 7,
                    Deviation3Level: 8,
                    Deviation4Level: 9,
                    Duration: 10,
                    Amount: 11,
                    Partnumber: 12,
                    Comment: 13
                }

                $scope.data = {};
                $scope.data.deviationFilters = [{
                    displayName: "Deviation Level 1",
                    filterField: FIELD.Deviation1Level,
                    data: [],
                    options: [],
                    activeFilter: ''
                }, {
                    displayName: "Deviation Level 2",
                    filterField: FIELD.Deviation2Level,
                    data: [],
                    options: [],
                    activeFilter: ''
                }, {
                    displayName: "Deviation Level 3",
                    filterField: FIELD.Deviation3Level,
                    data: [],
                    options: [],
                    activeFilter: ''
                }, {
                    displayName: "Deviation Level 4",
                    filterField: FIELD.Deviation4Level,
                    data: [],
                    options: [],
                    activeFilter: ''
                }, {
                    data: []
                }]

                $scope.data.companyFilters = [{
                    displayName: "Plant",
                    filterField: FIELD.Plant,
                    data: [],
                    options: [],
                    activeFilter: ''
                }, {
                    displayName: "Cost Center",
                    filterField: FIELD.CostCenter,
                    data: [],
                    options: [],
                    activeFilter: ''
                }, {
                    displayName: "Work Center",
                    filterField: FIELD.WorkCenter,
                    data: [],
                    options: [],
                    activeFilters: ''
                }, {
                    data: []
                }]

                $scope.data.xAxis = ["Deviation", "Shift", "Partnumber", "Comment"]
                $scope.controls.xAxis = $scope.data.xAxis[0];

                $scope.$watch('controls.xAxis', function (newObj, oldObj) {
                    if (!newObj) return;
                    switch (newObj) {
                        case "Deviation":
                            $scope.chartOptions.xAxis = "Deviation";
                            break;
                        case "Shift":
                            $scope.chartOptions.xAxis = FIELD.Shift
                            break;
                        case "Partnumber":
                            $scope.chartOptions.xAxis = FIELD.Partnumber
                            break;
                        case "Comment":
                            $scope.chartOptions.xAxis = FIELD.Comment
                            break;
                        default:
                            $scope.chartOptions.xAxis = newObj;
                    }
                    if (newObj !== oldObj) $scope.paintGraph();
                }, true);

                $scope.data.yAxis = ["Amount ($)", "Duration (h)", "Count"]
                $scope.controls.yAxis = $scope.data.yAxis[0];

                $scope.$watch('controls.yAxis', function (newObj, oldObj) {
                    if (!newObj) return;
                    switch (newObj) {
                        case "Duration (h)":
                            $scope.chartOptions.yAxis = FIELD.Duration;
                            break;
                        case "Count":
                            $scope.chartOptions.yAxis = FIELD.Count
                            break;
                        default:
                            $scope.chartOptions.yAxis = FIELD.Amount
                    }
                    if (newObj !== oldObj) $scope.paintGraph();
                }, true);

                $scope.data.colorParameters = ["Year", "Month", "Month Year", "Week", "Weekday",
                    "Shift", "Plant", "Cost Center", "Work Center", "Deviation 1st Level",
                    "Deviation 2nd Level", "Deviation 3rd Level", "Deviation 4th Level",
                    "Partnumber", "Comment", "None"]
                $scope.$watch('controls.colorParameter', function (newObj, oldObj) {
                    switch (newObj) {
                        case "Shift":
                            $scope.chartOptions.colorParameter = FIELD.Shift;
                            break;
                        case "Plant":
                            $scope.chartOptions.colorParameter = FIELD.Shift;
                            break;
                        case "Cost Center":
                            $scope.chartOptions.colorParameter = FIELD.CostCenter;
                            break;
                        case "Deviation 1st Level":
                            $scope.chartOptions.colorParameter = FIELD.Deviation1Level;
                            break;
                        case "Deviation 2nd Level":
                            $scope.chartOptions.colorParameter = FIELD.Deviation2Level;
                            break;
                        case "Deviation 3rd Level":
                            $scope.chartOptions.colorParameter = FIELD.Deviation3Level;
                            break;
                        case "Deviation 4th Level":
                            $scope.chartOptions.colorParameter = FIELD.Deviation4Level;
                            break;
                        case "Partnumber":
                            $scope.chartOptions.colorParameter = FIELD.Partnumber;
                            break;
                        case "Comment":
                            $scope.chartOptions.colorParameter = FIELD.Comment;
                            break;
                        default:
                            $scope.chartOptions.colorParameter = FIELD.WorkCenter
                    }
                    if (newObj) $scope.paintGraph();
                }, true);

                $scope.date = {
                    date: {
                        startDate: moment().subtract(2, 'years'),
                        endDate: moment()
                    },
                    dateOptions: {
                        opens: 'left',
                        ranges: {
                            'Today': [moment().startOf('day'), moment()],
                            'Last Week': [moment().subtract(7, 'days'), moment()],
                            'Last Month': [moment().subtract(1, 'months'), moment()],
                            'Last 3 Months': [moment().subtract(3, 'months'), moment()],
                            'Last 6 Months': [moment().subtract(6, 'months'), moment()]
                        },
                        timePicker: true,
                        timePickerIncrement: 1,
                        format: 'M/D/YY H:mm'
                    }
                };

                function mapEntryToLine(entry) {
                    if (!entry) return [];
                    var line = [];
                    var time = moment(entry.timestamp)
                    line[0] = time.format('M/D/YYYY');
                    line[1] = time.format('h:mm:ss a');
                    line[2] = entry.shift;
                    line[3] = entry.plant;
                    line[4] = entry.costCenter;
                    line[5] = entry.workCenter;
                    line[6] = entry.deviation1Level;
                    line[7] = entry.deviation2Level;
                    line[8] = entry.deviation3Level;
                    line[9] = entry.deviation4Level;
                    line[10] = entry.duration;
                    line[11] = entry.amount;
                    line[12] = entry.partNumber;
                    line[13] = entry.comments;
                    return line;
                }

                $scope.$watch('date.date', function (newObj, oldObj) {
                    if (oldObj && newObj) {
                        usSpinnerService.spin('spinner-notifications');
                        $timeout($scope.loadGraphWithDateFilter, 100);
                        // DeviationEntryService.getDeviationEntries({
                        //     from: $scope.date.date.startDate.valueOf(),
                        //     to: $scope.date.date.endDate.valueOf()
                        // }).then(function (entries) {
                        //     var lines = entries.content.map(function (entry) {
                        //         return mapEntryToLine(entry);
                        //     });
                        //     $scope.init(lines);
                        // });
                    }
                });


                $scope.getOptions = function (matrix, field) {
                    var options = {};
                    for (var i = 1; i < matrix.length; i++) {
                        if (!matrix[i][field]) continue;
                        if (options[matrix[i][field]]) continue;
                        options[matrix[i][field]] = true;
                    }
                    return Object.keys(options);
                }

                $scope.lines = [];

                var fromTime, toTime;

                $scope.selectDeviationOption = function (index, option) {
                    usSpinnerService.spin('spinner-notifications');
                    for (var i = index + 1; i < $scope.data.deviationFilters.length; i++) {
                        $scope.data.deviationFilters[i].data = [];
                        $scope.data.deviationFilters[i].options = [];
                        $scope.data.deviationFilters[i].activeFilter = '';
                    }
                    if (index < $scope.data.deviationFilters.length) {
                        $scope.data.deviationFilters[index + 1].data = $.grep(
                            $scope.data.deviationFilters[index].data, function (line) {
                                var text = line[$scope.data.deviationFilters[index].filterField];
                                return option ? text === option : true;
                            })
                        $scope.data.deviationFilters[index + 1].options = $scope.getOptions(
                            $scope.data.deviationFilters[index + 1].data,
                            $scope.data.deviationFilters[index + 1].filterField);
                    }
                    if (option)
                        $scope.data.deviationFilters[index].activeFilter = option;
                    else
                        $scope.data.deviationFilters[index].activeFilter = $scope.data.deviationFilters[index].displayName;
                    $scope.currentDeviationFiltered = $scope.data.deviationFilters[index + 1].data;
                }

                $scope.loadByDeviation = function (index) {
                    $scope.data.xAxisLevel = $scope.data.deviationFilters[index].filterField;
                    $scope.loadGraph($scope.data.deviationFilters[index].data);
                }

                _.intersectionObjects = _.intersect = function (array) {
                    var slice = Array.prototype.slice;
                    var rest = slice.call(arguments, 1);
                    return _.filter(_.uniq(array), function (item) {
                        return _.every(rest, function (other) {
                            return _.any(other, function (element) {
                                return _.isEqual(element, item);
                            });
                        });
                    });
                };

                $scope.selectCompanyOption = function (index, option) {
                    usSpinnerService.spin('spinner-notifications');
                    for (var i = index + 1; i < $scope.data.companyFilters.length; i++) {
                        $scope.data.companyFilters[i].data = [];
                        $scope.data.companyFilters[i].options = [];
                        $scope.data.companyFilters[i].activeFilter = '';
                    }
                    if (index < $scope.data.companyFilters.length - 1) {
                        $scope.data.companyFilters[index + 1].data = $.grep(
                            $scope.data.companyFilters[index].data, function (line) {
                                var text = line[$scope.data.companyFilters[index].filterField];
                                return option ? text === option : true;
                            });
                        $scope.data.companyFilters[index + 1].options = $scope.getOptions(
                            $scope.data.companyFilters[index + 1].data,
                            $scope.data.companyFilters[index + 1].filterField);
                    }
                    if (option)
                        $scope.data.companyFilters[index].activeFilter = option;
                    else
                        $scope.data.companyFilters[index].activeFilter = $scope.data.companyFilters[index].displayName;
                    $scope.currentCompanyFiltered = $scope.data.companyFilters[index + 1].data;
                }

                $scope.$watch('currentDeviationFiltered', function (newObj) {
                    if ($scope.currentDeviationFiltered) {
                        setTimeout($scope.loadGraphWithFilters, 100);
                    }
                });
                $scope.$watch('currentCompanyFiltered', function (newObj) {
                    if ($scope.currentDeviationFiltered) {
                        setTimeout($scope.loadGraphWithFilters, 100);
                    }
                });

                $scope.isActive = function (activeOption, options) {
                    return options.indexOf(activeOption) > -1;
                }

                $scope.init = function (lines) {
                    if (!lines || lines.length == 0) return;
                    $scope.data.companyFilters[0].data = lines;
                    $scope.data.companyFilters[0].options = $scope.getOptions(lines, FIELD.Plant);

                    $scope.data.deviationFilters[0].data = lines;
                    $scope.data.deviationFilters[0].options = $scope.getOptions(lines,
                        FIELD.Deviation1Level);
                    $scope.loadGraph(lines);
                }

                $scope.loadGraph = function (lines) {
                    if (!lines || lines.length == 0) return;
                    $scope.currentLines = lines;
                    $scope.loadGraphWithFilters();
                }

                $scope.loadGraphWithFilters = function () {
                    $scope.filteredLines = $scope.currentDeviationFiltered || [];

                    if ($scope.currentDeviationFiltered && !$scope.currentCompanyFiltered) {
                        $scope.filteredLines = $scope.currentDeviationFiltered
                    } else if (!$scope.currentDeviationFiltered && $scope.currentCompanyFiltered) {
                        $scope.filteredLines = $scope.currentCompanyFiltered;
                    } else if (!$scope.currentDeviationFiltered && !$scope.currentCompanyFiltered) {
                        $scope.filteredLines = $scope.currentLines;
                    }

                    $scope.filteredLines = $
                        .grep(
                            $scope.filteredLines,
                            function (line) {
                                for (var i = 0; i < $scope.data.companyFilters.length - 1; i++) {
                                    if ($scope.data.companyFilters[i].activeFilter
                                        && $scope.data.companyFilters[i].displayName !== $scope.data.companyFilters[i].activeFilter
                                        && $scope.data.companyFilters[i].activeFilter !== line[$scope.data.companyFilters[i].filterField])
                                        return false;
                                }
                                return true;
                            });

                    // if ($scope.currentDeviationFiltered &&
                    // !$scope.currentCompanyFiltered) {
                    // $scope.filteredLines = $scope.currentDeviationFiltered
                    // } else if (!$scope.currentDeviationFiltered &&
                    // $scope.currentCompanyFiltered)
                    // {
                    // $scope.filteredLines = $scope.currentCompanyFiltered;
                    // } else if (!$scope.currentDeviationFiltered &&
                    // !$scope.currentCompanyFiltered) {
                    // $scope.filteredLines = $scope.currentLines;
                    // } else {
                    // $scope.filteredLines =
                    // _.intersectionObjects($scope.currentDeviationFiltered,
                    // $scope.currentCompanyFiltered);
                    // }
                    $scope.loadGraphWithDateFilter();
                }

                $scope.loadGraphWithDateFilter = function () {
                    if (!$scope.filteredLines) return;
                    $scope.dateFilteredLines = $.grep($scope.filteredLines, function (line) {
                        var date = moment(line[0] + " " + line[1], "M/D/YYYY h:mm:ss a", true);
                        var startDate = $scope.date.date.startDate;
                        var endDate = $scope.date.date.endDate;
                        return (date.isBefore(endDate) && date.isAfter(startDate))
                            || date.isSame(startDate) || date.isSame(endDate);
                    });

                    $scope.paintGraph();
                }

                $scope.initAnalysis = function (type) {
                    $scope.data.analysisGroup = CurrentAnalysisGroupService.getCurrentAnalysisGroup();
                    $scope.data.analysisType = type;

                    if ($scope.data.analysisGroup == null) {
                        AnalysisResultService.getAnalysisGroup($stateParams.uuid).then(function (analysisGroup) {
                            CurrentAnalysisGroupService.setCurrentAnalysisGroup(analysisGroup);
                            $scope.data.analysisGroup = analysisGroup;
                        })
                    }

                    DatasourceService.getDatasourceMedia($stateParams.uuid, {
                        type: type,
                        version: $stateParams.version
                    }).then(function (result) {
                        $scope.init(result);
                    });
                }

            }]);
angular.module('app.controllers.oee.charts', []).controller("OEEChartCtrl",
    ["$scope", "$controller", "$state", "$interval", "$timeout", "$q", "DeviationEntryService", "OEEEntryService", "FilterService", function($scope, $controller, $state, $interval, $timeout, $q, DeviationEntryService, OEEEntryService, FilterService) {


        angular.extend(this, $controller('DataFilterCtrl', {
            $scope: $scope
        }));

        var data = {
            chart: {
                type: 'multiBarChart',
                height: 400,
                margin: {
                    top: 20,
                    right: 20,
                    bottom: 45,
                    left: 45
                },
                multibar: {
                    dispatch: { //container of event handlers
                        elementClick: function(e) {
                            var query = FilterService.getFilter();

                            query.dateRange.startDate = e.data.from;
                            query.dateRange.endDate = e.data.to;
                            query.workCenters = $scope.filter.query.workCenters;
                            query.shifts = $scope.filter.query.shifts.map(function(shift) {
                                return $scope.filter.shifts[parseInt(shift) - 1];
                            });

                            FilterService.saveFilter(query);
                            if (e.data.series == 0)
                                $state.go("Datasource.OEE");
                            else if (e.data.series == 2)
                                $state.go("Datasource.Deviation");
                        }
                    }
                },
                clipEdge: true,
                duration: 500,
                stacked: true,
                xAxis: {
                    showMaxMin: false,
                    tickFormat: function(d) {
                        return d3.time.format('%Y-%m')(new Date(d))
                    }
                },
                yAxis: {
                    axisLabel: 'OEE',
                    axisLabelDistance: -20,
                    tickFormat: function(d) {
                        return d3.format(',.0f')(d * 100) + '%';
                    }
                },
                color: ['#348fe2', '#b6c2c9', '#f59c1a']
            }
        };

        var monthlyData = angular.copy(data);
        var dailyData = angular.copy(data);
        dailyData.chart.xAxis.tickFormat = function(d) {
            return d3.time.format('%Y-%m-%d')(new Date(d))
        };


        $scope.data.monthly = angular.copy(monthlyData);
        $scope.data.monthly.title = {
            enable: true,
            text: 'OEE Monthly Analytics'
        };

        $scope.data.daily = angular.copy(dailyData);
        $scope.data.daily.title = {
            enable: true,
            text: 'OEE Daily Analytics'
        };

        $scope.data.monthlyData = [];
        $scope.data.dailyData = [];

        $scope.getMonthlyOEE = function(number) {
            var now = moment($scope.filter.query.date).endOf('day');
            var endMonth = now.endOf('months');
            var from = angular.copy(endMonth).subtract(number, 'months').endOf('month');
            var to = angular.copy(endMonth);
            $scope.getOEE(from, to);
        };

        $scope.getDailyOEE = function(number) {
            var now = moment($scope.filter.query.date).endOf('day');
            var endOfDay = now.endOf('day');
            var from = angular.copy(endOfDay).subtract(number, 'days');
            var to = angular.copy(endOfDay);
            $scope.getOEE(from, to, 'days');
        };

        function pad(num, size) {
            var s = num + "";
            while (s.length < size) s = "0" + s;
            return s;
        }

        function fill(oeeData, deviationData, from, to, freq) {
            var intervalSize = Math.ceil(to.diff(from, freq, true));
            var oee = [];
            var referenceDate = moment($scope.filter.query.date).add(1, freq);
            for (var i = 0; i < intervalSize; i++) {
                var from = angular.copy(referenceDate).subtract(intervalSize - i, freq).startOf(freq);
                var to = angular.copy(referenceDate).subtract(intervalSize - i - 1, freq).startOf(freq).subtract(1, 'days').endOf('day');
                oee[i] = {
                    x: angular.copy(from).valueOf(),
                    y: 0,
                }
                oee[i].from = from;
                oee[i].to = to;
            }
            var deviation = angular.copy(oee);
            var missing = angular.copy(oee);
            for (var i = 0; i < oeeData.length; i++) {
                var entry = oeeData[i];
                var dateStr = entry['_id'].year + '-' + pad(entry['_id'].month, 2) + '-' + pad((freq == 'days' ? entry['_id'].day : 1), 2);
                var date = moment(dateStr);
                var index = intervalSize - 1 - Math.floor(to.diff(date, freq, true));
                if (!oee[index]) oee[index] = {};
                oee[index].y = entry.sumPlannedOperatingTime == 0 ? 0 : entry.sumUptime / entry.sumPlannedOperatingTime;
                oee[index].sumPlannedOperatingTime = entry.sumPlannedOperatingTime;
            }
            for (var i = 0; i < deviationData.length; i++) {
                var entry = deviationData[i];
                var dateStr = entry['_id'].year + '-' + pad(entry['_id'].month, 2) + '-' + pad((freq == 'days' ? entry['_id'].day : 1), 2);
                var date = moment(dateStr);
                var index = intervalSize - 1 - Math.floor(to.diff(date, freq, true));
                if (oee[index] && oee[index].sumPlannedOperatingTime > 0 && deviation[index])
                    deviation[index].y = oee[index] ? 60 * entry.sumDuration / oee[index].sumPlannedOperatingTime : 0;
            }

            return [{
                key: "OEE",
                values: angular.copy(oee)
            }, {
                key: "Missing",
                values: angular.copy(missing).map(function(entry, i) {
                    return {
                        x: entry.x,
                        y: 1 - (oee[i].y ? oee[i].y : 0) - (deviation[i].y ? deviation[i].y : 0)
                    }
                })
            }, {
                key: "Deviation",
                values: angular.copy(deviation)
            }];
        }


        $scope.data.dailyQueries = [{
            displayName: '2m',
            number: 60
        }, {
            displayName: '1m',
            number: 30
        }, {
            displayName: '2w',
            number: 14
        }, {
            displayName: '1w',
            number: 7
        }];

        $scope.data.activeDailyQuery = $scope.data.dailyQueries.length - 1;

        $scope.data.monthlyQueries = [{
            displayName: '5y',
            number: 60
        }, {
            displayName: '3y',
            number: 36
        }, {
            displayName: '1y',
            number: 12
        }, {
            displayName: '6m',
            number: 6
        }];

        $scope.data.activeMonthlyQuery = $scope.data.monthlyQueries.length - 1;

        $scope.getGraphOEE = function(index, query, type) {
            if (type == 'days') {
                $scope.data.activeDailyQuery = index;
                $scope.getDailyOEE(query.number)
            }
            else {
                $scope.data.activeMonthlyQuery = index;
                $scope.getMonthlyOEE(query.number)
            }
        };

        $scope.store = {};

        function fillCharts(oeeData, deviationData, from, to, freq) {
            if (freq === 'days') {
                $scope.data.dailyData = [];
                $scope.data.dailyData = fill(oeeData, deviationData, from, to, 'days');
            }
            else {
                $scope.data.monthlyData = [];
                $scope.data.monthlyData = fill(oeeData, deviationData, from, to, 'months');
            }
            $timeout(function() {
                d3.selectAll('.nv-legendWrap').attr('transform', 'translate(180 340)');
            }, 1)
        }

        $scope.getOEE = function(from, to, freq) {

            var workCenters = $scope.filter.query.workCenters.map(function(workCenter) {
                if ($scope.filter.workCenters[workCenter.id]) return $scope.filter.workCenters[workCenter.id].label;
            }).filter(function(workCenter) {
                return workCenter !== undefined;
            });

            if (workCenters.length == 0 || $scope.filter.query.shifts.length == 0) {
                fillCharts([], [], from, to, freq);
                return;
            }

            var devPromise = DeviationEntryService.getDeviationEntriesStatistics({
                from: from.valueOf(),
                to: to.valueOf(),
                offset: moment().utcOffset(),
                workCenters: workCenters,
                shifts: $scope.filter.query.shifts,
                freq: freq
            });
            var oeePromise = OEEEntryService.getOEEEntriesStatistics({
                from: from.valueOf(),
                to: to.valueOf(),
                offset: moment().utcOffset(),
                workCenters: workCenters,
                shifts: $scope.filter.query.shifts,
                freq: freq
            });

            if (!$scope.store[freq]) $scope.store[freq] = {};
            $scope.store[freq].from = from;
            $scope.store[freq].to = to;

            $q.all([devPromise, oeePromise])
                .then(function(res) {
                    var deviation = res[0];
                    var oee = res[1];
                    var deviationData = deviation.reverse();
                    var oeeData = oee.reverse();
                    fillCharts(oeeData, deviationData, from, to, freq);
                });
        }

        $scope.$on('$refresh', function() {
            $scope.refresh();
        });

        $scope.refresh = function() {
            var query = FilterService.getFilter();
            if (query) $scope.filter.query = query;
            $scope.getMonthlyOEE($scope.data.monthlyQueries[$scope.data.activeMonthlyQuery].number);
            $scope.getDailyOEE($scope.data.dailyQueries[$scope.data.activeDailyQuery].number);
        }

    }]);
angular.module('app.controllers.oee', ['nvd3', 'app.controllers.oee.charts']).controller(
    "OEECtrl",
    ["$rootScope", "$scope", "$q", "$state", "growl", "$interval", "$timeout", "$controller", "OEEEntryService",
        "DeviationEntryService", "FilterService", "PlansService",
        function($rootScope, $scope, $q, $state, growl, $interval, $timeout, $controller, OEEEntryService,
                 DeviationEntryService, FilterService, PlansService) {

            $rootScope.state = 'OEE';

            angular.extend(this, $controller('DataFilterCtrl', {
                $scope: $scope
            }));

            $scope.controls = {
                topDownOEE: false,
                topDownTEEP: false
            };

            var MINUTES_IN_DAY = 24 * 60;

            $scope.clear = function() {
                $scope.filter.query.date = null;
            };
            $scope.maxDate = new Date();
            $scope.toggleMin = function() {
                $scope.minDate = $scope.minDate ? null : new Date();
            };
            $scope.open = function($event) {
                $event.preventDefault();
                $event.stopPropagation();
                $scope.filter.date.opened = $scope.filter.date.opened ? false : true;
            };
            $scope.dateOptions = {
                formatYear: 'yyyy',
                startingDay: 1
            };

            var tomorrow = new Date();
            tomorrow.setDate(tomorrow.getDate() + 1);
            var afterTomorrow = new Date();
            afterTomorrow.setDate(tomorrow.getDate() + 2);
            $scope.events = [{
                date: tomorrow,
                status: 'full'
            }, {
                date: afterTomorrow,
                status: 'partially'
            }];
            $scope.getDayClass = function(date, mode) {
                if (mode === 'day') {
                    var dayToCheck = new Date(date).setHours(0, 0, 0, 0);
                    for (var i = 0; i < $scope.events.length; i++) {
                        var currentDay = new Date($scope.events[i].date).setHours(0, 0, 0, 0);
                        if (dayToCheck === currentDay) {
                            return $scope.events[i].status;
                        }
                    }
                }
                return '';
            };

            function query() {
                if (!$scope.filter.query.workCenters) return;
                var workCenters = $scope.filter.query.workCenters.map(function(workCenter) {
                    if ($scope.filter.workCenters[workCenter.id]) return $scope.filter.workCenters[workCenter.id].label;
                }).filter(function(workCenter) {
                    return workCenter !== undefined;
                });

                var from = moment($scope.filter.query.date).startOf('day');
                var to = moment($scope.filter.query.date).endOf('day');

                if (workCenters.length == 0 || $scope.filter.query.shifts.length == 0) {
                    $scope.getTopBottomOEE([], []);
                    $scope.getBottomUpOEE([]);
                    return;
                }

                var devPromise = DeviationEntryService.getDeviationEntries({
                    from: from.valueOf(),
                    to: to.valueOf(),
                    workCenters: workCenters,
                    shifts: $scope.filter.query.shifts
                });

                var oeePromise = OEEEntryService.getOeeEntries({
                    from: from.valueOf(),
                    to: to.valueOf(),
                    workCenters: workCenters,
                    shifts: $scope.filter.query.shifts
                });

                $q.all([devPromise, oeePromise])
                    .then(function(res) {
                        var deviation = res[0].content;
                        var oee = res[1].content;
                        $scope.getTopBottomOEE(deviation, oee);
                        $scope.getBottomUpOEE(oee);
                    });
            }

            $scope.filter.options = {
                scrollableHeight: '200px',
                scrollable: true,
                smartButtonMaxItems: 3
            };

            $scope.getBottomUpOEE = function(oeeEntries) {
                $scope.data.bottomUpOee = undefined;
                $scope.data.oeeData = undefined;
                $scope.data.totalEffectivenessBottomUp = undefined;
                if (!oeeEntries || oeeEntries.length == 0) return;
                $scope.data.oeeData = {
                    plannedOperatingTime: 0,
                    weightedAvgCycleTime: 0,
                    goodPiecesCompleted: 0
                }
                var sumTop = 0;
                var sumBottom = 0;
                var workCenters = [];
                oeeEntries.forEach(function(entry) {
                    $scope.data.oeeData.plannedOperatingTime += entry.plannedOperatingTime;
                    $scope.data.oeeData.weightedAvgCycleTime += entry.weightedAvgCycleTime;
                    $scope.data.oeeData.goodPiecesCompleted += entry.goodPiecesCompleted;
                    if (entry.plannedOperatingTime > 0) {
                        sumTop += entry.goodPiecesCompleted * entry.weightedAvgCycleTime;
                        sumBottom += entry.plannedOperatingTime;
                    }
                    if (workCenters.indexOf(entry.workCenter) == -1)
                        workCenters.push(entry.workCenter);
                });
                $scope.data.oeeData.plannedOperatingTime = $scope.data.oeeData.plannedOperatingTime;
                $scope.data.oeeData.weightedAvgCycleTime = $scope.data.oeeData.weightedAvgCycleTime / oeeEntries.length;
                $scope.data.oeeData.goodPiecesCompleted = $scope.data.oeeData.goodPiecesCompleted;
                $scope.data.bottomUpOee = 100 * sumTop / sumBottom;
                $scope.data.totalEffectivenessBottomUp = 100 * sumTop / (MINUTES_IN_DAY * workCenters.length);
            };

            $scope.getTopBottomOEE = function(deviationEntries, oeeEntries) {
                $scope.data.topBottomOee = 0;
                $scope.data.totalEffectivenessTopDown = 0;
                if (!oeeEntries || oeeEntries.length == 0)
                    return;

                var sumTop = 0;
                var sumBottom = 0;
                var workCenters = [];
                oeeEntries.forEach(function(oeeEntry) {
                    var deviationMatches = $.grep(deviationEntries, function(entry) {
                        return entry.deviation1Level === 'OEE Data' && entry.deviation1Level !== 'Planned Downtime' &&
                            entry.shift === oeeEntry.shift && entry.workCenter === oeeEntry.workCenter;
                    });
                    var sumDuration = 0;
                    deviationMatches.forEach(function(entry) {
                        if (entry.duration > 0)
                            sumDuration += entry.duration;
                    });

                    sumTop += (oeeEntry.plannedOperatingTime - 60 * sumDuration);
                    sumBottom += oeeEntry.plannedOperatingTime;
                    if (workCenters.indexOf(oeeEntry.workCenter) == -1)
                        workCenters.push(oeeEntry.workCenter);
                });
                $scope.data.topBottomOee = 100 * (sumTop / sumBottom);
                $scope.data.totalEffectivenessTopDown = 100 * sumTop / (MINUTES_IN_DAY * workCenters.length);
            };

            $scope.goToAnalysis = function() {
                if ($rootScope.currentUser.roles[0] && $rootScope.currentUser.roles[0].name === "SuperAdmin") {
                    $scope.selectState('/analysis');
                    return;
                }

                PlansService.refreshActiveSubscription().then(function() {
                    var subscriptionPlan = PlansService.getActiveSubscription();
                    if (subscriptionPlan.providerId === 'started') {
                        growl.error("The Starter Subscription Plan does not include access to the Analysis Tools", {
                            title: 'Insufficient Subscription'
                        });
                    }
                    else if ($rootScope.currentUser.organization.nusers > subscriptionPlan.properties.nUsers) {
                        growl.info("You have exceeded the maximum number of users allowed for your Subscription Plan. You " +
                            "must remove " + ($rootScope.currentUser.organization.nusers - subscriptionPlan.properties.nUsers) +
                            " user(s) before accessing the Analysis Tools", {
                            title: 'Too Many Users'
                        });
                    }
                    else
                        $scope.selectState('/analysis');
                });
            };

            var timer = $interval(query, 20000);

            $scope.$on('$refresh', function() {
                query();
            });

            $scope.$on('$destroy', function() {
                $interval.cancel(timer);
            });

        }]);
angular.module('app.controllers.datasources.filters', []).controller(
    'DataFilterCtrl',
    ['$rootScope', '$scope', 'DeviationEntryService', 'FilterService',
        function($rootScope, $scope, DeviationEntryService, FilterService) {

            var querySaved = false;

            var getQuery = function() {
                var query = FilterService.getFilter();
                if (query) {
                    querySaved = true;
                    query.date = moment(query.date).format('YYYY-MM-DD');
                    return angular.copy(query);
                }
                else
                    return {
                        workCenters: [],
                        shifts: [1, 2, 3, 4, 5, 6, 7, 8],
                        date: moment().format('YYYY-MM-DD'),
                        dateRange: {
                            startDate: moment().subtract(2, 'years'),
                            endDate: moment()
                        }
                    };
            };

            $scope.filter = {
                date: {
                    opened: false
                },
                dateOptions: {
                    opens: 'left',
                    ranges: {
                        'Today': [moment().startOf('day'), moment()],
                        'Last Week': [moment().subtract(7, 'days'), moment()],
                        'Last Month': [moment().subtract(1, 'months'), moment()],
                        'Last 3 Months': [moment().subtract(3, 'months'), moment()],
                        'Last 6 Months': [moment().subtract(6, 'months'), moment()]
                    },
                },
                shifts: [1, 2, 3, 4, 5, 6, 7, 8],
                workCenters: [],
                query: getQuery(),
                options: {
                    scrollableHeight: '200px',
                    scrollable: true,
                    smartButtonMaxItems: 3
                }
            };

            $scope.getNumber = function(num) {
                return new Array(num);
            };

            $scope.getQueryOptions = function(tableState) {
                if (!$scope.tableState && tableState)
                    $scope.tableState = tableState;

                var pagination = tableState.pagination;

                var start = pagination.start || 0;     // This is NOT the page number, but the index of item in the list that you want to use to display the table.
                var number = pagination.number || 10;  // Number of entries showed per page.

                var workCenters = $scope.filter.query.workCenters.map(function(workCenter) {
                    if ($scope.filter.workCenters[workCenter.id]) return $scope.filter.workCenters[workCenter.id].label;
                }).filter(function(workCenter) {
                    return workCenter !== undefined;
                });

                var shifts = $scope.filter.query.shifts.map(Number);

                var from = moment($scope.filter.query.dateRange.startDate).startOf('day');
                var to = moment($scope.filter.query.dateRange.endDate).endOf('day');

                return {
                    from: from.valueOf(),
                    to: to.valueOf(),
                    workCenters: workCenters,
                    shifts: shifts,
                    start: start / number,
                    number: number
                }
            };

            $scope.areItemsSelected = function(items) {
                if (!items) return false;
                return $.grep(items, function(entry) {
                    return entry.isSelected;
                }).length > 0;
            };

            $scope.validateDateFormat = function(dateString) {
                if (!/^\d{1,2}\/\d{1,2}\/\d{4}$/.test(dateString))
                    return false;
                var parts = dateString.split("/");
                var day = parseInt(parts[1], 10);
                var month = parseInt(parts[0], 10);
                var year = parseInt(parts[2], 10);
                if (year < 1000 || year > 3000 || month == 0 || month > 12)
                    return false;
                var monthLength = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
                if (year % 400 == 0 || (year % 100 != 0 && year % 4 == 0))
                    monthLength[1] = 29;
                return day > 0 && day <= monthLength[month - 1];
            };

            $scope.validateTimeFormat = function(timeString) {
                var utcPattern = /^(1[012]|[1-9]):([0-5][0-9]):([0-5][0-9]) (am|pm|AM|PM)$/; //9:44:56 PM
                var timezonePattern = /^(2[0123]|1[0-9]|0[1-9]):([0-5][0-9]):([0-5][0-9])(-|\+)(1[012]|0[0-9]):([03]0)$/; //15:44:56-06:00
                return utcPattern.test(timeString) || timezonePattern.test(timeString);
            };

            $scope.validateShift = function(shiftString) {
                var f = /^([12345678])$/;
                return f.test(shiftString);
            };

            $scope.validateString = function(str) {
                return typeof str === 'string';
            };

            $scope.validateNumber = function(number) {
                if (!number) return false;
                number = number.replace(/[",]+/g, '');
                return (number - 0) == number && ('' + number).trim().length > 0;
            };

            $scope.downloadSample = function(type) {
                if (window.navigator.msSaveOrOpenBlob && window.Blob) {
                    var blob = new Blob([$scope.data.samplePreview], {type: "text/csv"});
                    navigator.msSaveOrOpenBlob(blob, type + ".csv");
                } else {
                    var csvContent = "data:text/csv;charset=utf-8," + $scope.data.samplePreview;
                    var encodedUri = encodeURI(csvContent);
                    var link = document.createElement("a");
                    link.setAttribute("href", encodedUri);
                    link.setAttribute("download", type + ".csv");
                    document.body.appendChild(link);
                    link.click();
                }
            };

            var sortByName = function(a, b) {
                var nameA = a.toUpperCase();
                var nameB = b.toUpperCase();
                if (nameA < nameB) {
                    return -1;
                }
                if (nameA > nameB) {
                    return 1;
                }

                return 0;
            }

            var getDeviationEntriesWorkCenters = function() {
                return DeviationEntryService.getDeviationEntriesWorkCenters().then(function(workCenters) {
                    var index = 0;
                    workCenters = workCenters.sort(sortByName);
                    $scope.filter.workCenters = workCenters.map(function(workCenter) {
                        return {
                            id: index++,
                            label: workCenter
                        }
                    });
                });
            };

            $scope.$watch('filter.query', function() {
                if (!$scope.filter.query) return;
                if (!$scope.filter.query.date || !$scope.filter.query.workCenters || !$scope.filter.query.shifts || !$scope.filter.workCenters) return;
                FilterService.saveFilter($scope.filter.query);
                $rootScope.$broadcast('$refresh');
            }, true);

            $scope.init = function() {
                getDeviationEntriesWorkCenters().then(function() {
                    if (!querySaved)
                        $scope.filter.query.workCenters = $scope.filter.workCenters.map(function(workCenter) {
                            return {
                                id: workCenter.id
                            }
                        });
                    $rootScope.$broadcast('$refresh');
                })
            };
            $scope.init();
        }
    ]);





angular.module('app.controllers.datasources.oee', []).controller(
    'DatasourcesOEECtrl',
    ['$rootScope', '$scope', '$controller', '$http', '$log', '$timeout', 'growl', 'OEEEntryService', 'APP_CONSTANTS',
        function($rootScope, $scope, $controller, $http, $log, $timeout, growl, OEEEntryService, APP_CONSTANTS) {

            $rootScope.state = 'OEE Data';

            $scope.data = {
                oeeEntries: [],
                newEntry: {}
            };

            $scope.buttons = {
                confirmed: false,
                isCollapsed: false
            };

            $scope.controls = {};

            angular.extend(this, $controller('DataFilterCtrl', {
                $scope: $scope
            }));

            $scope.data.samplePreview = "Date,Time,Work Center,Shift,Planned Operating Time,Weighted Average Cycle Time (min),Good Pieces Completed\n" +
                "4/30/2015,9:00:00 PM,MCF45CP,1,420,3.24,138\n" +
                "4/30/2015,15:00:00-06:00,MCF45CP,2,420,3.24,138\n" +
                "4/30/2015,MCF45CP,3,420,3.24,138";


            $scope.initDate = function() {
                $scope.data.newEntry.timestamp = $scope.latestDate ? $scope.latestDate : new Date();
            };
            $scope.initDate();
            $scope.clear = function() {
                $scope.data.newEntry.timestamp = null;
            };
            $scope.maxDate = new Date();
            $scope.toggleMin = function() {
                $scope.minDate = $scope.minDate ? null : new Date();
            };
            $scope.open = function($event) {
                $event.preventDefault();
                $event.stopPropagation();
                $scope.data.opened = $scope.data.opened ? false : true;
            };
            $scope.dateOptions = {
                formatYear: 'yy',
                startingDay: 1
            };
            var tomorrow = new Date();
            tomorrow.setDate(tomorrow.getDate() + 1);
            var afterTomorrow = new Date();
            afterTomorrow.setDate(tomorrow.getDate() + 2);
            $scope.events = [{
                date: tomorrow,
                status: 'full'
            }, {
                date: afterTomorrow,
                status: 'partially'
            }];
            $scope.getDayClass = function(date, mode) {
                if (mode === 'day') {
                    var dayToCheck = new Date(date).setHours(0, 0, 0, 0);
                    for (var i = 0; i < $scope.events.length; i++) {
                        var currentDay = new Date($scope.events[i].date).setHours(0, 0, 0, 0);
                        if (dayToCheck === currentDay) {
                            return $scope.events[i].status;
                        }
                    }
                }
                return '';
            };

            function processLine(text) {
                var re_valid = /^\s*(?:'[^'\\]*(?:\\[\S\s][^'\\]*)*'|"[^"\\]*(?:\\[\S\s][^"\\]*)*"|[^,'"\s\\]*(?:\s+[^,'"\s\\]+)*)\s*(?:,\s*(?:'[^'\\]*(?:\\[\S\s][^'\\]*)*'|"[^"\\]*(?:\\[\S\s][^"\\]*)*"|[^,'"\s\\]*(?:\s+[^,'"\s\\]+)*)\s*)*$/;
                var re_value = /(?!\s*$)\s*(?:'([^'\\]*(?:\\[\S\s][^'\\]*)*)'|"([^"\\]*(?:\\[\S\s][^"\\]*)*)"|([^,'"\s\\]*(?:\s+[^,'"\s\\]+)*))\s*(?:,|$)/g;
                if (!re_valid.test(text)) return null;
                var a = [];
                text.replace(re_value,
                    function(m0, m1, m2, m3) {
                        if (m1 !== undefined) a.push(m1.replace(/\\'/g, "'"));
                        else if (m2 !== undefined) a.push(m2.replace(/\\"/g, '"'));
                        else if (m3 !== undefined) a.push(m3);
                        return '';
                    });
                if (/,\s*$/.test(text)) a.push('');
                return a;
            }


            function processData(allText) {
                var allTextLines = allText.split(/\r\n|\n|\r/);
                allTextLines = allTextLines.filter(function(line) {
                    return line != ""
                });
                var lines = [];
                while (allTextLines.length > 0)
                    lines.push(processLine(allTextLines.shift()));
                return lines;
            }

            $scope.validateData = function() {
                $scope.data.errors = [];
                $scope.data.filePreview.forEach(function(row, idx) {
                    if (!row.length) return;
                    //Needed for backwards compatibility: if no time specified, auto fill 12PM UTC
                    if (row.length < 7 && !$scope.validateTimeFormat(row[1]))
                        row.splice(1, 0, "12:00:00 PM");
                    var errors = [];
                    for (var i = 0; i < 7; i++) {
                        switch (i) {
                            case 0:
                                if (!$scope.validateDateFormat(row[i]))
                                    errors.push(i);
                                break;
                            case 1:
                                if (!$scope.validateTimeFormat(row[i]))
                                    errors.push(i);
                                break;
                            case 2:
                                if (!$scope.validateString(row[i]))
                                    errors.push(i);
                                break;
                            case 3:
                            case 4:
                            case 5:
                            case 6:
                                if (row[i] == undefined || (row[i] && !$scope.validateNumber(row[i])))
                                    errors.push(i);
                                break;
                            default:
                                break;
                        }
                    }
                    row.errors = errors;
                })
            };

            $http.get('app/main/datasources/data/oee/data/fields.json')
                .then(function(res) {
                    $scope.data.fieldsDescription = res.data;
                });

            $scope.copy = function(text) {
                copyTextToClipboard(text)
            };

            function copyTextToClipboard(text) {
                var textArea = document.createElement("textarea");
                textArea.style.position = 'fixed';
                textArea.style.top = 0;
                textArea.style.left = 0;
                textArea.style.width = '2em';
                textArea.style.height = '2em';
                textArea.style.padding = 0;
                textArea.style.border = 'none';
                textArea.style.outline = 'none';
                textArea.style.boxShadow = 'none';
                textArea.style.background = 'transparent';
                textArea.value = text;
                document.body.appendChild(textArea);
                textArea.select();
                try {
                    var successful = document.execCommand('copy');
                    var msg = successful ? 'successful' : 'unsuccessful';
                    console.log('Copying text command was ' + msg);
                } catch (err) {
                    console.log('Oops, unable to copy');
                }
                document.body.removeChild(textArea);
            }

            $scope.add = function() {
                $scope.data.newEntry = {};
                $scope.initDate();
            };

            $scope.upload = function() {
                $timeout(function() {
                    $("#input-1a").fileinput({
                        showUpload: false,
                        mainClass: "input-group-sm"
                    });

                    $('#input-1a').on('fileloaded', function(event, file, previewId, index, reader) {
                        $scope.data.file = file;
                        $scope.$apply();
                    });

                    $('#input-1a').on('fileclear', function(event, file, previewId, index, reader) {
                        $scope.data.file = null;
                        $scope.controls.analysis = false;
                        $scope.$apply();
                    });
                }, 1)
            };

            $scope.preAnalyze = function() {
                if ($scope.data.file.size > 25000) {
                    growl.warning("File is too large for pre-validation.", {
                        title: 'Error'
                    });
                    return;
                }
                $scope.controls.analysis = true;
                var reader = new FileReader();
                reader.onload = function(e) {
                    var data = processData(e.target.result);
                    data.shift();
                    $scope.data.filePreview = data;
                    $scope.validateData();
                };
                reader.readAsText($scope.data.file);
            };

            $scope.save = function() {
                if (!$scope.data.newEntry.uuid) {
                    OEEEntryService.addOeeEntries([$scope.data.newEntry]).then(function(entries) {
                        if (entries.length == 0) {
                            growl.error("Entry was not saved. Please validate the format of each entry and " +
                                "create matching Work Centers before uploading data. Please also make sure you have " +
                                "permission to upload data for the specified Work Center", {
                                title: 'Warning'
                            });
                            $scope.edit($scope.data.newEntry);
                        }
                        else {
                            growl.info("Entry added successfully.", {
                                title: 'Info'
                            });
                            $scope.refresh();
                        }
                    });
                } else {
                    OEEEntryService.updateOeeEntry($scope.data.newEntry.uuid, $scope.data.newEntry).then(function(entry) {
                        $scope.buttons.isCollapsed = false;
                    }, function(err) {
                        $scope.buttons.isCollapsed = true;
                        $scope.data.newEntry = angular.copy($scope.data.backUp);
                    });
                }
            };

            $scope.edit = function(entry) {
                $scope.data.backUp = angular.copy(entry);
                $scope.buttons.isCollapsed = true;
                $scope.data.newEntry = entry;
            };

            $scope.remove = function() {
                var entries = $.grep($scope.data.displayedOeeEntries, function(entry) {
                    return entry.isSelected;
                });
                entries.forEach(function(entry) {
                    OEEEntryService.deleteOeeEntry(entry.uuid).then(function() {
                        $scope.getOEEEntries($scope.tableState);
                    });
                });
            };

            $scope.submitFile = function() {
                var reader = new FileReader();
                reader.onload = function(e) {
                    var data = processData(e.target.result);
                    var numSubmitted = data.length - 1;
                    var fd = new FormData();
                    fd.append('file', $scope.data.file);

                    $http.post(APP_CONSTANTS.API_PATH + "/api/v2/oeeentries/source", fd, {
                        transformRequest: angular.identity,
                        headers: {
                            'Content-Type': undefined
                        }
                    }).then(function(result) {
                        $scope.buttons.upload = false;
                        $scope.refresh();
                        growl.info(result.data.length + " entries have been added.", {
                            title: 'Info'
                        });
                        if (result.data.length < numSubmitted) {
                            growl.warning(numSubmitted - result.data.length + " entries have not been saved. Please " +
                                "validate the format of each entry and create matching Work Centers before uploading " +
                                "data. Please also make sure you have permission to upload data for each Work Center", {
                                title: 'Warning'
                            });
                        }
                    }, function(err) {
                        growl.error("There has been an error processing your request.", {
                            title: 'Error'
                        });
                    }, function(err) {
                        $scope.controls.initFlag = false;
                    });
                };
                reader.readAsText($scope.data.file);
            };
            $scope.tableState;
            $scope.refresh = function() {
                $scope.getOEEEntries($scope.tableState);
            };

            $scope.$watch('filter', function() {
                if (!$scope.tableState) return;
                $scope.getOEEEntries($scope.tableState);
            }, true);


            $scope.getOEEEntries = function(tableState) {
                var options = $scope.getQueryOptions(tableState);
                if (options.shifts.length == 0 || options.workCenters.length == 0) {
                    $scope.data.displayedOeeEntries = [];
                    tableState.pagination.numberOfPages = 0;
                    return;
                }
                OEEEntryService.getOeeEntries(options).then(function(result) {
                    $scope.data.displayedOeeEntries = result.content;
                    tableState.pagination.numberOfPages = result.totalPages;
                });
            };
        }]);

angular.module('app.controllers.datasources.deviation', ['app.controllers.datasources.filters']).controller(
    'DatasourcesDeviationCtrl',
    ['$scope', '$controller', '$http', '$log', '$timeout', 'growl', '$modal', '$modalStack', '$location', 'DeviationEntryService', 'APP_CONSTANTS',
        function($scope, $controller, $http, $log, $timeout, growl, $modal, $modalStack, $location, DeviationEntryService, APP_CONSTANTS) {

            $scope.data = {
                deviationEntries: [],
                newEntry: {},
                process: false,
                selectedShifts: []
            };

            $scope.buttons = {
                confirmed: false,
                isCollapsed: false
            }

            $scope.controls = {};

            angular.extend(this, $controller('DataFilterCtrl', {
                $scope: $scope
            }));

            // $scope.selectAll = function() {
            //     $scope.data.displayedDeviationEntries.forEach(function(entry) {
            //         entry.isSelected = true;
            //     })
            // }

            $scope.showDeviationCodes = function() {
                var opts = {
                    templateUrl: "app/main/datasources/data/deviation/views/deviations.html",
                    scope: $scope,
                    size: 'md'
                };
                var instance = $modal.open(opts);
            }

            $scope.copyDeviations = function() {
                var text = '';
                if ($scope.data.selectedDeviation)
                    text += format($scope.data.selectedDeviation.displayName) + ',';
                if ($scope.data.selectedType)
                    text += format($scope.data.selectedType.displayName) + ',';
                if ($scope.data.selectedGroup)
                    text += format($scope.data.selectedGroup.displayName) + ',';
                if ($scope.data.selectedGroupType)
                    text += format($scope.data.selectedGroupType.displayName);
                copyTextToClipboard(text);
            }

            function format(text) {
                if (text.indexOf(",") > -1)
                    return "\"" + text + "\"";
                else
                    return text;
            }

            $scope.copy = function(text) {
                copyTextToClipboard(text)
            }

            $scope.close = function() {
                $modalStack.dismissAll();
                if (!$scope.data.selectedDeviation) return;
                $scope.data.newEntry.deviation1Level = $scope.data.selectedDeviation.displayName;
                $scope.data.newEntry.deviation2Level = $scope.data.selectedType.displayName;
                $scope.data.newEntry.deviation3Level = $scope.data.selectedGroup.displayName;
                $scope.data.newEntry.deviation4Level = $scope.data.selectedGroupType.displayName;
            }


            $scope.data.samplePreview = "Date,Time,Shift,Plant,Cost Center,Work Center,Deviation 1st level,Deviation 2nd Level,Deviation 3rd Level,Deviation 4th level,Duration,Amount,Partnumber,Comment\n" +
                "11/30/2014,9:00:00 PM,3,US10,10155541,MCF12CP,OEE Data,Technical,Mechanical Failure,Breakdown machine,4.72,177.84,N/A,\"Spindle Failure, should be fixed\"\n" +
                "11/30/2014,15:00:00-06:00,3,US10,10155541,MCF12CP,OEE Data,Technical,Mechanical Failure,Breakdown machine,4.72,177.84,N/A,\"Spindle Failure, should be fixed\""

            function copyTextToClipboard(text) {
                var textArea = document.createElement("textarea");
                textArea.style.position = 'fixed';
                textArea.style.top = 0;
                textArea.style.left = 0;
                textArea.style.width = '2em';
                textArea.style.height = '2em';
                textArea.style.padding = 0;
                textArea.style.border = 'none';
                textArea.style.outline = 'none';
                textArea.style.boxShadow = 'none';
                textArea.style.background = 'transparent';
                textArea.value = text;
                document.body.appendChild(textArea);
                textArea.select();
                try {
                    var successful = document.execCommand('copy');
                    var msg = successful ? 'successful' : 'unsuccessful';
                    console.log('Copying text command was ' + msg);
                } catch (err) {
                    console.log('Oops, unable to copy');
                }
                document.body.removeChild(textArea);
            }

            $http.get('app/main/datasources/data/deviation/data/fields.json')
                .then(function(res) {
                    $scope.data.fieldsDescription = res.data;
                });

            $http.get('app/main/datasources/data/deviation/data/deviations.json')
                .then(function(res) {
                    $scope.data.deviationData = res.data;
                });

            $scope.initDate = function() {
                $scope.data.newEntry.timestamp = $scope.latestDate ? $scope.latestDate : new Date();
            };
            $scope.initDate();
            $scope.clear = function() {
                $scope.data.newEntry.timestamp = null;
            };
            $scope.maxDate = new Date();
            $scope.toggleMin = function() {
                $scope.minDate = $scope.minDate ? null : new Date();
            };
            $scope.open = function($event) {
                $event.preventDefault();
                $event.stopPropagation();
                $scope.data.opened = $scope.data.opened ? false : true;
            };
            $scope.dateOptions = {
                formatYear: 'yy',
                startingDay: 1
            };
            var tomorrow = new Date();
            tomorrow.setDate(tomorrow.getDate() + 1);
            var afterTomorrow = new Date();
            afterTomorrow.setDate(tomorrow.getDate() + 2);
            $scope.events = [{
                date: tomorrow,
                status: 'full'
            }, {
                date: afterTomorrow,
                status: 'partially'
            }];
            $scope.getDayClass = function(date, mode) {
                if (mode === 'day') {
                    var dayToCheck = new Date(date).setHours(0, 0, 0, 0);
                    for (var i = 0; i < $scope.events.length; i++) {
                        var currentDay = new Date($scope.events[i].date).setHours(0, 0, 0, 0);
                        if (dayToCheck === currentDay) {
                            return $scope.events[i].status;
                        }
                    }
                }
                return '';
            };

            $scope.add = function() {
                $scope.data.newEntry = {};
                $scope.initDate();
            };

            function processLine(text) {
                var re_valid = /^\s*(?:'[^'\\]*(?:\\[\S\s][^'\\]*)*'|"[^"\\]*(?:\\[\S\s][^"\\]*)*"|[^,'"\s\\]*(?:\s+[^,'"\s\\]+)*)\s*(?:,\s*(?:'[^'\\]*(?:\\[\S\s][^'\\]*)*'|"[^"\\]*(?:\\[\S\s][^"\\]*)*"|[^,'"\s\\]*(?:\s+[^,'"\s\\]+)*)\s*)*$/;
                var re_value = /(?!\s*$)\s*(?:'([^'\\]*(?:\\[\S\s][^'\\]*)*)'|"([^"\\]*(?:\\[\S\s][^"\\]*)*)"|([^,'"\s\\]*(?:\s+[^,'"\s\\]+)*))\s*(?:,|$)/g;
                if (!re_valid.test(text)) return null;
                var a = [];
                text.replace(re_value,
                    function(m0, m1, m2, m3) {
                        if (m1 !== undefined) a.push(m1.replace(/\\'/g, "'"));
                        else if (m2 !== undefined) a.push(m2.replace(/\\"/g, '"'));
                        else if (m3 !== undefined) a.push(m3);
                        return '';
                    });
                if (/,\s*$/.test(text)) a.push('');
                return a;
            };


            function processData(allText) {
                var allTextLines = allText.split(/\r\n|\n|\r/);
                allTextLines = allTextLines.filter(function(line) {
                    return line != ""
                });
                var lines = [];
                while (allTextLines.length > 0)
                    lines.push(processLine(allTextLines.shift()));
                return lines;
            }

            $scope.upload = function() {
                $timeout(function() {
                    $("#input-1a").fileinput({
                        showUpload: false,
                        mainClass: "input-group-sm"
                    });

                    $('#input-1a').on('fileloaded', function(event, file, previewId, index, reader) {
                        $scope.data.file = file;
                        $scope.$apply();
                    });

                    $('#input-1a').on('fileclear', function(event, file, previewId, index, reader) {
                        $scope.data.file = null;
                        $scope.controls.analysis = false;
                        $scope.$apply();
                    });
                }, 1)
            }

            $scope.preAnalyze = function() {
                if ($scope.data.file.size > 25000) {
                    growl.warning("File is too large for pre-validation.", {
                        title: 'Error'
                    });
                    return;
                }
                $scope.controls.analysis = true;
                var reader = new FileReader();
                reader.onload = function(e) {
                    var data = processData(e.target.result);
                    data.shift()
                    $scope.data.filePreview = data;
                    $scope.validateData();
                };
                reader.readAsText($scope.data.file);
            }

            $scope.validateData = function() {
                $scope.data.errors = [];
                $scope.data.filePreview.forEach(function(row, idx) {
                    if (!row.length) return;
                    var errors = [];
                    for (var i = 0; i < 14; i++) {
                        switch (i) {
                            case 0:
                                if (!$scope.validateDateFormat(row[i]))
                                    errors.push(i);
                                break;
                            case 1:
                                if (!$scope.validateTimeFormat(row[i]))
                                    errors.push(i);
                                break;
                            case 2:
                                if (!$scope.validateShift(row[i]))
                                    errors.push(i);
                                break;
                            case 3:
                            case 4:
                            case 6:
                            case 7:
                            case 8:
                            case 9:
                            case 12:
                            case 13:
                                if (row[i] == undefined || !$scope.validateString(row[i]))
                                    errors.push(i);
                                break;
                            case 5:
                                if (!row[i] || !$scope.validateString(row[i]))
                                    errors.push(i);
                                break;
                            case 10:
                            case 11:
                                if (row[i] == undefined || (row[i] && !$scope.validateNumber(row[i])))
                                    errors.push(i);
                                break;
                            default:
                                break;
                        }
                    }
                    row.errors = errors;
                })
            }

            $scope.save = function() {
                if (!$scope.data.newEntry.uuid) {
                    DeviationEntryService.addDeviationEntries([$scope.data.newEntry]).then(function(entries) {
                        if (entries.length == 0) {
                            growl.error("Entry was not saved. Please validate the format of each entry and " +
                                "create matching Work Centers before uploading data. Please also make sure you have " +
                                "permission to upload data for the specified Work Center", {
                                title: 'Warning'
                            });
                            $scope.edit($scope.data.newEntry);
                        }
                        else {
                            growl.info("Entry added successfully.", {
                                title: 'Info'
                            });
                            $scope.refresh();
                        }
                    });
                } else {
                    DeviationEntryService.updateDeviationEntry($scope.data.newEntry.uuid, $scope.data.newEntry).then(function(entry) {
                        $scope.buttons.isCollapsed = false;
                    }, function(err) {
                        $scope.buttons.isCollapsed = true;
                        $scope.data.newEntry = angular.copy($scope.data.backUp);
                    });
                }
            }

            $scope.edit = function(entry) {
                $scope.data.backUp = angular.copy(entry);
                $scope.buttons.isCollapsed = true;
                $scope.data.newEntry = entry;
            }

            $scope.remove = function() {
                var entries = $.grep($scope.data.displayedDeviationEntries, function(entry) {
                    return entry.isSelected;
                })
                entries.forEach(function(entry) {
                    DeviationEntryService.deleteDeviationEntry(entry.uuid).then(function() {
                        $scope.getDeviationEntries($scope.tableState);
                    });
                });
            }

            $scope.submitFile = function() {
                var reader = new FileReader();
                reader.onload = function(e) {
                    var data = processData(e.target.result);
                    var numSubmitted = data.length - 1;
                    var fd = new FormData();
                    fd.append('file', $scope.data.file);

                    $http.post(APP_CONSTANTS.API_PATH + "/api/v2/deviationentries/source", fd, {
                        transformRequest: angular.identity,
                        headers: {
                            'Content-Type': undefined
                        }
                    }).then(function(result) {
                        $scope.buttons.upload = false;
                        $scope.refresh();
                        growl.info(result.data.length + " entries have been added.", {
                            title: 'Info'
                        });
                        if (result.data.length < numSubmitted) {
                            growl.warning(numSubmitted - result.data.length + " entries have not been saved. Please " +
                                "validate the format of each entry and create matching Work Centers before uploading " +
                                "data. Please also make sure you have permission to upload data for each Work Center", {
                                title: 'Warning'
                            });
                        }
                    }, function(err) {
                        growl.error("There has been an error processing your request.", {
                            title: 'Error'
                        });
                    }, function(err) {
                        $scope.controls.initFlag = false;
                    });
                };
                reader.readAsText($scope.data.file);
            }

            $scope.refresh = function() {
                $scope.getDeviationEntries($scope.tableState);
            }

            $scope.$watch('filter', function() {
                if (!$scope.tableState) return;
                $scope.getDeviationEntries($scope.tableState);
            }, true);

            $scope.getDeviationEntries = function(tableState) {
                var options = $scope.getQueryOptions(tableState);
                if (options.shifts.length == 0 || options.workCenters.length == 0) {
                    $scope.data.displayedDeviationEntries = [];
                    tableState.pagination.numberOfPages = 0;
                    return;
                }
                DeviationEntryService.getDeviationEntries(options).then(function(result) {
                    $scope.data.displayedDeviationEntries = result.content;
                    tableState.pagination.numberOfPages = result.totalPages;
                });

            };
        }
    ]);


/*******************************************************************************
 * Copyright (c) 2015 Bosch Software Innovations GmbH. All rights reserved.
 ******************************************************************************/

'use strict';

var multimetric = angular.module('improvidus-multimetric', []);

multimetric
    .controller(
        'MultimetricCtrl',
        [
            '$rootScope',
            '$scope',
            '$http',
            '$controller',
            '$location',
            '$state',
            '$stateParams',
            'DatasourceService',
            'FIELD',
            'usSpinnerService',
            'DisplayService',
            function($rootScope, $scope, $http, $controller, $location, $state, $stateParams, DatasourceService, FIELD,
                     usSpinnerService, DisplayService) {

                angular.extend(this, $controller('FiltersCtrl', {
                    $scope: $scope
                }));

                $scope.initAnalysis('mmparetos');

                $scope.data.search = {
                    uuid: $location.$$search.uuid,
                    type: $location.$$search.type,
                    version: $location.$$search.version
                }

                setTimeout(function() {
                    if ($location.$$search.uuid)
                        $location.search("uuid", $scope.data.search.uuid);
                }, 1);


                function compare(a, b) {
                    if (a.deviation < b.deviation)
                        return -1;
                    if (a.deviation > b.deviation)
                        return 1;
                    return 0;
                }


                $scope.paintGraph = function() {
                    var lines = $scope.dateFilteredLines;

                    if (!lines) return;

                    var words = [];

                    var field = $scope.chartOptions.xAxis === 'Deviation'
                        ? ($scope.data.xAxisLevel || FIELD.Deviation1Level) : $scope.chartOptions.xAxis;
                    for (var i = 0; i < lines.length; i++) {
                        var lineSize = lines[i].length - 1;
                        words.push({
                            deviation: lines[i][field === FIELD.Comment ? lines[i].length - 1 : field],
                            color: lines[i][$scope.chartOptions.colorParameter],
                            value: parseFloat(lines[i][$scope.chartOptions.yAxis]) || 1
                        })
                    }

                    var wordsMap = {};

                    for (var i = 0; i < words.length; i++) {
                        if (!words[i].deviation) continue;
                        if (!wordsMap[words[i].deviation]) {
                            wordsMap[words[i].deviation] = {}
                            if (!words[i].color) continue;
                            if (!wordsMap[words[i].deviation][words[i].color]) {
                                wordsMap[words[i].deviation][words[i].color] = 0;
                            }
                            wordsMap[words[i].deviation][words[i].color] += words[i].value;
                        } else {
                            if (!words[i].color) continue;
                            if (!wordsMap[words[i].deviation][words[i].color]) {
                                wordsMap[words[i].deviation][words[i].color] = 0;
                            }
                            wordsMap[words[i].deviation][words[i].color] += words[i].value;
                        }
                    }

                    var dataProvider = [];
                    var graphs = [{
                        title: "All",
                        id: "all",
                        legendValueText: " ",
                        legendPeriodValueText: " "
                    }];

                    var series = {};

                    for (var category in wordsMap) {
                        var point = angular.copy(wordsMap[category]);
                        point.deviation = category;
                        dataProvider.push(point);
                        for (var serie in wordsMap[category]) {
                            if (!series[serie]) {
                                series[serie] = true
                                graphs
                                    .push({
                                        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
                                        "fillAlphas": 0.8,
                                        "lineAlpha": 0.3,
                                        "title": serie,
                                        "type": "column",
                                        "valueField": serie
                                    });
                            }
                        }
                    }

                    dataProvider.sort(compare);

                    AmCharts.exportCFG.menu[0].menu.splice(3, 1);

                    var chart = AmCharts.makeChart("chartdiv", {
                        "type": "serial",
                        "theme": "light",
                        "dataProvider": dataProvider,
                        "valueAxes": [{
                            "title": $scope.controls.yAxis,
                            "stackType": "regular",
                            "axisAlpha": 0.3,
                            "gridAlpha": 0
                        }],
                        "graphs": graphs,
                        "categoryField": "deviation",
                        "categoryAxis": {
                            "gridPosition": "start",
                            "gridAlpha": 0,
                            "tickPosition": "start",
                            "tickLength": 20,
                            "position": "left",
                            "labelRotation": 0,
                            "minHorizontalGap": 0,
                            "ignoreAxisWidth": true,
                            "autoWrap": true
                        },
                        "numberFormatter": {
                            "precision": 2,
                            "decimalSeparator": ",",
                            "thousandsSeparator": "."
                        },
                        "marginBottom": 75,
                        "startDuration": 0,
                        "export": AmCharts.exportCFG,
                        "colors": ["#1F77B4", "#AEC7E8", "#FF7F0E", "#FFBB78", "#2CA02C", "#98DF8A",
                            "#D62728", "#FF9896", "#9467BD", "#C5B0D5", "#8C564B", "#C49C94", "#E377C2",
                            "#F7B6D2", "#7F7F7F", "#C7C7C7", "#BCBD22", "#DBDB8D", "#17BECF", "#9EDAE5",
                            "#1F77B4", "#AEC7E8", "#FF7F0E", "#FFBB78", "#2CA02C", "#98DF8A", "#D62728",
                            "#FF9896", "#9467BD", "#C5B0D5", "#8C564B", "#C49C94", "#E377C2", "#F7B6D2",
                            "#7F7F7F", "#C7C7C7", "#BCBD22", "#DBDB8D", "#17BECF", "#9EDAE5", "#1F77B4",
                            "#AEC7E8", "#FF7F0E", "#FFBB78", "#2CA02C", "#98DF8A", "#D62728", "#FF9896",
                            "#9467BD", "#C5B0D5", "#8C564B", "#C49C94", "#E377C2", "#F7B6D2", "#7F7F7F",
                            "#C7C7C7", "#BCBD22", "#DBDB8D", "#17BECF", "#9EDAE5"]

                    });

                    function legendHandler(evt) {
                        var state = evt.dataItem.hidden;
                        if (evt.dataItem.id == 'all') {
                            for (var i1 in evt.chart.graphs) {
                                if (evt.chart.graphs[i1].id != 'all') {
                                    evt.chart[evt.dataItem.hidden ? 'hideGraph' : 'showGraph']
                                    (evt.chart.graphs[i1]);
                                }
                            }
                        }
                    }

                    chart.addListener("dataUpdated", function() {
                        usSpinnerService.stop('spinner-notifications');
                    });

                    var legend = new AmCharts.AmLegend();
                    chart.addLegend(legend, "legend");

                    chart.legend.addListener('hideItem', legendHandler);
                    chart.legend.addListener('showItem', legendHandler);

                    chart.validateData();

                }

                $scope.name = {
                    display: "N/A",
                    version: 99
                }
                $scope.name = DisplayService.getDisplay();

            }]);
/*******************************************************************************
 * Copyright (c) 2015 Bosch Software Innovations GmbH. All rights reserved.
 ******************************************************************************/

'use strict';

var wordcloud = angular.module('improvidus-wordcloud', []);

wordcloud.controller('WordcloudCtrl', [
    '$rootScope',
    '$scope',
    '$http',
    '$controller',
    '$stateParams',
    'FIELD',
    'CurrentAnalysisGroupService',
    'AnalysisResultService',
    'usSpinnerService',
    'DisplayService',
    'DatasourceService',
    function ($rootScope, $scope, $http, $controller, $stateParams, FIELD, CurrentAnalysisGroupService, AnalysisResultService, usSpinnerService, DisplayService, DatasourceService) {

        angular.extend(this, $controller('FiltersCtrl', {
            $scope: $scope
        }));

        $scope.initAnalysis('wordcloud');

        var fill = d3.scale.category20b();

        var w = window.innerWidth, h = window.innerHeight;

        var max, fontSize, scale;

        var layout = d3.layout.cloud().timeInterval(Infinity).size([w, h]).fontSize(function (d) {
            return fontSize(+d.value);
        }).text(function (d) {
            return d.key;
        }).on("end", draw);

        function getOffset(el) {
            var _x = 0;
            var _y = 0;
            while (el && !isNaN(el.offsetLeft) && !isNaN(el.offsetTop)) {
                _x += el.offsetLeft - el.scrollLeft;
                _y += el.offsetTop - el.scrollTop;
                el = el.offsetParent;
            }
            return {
                top: _y,
                left: _x
            };
        }

        var svg = d3.select("#wordcloud").append("svg").attr("width", w).attr("height", h).attr(
            "height", h);

        var vis = svg.append("g").attr("transform", "translate(" + [w >> 1, h >> 1] + ")").attr(
            "height", h);

        window.onresize = function (event) {
            update();
        };

        function draw(data, bounds) {

            var min, max;

            // data.forEach(function(line) {
            // if (line.Cor) {
            // if (!min)
            // min = line.Cor;
            // else
            // min = line.Cor < min ? line.Cor : min;
            // if (!max)
            // max = line.Cor;
            // else
            // max = line.Cor > max ? line.Cor : max;
            // }
            // })
            //
            // if (min === max) {
            // growl.error("Correlations could not be calculated. Select a different
            // Datasource", {
            // title: "Invalid Data"
            // });
            // return;
            // }
            //
            // var rainbow = new Rainbow();
            // rainbow.setNumberRange(min, max);
            // rainbow.setSpectrum("#539A85", "#DCCF47", "#C46F50");

            var w = $("#wordcloud").width(), h = $("#wordcloud").height();

            svg.attr("width", w).attr("height", h);

            scale = bounds ? Math.min(w / Math.abs(bounds[1].x - w / 2), w
                    / Math.abs(bounds[0].x - w / 2), h / Math.abs(bounds[1].y - h / 2), h
                    / Math.abs(bounds[0].y - h / 2)) / 1.5 : 1;

            var text = vis.selectAll("text").data(data, function (d) {
                return d.text.toLowerCase();
            });
            text.transition().duration(1000).attr("transform", function (d) {
                return "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")";
            }).style("font-size", function (d) {
                return d.size + "px";
            });
            text.enter().append("text").attr("text-anchor", "middle").attr("transform", function (d) {
                return "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")";
            }).style("font-size", function (d) {
                return d.size + "px";
            }).style("opacity", 1).transition().duration(1000).style("opacity", 1);
            text.style("font-family", function (d) {
                return d.font;
            }).style("fill", function (d) {
                return fill(d.text.toLowerCase());
            }).text(function (d) {
                return d.text;
            });

            vis.transition().attr("transform",
                "translate(" + [w >> 1, h >> 1] + ")scale(" + scale + ")");
        }

        function update() {
            d3.select("#wordcloud").selectAll("svg").remove();
            svg = d3.select("#wordcloud").append("svg").attr("width", w).attr("height", h).attr(
                "height", h);
            vis = svg.append("g").attr("transform", "translate(" + [w >> 1, h >> 1] + ")").attr(
                "height", h);
            layout = d3.layout.cloud().timeInterval(Infinity).size([w, h]).fontSize(function (d) {
                return fontSize(+d.value);
            }).text(function (d) {
                return d.key;
            }).on("end", draw);
            layout.font('impact').spiral('archimedean').rotate(function () {
                return 0;
            }).random(function () {
                return 0.5;
            });
            var tags = $scope.words;
            fontSize = d3.scale['sqrt']().range([10, 100]);
            if (tags.length) {
                fontSize.domain([+tags[tags.length - 1].value || 1, +tags[0].value]);
            }

            tags = tags.splice(0, 100);
            layout.stop().words(tags).start();

            usSpinnerService.stop('spinner-notifications');
        }

        function draw2(words) {
            update();
            d3.select("#wordcloud").append("svg").attr("width", width).attr("height", height).append(
                "g").selectAll("text").data(words).enter().append("text").style("font-size",
                function (d) {
                    return d.size + "px";
                }).style("font-family", "Impact").style("fill", function (d, i) {
                return fill(i);
            }).attr("text-anchor", "middle").attr("transform", function (d) {
                return "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")";
            }).text(function (d) {
                return d.text;
            });
        }

        $scope.paintGraph = function () {
            var lines = $scope.dateFilteredLines;

            var words = {};

            for (var i = 1; i < lines.length; i++) {
                var lineSize = lines[i].length - 1;
                if (lines[i][lineSize] && lines[i][lineSize] !== "") {
                    if (!words[lines[i][lineSize]]) {
                        words[lines[i][lineSize]] = {
                            count: 0,
                            value: 0,
                            duration: 0
                        }
                    }
                    if ($scope.controls.yAxis === 'Count')
                        words[lines[i][lineSize]].value++;
                    else if ($scope.controls.yAxis === 'Duration (h)')
                        words[lines[i][lineSize]].value += parseFloat(lines[i][FIELD.Duration]);
                    else
                        words[lines[i][lineSize]].value += parseFloat(lines[i][FIELD.Amount]);
                }
            }

            delete words["null"];

            var wordsList = [];
            var channel = "value"

            var min, max;

            for (var key in words) {
                var word = words[key];
                if (!min || word[channel] < min) {
                    min = word[channel];
                }
                if (!max || word[channel] > max) {
                    max = word[channel];
                }
            }

            for (var key in words) {
                wordsList.push({
                    key: key,
                    value: 10 + 150 * (words[key][channel] / max)
                });
            }

            var xScale = d3.scale.linear().domain([0, d3.max(wordsList, function (d) {
                return d.value;
            })]).range([10, 100]);

            wordsList.sort(function (a, b) {
                return (b.value - a.value);
            });

            $scope.words = wordsList;
            update();

        }

        $scope.name = {
            display: "N/A",
            version: 99
        }
        $scope.name = DisplayService.getDisplay();

    }]);

/*******************************************************************************
 * Copyright (c) 2015 Bosch Software Innovations GmbH. All rights reserved.
 ******************************************************************************/

'use strict';

var correlations = angular.module('improvidus-correlations', []);

correlations.controller('CorrelationsCtrl', [
    '$rootScope',
    '$scope',
    '$http',
    '$location',
    '$timeout',
    '$stateParams',
    'growl',
    'AnalysisResultService',
    'CurrentAnalysisGroupService',
    'DatasourceService',
    'DisplayService',
    function ($rootScope, $scope, $http, $location, $timeout, $stateParams, growl, AnalysisResultService, CurrentAnalysisGroupService, DatasourceService, DisplayService) {

        $scope.name = {};

        $scope.data = {};

        $scope.filters = {
            enableAll: true
        }

        $scope.$watch('filters.enableAll', function (newValue) {
            if ($scope.datasource) {
                $scope.datasource.forEach(function (event) {
                    event.selected = newValue;
                })
            }
        })

        $scope.flag = true;

        $scope.init = function (lines) {

            $scope.lines = lines;

            var map = {}
            var datasource = [];

            lines.forEach(function (line) {
                if (!map[line.Event1]) {
                    map[line.Event1] = 1;
                } else {
                    map[line.Event1] = map[line.Event1] + 1
                }
            })

            var length;
            var allTheSame = true;
            for (var i in map) {
                if (!length)
                    length = map[i];
                else {
                    if (map[i] !== length) {
                        allTheSame = false;
                        break;
                    }
                }
            }

            var datasource = [];
            var currentEvent;
            var dataPoint;
            for (var i = 0; i < lines.length; i++) {
                if (lines[i].Event1 !== currentEvent) {
                    if (currentEvent) {
                        dataPoint.selected = true;
                        datasource.push(dataPoint);
                    }
                    currentEvent = lines[i].Event1;
                    dataPoint = {
                        Event1: currentEvent
                    }
                }
                dataPoint['Event2-' + i % length] = lines[i].Event2;
                dataPoint['Cor-' + i % length] = lines[i].Cor;
                dataPoint['Lag-' + i % length] = lines[i].Lag;
            }
            dataPoint.selected = true;
            datasource.push(dataPoint);

            $scope.datasource = datasource;

            var min, max;

            lines.forEach(function (line) {
                if (line.Cor) {
                    if (!min)
                        min = line.Cor;
                    else
                        min = line.Cor < min ? line.Cor : min;
                    if (!max)
                        max = line.Cor;
                    else
                        max = line.Cor > max ? line.Cor : max;
                }
            })
            
            if (min === max) {
                growl.error("Correlations could not be calculated. Select a different Datasource", {
                    title: "Invalid Data"
                });
                return;
            }

            var colors = ["#FFFFFF", "#E7E7E7", "#CBCBCB", "#7070E4", "#0505FD"];

            var rainbow = new Rainbow();
            rainbow.setNumberRange(min, max);
            rainbow.setSpectrum("#FFFFFF", "#E7E7E7", "#CBCBCB", "#7070E4", "#0505FD");

            // now let's populate the source data with the colors based on the value
            // as well as replace the original value with 1
            for (i in datasource) {
                for (var h = 0; h <= length; h++) {
                    if (datasource[i]['Cor-' + h]) {
                        datasource[i]['Color-' + h] = '#' + rainbow.colourAt(datasource[i]['Cor-' + h]);
                        datasource[i]['Value-' + h] = datasource[i]['Event2-' + h];
                        datasource[i]['Event2-' + h] = 1;
                    }
                }
            }

            // define graph objects for each hour
            var graphs = [];
            for (var h = 0; h <= length; h++) {
                graphs.push({
                    "balloonFunction": function (item, graph) {
                        var result = graph.balloonText;
                        result = result.replace("[[Event1]]", item.category);
                        result = result.replace("[[Event2]]", item.dataContext[graph.eventField]);
                        result = result.replace("[[Cor]]", item.dataContext[graph.corField]);
                        result = result.replace("[[Lag]]", item.dataContext[graph.lagField]);
                        return result;
                    },
                    "balloonText": "<div style=\"text-align:left; font-size:13px\">"
                    + "Event1: <strong>[[Event1]]</strong><br />"
                    + "Event2: <strong>[[Event2]]</strong><br />"
                    + "Max Correlation (r): <strong>[[Cor]]</strong><br />"
                    + "Lag @ Max Correlation: <strong>[[Lag]] days</strong></div>",
                    "fillAlphas": 1,
                    "lineAlpha": 0,
                    "type": "column",
                    "colorField": "Color-" + h,
                    "valueField": "Event2-" + h,
                    "eventField": "Value-" + h,
                    "lagField": "Lag-" + h,
                    "corField": "Cor-" + h
                });
            }

            var chart = AmCharts.makeChart("chartdiv", {
                "type": "serial",
                "dataProvider": datasource,
                "valueAxes": [{
                    "stackType": "regular",
                    "axisAlpha": 0,
                    "gridAlpha": 0,
                    "autoGridCount": false,
                    "gridCount": length,
                    "labelsEnabled": false
                }],
                "graphs": graphs,
                "columnWidth": 1,
                "categoryField": "Event1",
                "categoryAxis": {
                    "gridPosition": "start",
                    "axisAlpha": 0,
                    "gridAlpha": 0,
                    "position": "left",
                    "labelsEnabled": false
                },
                "titles": [{
                    "size": 15,
                    "text": "Event Correlations",
                    "bold": false
                }, {
                    "text": "Positive lag means that Event1 happens after Event2, and vice versa"
                }]
            });

            chart.customLegend = document.getElementById('chartdiv');

            var width = 200, height = 40;

            d3.select('.heatmap-legend div').selectAll("svg").remove();
            var svg = d3.select('.heatmap-legend div').append('svg').attr('width', width).attr(
                'height', height);

            var title = svg.append('g').append('text').text("Max Correlations");

            var grad = svg.append('defs').append('linearGradient').attr('id', 'grad').attr('x1', '0%')
                .attr('x2', '100%').attr('y1', '0%').attr('y2', '0%');

            grad.selectAll('stop').data(colors).enter().append('stop').attr('offset', function (d, i) {
                return (i / colors.length) * 100 + '%';
            }).style('stop-color', function (d) {
                return d;
            }).style('stop-opacity', 0.9);

            svg.append('rect').attr('x', 0).attr('y', 0).attr('width', width)
                .attr('height', height / 2).attr('fill', 'url(#grad)');

            var g = svg.append('g').selectAll('.label').data(colors).enter();

            g.append('text').text(function (d, i) {
                if (i == 0) {
                    return min;
                } else if (i == colors.length - 1) {
                    return max;
                }
                return '';
            }).attr('transform', function (d, i) {
                return 'translate(' + (xPos(i) + 2) + ',' + ((height) - 7) + ')';
            })

            function xPos(i) {
                return (width / colors.length) * i;
            }

        }

        $scope.name = {
            display: "N/A",
            version: 99
        }
        $scope.name = DisplayService.getDisplay();

        $scope.initAnalysis = function () {
            $scope.data.analysisGroup = CurrentAnalysisGroupService.getCurrentAnalysisGroup();
            $scope.data.analysisType = 'correlations';

            if ($scope.data.analysisGroup == null) {
                AnalysisResultService.getAnalysisGroup($stateParams.uuid).then(function (analysisGroup) {
                    CurrentAnalysisGroupService.setCurrentAnalysisGroup(analysisGroup);
                    $scope.data.analysisGroup = analysisGroup;
                })
            }

            DatasourceService.getDatasourceMedia($stateParams.uuid, {
                type: 'correlations',
                version: $stateParams.version
            }).then(function (result) {
                $scope.init(result);
            });
        }
        $scope.initAnalysis('correlations');


        $scope.selectAnalysis = function (route, type) {
            var version = $location.$$search.version;
            var uuid = $location.$$search.uuid;
            $location.path(route).search("uuid", uuid).search("type", type).search("version", version);
        }

    }]);

/*******************************************************************************
 * Copyright (c) 2015 Bosch Software Innovations GmbH. All rights reserved.
 ******************************************************************************/

'use strict';

var recommendations = angular.module('improvidus-recommendations', []);

recommendations.controller('RecommendationsCtrl', [
    '$rootScope',
    '$scope',
    '$http',
    '$location',
    '$timeout',
    '$stateParams',
    'growl',
    'AnalysisResultService',
    'CurrentAnalysisGroupService',
    'DatasourceService',
    'usSpinnerService',
    'DisplayService',
    function($rootScope, $scope, $http, $location, $timeout, $stateParams, growl, AnalysisResultService, CurrentAnalysisGroupService, DatasourceService, usSpinnerService, DisplayService) {

        $scope.name = {};

        $scope.data = {};

        $scope.filters = {
            enableAll: true
        }

        var colors = ["#1F77B4", "#AEC7E8", "#FF7F0E", "#FFBB78", "#2CA02C", "#98DF8A", "#D62728",
            "#FF9896", "#9467BD", "#C5B0D5", "#8C564B", "#C49C94", "#E377C2", "#F7B6D2", "#7F7F7F",
            "#C7C7C7", "#BCBD22", "#DBDB8D", "#17BECF", "#9EDAE5"]

        $scope.$watch('filters.enableAll', function(newValue) {
            if ($scope.datasource) {
                $scope.datasource.forEach(function(event) {
                    event.selected = newValue;
                })
            }
        })

        $scope.flag = true;
        // $scope.$on('datasource.selected', function(event, datasource) {
        //   if ($scope.flag) {
        //     $scope.load(datasource);
        //     $scope.flag = false;
        //   } else {
        //     $scope.flag = true;
        //   }
        // })

        // $scope.load = function(datasource) {
        //   usSpinnerService.spin('spinner-notifications');
        //   DatasourceService.getDatasourceMedia(datasource.uuid, {
        //     type: "recommendations"
        //   }).then(function(data) {
        //     $scope.init(data);
        //   }, function(err) {
        //     usSpinnerService.stop('spinner-notifications');
        //   })
        // }

        // if ($rootScope.selections.datasource.uuid)
        //   $scope.load($rootScope.selections.datasource);
        // else
        //   $location.path('/datasources');

        $scope.options = {
            rank: 1
        }

        $scope.init = function(lines) {
            $("input[name='rank']").TouchSpin({
                min: 1,
                step: 1,
                max: lines[lines.length - 1].Rank
            });
            $scope.lines = lines;
            $scope.loadGraph();
        }

        $scope.$watch('options.rank', function(newValue) {
            if (!newValue || !$scope.lines) return;
            $scope.loadGraph();
        })

        AmCharts.exportCFG.menu[0].menu.splice(3, 1);

        $scope.loadGraph = function() {

            var datasource = $.grep($scope.lines, function(line) {
                return line.Rank === parseInt($scope.options.rank);
            });

            var chart = AmCharts.makeChart("chartdiv", {
                "type": "serial",
                "theme": "light",
                "dataProvider": datasource,
                "valueAxes": [{
                    "gridColor": "#FFFFFF",
                    "gridAlpha": 0.2,
                    "dashLength": 0,
                    "title": "Amount $",
                }],
                "gridAboveGraphs": true,
                "graphs": [{
                    "balloonText": datasource[0].X_Axis_Label + ": <b>[[category]]</b>"
                    + "<br/>Amount $: <b>[[value]]</b>",
                    "fillAlphas": 0.8,
                    "lineAlpha": 0.2,
                    "type": "column",
                    "fillColors": colors[($scope.options.rank - 1) % colors.length],
                    "valueField": "Frequency"
                }],
                "chartCursor": {
                    "categoryBalloonEnabled": false,
                    "cursorAlpha": 0,
                    "zoomable": false
                },
                "categoryField": "Category",
                "categoryAxis": {
                    "gridPosition": "start",
                    "gridAlpha": 0,
                    "tickPosition": "start",
                    "tickLength": 20,
                    "position": "left",
                    "labelRotation": 0,
                    "minHorizontalGap": 10,
                    "ignoreAxisWidth": true,
                    "autoWrap": true
                },
                "marginBottom": 75,
                "startDuration": 0,
                "titles": [
                    {
                        "text": "Pareto Rank: " + $scope.options.rank,
                        "size": 14
                    },
                    {
                        "text": "Filters: " + datasource[0].Filters,
                        "size": 14
                    },
                    {
                        "text": "% of Categories that cover 80% of the Data: "
                        + datasource[0].Score.toFixed(2),
                        "size": 14
                    }, {
                        "text": datasource[0].X_Axis_Label,
                        "size": 14,
                        "bold": false
                    }],
                "export": AmCharts.exportCFG

            });

            chart.addListener("dataUpdated", function() {
                usSpinnerService.stop('spinner-notifications');
            });

        }

        $scope.name = {
            display: "N/A",
            version: 99
        }
        $scope.name = DisplayService.getDisplay();

//      $scope.initAnalysis = function() {
//        if ($location.$$search.uuid && $location.$$search.type) {
//          DatasourceService.getDatasourceMedia($location.$$search.uuid, {
//            type: $location.$$search.type,
//            version: $location.$$search.version
//          }).then(function (result) {
//            $scope.init(result);
//          });
//          AnalysisResultService.getAnalysisResult($location.$$search.uuid).then(function(analysis) {
//        	$scope.name.display = analysis.displayName;
//          	$scope.name.version = $location.$$search.version;
//            DisplayService.saveDisplay($scope.name);
//            $scope.name.display = analysis.results[0].displayName;
//            $scope.name.version = $location.$$search.version;
//          })
//        }
//      }
//
//      $timeout($scope.initAnalysis, 100);

        $scope.initAnalysis = function() {
            $scope.data.analysisGroup = CurrentAnalysisGroupService.getCurrentAnalysisGroup();
            $scope.data.analysisType = 'recommendations';

            if ($scope.data.analysisGroup == null) {
                AnalysisResultService.getAnalysisGroup($stateParams.uuid).then(function(analysisGroup) {
                    CurrentAnalysisGroupService.setCurrentAnalysisGroup(analysisGroup);
                    $scope.data.analysisGroup = analysisGroup;
                })
            }

            DatasourceService.getDatasourceMedia($stateParams.uuid, {
                type: 'recommendations',
                version: $stateParams.version
            }).then(function(result) {
                $scope.init(result);
            });
        }
        $scope.initAnalysis();

        $scope.selectAnalysis = function(route, type) {
            var version = $location.$$search.version;
            var uuid = $location.$$search.uuid;
            $location.path(route).search("uuid", uuid).search("type", type).search("version", version);
        }

    }]);

var administration = angular.module('com.bosch.administration', ['ui.router']);
administration.controller("adminBaseCtrl",
    ["$scope", "$filter", "AdminPlantsService", "AdminCostCenterService", "AdminSchedulesService",
        function($scope, $filter, AdminPlantsService, AdminCostCenterService, AdminSchedulesService) {

            $scope.selectAll = function(value) {
                $scope.data.displayedUsers.forEach(function(user) {
                    if (user) user.selected = value;
                })
            }

            var manualSchedule = {
                displayName: "**Manual Schedule**",
                uuid: null
            };

            $scope.sortByDisplayName = function(a, b) {
                var nameA = a.displayName.toUpperCase();
                var nameB = b.displayName.toUpperCase();
                if (nameA < nameB) {
                    return -1;
                }
                if (nameA > nameB) {
                    return 1;
                }

                return 0;
            }

            $scope.loadScheduleTemplates = function() {
                AdminSchedulesService.getSchedules().then(function(schedules) {
                    $scope.data.scheduleTemplates = schedules.plain().sort($scope.sortByDisplayName);
                    $scope.data.scheduleTemplates.unshift(angular.copy(manualSchedule));
                })
            };

            $scope.getUsers = function callServer(tableState) {

                if (!tableState) return;
                if (!$scope.data.tableState && tableState) $scope.data.tableState = tableState;

                var pagination = tableState.pagination;

                var start = pagination.start || 0;     // This is NOT the page number, but the index of item in the list that you want to use to display the table.
                var number = pagination.number || 10;  // Number of entries showed per page.

                var searchedUsers = $filter('filter')($scope.data.users,
                    tableState.search.predicateObject);

                var orderedUsers = $filter('orderBy')(searchedUsers, 'selected', true);

                var filteredUsers = $.grep(orderedUsers, function(user) {
                    return !$scope.data.onlyMembers ||
                        ($scope.data.onlyMembers && user.selected == true);
                })


                $scope.data.displayedUsers = filteredUsers.slice(start, start + number);
                tableState.pagination.numberOfPages = Math.ceil(filteredUsers.length / number);
            }

            $scope.$watch('data.onlyMembers', function(newObj) {
                if (newObj == undefined) return;
                $scope.getUsers($scope.data.tableState);
            });

            //JS Tree Config

            $scope.typesConfig = {
                "plant": {
                    "icon": "fa fa-industry text-primary fa-lg"
                },
                "costCenter": {
                    "icon": "fa fa-usd text-primary fa-lg"
                },
                "workCenter": {
                    "icon": "fa fa-gear text-primary fa-lg"
                }
            };

            $scope.dndConfig = {
                "is_draggable": function(node) {
                    return true;
                }
            };

            $scope.sortConfig = {
                sort: function(a, b) {
                    var nodeA = this.get_node(a);
                    var nodeB = this.get_node(b);
                    var aType = nodeA.original.className;
                    var bType = nodeB.original.className;
                    var aName = nodeA.original.displayName.toLowerCase();
                    var bName = nodeB.original.displayName.toLowerCase();
                    if (aType == bType) {
                        return (aName < bName) ? -1 : (aName > bName) ? 1 : 0;
                    } else {
                        return (aType < bType) ? 1 : -1;
                    }
                }
            };

            $scope.treeCore = {
                "check_callback": function(operation, node, node_parent, node_position, more) {
                    if (operation === 'move_node') {
                        if ((node.type == "costCenter" && node_parent.type == "plant") ||
                            (node.type == "workCenter" && node_parent.type == "costCenter"))
                            return true;
                        return false;
                    }
                }
            };

            $scope.moveNodeCB = function(e, data) {
                if (data.parent !== data.old_parent) {
                    if (data.node.type == "costCenter") {
                        var oldPlantNode = $scope.treeInstance.jstree('get_node', data.old_parent);
                        var newPlantNode = $scope.treeInstance.jstree('get_node', data.parent);
                        oldPlantNode.original.costCenters = oldPlantNode.original.costCenters.filter(function(costCenter) {
                            return costCenter.uuid !== data.node.id;
                        });
                        newPlantNode.original.costCenters.push(data.node.original);

                        AdminPlantsService.updatePlant(oldPlantNode.id, oldPlantNode.original).then(function() {
                            AdminPlantsService.updatePlant(newPlantNode.id, newPlantNode.original).then(function() {
                                growl.info("Successfully moved " + data.node.text + " to " + newPlantNode.text, {
                                    title: 'Info'
                                });
                            })
                        })
                    }
                    else {
                        var oldCostCenterNode = $scope.treeInstance.jstree('get_node', data.old_parent);
                        var newCostCenterNode = $scope.treeInstance.jstree('get_node', data.parent);
                        oldCostCenterNode.original.workCenters = oldCostCenterNode.original.workCenters.filter(function(workCenter) {
                            return workCenter.uuid !== data.node.id;
                        });
                        newCostCenterNode.original.workCenters.push(data.node.original);

                        AdminCostCenterService.updateCostCenter(oldCostCenterNode.parent, oldCostCenterNode.id, oldCostCenterNode.original).then(function() {
                            AdminCostCenterService.updateCostCenter(newCostCenterNode.parent, newCostCenterNode.id, newCostCenterNode.original).then(function() {
                                growl.info("Successfully moved " + data.node.text + " to " + newCostCenterNode.text, {
                                    title: 'Info'
                                });
                            })
                        })

                    }
                }
            };

        }]);
administration.controller("adminOrganizationsCtrl",
    ["$rootScope", "$scope", "PermissionsService", "$modalStack", "$modal", "$log", "AdminOrganizationsService",
        function($rootScope, $scope, PermissionsService, $modalStack, $modal, $log, AdminOrganizationsService) {

            $rootScope.state = 'Organizations';

            $scope.data = {
                organizations: [],
                displayedOrganizations: [],
                selectedOrganizations: []
            }

            $scope.orgForm = {
                "user": {
                    "lastName": "",
                },
                "org": {
                    "address": {},
                    "contacts": [{}]
                },
                step: 1
            }

            $scope.buttons = {
                isCollapsed: false,
                invalid: false,
                submitted: false
            }

            $scope.add = function() {
                $modalStack.dismissAll();
                var opts = {
                    templateUrl: "app/main/administration/organizations/views/add.html",
                    scope: $scope,
                    size: 'md'
                };
                var instance = $modal.open(opts);
            }

            $scope.save = function() {
                $scope.orgForm.user.username = $scope.orgForm.user.email;
                $scope.orgForm.org.name = $scope.orgForm.org.displayName.toUpperCase().split(" ")
                    .join("_");
                $scope.buttons.submitted = true;
                AdminOrganizationsService.addOrganization($scope.orgForm).then(function(org) {
                    $scope.getOrganizations();
                    $modalStack.dismissAll();
                    $scope.buttons.submitted = false;
                }, function(err) {
                    $log.error(err);
                    $scope.buttons.submitted = false;
                })
            }

            $scope.getOrganizations = function() {
                AdminOrganizationsService.getOrganizations({}).then(function(organizations) {
                    $scope.data.organizations = organizations.plain();
                    var organizationUuids = organizations.map(function(org) {
                        return org.uuid;
                    });
                    AdminOrganizationsService.getOrganizationStatistics({
                        organizationUuids: organizationUuids
                    }).then(function(statistics) {
                        if (statistics == null || statistics.length == 0) return;
                        $scope.data.totalStatistics = statistics.pop();
                        $scope.data.orgStatistics = statistics.plain();
                    })
                }, function(err) {
                    $log.error(err);
                })
            }

            $scope.refresh = function() {
                $scope.getOrganizations();
            };

            $scope.getOrganizations();

        }]);
administration.controller("adminOrganizationDetailsCtrl",
    ["$rootScope", "$scope", "$stateParams", "growl", "AdminOrganizationsService",
        function($rootScope, $scope, $stateParams, growl, AdminOrganizationsService) {

            $scope.data = {
                configuration: null,
                copyConfiguration: null,
                organization: null
            };

            $scope.controls = {
                edit: false
            };

            $scope.saveOrganizationConfiguration = function() {
                AdminOrganizationsService.saveOrganizationConfiguration($rootScope.currentUser.organization.uuid, $scope.data.configuration).then(function(configuration) {
                    growl.info("Configuration saved", {
                        title: 'Info'
                    });
                    $rootScope.org.configuration = configuration.plain();
                }, function(error) {
                    growl.error("Configuration failed to update", {
                        title: 'Error'
                    });
                    $scope.data.configuration = $scope.data.copyConfiguration;
                })
            };

            $scope.editConfiguration = function() {
                $scope.data.copyConfiguration = angular.copy($scope.data.configuration)
            };

            $scope.cancel = function() {
                $scope.data.configuration = $scope.data.copyConfiguration;
            };

            function getOrganizationConfiguration(uuid) {
                var defaultConfig = {
                    organizationUuid: uuid,
                    conf: {
                        numShifts: 3
                    }
                };

                if (uuid == $rootScope.currentUser.organization.uuid) {
                    AdminOrganizationsService.getOrganizationConfiguration(uuid).then(function(configuration) {
                        if (configuration != null)
                            $scope.data.configuration = configuration.plain();
                        else
                            $scope.data.configuration = defaultConfig;
                    })
                }
            }

            function getOrganizationStatistics(uuid) {
                AdminOrganizationsService.getOrganizationStatistics({
                    organizationUuids: uuid
                }).then(function(statistics) {
                    if (statistics == null || statistics.length == 0) return;
                    $scope.data.organization.statistics = statistics[0];
                })
            }

            $scope.load = function(uuid) {
                AdminOrganizationsService.getOrganization(uuid).then(function(organization) {
                    if (organization == null) {
                        growl.error("Organization does not exist", {
                            title: 'Error'
                        });
                        $scope.selectState('/admin/organizations');
                        return;
                    }
                    $scope.data.organization = organization.plain();
                    getOrganizationConfiguration(uuid);
                    getOrganizationStatistics(uuid);
                }, function(error) {
                    growl.error("Organization does not exist or User does not have sufficient permissions", {
                        title: 'Error'
                    });
                    $scope.selectState('/admin/organizations');
                });
            };

            if ($stateParams.uuid) {
                $scope.load($stateParams.uuid);
            }
            else {
                $scope.selectState('/admin/organizations/' + $rootScope.currentUser.organization.uuid);
            }
        }]);
administration.controller("adminUsersCtrl",
    ["$rootScope", "$scope", "$state", "PermissionsService", "$modalStack", "$modal", "$log", "growl", "UsersAdministrationService",
        "UserService", "AdminUsersService",
        function($rootScope, $scope, $state, PermissionsService, $modalStack, $modal, $log, growl, UsersAdministrationService,
                 UserService, AdminUsersService) {

            $rootScope.state = 'Users';

            $scope.data = {
                users: [],
                displayedUsers: [],
                selectedUsers: []
            }

            $scope.userForm = {
                role: "Operator",
                step: 1
            }

            $scope.userInviteForm = {
                role: "Operator",
                step: 1
            }

            $scope.buttons = {
                isCollapsed: false,
                invalid: false
            };

            $scope.controls = {
                processing: false
            }

            $scope.add = function() {
                if ($rootScope.currentUser.roles[0].displayName == "SuperAdmin")
                    $scope.userForm.role = "SuperAdmin";

                $modalStack.dismissAll();
                var opts = {
                    templateUrl: "app/main/administration/users/views/add.html",
                    scope: $scope,
                    size: 'md'
                };
                $modal.open(opts);
            };

            $scope.invite = function() {
                if ($rootScope.currentUser.roles[0].displayName == "SuperAdmin")
                    $scope.userInviteForm.role = "SuperAdmin";

                $modalStack.dismissAll();
                var opts = {
                    templateUrl: "app/main/administration/users/views/invite.html",
                    scope: $scope,
                    size: 'md'
                };
                $modal.open(opts);
            };

            $scope.inviteUser = function() {
                $scope.buttons.isCollapsed = false;
                $scope.controls.processing = true;
                AdminUsersService.assignUser($scope.currentUser.organization.uuid, {
                    roleName: $scope.userInviteForm.role,
                    email: $scope.userInviteForm.email
                }).then(function() {
                    growl.info("Invited " + $scope.userInviteForm.email + " to join the Organization", {
                        title: 'Info'
                    });
                    $scope.controls.processing = false;
                    $modalStack.dismissAll();
                    $scope.getUsers();
                }, function() {
                    $scope.controls.processing = false;
                });
            };

            $scope.edit = function(user) {
                $scope.data.updatedUser = angular.copy(user);
                for (var i = 0; i < $scope.data.updatedUser.roles.length; i++) {
                    if ($scope.data.updatedUser.roles[i].organization.uuid == $rootScope.currentUser.organization.uuid) {
                        $scope.data.newRoleName = $scope.data.updatedUser.roles[i].displayName.replace(/\s+/g, '');
                    }
                }
                if ($scope.data.newRoleName == "SuperAdmin")
                    return;
                $modalStack.dismissAll();
                var opts = {
                    templateUrl: "app/main/administration/users/views/edit.html",
                    scope: $scope,
                    size: 'md'
                };
                $modal.open(opts);
            }

            $scope.save = function() {
                $scope.userForm.user.username = $scope.userForm.user.email;
                $scope.buttons.isCollapsed = false;
                $scope.controls.processing = true;
                AdminUsersService.addUser($scope.userForm.user, {
                    roleName: $scope.userForm.role,
                    organizationUuid: $scope.currentUser.organization.uuid
                }).then(function() {
                    growl.info("User created successfully. A registration email has been sent to " + $scope.userForm.user.email, {
                        title: 'Info'
                    });
                    $scope.controls.processing = false;
                    $modalStack.dismissAll();
                    $scope.getUsers();
                }, function() {
                    $scope.controls.processing = false;
                });
            }

            $scope.close = function() {
                $modalStack.dismissAll();
            }

            $scope.updateUserRole = function(user, roleName) {
                user.authorities = null;
                AdminUsersService.assignOrganizationUserRole(user, $rootScope.currentUser.organization.uuid, {
                    roleName: roleName
                }).then(function() {
                    growl.info("User updated successfully", {
                        title: 'Info'
                    });
                    $scope.getUsers();
                }, function(error) {
                    growl.error("Cannot change your own user role", {
                        title: 'Update User Error'
                    });
                });
            }

            $scope.removeUsers = function() {
                $scope.data.selectedUsers.forEach(function(user) {
                    if (user.organizationMemberships.length == 1) {
                        //Delete User
                        AdminUsersService.deleteUser(user.uuid).then(function() {
                            growl.info("User deleted successfully", {
                                title: 'Info'
                            });
                            $scope.getUsers();
                        }, function(error) {
                            growl.error("Cannot delete user that is currently logged on", {
                                title: 'User Removal Error'
                            });
                        });
                    }
                    else {
                        //Remove User from organization
                        AdminUsersService.unassignUser($rootScope.currentUser.organization.uuid, user.uuid).then(function() {
                            growl.info("User removed successfully", {
                                title: 'Info'
                            });
                            if (user.uuid == $rootScope.currentUser.uuid) {
                                UserService.getUser().then(function(user) {
                                    $rootScope.currentUser = user.plain();
                                    $state.reload(true);
                                    $scope.selectState('/');
                                })
                            }
                            else
                                $scope.getUsers();

                        }, function(error) {
                            growl.error("Cannot remove user that is currently logged on", {
                                title: 'User Removal Error'
                            });
                        });
                    }
                });
            };

            // $scope.getUserRole = function(roles) {
            //     var userRole;
            //     roles.forEach(function(role) {
            //         if (role.organization.uuid == $rootScope.currentUser.organization.uuid) {
            //             userRole = role;
            //             return;
            //         }
            //     })
            //     return userRole;
            // };

            $scope.setActiveRoles = function() {
                $scope.data.users.forEach(function(user) {
                    user.roles.forEach(function(role) {
                        if (role.organization.uuid == $rootScope.currentUser.organization.uuid) {
                            user.activeRole = role;
                        }
                    })
                })
            };

            $scope.getUsers = function() {
                UsersAdministrationService.getOrganizationUsers(
                    $scope.currentUser.organization.uuid)
                    .then(function(users) {
                        $scope.data.users = users;
                        $rootScope.currentUser.organization.nusers = users.length;
                        $scope.setActiveRoles();
                    });
            };

            $scope.selectedUsers = function() {
                return $.grep($scope.data.users, function(user) {
                    return user.selected;
                }).length;
            }

            $scope.refresh = function() {
                $scope.getUsers();
            };

            $scope.getUsers();

        }]);
administration.controller("adminSchedulesCtrl",
    ["$rootScope", "$scope", "$state", "$modalStack", "$modal", "$log", "growl", "AdminSchedulesService", "AdminOrganizationsService",
        function($rootScope, $scope, $state, $modalStack, $modal, $log, growl, AdminSchedulesService, AdminOrganizationsService) {

            $rootScope.state = 'Schedules';

            $scope.data = {
                scheduleTemplates: [],
                displayedScheduleTemplates: [],
                selectedScheduleTemplates: []
            };

            var blankShift = [null, null, null, null, null, null, null];
            var blankSchedule = [angular.copy(blankShift), angular.copy(blankShift), angular.copy(blankShift)];

            $scope.scheduleForm = {
                step: 1,
                scheduleTemplate: {
                    name: null,
                    description: null,
                    schedule: blankSchedule
                }
            };

            $scope.buttons = {
                isCollapsed: false,
                invalid: false
            };

            $scope.controls = {
                processing: false
            };

            $scope.insertShift = function() {
                var blankShift = [null, null, null, null, null, null, null];
                $scope.scheduleForm.scheduleTemplate.schedule.push(blankShift);
            };

            $scope.removeShift = function(index) {
                $scope.scheduleForm.scheduleTemplate.schedule.splice(index, 1);
            };

            $scope.addScheduleTemplate = function() {
                $modalStack.dismissAll();
                var opts = {
                    templateUrl: "app/main/administration/schedules/views/add.html",
                    scope: $scope,
                    size: 'lg'
                };
                $modal.open(opts);
            };

            $scope.save = function() {
                AdminSchedulesService.addSchedule($scope.scheduleForm.scheduleTemplate).then(function() {
                    growl.info("Schedule Template created successfully", {
                        title: 'Info'
                    });
                    $modalStack.dismissAll();
                    $scope.refresh();
                }, function(error) {
                    growl.error("Error occurred while saving schedule template.", {
                        title: 'Creation Failed'
                    });
                });
            };

            $scope.close = function() {
                $modalStack.dismissAll();
            };

            $scope.removeSchedules = function() {
                $scope.data.selectedSchedules.forEach(function(schedule) {
                    AdminSchedulesService.deleteSchedule(schedule.uuid).then(function() {
                        growl.info("Schedule deleted successfully", {
                            title: 'Info'
                        });
                        $scope.refresh();
                    }, function(error) {
                        growl.error("Schedule was not removed", {
                            title: 'Schedule Removal Error'
                        });
                    });
                });
            };

            $scope.getSchedules = function() {
                AdminSchedulesService.getSchedules()
                    .then(function(schedules) {
                        $scope.data.scheduleTemplates = schedules.plain();
                    });
            };

            $scope.refresh = function() {
                $scope.getSchedules();
                $scope.scheduleForm = {
                    step: 1,
                    scheduleTemplate: {
                        name: null,
                        description: null,
                        schedule: blankSchedule
                    }
                };
            };

            $scope.getSchedules();

        }]);
administration.controller("adminScheduleDetailsCtrl",
    ["$rootScope", "$scope", "$state", "$modalStack", "$modal", "$log", "$stateParams", "growl", "AdminSchedulesService",
        function($rootScope, $scope, $state, $modalStack, $modal, $log, $stateParams, growl, AdminSchedulesService) {

            $rootScope.state = 'ScheduleDetails';

            $scope.data = {
                scheduleTemplate: null,
                scheduleTemplateCopy: null
            };

            $scope.controls = {
                edit: false
            };

            $scope.close = function() {
                $modalStack.dismissAll();
            };

            $scope.editScheduleTemplate = function() {
                $scope.data.scheduleTemplateCopy = angular.copy($scope.data.scheduleTemplate);
            };

            $scope.cancel = function() {
                $scope.data.scheduleTemplate = angular.copy($scope.data.scheduleTemplateCopy);
            };

            $scope.insertShift = function() {
                var blankShift = [null, null, null, null, null, null, null];
                $scope.data.scheduleTemplate.schedule.push(blankShift);
            };

            $scope.removeShift = function(index) {
                $scope.data.scheduleTemplate.schedule.splice(index, 1);
            };

            $scope.removeScheduleTemplate = function() {
                $modalStack.dismissAll();
                var opts = {
                    templateUrl: "app/main/administration/schedules/views/remove-modal.html",
                    scope: $scope,
                    size: 'md'
                };
                var instance = $modal.open(opts);
            };

            $scope.remove = function(scheduleTemplate) {
                AdminSchedulesService.deleteSchedule(scheduleTemplate.uuid).then(function(response) {
                    growl.info("Successfully deleted schedule template", {
                        title: 'Info'
                    });
                    $scope.selectState('/admin/schedules')
                });
            };

            $scope.saveScheduleTemplate = function(scheduleTemplate) {
                AdminSchedulesService.updateSchedule(scheduleTemplate.uuid, scheduleTemplate)
                    .then(function(scheduleTemplate) {
                        if (!scheduleTemplate) return;
                        growl.info("Updated " + scheduleTemplate.displayName, {
                            title: 'Info'
                        });
                        $scope.data.scheduleTemplate = scheduleTemplate.plain();
                    }, function(error) {
                        growl.error("Error occurred while updating schedule template.", {
                            title: 'Update Failed'
                        });
                    });
            };

            if ($stateParams.scheduleTemplateUuid) {
                AdminSchedulesService.getSchedule($stateParams.scheduleTemplateUuid).then(function(scheduleTemplate) {
                    $scope.data.scheduleTemplate = scheduleTemplate.plain();
                    $rootScope.state = scheduleTemplate.displayName;
                }, function(err) {
                    if (err.status == 403)
                        $location.path("admin/schedules");
                })
            }
            else {
                $state.go("Admin.ScheduleTemplates");
            }

        }]);
administration.controller("adminPlantsCtrl",
    ["$rootScope", "$scope", "$controller", "$modalStack", "$modal", "$log", "growl", "AdminPlantsService",
        "UsersAdministrationService",
        function($rootScope, $scope, $controller, $modalStack, $modal, $log, growl, AdminPlantsService,
                 UsersAdministrationService) {

            $rootScope.state = 'Plants';

            $scope.data = {
                users: [],
                plants: [],
                displayedPlants: [],
                selectedPlants: [],
                tableState: undefined
            };

            angular.extend(this, $controller('adminBaseCtrl', {
                $scope: $scope
            }));

            var resetPlantForm = function() {
                $scope.plantForm = {
                    "users": [],
                    "plant": {},
                    "newPlant": {},
                    step: 1
                };
                $scope.data.selectAll = false;
            };
            resetPlantForm();

            $scope.buttons = {
                isCollapsed: false,
                invalid: false,
                submitted: false
            };

            $scope.add = function() {
                $modalStack.dismissAll();
                var opts = {
                    templateUrl: "app/main/administration/plants/views/add.html",
                    scope: $scope,
                    size: 'lg'
                };
                resetPlantForm();
                $scope.data.users = [];
                $scope.data.displayedUsers = [];
                UsersAdministrationService.getOrganizationUsers(
                    $scope.currentUser.organization.uuid)
                    .then(function(users) {
                        $scope.data.users = users.plain();
                        $scope.data.displayedUsers = users.plain();
                    });
                var instance = $modal.open(opts);
            };

            $scope.save = function() {
                $scope.buttons.submitted = true;
                var plant = {attributes: $scope.plantForm.plant};
                AdminPlantsService.addPlant(plant).then(function(plant) {
                    $log.info(plant);
                    var users = $.grep($scope.data.displayedUsers, function(user) {
                        return user.selected;
                    });
                    $scope.assignUsers(plant, users)
                    $scope.refresh();
                    $scope.buttons.submitted = false;
                    growl.info("Created " + plant.displayName, {
                        title: 'Info'
                    });
                    $modalStack.dismissAll();
                }, function(err) {
                    $scope.buttons.submitted = false;
                    $log.error(err);
                })
            };

            $scope.assignUsers = function(plant, users) {
                users.forEach(function(user) {
                    AdminPlantsService.assignUser(plant.uuid, user.uuid).then(function() {
                        $log.info(
                            'Assigned User ' + user.displayName + ' to Plant ' + plant.displayName);
                    });
                })
            };

            $scope.getPlants = function() {
                AdminPlantsService.getPlants({}).then(function(plants) {
                    $scope.data.plants = plants.plain().sort($scope.sortByDisplayName);
                }, function(err) {
                    $log.error(err);
                })
            };

            $scope.refresh = function() {
                $scope.getPlants();
                //$scope.treeInstance.jstree('refresh');
            };

            $scope.getPlants();

        }]);
administration.controller("adminPlantDetailsCtrl",
    ["$rootScope", "$scope", "$log", "$stateParams", "$filter", "$modalStack", "$modal", "$location",
        "$controller",
        "$state",
        "growl",
        "UsersAdministrationService",
        "AdminPlantsService",
        function($rootScope, $scope, $log, $stateParams, $filter, $modalStack, $modal, $location, $controller,
                 $state, growl, UsersAdministrationService, AdminPlantsService) {

            $scope.data = {
                users: [],
                displayedUsers: [],
                onlyMembers: false,
                updateAll: false,
                tableState: undefined
            }

            angular.extend(this, $controller('adminBaseCtrl', {
                $scope: $scope
            }));

            $scope.updateAll = function() {
                $scope.data.users.forEach(function(user) {
                    if (!user.selected && $scope.data.updateAll == true) {
                        AdminPlantsService.assignUser($scope.data.plant.uuid, user.uuid)
                            .then(function() {
                                user.selected = true;
                                $log.info('Assigned User ' + user.displayName + ' to Plant ' +
                                    $scope.data.plant.displayName);
                            });
                    } else if (user.selected == true && $scope.data.updateAll == false) {
                        AdminPlantsService.unassingUser($scope.data.plant.uuid, user.uuid)
                            .then(function() {
                                user.selected = false;
                                $log.info('Assigned User ' + user.displayName + ' to Plant ' +
                                    $scope.data.plant.displayName);
                            });
                    }
                })
            }

            $scope.remove = function() {
                $modalStack.dismissAll();
                var opts = {
                    templateUrl: "app/main/administration/plants/views/remove-modal.html",
                    scope: $scope,
                    size: 'md'
                };
                var instance = $modal.open(opts);
            }

            $scope.removePlant = function() {
                AdminPlantsService.deletePlant($scope.data.plant.uuid).then(function(plant) {
                    $modalStack.dismissAll();
                    growl.info("Removed " + $scope.data.plant.displayName, {
                        title: 'Info'
                    });
                    $location.path("admin/plants");
                }, function(err) {
                    $log.error(err);
                })
            }

            $scope.close = function() {
                $modalStack.dismissAll();
            }


            $scope.loadUsers = function() {
                UsersAdministrationService.getOrganizationUsers(
                    $scope.currentUser.organization.uuid)
                    .then(function(users) {
                        $scope.data.users = users;
                        return AdminPlantsService.getUsers($scope.data.plant.uuid);
                    }).then(function(plantUsers) {
                    var uuids = plantUsers.map(function(user) {
                        if (user) return user.uuid;
                    })
                    $scope.data.users.forEach(function(user) {
                        if (user)
                            user.selected = uuids.indexOf(user.uuid) > -1 ? true : false;
                    });
                    $scope.getUsers($scope.data.tableState);
                });
            }

            $scope.updateEntry = function(user) {
                if (user.selected == true) {
                    AdminPlantsService.assignUser($scope.data.plant.uuid, user.uuid)
                        .then(function() {
                            $log.info('Assigned User ' + user.displayName + ' to Plant ' +
                                $scope.data.plant.displayName);
                        });
                } else if (user.selected == false) {
                    AdminPlantsService.unassingUser($scope.data.plant.uuid, user.uuid)
                        .then(function() {
                            $log.info('Unassigned User ' + user.displayName + ' to Plant ' +
                                $scope.data.plant.displayName);
                        });
                }
                $scope.getUsers($scope.data.tableState);
            }

            if ($stateParams.plantUuid) {
                AdminPlantsService.getPlant($stateParams.plantUuid).then(function(plant) {
                    $scope.data.plant = plant;
                    $rootScope.state = plant.displayName;
                    $scope.loadUsers();
                }, function(err) {
                    if (err.status == 403)
                        $location.path("admin/plants");
                })
            }
            else {
                $state.go("Admin.Plants");
            }

        }])
;
administration.component('plantItem', {
    bindings: {
        plant: '='
    },
    template: '<span>{{$ctrl.plant.count || 0}}</span>',
    controller: ['AdminPlantsService', function(AdminPlantsService) {
        var that = this;
        AdminPlantsService.getUsers(this.plant.uuid, {count: true}).then(function(count) {
            that.plant.count = count.length;
        });
    }]
});

administration.component('plantCostCentersCount', {
    bindings: {
        plant: '='
    },
    template: '<span>{{$ctrl.plant.costCenterCount || 0}}</span>',
    controller: ['AdminCostCenterService', function(AdminCostCenterService) {
        var that = this;
        AdminCostCenterService.getCostCenters(this.plant.uuid).then(function(costCenters) {
            that.plant.costCenterCount = costCenters.length;
        });
    }]
});


// administration.component('userIcon', {
//     bindings: {
//         user: '='
//     },
//     template: `<span>{{$ctrl.plant.count}}</span>`,
//     controller: function (AdminPlantsService) {
//         var that = this;
//         AdminPlantsService.getUsers(this.plant.uuid, {count: true}).then(function (count) {
//             var newCount = count ? count.length : 0;
//             that.plant.count = newCount;
//         });
//     }
// });
//
//
// $http.get("api/v1/media", {
//     responseType: "blob",
//     cache: true,
//     params: {
//         'name': 'user-profile',
//         'ownerUuid': scope.user.uuid
//     }
// }).success(function(data, status, headers, config) {
//     var fr = new FileReader();
//     fr.onload = function(e) {
//         if (!e.target.result || !e.target.result.split(',')[1])
//             element.attr('src', 'images/user.png');
//         else
//             element.attr('src', 'data:image/jpeg;base64,' + e.target.result.split(',')[1])
//     };
//     fr.readAsDataURL(data);
// }).error(function(data, status, headers, config) {
//     element.attr('src', 'images/user.png');
// });
administration.component('costCenterItem', {
    bindings: {
        plant: '=',
        costCenter: '='
    },
    templateUrl: 'app/main/administration/costcenters/components/costcenter.item.component.html',
    controller: ['AdminCostCenterService', 'AdminWorkCenterService',
        function(AdminCostCenterService, AdminWorkCenterService) {
            var that = this;
            AdminCostCenterService.getUsers(that.plant.uuid, that.costCenter.uuid, {count: true})
                .then(function(count) {
                    that.nUsers = count.length;
                });
            AdminWorkCenterService.getWorkCenters(that.plant.uuid, that.costCenter.uuid,
                {count: true})
                .then(function(count) {
                    that.nWorkCenters = count.length;
                });

        }]
});
administration.controller("adminCostCenterDetailsCtrl",
    ["$rootScope", "$scope", "$log", "$q", "$stateParams", "$controller", "$modalStack", "$modal", "$location",
        "$state", "growl", "AdminPlantsService", "AdminCostCenterService", "AdminUsersService",
        function($rootScope, $scope, $log, $q, $stateParams, $controller, $modalStack, $modal, $location, $state, growl,
                 AdminPlantsService, AdminCostCenterService) {

            $scope.data = {
                users: [],
                displayedUsers: [],
                onlyMembers: false,
                updateAll: false,
                tableState: undefined
            }

            angular.extend(this, $controller('adminBaseCtrl', {
                $scope: $scope
            }));

            $scope.loadUsers = function() {
                $scope.data.users = [];
                AdminPlantsService.getUsers($scope.data.plant.uuid)
                    .then(function(users) {
                        $scope.data.users = users;
                        return AdminCostCenterService.getUsers($scope.data.plant.uuid,
                            $scope.data.costCenter.uuid);
                    }).then(function(costCenterUsers) {
                    var uuids = costCenterUsers.map(function(user) {
                        if (user) return user.uuid;
                    })
                    $scope.data.users.forEach(function(user) {
                        if (user)
                            user.selected = uuids.indexOf(user.uuid) > -1 ? true : false;
                    });
                });
            }

            $scope.updateEntry = function(user) {
                if (user.selected == true) {
                    AdminCostCenterService.assignUser($scope.data.plant.uuid,
                        $scope.data.costCenter.uuid, user.uuid)
                        .then(function() {
                            $log.info('Assigned User ' + user.displayName + ' to Cost Center ' +
                                $scope.data.costCenter.displayName);
                        });
                } else if (user.selected == false) {
                    AdminCostCenterService.unassignUser($scope.data.plant.uuid,
                        $scope.data.costCenter.uuid, user.uuid)
                        .then(function() {
                            $log.info('Unassigned User ' + user.displayName + ' to Cost Center ' +
                                $scope.data.costCenter.displayName);
                        });
                }
                $scope.getUsers($scope.data.tableState);
            }

            $scope.remove = function() {
                $modalStack.dismissAll();
                var opts = {
                    templateUrl: "app/main/administration/costcenters/views/remove-modal.html",
                    scope: $scope,
                    size: 'md'
                };
                var instance = $modal.open(opts);
            }

            $scope.removeCostCenter = function() {
                AdminCostCenterService.deleteCostCenter($scope.data.plant.uuid,
                    $scope.data.costCenter.uuid).then(function() {
                    $modalStack.dismissAll();
                    growl.info("Removed " + $scope.data.costCenter.displayName, {
                        title: 'Info'
                    });
                    $location.path("admin/plants/" + $scope.data.plant.uuid);
                }, function(err) {
                    $log.error(err);
                })
            }

            $scope.close = function() {
                $modalStack.dismissAll();
            }

            if ($stateParams.plantUuid) {
                if ($stateParams.costCenterUuid) {
                    AdminPlantsService.getPlant($stateParams.plantUuid).then(function(plant) {
                        $scope.data.plant = plant;
                        return AdminCostCenterService.getCostCenter($stateParams.plantUuid,
                            $stateParams.costCenterUuid);
                    }).then(function(costCenter) {
                        $scope.data.costCenter = costCenter;
                        $rootScope.state = costCenter.displayName;
                        $scope.loadUsers();
                        $scope.loadScheduleTemplates();
                    })
                }
                else {
                    $location.path("admin/plants/" + $stateParams.plantUuid + "/costcenters");
                }
            }
            else {
                $state.go("Admin.Plants");
            }


        }])
;
administration.controller("adminCostCentersCtrl",
    ["$rootScope", "$scope", "$timeout", "$stateParams", "$controller",
        "$modalStack", "$modal", "$log", "$filter", "growl", "AdminPlantsService",
        "AdminCostCenterService", "AdminUsersService", "DataService",
        function($rootScope, $scope, $timeout, $stateParams, $controller,
                 $modalStack, $modal, $log, $filter, growl, AdminPlantsService, AdminCostCenterService) {

            $rootScope.state = 'Cost Centers';

            $scope.data = {
                costCenters: [],
                displayedCostCenters: [],
                selectedCostCenters: [],
                users: [],
                displayedUsers: [],
                tableState: undefined
            }

            angular.extend(this, $controller('adminBaseCtrl', {
                $scope: $scope
            }));

            if ($stateParams.plantUuid) {
                AdminPlantsService.getPlant($stateParams.plantUuid).then(function(plant) {
                    $scope.data.plant = plant;
                    $scope.getCostCenters([plant]);
                })
            }

            $scope.costCenterForm = {
                costCenter: {},
                newCostCenter: {},
                step: 1
            }

            $scope.buttons = {
                isCollapsed: false,
                invalid: false
            }

            $scope.add = function() {
                $modalStack.dismissAll();
                var opts = {
                    templateUrl: "app/main/administration/costcenters/views/add.html",
                    scope: $scope,
                    size: 'lg'
                };
                $scope.data.plants = [$scope.data.plant];
                $scope.costCenterForm.plant = $scope.data.plant;
                $scope.data.plantSelectionDisabled = true;
                $scope.data.users = [];
                $scope.data.displayedUsers = [];
                $scope.data.selectAll = false;
                AdminPlantsService.getUsers($scope.data.plant.uuid).then(function(users) {
                    $scope.data.users = users;
                    $scope.data.displayedUsers = users.plain();
                })
                var instance = $modal.open(opts);
            }


            $scope.save = function() {
                AdminCostCenterService.addCostCenter(
                    $scope.costCenterForm.plant.uuid, $scope.costCenterForm.costCenter)
                    .then(function(costCenter) {
                        var users = $.grep($scope.data.displayedUsers, function(user) {
                            return user.selected;
                        });
                        $scope.assignUsers($scope.costCenterForm.plant, costCenter, users)
                        $scope.refresh();
                        growl.info("Created " + costCenter.displayName, {
                            title: 'Info'
                        });
                        $modalStack.dismissAll();
                    }, function(err) {
                        $log.error(err);
                    });
            }

            $scope.assignUsers = function(plant, costCenter, users) {
                users.forEach(function(user) {
                    AdminCostCenterService.assignUser(plant.uuid, costCenter.uuid, user.uuid)
                        .then(function() {
                            $log.info('Assigned User ' + user.displayName + ' to Cost Center ' +
                                costCenter.displayName);
                        });
                })
            }

            $scope.getCostCenters = function(plants) {
                $scope.data.costCenters = [];
                for (var i = 0; i < plants.length; i++) {
                    AdminCostCenterService.getCostCenters(plants[i].uuid, {})
                        .then(function(costCenters) {
                            if (!costCenters) return;
                            $scope.data.costCenters = costCenters.plain().sort($scope.sortByDisplayName);
                        }, function(err) {
                            $log.error(err);
                        })
                }
                // $timeout(function() {
                //     var obj = [];
                //     $scope.data.all.forEach(function(a) {
                //         for (i = 0; i < a.length; i++)
                //             obj.push(a[i]);
                //     })
                //     $scope.data.costCenters = obj;
                //     $scope.countWorkCenters();
                //     DataService.saveCostCenters(obj);
                // }, 75);
            }

            $scope.refresh = function() {
                $scope.getCostCenters([$scope.data.plant]);
                $scope.data.selectAll = false;
                $scope.costCenterForm.costCenter = {};
            }

            $scope.countWorkCenters = function() {
                $scope.data.costCenters.forEach(function(cost) {
                    var count = 0;
                    cost.workCenterCount = cost.workCenters.length;
                })
            }

            $scope.selectedUsers = function() {
                return $.grep($scope.data.users, function(user) {
                    return user.selected;
                }).length;
            }

            $scope.remove = function() {
                $modalStack.dismissAll();
                var opts = {
                    templateUrl: "app/main/administration/costcenters/views/remove-modal.html",
                    scope: $scope,
                    size: 'md'
                };
                var instance = $modal.open(opts);
            }

            $scope.close = function() {
                $modalStack.dismissAll();
            }

            $scope.removeCostCenter = function() {
                $scope.data.plants.forEach(function(plant) {
                    for (var i = 0; i < plant.costCenters.length; i++)
                        for (var j = 0; j < $scope.data.selectedCostCenters.length; j++)
                            if (plant.costCenters[i].uuid ==
                                $scope.data.selectedCostCenters[j].uuid) {
                                AdminCostCenterService.deleteCostCenter(plant.uuid,
                                    plant.costCenters[i].uuid).then(function() {
                                    $scope.refresh();
                                }, function(err) {
                                    $log.error(err);
                                })
                            }
                })
            }

            $scope.confirmEdit = function() {
                var costCenter;
                for (var i = 0; i < $scope.data.displayedCostCenters.length; i++)
                    if ($scope.data.displayedCostCenters[i].edit == true)
                        costCenter = $scope.data.displayedCostCenters[i];

                $scope.data.plants.forEach(function(plant) {
                    for (var i = 0; i < plant.costCenters.length; i++)
                        if (plant.costCenters[i].uuid == costCenter.uuid) {
                            $scope.costCenterForm.newCostCenter.name = $scope.data.newCostCenterName
                            AdminCostCenterService.updateCostCenter(plant.uuid, costCenter.uuid,
                                $scope.costCenterForm.newCostCenter).then(function() {
                                $scope.refresh();
                            });
                        }
                });
            };
        }]);
administration.component('workCenterItem', {
    bindings: {
        plant: '=',
        costCenter: '=',
        workCenter: '='
    },
    templateUrl: 'app/main/administration/workcenters/components/workcenter.item.component.html',
    controller: ['AdminWorkCenterService', function(AdminWorkCenterService) {
        var that = this;
        AdminWorkCenterService.getUsers(that.plant.uuid, that.costCenter.uuid, that.workCenter.uuid,
            {count: true})
            .then(function(count) {
                that.nUsers = count.length;
            });
    }]
});
administration.controller("adminWorkCenterDetailsCtrl",
    ["$rootScope", "$scope", "$log", "$q", "$stateParams", "$controller", "$modalStack", "$modal", "$location",
        "$state", "growl", "AdminPlantsService", "AdminCostCenterService", "AdminWorkCenterService", "AdminSchedulesService",
        function($rootScope, $scope, $log, $q, $stateParams, $controller, $modalStack, $modal, $location, $state, growl,
                 AdminPlantsService, AdminCostCenterService, AdminWorkCenterService, AdminSchedulesService) {

            $scope.data = {
                users: [],
                scheduleTemplates: [],
                displayedUsers: [],
                onlyMembers: false,
                updateAll: false,
                tableState: undefined
            };

            var manualSchedule = {
                displayName: "**Manual Schedule**",
                uuid: null
            };

            angular.extend(this, $controller('adminBaseCtrl', {
                $scope: $scope
            }));

            $scope.loadUsers = function() {
                $scope.data.users = [];
                AdminCostCenterService.getUsers($scope.data.plant.uuid, $scope.data.costCenter.uuid)
                    .then(function(costCenterUsers) {
                        $scope.data.users = costCenterUsers;
                        return AdminWorkCenterService.getUsers($scope.data.plant.uuid,
                            $scope.data.costCenter.uuid, $scope.data.workCenter.uuid);
                    }).then(function(workCenterUsers) {
                    var uuids = workCenterUsers.map(function(user) {
                        if (user) return user.uuid;
                    })
                    $scope.data.users.forEach(function(user) {
                        if (user)
                            user.selected = uuids.indexOf(user.uuid) > -1 ? true : false;
                    });
                    $scope.getUsers($scope.data.tableState);
                })
            };

            $scope.updateWorkCenterSchedule = function(workCenter) {
                if (workCenter.scheduleTemplate && workCenter.scheduleTemplate.uuid == null)
                    workCenter.scheduleTemplate = null;
                AdminWorkCenterService.updateWorkCenter($scope.data.plant.uuid, $scope.data.costCenter.uuid, workCenter.uuid, workCenter).then(function(workCenter) {
                    $scope.data.workCenter = workCenter.plain();
                    if ($scope.data.workCenter.scheduleTemplate == null)
                        $scope.data.workCenter.scheduleTemplate = angular.copy(manualSchedule);
                    growl.info('Assigned Schedule ' + $scope.data.workCenter.scheduleTemplate.displayName + ' to Work Center ' +
                        $scope.data.workCenter.displayName, {
                        title: 'Info'
                    });
                    $log.info('Assigned Schedule ' + $scope.data.workCenter.scheduleTemplate.displayName + ' to Work Center ' +
                        $scope.data.workCenter.displayName);
                })
            };

            $scope.updateEntry = function(user) {
                if (user.selected == true) {
                    AdminWorkCenterService.assignUser($scope.data.plant.uuid,
                        $scope.data.costCenter.uuid, $scope.data.workCenter.uuid, user.uuid)
                        .then(function() {
                            $log.info('Assigned User ' + user.displayName + ' to Work Center ' +
                                $scope.data.workCenter.displayName);
                        });
                } else if (user.selected == false) {
                    AdminWorkCenterService.unassignUser($scope.data.plant.uuid,
                        $scope.data.costCenter.uuid, $scope.data.workCenter.uuid, user.uuid)
                        .then(function() {
                            $log.info('Unassigned User ' + user.displayName + ' to Work Center ' +
                                $scope.data.workCenter.displayName);
                        });
                }
                $scope.getUsers($scope.data.tableState);
            }

            $scope.remove = function() {
                $modalStack.dismissAll();
                var opts = {
                    templateUrl: "app/main/administration/workcenters/views/remove-modal.html",
                    scope: $scope,
                    size: 'md'
                };
                var instance = $modal.open(opts);
            }

            $scope.removeWorkCenter = function() {
                AdminWorkCenterService.deleteWorkCenter($scope.data.plant.uuid,
                    $scope.data.costCenter.uuid, $scope.data.workCenter.uuid).then(function() {
                    $modalStack.dismissAll();
                    growl.info("Removed " + $scope.data.workCenter.displayName, {
                        title: 'Info'
                    });
                    $location.path("admin/plants/" + $scope.data.plant.uuid + "/costcenters/" +
                        $scope.data.costCenter.uuid);
                }, function(err) {
                    $log.error(err);
                })
            }

            $scope.close = function() {
                $modalStack.dismissAll();
            }

            if ($stateParams.plantUuid) {
                if ($stateParams.costCenterUuid) {
                    if ($stateParams.workCenterUuid) {
                        AdminPlantsService.getPlant($stateParams.plantUuid).then(function(plant) {
                            $scope.data.plant = plant;
                            return AdminCostCenterService.getCostCenter($stateParams.plantUuid,
                                $stateParams.costCenterUuid);
                        }).then(function(costCenter) {
                            $scope.data.costCenter = costCenter;
                            return AdminWorkCenterService.getWorkCenter($stateParams.plantUuid,
                                $stateParams.costCenterUuid, $stateParams.workCenterUuid);
                        }).then(function(workCenter) {
                            $scope.data.workCenter = workCenter.plain();
                            if ($scope.data.workCenter.scheduleTemplate == null)
                                $scope.data.workCenter.scheduleTemplate = angular.copy(manualSchedule);
                            $rootScope.state = workCenter.displayName;
                            $scope.loadUsers();
                            $scope.loadScheduleTemplates();
                        });
                    }
                    else {
                        $location.path("admin/plants/" + $stateParams.plantUuid + "/costcenters/" +
                            $stateParams.costCenterUuid + "/workcenters");
                    }
                }
                else {
                    $location.path("admin/plants/" + $stateParams.plantUuid + "/costcenters");
                }
            }
            else {
                $state.go("Admin.Plants");
            }


        }])
;
administration.controller("adminWorkCentersCtrl",
    ["$rootScope", "$scope", "$timeout", "$stateParams", "$controller", "$modalStack", "$modal", "$log", "growl",
        "AdminOrganizationsService",
        "AdminPlantsService", "AdminCostCenterService", "AdminWorkCenterService",
        function($rootScope, $scope, $timeout, $stateParams, $controller, $modalStack, $modal, $log, growl,
                 AdminOrganizationsService, AdminPlantsService, AdminCostCenterService,
                 AdminWorkCenterService) {

            $rootScope.state = 'Work Centers';

            $scope.data = {
                workCenters: [],
                displayedWorkCenters: [],
                selectedWorkCenters: [],
                users: [],
                displayedUsers: [],
                tableState: undefined
            }

            angular.extend(this, $controller('adminBaseCtrl', {
                $scope: $scope
            }));

            if ($stateParams.plantUuid && $stateParams.costCenterUuid) {
                AdminPlantsService.getPlant($stateParams.plantUuid).then(function(plant) {
                    $scope.data.plant = plant;
                    return AdminCostCenterService.getCostCenter(plant.uuid,
                        $stateParams.costCenterUuid);
                }).then(function(costCenter) {
                    $scope.data.costCenter = costCenter;
                    $scope.getWorkCenters();
                })
            }

            var manualSchedule = {
                displayName: "**Manual Schedule**",
                uuid: null
            };

            var resetWorkCenterForm = function() {
                $scope.workCenterForm = {
                    "users": [],
                    "workCenter": {
                        scheduleTemplate: manualSchedule
                    },
                    "newWorkCenter": {},
                    step: 1
                };
                $scope.data.selectAll = false;
            }
            resetWorkCenterForm();

            $scope.buttons = {
                isCollapsed: false,
                invalid: false
            }

            $scope.add = function() {
                $modalStack.dismissAll();
                var opts = {
                    templateUrl: "app/main/administration/workcenters/views/add.html",
                    scope: $scope,
                    size: 'lg'
                };
                $scope.data.displayedUsers = [];
                $scope.data.plants = [$scope.data.plant];
                $scope.workCenterForm.plant = $scope.data.plant;

                $scope.data.costCenters = [$scope.data.costCenter];
                $scope.workCenterForm.costCenter = $scope.data.costCenter;

                $scope.loadScheduleTemplates();
                AdminCostCenterService.getUsers($scope.data.plant.uuid, $scope.data.costCenter.uuid)
                    .then(function(users) {
                        $scope.data.users = users.plain();
                        $scope.data.displayedUsers = users.plain();
                    });
                var instance = $modal.open(opts);
            }


            $scope.save = function() {
                $scope.workCenterForm.workCenter.name
                    = $scope.workCenterForm.workCenter.displayName;
                if ($scope.workCenterForm.workCenter.scheduleTemplate.uuid == null)
                    $scope.workCenterForm.workCenter.scheduleTemplate = null;
                AdminWorkCenterService.addWorkCenter(
                    $scope.workCenterForm.plant.uuid, $scope.workCenterForm.costCenter.uuid,
                    $scope.workCenterForm.workCenter).then(function(workCenter) {
                    var users = $.grep($scope.data.displayedUsers, function(user) {
                        if (user) return user.selected;
                    });
                    $scope.assignUsers($scope.workCenterForm.plant,
                        $scope.workCenterForm.costCenter, workCenter, users)
                    $scope.refresh();
                    growl.info("Created " + workCenter.displayName, {
                        title: 'Info'
                    });
                    $modalStack.dismissAll();
                    resetWorkCenterForm();
                }, function(err) {
                    $log.error(err);
                    if (err.status == 409) {
                        growl.error("Work Center with the name " + $scope.workCenterForm.workCenter.displayName +
                            " already exists within the Organization. Please choose a new name.", {
                            title: 'Duplicate Work Center Name'
                        });
                        $scope.workCenterForm.step = 1;
                        $scope.workCenterForm.workCenter.scheduleTemplate = $scope.workCenterForm.workCenter.scheduleTemplate == null ? manualSchedule : $scope.workCenterForm.workCenter.scheduleTemplate;
                    }
                    else
                        resetWorkCenterForm();
                })
            }

            $scope.assignUsers = function(plant, costCenter, workCenter, users) {
                users.forEach(function(user) {
                    AdminWorkCenterService.assignUser(plant.uuid, costCenter.uuid, workCenter.uuid,
                        user.uuid).then(function() {
                        $log.info('Assigned User ' + user.displayName + ' to Work Center ' +
                            workCenter.displayName);
                        //$scope.data.users.push(user);
                    });
                })
            }

            $scope.getWorkCenters = function() {
                $scope.data.workCenters = [];
                AdminWorkCenterService.getWorkCenters(
                    $scope.data.plant.uuid, $scope.data.costCenter.uuid, {})
                    .then(function(workCenters) {
                        if (!workCenters) return;
                        $scope.data.workCenters = workCenters.plain().sort($scope.sortByDisplayName);
                    }, function(err) {
                        $log.error(err);
                    });
            }

            $scope.refresh = function() {
                $scope.getWorkCenters();
            }

            $scope.selectedUsers = function() {
                return $.grep($scope.data.users, function(user) {
                    return user.selected;
                }).length;
            }

            $scope.remove = function() {
                $modalStack.dismissAll();
                var opts = {
                    templateUrl: "app/main/administration/workcenters/views/remove-modal.html",
                    scope: $scope,
                    size: 'md'
                };
                var instance = $modal.open(opts);
            }

            $scope.close = function() {
                $modalStack.dismissAll();
            }

            $scope.removeWorkCenter = function() {
                $scope.data.plants.forEach(function(plant) {
                    for (var i = 0; i < plant.costCenters.length; i++)
                        for (var x = 0; x < plant.costCenters[i].workCenters.length; x++)
                            for (var j = 0; j < $scope.data.selectedWorkCenters.length; j++)
                                if (plant.costCenters[i].workCenters[x].uuid ==
                                    $scope.data.selectedWorkCenters[j].uuid) {
                                    AdminWorkCenterService.deleteWorkCenter(plant.uuid,
                                        plant.costCenters[i].uuid,
                                        $scope.data.selectedWorkCenters[j].uuid)
                                        .then(function() {
                                            $scope.refresh();
                                        })
                                }
                })
            }

            $scope.confirmEdit = function() {
                var workCenter;
                for (var i = 0; i < $scope.data.displayedWorkCenters.length; i++)
                    if ($scope.data.displayedWorkCenters[i].edit == true)
                        workCenter = $scope.data.displayedWorkCenters[i];

                $scope.data.plants.forEach(function(plant) {
                    for (var i = 0; i < plant.costCenters.length; i++)
                        for (var j = 0; j < plant.costCenters[i].workCenters.length; j++)
                            if (plant.costCenters[i].workCenters[j].uuid == workCenter.uuid) {
                                $scope.workCenterForm.newWorkCenter.name
                                    = $scope.data.newWorkCenterName
                                AdminWorkCenterService.updateWorkCenter(plant.uuid,
                                    plant.costCenters[i].uuid, workCenter.uuid,
                                    $scope.workCenterForm.newWorkCenter).then(function() {
                                    $scope.refresh();
                                });
                            }
                });
            }

            // $scope.getPlants = function() {
            //     AdminPlantsService.getPlants({}).then(function(plants) {
            //         $scope.data.plants = plants;
            //         $scope.getWorkCenters();
            //     }, function(err) {
            //         $log.error(err);
            //     })
            // }
            //
            // $scope.data.plants = DataService.getPlants();
            // if ($scope.data.plants.length)
            //     $scope.getWorkCenters();
            // else
            //     $scope.getPlants();

        }]);
administration.config(["$stateProvider", function($stateProvider) {

    var all = ['SUPER_ADMIN', 'ADMIN', 'GENERAL_MANAGER', 'PLANT_MANAGER',
        'PRODUCTION_MANAGER'];

    var def = {
        permissions: {
            only: all,
            redirectTo: 'OEE'
        }
    }

    $stateProvider.state('Admin', {
        url: "/admin",
        templateUrl: "app/main/administration/admin.html",
        // controller: "adminCtrl",
        redirectTo: 'Admin.Organizations'
    });

    $stateProvider.state('Admin.Organizations', {
        url: "/organizations",
        templateUrl: "app/main/administration/organizations/views/organizations.html",
        controller: "adminOrganizationsCtrl",
        data: {
            permissions: {
                only: ['SUPER_ADMIN'],
                redirectTo: 'Admin.OrganizationDetails'
            }
        },
        reloadOnSearch: false
    });

    $stateProvider.state('Admin.OrganizationDetails', {
        url: "/organizations/:uuid",
        templateUrl: "app/main/administration/organizations/views/details.html",
        controller: "adminOrganizationDetailsCtrl",
        data: {
            permissions: {
                only: ['SUPER_ADMIN', 'ADMIN'],
                redirectTo: 'Admin.Users'
            }
        },
        reloadOnSearch: false
    });

    $stateProvider.state('Admin.Users', {
        url: "/users",
        templateUrl: "app/main/administration/users/views/users.html",
        controller: "adminUsersCtrl",
        reloadOnSearch: false,
        data: {
            permissions: {
                only: ['SUPER_ADMIN', 'ADMIN', 'GENERAL_MANAGER'],
                redirectTo: 'Admin.Plants'
            }
        },
    });

    $stateProvider.state('Admin.ScheduleTemplates', {
        url: "/schedules",
        templateUrl: "app/main/administration/schedules/views/schedules.html",
        controller: "adminSchedulesCtrl",
        reloadOnSearch: false,
        data: def,
    });

    $stateProvider.state('Admin.ScheduleTemplateDetails', {
        url: "/schedules/:scheduleTemplateUuid",
        templateUrl: "app/main/administration/schedules/views/details.html",
        controller: "adminScheduleDetailsCtrl",
        reloadOnSearch: false,
        data: def,
    });

    $stateProvider.state('Admin.Plants', {
        url: "/plants",
        templateUrl: "app/main/administration/plants/views/plants.html",
        controller: "adminPlantsCtrl",
        reloadOnSearch: false,
        data: def,
    });

    $stateProvider
        .state('Admin.PlantDetails', {
            url: "/plants/:plantUuid",
            templateUrl: "app/main/administration/plants/views/details.html",
            controller: "adminPlantDetailsCtrl",
            reloadOnSearch: false,
            data: def,
        });

    $stateProvider.state('Admin.PlantCostCenters', {
        url: "/plants/:plantUuid/costcenters",
        templateUrl: "app/main/administration/costcenters/views/costcenters.html",
        controller: "adminCostCentersCtrl",
        reloadOnSearch: false,
        data: def,
    });

    $stateProvider.state('Admin.CostCenterDetails', {
        url: "/plants/:plantUuid/costcenters/:costCenterUuid",
        templateUrl: "app/main/administration/costcenters/views/details.html",
        controller: "adminCostCenterDetailsCtrl",
        reloadOnSearch: false,
        data: def,
    });

    $stateProvider.state('Admin.CostCenterWorkCenters', {
        url: "/plants/:plantUuid/costcenters/:costCenterUuid/workcenters",
        templateUrl: "app/main/administration/workcenters/views/workcenters.html",
        controller: "adminWorkCentersCtrl",
        reloadOnSearch: false,
        data: def,
    });

    $stateProvider.state('Admin.WorkCenterDetails', {
        url: "/plants/:plantUuid/costcenters/:costCenterUuid/workcenters/:workCenterUuid",
        templateUrl: "app/main/administration/workcenters/views/details.html",
        controller: "adminWorkCenterDetailsCtrl",
        reloadOnSearch: false,
        data: def,
    });

}]);
administration.run([
    '$rootScope',
    '$q',
    'PermissionStore',
    'UserService',
    'PlansService',
    function($rootScope, $q, PermissionStore, UserService, PlansService) {

        var currentUser;

        function hasRole(roleName, deferred) {
            var results = $.grep(currentUser.roles, function(role) {
                    return role.name === roleName && role.organization.uuid === currentUser.organization.uuid;
                }).length > 0;
            if (roleName == 'SUPER_ADMIN')
                deferred.resolve()
            else
                results && PlansService.getActiveSubscription() ? deferred.resolve() :
                    deferred.reject();
        }

        function resolveUser(roleName, deferred) {
            UserService.getUser().then(function(user) {
                currentUser = user.plain();
                return PlansService.refreshActiveSubscription()
            }).then(function() {
                hasRole(roleName, deferred);
            })
        }

        var permissions = [{
            name: 'SUPER_ADMIN',
            displayName: 'SuperAdmin'
        }, {
            name: 'ADMIN',
            displayName: 'Admin'
        }, {
            name: 'GENERAL_MANAGER',
            displayName: 'GeneralManager'
        }, {
            name: 'PLANT_MANAGER',
            displayName: 'PlantManager'
        }, {
            name: 'PRODUCTION_MANAGER',
            displayName: 'ProductionManager'
        }, {
            name: 'OPERATOR',
            displayName: 'Operator'
        }]

        permissions.forEach(function(permission) {
            PermissionStore.definePermission(permission.name, function() {
                var deferred = $q.defer();
                // return true;
                // if (!angular.equals(_.omit(currentUser, 'lastModifiedDate'), _.omit($rootScope.currentUser, 'lastModifiedDate')))
                //     console.log("NOT EQUAL");
                if (currentUser && angular.equals(_.omit(currentUser, 'lastModifiedDate'), _.omit($rootScope.currentUser, 'lastModifiedDate')))
                    hasRole(permission.displayName, deferred);
                else
                    resolveUser(permission.displayName, deferred);
                return deferred.promise;
            });
        });

        function hasAnalysisAccess(deferred) {
            var subscriptionPlan = PlansService.getActiveSubscription();
            var isSuperAdmin = $.grep(currentUser.roles, function(role) {
                    return role.name === "SuperAdmin";
                }).length > 0;
            if (isSuperAdmin) {
                deferred.resolve();
                return;
            }
            var validPlan = subscriptionPlan.providerId !== 'started';
            var validNUsers = (subscriptionPlan.properties.nUsers === -1 ||
            currentUser.organization.nusers <= subscriptionPlan.properties.nUsers);

            validNUsers && validPlan ? deferred.resolve() :
                deferred.reject();
        }

        PermissionStore.definePermission("ANALYSIS_ACCESS", function() {
            var deferred = $q.defer();
            UserService.getUser().then(function(user) {
                currentUser = user.plain();
                return PlansService.refreshActiveSubscription()
            }).then(function() {
                hasAnalysisAccess(deferred);
            })
            return deferred.promise;
        });


        // PlansService.refreshActiveSubscription();
    }]);
var datasources = angular.module('com.bosch.datasources', ['ui.router']);
datasources.config(["$stateProvider", function($stateProvider) {

    $stateProvider.state('Datasource', {
        url: "/datasources",
        templateUrl: "app/main/datasources/datasource.html",
        redirectTo: 'Datasource.Deviation'
    }).state('Datasource.Deviation', {
        url: "/deviation",
        templateUrl: "app/main/datasources/data/deviation/deviation.html",
        controller: "DatasourcesDeviationCtrl",
        reloadOnSearch: false
    }).state('Datasource.OEE', {
        url: "/oee",
        templateUrl: "app/main/datasources/data/oee/oee.html",
        controller: "DatasourcesOEECtrl",
        reloadOnSearch: false
    });

}]);
// import "angular-ui-router";
// import "angular-spinner";

var analysis = angular.module('com.bosch.analysis', ['ui.router', 'angularSpinner']);
analysis.config(["$stateProvider", function ($stateProvider) {

    $stateProvider.state('Analysis', {
        url: "/analysis",
        reloadOnSearch: false,
        templateUrl: "app/main/analysis/views/analysis.html",
        redirectTo: 'Analysis.List'
    })

    $stateProvider.state('Analysis.List', {
        url: "/list",
        controller: "AnalysisCtrl",
        reloadOnSearch: false,
        templateUrl: "app/main/analysis/views/list.html",
        data: {
            permissions: {
                only: ['ANALYSIS_ACCESS'],
                redirectTo: 'OEE'
            }
        }
    })

    $stateProvider.state('Analysis.Details', {
        url: "/:uuid",
        controller: "AnalysisDetailsCtrl",
        reloadOnSearch: false,
        templateUrl: "app/main/analysis/views/details.html",
        data: {
            permissions: {
                only: ['ANALYSIS_ACCESS'],
                redirectTo: 'OEE'
            }
        }
    })

    $stateProvider.state('Analysis.Multimetric', {
        url: "/:uuid/mmparetos/:version",
        templateUrl: "app/main/analysis/multimetric/views/multimetric.html",
        controller: "MultimetricCtrl",
        reloadOnSearch: false,
        data: {
            permissions: {
                only: ['ANALYSIS_ACCESS'],
                redirectTo: 'OEE'
            }
        }
    }).state('Analysis.Wordcloud', {
        url: "/:uuid/wordcloud/:version",
        templateUrl: "app/main/analysis/wordcloud/views/wordcloud.html",
        controller: "WordcloudCtrl",
        reloadOnSearch: false,
        data: {
            permissions: {
                only: ['ANALYSIS_ACCESS'],
                redirectTo: 'OEE'
            }
        }
    }).state('Analysis.Correlations', {
        url: "/:uuid/correlations/:version",
        templateUrl: "app/main/analysis/correlations/views/correlations.html",
        controller: "CorrelationsCtrl",
        reloadOnSearch: false,
        data: {
            permissions: {
                only: ['ANALYSIS_ACCESS'],
                redirectTo: 'OEE'
            }
        }
    }).state('Analysis.Recommendations', {
        url: "/:uuid/recommendations/:version",
        templateUrl: "app/main/analysis/recommendations/views/recommendations.html",
        controller: "RecommendationsCtrl",
        reloadOnSearch: false,
        data: {
            permissions: {
                only: ['ANALYSIS_ACCESS'],
                redirectTo: 'OEE'
            }
        }
    });

}]);
analysis.controller(
    'AnalysisCtrl',
    ['$scope', '$controller', '$http', '$location', '$log', '$timeout', 'growl',
        'AnalysisResultService', '$modal', '$modalStack', 'CurrentAnalysisGroupService', 'DisplayService', 'DeviationEntryService',
        function($scope, $controller, $http, $location, $log, $timeout, growl,
                 AnalysisResultService, $modal, $modalStack, CurrentAnalysisGroupService, DisplayService, DeviationEntryService) {

            $scope.data = {
                oeeEntries: [],
                newEntry: {},
                count: [1, 2, 3, 4]
            };

            $scope.buttons = {
                confirmed: false,
                isCollapsed: false
            };

            $scope.analysisGroupForm = {
                step: 1
            };

            angular.extend(this, $controller('DataFilterCtrl', {
                $scope: $scope
            }));

            $scope.getNumber = function(num) {
                return new Array(num);
            };

            $scope.tableState;
            $scope.refresh = function(flag) {
                $scope.getAnalysisGroups($scope.tableState, flag);
            }

            $scope.getAnalysisGroups = function(tableState, flags) {
                var options = $scope.getQueryOptions(tableState);
                options = $scope.prepareAnalysisQuery(options);
                AnalysisResultService.getAnalysisGroups(options).then(function(result) {
                    result.content.sort(function(a, b) {
                        return a.createdDate >
                            b.createdDate;
                    });
                    $scope.data.displayedAnalysisGroups = result.content;
                    tableState.pagination.numberOfPages = result.totalPages;
                });

            };

            $scope.addAnalysis = function() {
                $modalStack.dismissAll();
                var opts = {
                    templateUrl: "app/main/analysis/views/add.html",
                    scope: $scope,
                    size: 'md'
                };
                var instance = $modal.open(opts);
            }

            $scope.process = function() {
                var options = $scope.getQueryOptions($scope.tableState);
                options = $scope.prepareAnalysisQuery(options);
                if ($scope.data.newEntry.displayName == null) {
                    growl.error("Please enter a name for the analysis.", {
                        title: 'Error'
                    });
                    return;
                }
                if (options.filters.shifts.params.shifts.length == 0) {
                    growl.error("Please select at least one shift.", {
                        title: 'Error'
                    });
                    return;
                }
                if (options.filters.workCenters.params.workCenters.length == 0) {
                    growl.error("Please select at least one work center.", {
                        title: 'Error'
                    });
                    return;
                }
                options.displayName = $scope.data.newEntry.displayName;
                options.description = $scope.data.newEntry.description;
                DeviationEntryService.analyze(options).then(function(result) {
                    if (!result) {
                        $modalStack.dismissAll();
                        $scope.refresh(false);
                        return;
                    }
                    if (result.status == 'BAD_REQUEST') {
                        growl.error("There has been an error processing your request.", {
                            title: 'Error'
                        });
                    }
                    else {
                        growl.info("Entries have been sent for analysis.", {
                            title: 'Info'
                        });
                        $modalStack.dismissAll();
                        $scope.refresh(false);
                    }
                }, function(err) {
                    growl.error("There has been an error processing your request.", {
                        title: 'Error'
                    });
                });
            }

            $scope.prepareAnalysisQuery = function(options) {
                return {
                    start: options.start,
                    number: options.number,
                    filters: {
                        time: {
                            params: {
                                from: options.from,
                                to: options.to
                            }
                        },
                        workCenters: {
                            params: {
                                workCenters: options.workCenters
                            }
                        },
                        shifts: {
                            params: {
                                shifts: options.shifts
                            }
                        }
                    }
                }
            }

            $scope.showDetails = function(analysis) {
                CurrentAnalysisGroupService.setCurrentAnalysisGroup(analysis);
                $location.path("/analysis/" + analysis.uuid);
            }
        }]);

analysis.controller(
    'AnalysisDetailsCtrl',
    ['$scope', '$location', '$stateParams', '$modal', '$modalStack', '$timeout', 'growl', 'AnalysisResultService',
        function($scope, $location, $stateParams, $modal, $modalStack, $timeout, growl, AnalysisResultService) {

            $scope.data = {};

            $scope.data = {
                analysisVersions: [],
                displayedAnalysisVersions: [],
                selectedAnalysisVersions: []
            }

            $scope.getAnalysisVersions = function() {
                if ($stateParams.uuid) {
                    AnalysisResultService.getAnalysisGroup($stateParams.uuid)
                        .then(function(analysisGroup) {
                            if (!analysisGroup) return;
                            $scope.data.analysisGroup = analysisGroup.plain();
                            $scope.data.analysisVersions = $scope.data.analysisGroup.versions;
                            $scope.checkResultsProgress($scope.data.analysisGroup.versions);
                        });
                }
            }

            $scope.checkResultsProgress = function(versions) {
                for (var i = 0; i < versions.length; i++) {
                    var results = Object.values(versions[i].results);
                    for (var j = 0; j < results.length; j++) {
                        if (results[j].status == 'IN_PROGRESS') {
                            $timeout($scope.getAnalysisVersions, 5000);
                            return;
                        }
                    }
                }
            }

            $scope.resultLabels = {
                "mmparetos": "Multi-Metric Pareto",
                "correlations": "Event Correlation",
                "wordcloud": "Word Cloud",
                "recommendations": "Recommendations"
            }

            $scope.getAnalysisVersions();

            $scope.showResult = function(type, version) {
                var status = $scope.data.analysisGroup.versions[version].results[type].status;
                if (status == 'COMPLETED')
                    $location.path('/analysis/' + $scope.data.analysisGroup.uuid + "/" + type + "/" + version);
                else if (status == 'FAIL')
                    growl.info(type + " analysis failed with cause: \n\n" + decodeURI($scope.data.analysisGroup.versions[version].results[type].comments), {
                        title: 'Info'
                    });
            }

            $scope.addVersion = function() {
                $modalStack.dismissAll();
                var opts = {
                    templateUrl: "app/main/analysis/views/new-version.html",
                    scope: $scope,
                    size: 'md'
                };
                var instance = $modal.open(opts);
            }

            $scope.close = function() {
                $modalStack.dismissAll();
            }

            $scope.reprocess = function(analysisGroup, versionDescription) {
                growl.info("Re-processing " + analysisGroup.displayName, {
                    title: 'Info'
                });
                analysisGroup.newVersionDescription = versionDescription;
                AnalysisResultService.reanalyze(analysisGroup).then(function(response) {
                    $scope.getAnalysisVersions();
                });
            }

            $scope.remove = function(analysisGroup) {
                growl.info("Deleting " + analysisGroup.displayName, {
                    title: 'Info'
                });
                AnalysisResultService.deleteAnalysisGroup(analysisGroup.uuid).then(function(response) {
                    $stateParams.uuid = null;
                    $scope.selectState('/analysis/list')
                });
            }

            $scope.removeAnalysisGroup = function() {
                $modalStack.dismissAll();
                var opts = {
                    templateUrl: "app/main/analysis/views/remove-group-modal.html",
                    scope: $scope,
                    size: 'md'
                };
                var instance = $modal.open(opts);
            }

            $scope.editAnalysisGroup = function() {
                $scope.data.copiedAnalysisGroup = angular.copy($scope.data.analysisGroup);
                $modalStack.dismissAll();
                var opts = {
                    templateUrl: "app/main/analysis/views/edit-group-modal.html",
                    scope: $scope,
                    size: 'md'
                };
                var instance = $modal.open(opts);
            }

            $scope.save = function(analysisGroup) {
                AnalysisResultService.updateAnalysisGroup(analysisGroup.uuid, analysisGroup)
                    .then(function(analysisGroup) {
                        growl.info("Updated " + analysisGroup.displayName, {
                            title: 'Info'
                        });
                        if (!analysisGroup) return;
                        $scope.data.analysisGroup = analysisGroup.plain();
                        $scope.data.analysisVersions = $scope.data.analysisGroup.versions;
                        $modalStack.dismissAll();
                    });
            }

        }]);

var shared = angular.module('com.bosch.shared', []);
shared.component('userDisplay', {
    bindings: {
        userUuid: '='
    },
    template: '<user-media class="user-media" user="$ctrl.user"/>',
    controller: ['$rootScope', 'AdminUsersService', function($rootScope, AdminUsersService) {
        var that = this;
        AdminUsersService.getUser($rootScope.currentUser.organization.uuid, that.userUuid)
            .then(function(user) {
                that.user = user;
            });

    }]
});
var subscription = angular.module('com.bosch.subscription',
    ['angular-stripe', 'gavruk.card', 'ui.router']);

// subscription.run(["PaymentService", "stripe",
//     function(PaymentService, stripe) {
//         PaymentService.getStripeKey().then(function(key) {
//             stripe.setPublishableKey(key.key);
//         });
//     }]);
subscription.config(["$stateProvider", function($stateProvider) {

    $stateProvider.state('Account.Subscription', {
        url: "/subscription",
        templateUrl: "app/main/account/subscription/views/subscription.html",
        reloadOnSearch: false,
        data: {
            permissions: {
                only: ['ADMIN', 'GENERAL_MANAGER'],
                redirectTo: 'OEE'
            }
        }
    });
}]);
subscription.controller("PaymentsCtrl", ["$rootScope",
    "$scope", "$log", "$modal", "$modalStack", "growl", "PaymentService",
    function($rootScope, $scope, $log, $modal, $modalStack, growl, PaymentService) {

        $rootScope.state = "Subscription";

        $scope.data = {
            selectedPayments: [],
            disableButtons: false
        };
        $scope.card = {};

        $scope.cardPlaceholders = {
            name: 'Your Full Name',
            number: 'xxxx xxxx xxxx xxxx',
            expiry: 'MM/YY',
            cvc: 'xxx'
        };

        $scope.cardMessages = {
            validDate: 'valid\nthru',
            monthYear: 'MM/YY',
        };

        $scope.cardOptions = {
            debug: false,
            formatting: true
        };

        $scope.$on('addCreditCard', function() {
            $scope.addCard();
        });

        $scope.addCard = function() {
            $modalStack.dismissAll();
            var opts = {
                templateUrl: "app/main/account/subscription/views/add-card-modal.html",
                scope: $scope,
                size: 'sm'
            };
            var instance = $modal.open(opts);
        }

        function setPrimary(payment, payments) {
            payments.forEach(function(p) {
                p.primary = p.uuid !== payment.uuid ? false : true;
            })
        }

        function onTokenCreated(response) {
            var card = {
                token: response.id,
                created: response.created,
                type: response.card.brand,
                last4: response.card.last4
            }
            var active = $scope.data.payments.length > 0 ? $scope.card.primary : true;
            var paymentMethod = {
                accountUuid: $scope.currentUser.organization.uuid,
                displayName: $scope.card.displayName,
                primary: active,
                provider: 'Stripe',
                hash: response.id,
                type: 'CREDIT_CARD',
                params: card
            }
            PaymentService.addPaymentMethod(paymentMethod).then(function(payment) {
                $scope.data.disableButtons = false;
                $modalStack.dismissAll();
                $scope.data.payments.push(payment);
                if (payment.primary == true)
                    setPrimary(payment, $scope.data.payments);
                $scope.formReset();
                $scope.refresh()
            }, function() {
                $scope.data.disableButtons = false;
                growl.error("There was an error with your request.", {
                    title: 'Error'
                });
            });
        }

        $scope.submit = function() {
            if (!$scope.card.displayName || $scope.card.displayName == '') {
                growl.error("Please, provide an alias for this card", {
                    title: 'Error'
                });
                return;
            }
            var card = {
                number: $scope.card.number,
                exp: $scope.card.exp,
                cvc: $scope.card.cvc
            }
            $scope.data.disableButtons = true;
            Stripe.card.createToken(card, function(status, response) {
                if (!response.error)
                    onTokenCreated(response);
                else {
                    $scope.data.disableButtons = false;
                    growl.error(response.error.message, {
                        title: 'Error'
                    });
                }
            });
        }

        $scope.getPayments = function() {
            PaymentService.getPaymentMethods().then(function(payments) {
                $scope.data.payments = payments
            });
        };

        $scope.formReset = function() {
            $scope.card = {};
            $scope.data.disableButtons = false;
        }

        $scope.refresh = function() {
            $scope.getPayments();
        }
        $scope.refresh();

        function getPrimary(newPayment) {
            var bckPrimary = $.grep($scope.data.payments, function(payment) {
                return payment.primary && (newPayment.uuid != payment.uuid);
            });
            if (bckPrimary.length == 1)
                return bckPrimary[0];
        }

        $scope.makeDefault = function(payment) {
            if ($scope.data.disableControls == true) return;
            if (payment.primary == false) {
                payment.primary = true;
                growl.warning("You should have always a card associated with your account.", {
                    title: 'Warning'
                });
                return;
            }
            $scope.backUp = angular.copy(getPrimary(payment));
            if (payment.primary == true)
                setPrimary(payment, $scope.data.payments);
            $scope.data.disableControls = true;
            PaymentService.updatePaymentMethod(payment)
                .then(function(updatedPayment) {
                    growl.success("Successfully changed your primary Payment Method.", {
                        title: 'Success'
                    });
                    $scope.data.disableControls = false;
                    $scope.refresh()
                }, function() {
                    $scope.data.disableControls = false;
                    setPrimary($scope.backUp, $scope.data.payments);
                });
        }

        $scope.remove = function() {
            var selection = $.grep($scope.data.payments, function(payment) {
                return payment.isSelected;
            })
            if (selection.length != 1 || selection[0].primary) {
                growl.warning(
                    "You cannot remove the Payment Method set as Primary. Please, " +
                    "mark another Payment Method as Primary before performing this operation.", {
                        title: 'Invalid Selection'
                    });
                return;
            }
            $scope.data.selectedPayment = selection[0];
            $modalStack.dismissAll();
            var opts = {
                templateUrl: "app/main/account/subscription/views/remove-card-modal.html",
                scope: $scope,
                size: 'sm'
            };
            var instance = $modal.open(opts);
        }

        $scope.removePaymentMethod = function() {
            $scope.data.disableControls = true;
            PaymentService.removePaymentMethod($scope.data.selectedPayment.uuid).then(function() {
                var index = $scope.data.payments.indexOf($scope.data.selectedPayment);
                $scope.data.payments.splice(index, 1);
                $modalStack.dismissAll();
                $scope.data.disableControls = false;
            }, function() {
                $scope.data.disableControls = false;
            })
        }

        $scope.close = function() {
            $modalStack.dismissAll();
        }

    }]);

subscription.controller("PlansCtrl", ["$rootScope",
    "$scope", "$log", "growl", "PlansService",
    function($rootScope, $scope, $log, growl, PlansService) {

        $rootScope.state = "Subscription";

        $scope.data.plans = [
            //     {
            //     displayName: "Started",
            //     providerId: "started",
            //     amount: "FREE",
            //     interval: '',
            //     content: [{
            //         strong: 1,
            //         small: "user"
            //     }, {
            //         strong: 10,
            //         small: "analysis"
            //     }]
            // },
            {
                displayName: "Team",
                providerId: "team",
                amount: "999",
                interval: 'month',
                content: [{
                    strong: 10,
                    small: "users"
                }, {
                    strong: "∞",
                    small: "analysis"
                }]
            }, {
                displayName: "Premium",
                providerId: "premium",
                amount: "2,999",
                interval: 'month',
                content: [{
                    strong: 50,
                    small: "users"
                }, {
                    strong: "∞",
                    small: "analysis"
                }]
            }
            // , {
            //     displayName: "Premium Plus",
            //     providerId: "premiumPlus",
            //     amount: "Contact for Pricing",
            //     interval: '',
            //     content: [{
            //         strong: "50+",
            //         small: "users"
            //     }, {
            //         strong: "∞",
            //         small: "analysis"
            //     }]
            // }
        ]

        PlansService.getSubscriptions().then(function(activePlans) {
            activePlans.forEach(function(activePlan) {
                var selection = $.grep($scope.data.plans, function(plan) {
                    return plan.providerId === activePlan.plan.providerId;
                });
                if (selection.length == 1)
                    selection[0].active = true;
            })
        })

        function setActivePlan(activePlan) {
            $scope.data.plans.forEach(function(plan) {
                if (!activePlan || plan.providerId != activePlan.providerId)
                    plan.active = false;
                else
                    plan.active = true;
            })
        }

        function getActivePlan() {
            var selection = $.grep($scope.data.plans, function(plan) {
                return plan.active;
            });
            if (selection.length == 1)
                return selection[0];
        }

        $scope.selectPlan = function(activePlan) {
            $scope.backUp = getActivePlan();
            setActivePlan(activePlan);
            $scope.data.disableButtons = true;
            PlansService.changeActiveSubscription(activePlan).then(function() {
                PlansService.refreshActiveSubscription();
                $scope.data.disableButtons = false;
                growl.success("Successfully changed Plan Subscription.", {
                    title: 'Success'
                });
            }, function(error) {
                $log.error(error);
                $scope.data.disableButtons = false;
                setActivePlan($scope.backUp);


                var description = error.data.message;
                if (description && description.indexOf("There is no primary payment method selected") > -1) {
                    $rootScope.$broadcast('addCreditCard');
                }
            })
        }

        $scope.cancelPlan = function(canceledPlan) {
            canceledPlan.active = false;
            $scope.data.disableButtons = true;
            PlansService.cancelActiveSubscription(canceledPlan.providerId).then(function() {
                PlansService.refreshActiveSubscription();
                $scope.data.disableButtons = false;
                growl.info(
                    "You have successfully canceled your subscription. You plan will still be " +
                    "valid until the end of the billing cycle.", {
                        title: 'Info',
                        ttl: 10000
                    });
            }, function(err) {
                $log.error(err);
                $scope.data.disableButtons = false;
                canceledPlan.active = true;
            })
        }


    }]);

/*******************************************************************************
 * Copyright (c) 2015 Bosch Software Innovations GmbH. All rights reserved.
 ******************************************************************************/
var profile = angular.module('com.bosch.profile',
    ['ui.router', 'ngFileUpload', 'angular-international']);



profile.config(["$stateProvider", function($stateProvider) {

    $stateProvider.state('Account.Profile', {
        url: "/profile",
        templateUrl: "app/main/account/profile/views/user.html",
        controller: "ProfileCtrl",
        reloadOnSearch: false
    });

}]);
profile.controller(
    "ProfileCtrl",
    [
        "$http",
        "$scope",
        "$rootScope",
        "$log",
        "$state",
        "growl",
        "countries",
        "languages",
        "ConfigurationService",
        "ProfileService",
        "Upload",
        "TranslationService",
        '$modal',
        '$modalStack',
        'PaymentService',
        'AdminUsersService',
        'APP_CONSTANTS',
        function($http, $scope, $rootScope, $log, $state, growl, countries, languages,
                 ConfigurationService, ProfileService, Upload, TranslationService,
                 $modal, $modalStack, PaymentService, AdminUsersService, APP_CONSTANTS) {

            $rootScope.state = "Profile";

            $scope.images = {};
            $scope.data = {};
            $scope.controls = {};
            $scope.payment = {};
            $scope.data = {
                show: false,
                exec: false,
                word: false,
                metric: false,
                corr: false,
                disable: false
            };
            $scope.add = {};

            $scope.getCountries = function() {
                return countries;
            }

            $scope.getLanguages = function() {
                return languages;
            }

            $scope.saveProfile = function() {
                $scope.saveProfilePicture();
                $scope.saveUserSettings();
            }

            $scope.saveUserSettings = function() {
                ConfigurationService.updateConfiguration($scope.user.configuration,
                    $scope.currentUser.uuid).then(function(configuration) {
                    $rootScope.user.configuration = configuration;
                    growl.success("PROFILE_HAS_BEEN_SUCCESSFULLY_SAVED", {
                        title: 'SUCCESS'
                    });
                    $scope.controls.countryEdit = false;
                });
            }

            $scope.$watch('data.picFile', function() {
                if (!$scope.data.picFile) return;
                $scope.saveProfilePicture();
            })

            $scope.saveProfilePicture = function() {
                if (!$scope.data.picFile) return;
                // $scope.data.disableControls = true;
                var fd = new FormData();
                fd.append('file', $scope.data.picFile);
                fd.append('name', "user-profile");
                $http.post(APP_CONSTANTS.API_PATH + "/api/v1/media", fd, {
                    transformRequest: angular.identity,
                    headers: {
                        'Content-Type': undefined,
                        // 'name': 'user-profile'
                    }
                }).success(function(result) {
                    // $scope.data.disableControls = false;
                    growl.success("PROFILE_HAS_BEEN_SUCCESSFULLY_SAVED", {
                        title: 'SUCCESS'
                    });
                    $log.info("Profile picture successfully saved")
                }).error(function(err) {
                    // $scope.data.disableControls = false;
                    growl.error("There is been a problem updating your profile picture", {
                        title: 'ERROR'
                    });
                    console.log(err);
                });
            }

            var exception = "com.bosch.im.authentication.AuthenticationDeniedRuntimeException";

            $scope.savePassword = function() {
                if ($scope.password.newPassErrors.length == 0
                    && $scope.password.newPass2Errors.length == 0) {
                    ProfileService.changePassword($scope.password.current, $scope.password.newPass)
                        .then(
                            function(result) {
                                $scope.passwordEdit = false;
                                growl.success("Successfully changed password", {
                                    title: "SUCCESS"
                                });
                            }, function(error) {
                                if (error.data.exception == exception) {
                                    growl.warning("Current password incorrect", {
                                        title: 'ERROR'
                                    });
                                }
                            });
                }
            }

            $http.get(APP_CONSTANTS.API_PATH + "/api/v1/media", {
                responseType: "blob",
                params: {
                    'name': 'user-profile'
                }
            }).success(function(data, status, headers, config) {
                var fr = new FileReader();
                fr.onload = function(e) {
                    $scope.updateProfilePicture(e.target.result.split(',')[1]);
                };
                fr.readAsDataURL(data);
            }).error(function(data, status, headers, config) {
                console.log(data);
            });

            $scope.updateProfilePicture = function(data) {
                $scope.data.profilePic = 'data:image/jpeg;base64,' + data;
                $scope.$apply();
            }

            $scope.$watch('password.newPass', function() {
                if (!$scope.password) return;
                $scope.password.newPassErrors = [];

                if (/[a-z]/.test($scope.password.newPass) == false) {
                    $scope.password.newPassErrors
                        .push('Your password should have at least one lower case letter.');
                }
                if (/[A-Z]/.test($scope.password.newPass) == false) {
                    $scope.password.newPassErrors
                        .push('Your password should have at least one upper case letter.');
                }
                if (/\d/.test($scope.password.newPass) == false) {
                    $scope.password.newPassErrors.push(
                        'Your password should have at least one number.');
                }
                if (/\W+/.test($scope.password.newPass) == false) {
                    $scope.password.newPassErrors
                        .push('Your password should have at least one special character.');
                }

            })

            $scope.$watch('password.newPass2', function() {
                if (!$scope.password) return;
                $scope.password.newPass2Errors = [];
                if ($scope.password.newPass2 !== $scope.password.newPass) {
                    $scope.password.newPass2Errors.push('Passwords should match');
                }
            })

            $scope.$watch('add.card', function() {
                if ($scope.add.card) {
                    var cardSize = $scope.add.card.length;
                    if (cardSize < 5) {
                        if (cardSize % 4 == 0) {
                            $scope.add.card += ' ';
                            cardSize++;
                        }
                    } else if (cardSize !== 5) {
                        if (cardSize % 5 == 4) {
                            $scope.add.card += ' ';
                            cardSize++;
                        }
                    }
                    $scope.add.number = $scope.add.card;
                    $scope.add.number = $scope.add.number.replace(/[\s]/g, '');
                }
            })

            $scope.choosePlan = function(type) {
                $scope.data.disable = true
                var plan = {plan: type};

                PaymentService.paymentPlan(plan).then(function(response) {
                    if (response.status) {
                        growl.info(response.response, {
                            title: 'Info'
                        });
                        switch (type) {
                            case 'exec':
                                $scope.data.exec = true;
                                break;
                            case 'word':
                                $scope.data.word = true;
                                break;
                            case 'correlation':
                                $scope.data.corr = true;
                                break;
                            case 'metric':
                                $scope.data.metric = true;
                                break;
                            default :
                                break;
                        }
                    } else {
                        growl.error(response.response, {
                            title: 'Error'
                        });
                    }
                    $scope.data.disable = false;
                });
            }

            $scope.close = function() {
                $scope.data.disable = false;
                $modalStack.dismissAll();
            }

            $scope.cancelPlan = function(type) {
                $scope.refresh();
                $scope.data.disable = true;
                $scope.data.current = type;
                $modalStack.dismissAll();
                var opts = {
                    templateUrl: "app/main/profile/views/cancel-modal.html",
                    scope: $scope,
                    size: 'md'
                };
                var instance = $modal.open(opts);
            }

            $scope.deleteSub = function() {
                var row;
                $scope.data.displayedPaymentPlans.forEach(function(entry) {
                    if (entry.planId == $scope.data.current)
                        row = entry;
                });
                PaymentService.deleteSubscription(row.subscriptionId).then(function(result) {
                    if (result.status) {
                        switch ($scope.data.current) {
                            case 'exec':
                                $scope.data.exec = false;
                                break;
                            case 'word':
                                $scope.data.word = false;
                                break;
                            case 'correlation':
                                $scope.data.corr = false;
                                break;
                            case 'metric':
                                $scope.data.metric = false;
                                break;
                            default :
                                break;
                        }
                        growl.info(result.response, {
                            title: 'Info'
                        });
                    } else {
                        growl.error(result.response, {
                            title: 'Error'
                        });
                    }
                    $scope.refresh();
                    $scope.data.disable = false;
                });
            }

            $scope.showCancel = function() {
                $scope.data.disable = true;
                var entries = $.grep($scope.data.displayedPayments, function(entry) {
                    return entry.isSelected;
                });
                $scope.data.row = entries[0];
                $modalStack.dismissAll();
                var opts = {
                    templateUrl: "app/main/profile/views/cancel-card-modal.html",
                    scope: $scope,
                    size: 'md'
                };
                var instance = $modal.open(opts);
            }

            $scope.deleteCard = function() {
                PaymentService.deleteCard($scope.data.row.cardId).then(function(result) {
                    if (result.status) {
                        growl.info(result.response, {
                            title: 'Info'
                        });
                    } else {
                        growl.error(result.response, {
                            title: 'Error'
                        });
                    }
                    $scope.refresh();
                    $scope.data.disable = false;
                    $scope.data.show = false;
                });
            };

        }]);
var account = angular.module('com.bosch.account',
    ['com.bosch.subscription', 'com.bosch.profile', 'ui.router']);
account.config(["$stateProvider", function($stateProvider) {

    $stateProvider.state('Account', {
        url: "/account",
        templateUrl: "app/main/account/views/account.html",
        reloadOnSearch: false,
        redirectTo: 'Account.Profile'
    });

}]);
/*******************************************************************************
 * Copyright (c) 2015 Bosch Software Innovations GmbH. All rights reserved.
 ******************************************************************************/
angular.module('app.controllers.base', []).controller(
    "BaseCtrl",
    ["$rootScope", "$scope", "$location", "screenSize",
        function($rootScope, $scope, $location, screenSize) {

          screenSize.when('sm', function() {
            $scope.desktop = screenSize.is('md, lg');
          });

          screenSize.when('xs', function() {
            $scope.desktop = screenSize.is('md, lg');
          });

          screenSize.when('md', function() {
            $scope.desktop = screenSize.is('md, lg');
          });

          screenSize.when('lg', function() {
            $scope.desktop = screenSize.is('md, lg');
          });

          $scope.desktop = screenSize.is('md, lg');

          $scope.select = function(state, uuid) {
            if ($location.$$path.endsWith(state)) {
              $location.search('uuid', uuid);
            } else {
              $location.path(state).search('uuid', uuid);
            }
          }

          $scope.obj2List = function(obj) {
            var list = [];
            for ( var index in obj) {
              list.push({
                key: index,
                value: obj[index]
              });
            }
            return list;
          }

        }]);
/*******************************************************************************
 * Copyright (c) 2015 Bosch Software Innovations GmbH. All rights reserved.
 ******************************************************************************/
var app = angular.module("app", ['angular-cache', 'ngAnimate',
    'angular-growl', 'angular-loading-bar', 'app.controllers.login', 'app.controllers.administration',
    'app.controllers.datasources.filters',
    'app.controllers.links', 'app.controllers.oee', 'app.controllers.services',
    'app.controllers.datasources.oee',
    'app.controllers.datasources.deviation', 'app.controllers.profile', 'colorpicker.module',
    'jkuri.touchspin', 'ngIdle', 'ngSanitize', 'isteven-multi-select',
    'oc.lazyLoad', 'pascalprecht.translate', 'permission', 'restangular', 'smart-table',
    'daterangepicker',
    'ui.bootstrap', 'ui.layout', 'ui.router', 'ui.select', 'improvidus-multimetric',
    'improvidus-wordcloud', 'improvidus-correlations', 'improvidus-recommendations',
    'improvidus-filters', 'ngFitText', 'angularjs-dropdown-multiselect',
    'com.bosch.shared', 'com.bosch.account',
    'com.bosch.administration', 'com.bosch.datasources', 'com.bosch.analysis', 'ngCookies', 'angular-jwt',
    'ng-tree', 'angularCSS']);

// app.config(["$httpProvider", "$locationProvider", function($httpProvider, $locationProvider) {
//     var csrf_header = $('meta[name=_csrf-header]').attr('content');
//     var csrf_token = $('meta[name=_csrf-token]').attr('content');
//     $httpProvider.defaults.headers.common[csrf_header] = csrf_token;
//     $httpProvider.defaults.headers.common['Content-Type'] = 'application/json;charset=UTF-8';
//
// }]);

app.config(['$httpProvider', '$cookiesProvider', 'jwtOptionsProvider', 'APP_CONSTANTS',
    function Config($httpProvider, $cookiesProvider, jwtOptionsProvider, APP_CONSTANTS) {
        $cookiesProvider.defaults.expires = new Date(moment().add(30, 'day'));
        $cookiesProvider.defaults.secure = APP_CONSTANTS.SSL_ENABLED;
        var refreshPromise;
        jwtOptionsProvider.config({
            tokenGetter: ['$cookies', 'APP_CONSTANTS', 'jwtHelper', 'AuthService', '$http', '$location', '$state',
                function($cookies, APP_CONSTANTS, jwtHelper, AuthService, $http, $location, $state) {
                    if (refreshPromise) {
                        return refreshPromise;
                    }

                    var accessToken = $cookies.get(APP_CONSTANTS.OAUTH.ACCESS_TOKEN_NAME);
                    //TODO: increase offset time
                    if (!accessToken || jwtHelper.isTokenExpired(accessToken, 5)) {
                        var refreshToken = $cookies.get(APP_CONSTANTS.OAUTH.REFRESH_TOKEN_NAME);
                        if (refreshToken == null) {
                            AuthService.revokeToken();
                            if ($location.path().indexOf("/public") == -1 && $location.path().indexOf("/landing") == -1) {
                                $location.path("/public");
                                $state.go('Landing');
                            }
                            return undefined;
                        }
                        refreshPromise = AuthService.getRefreshToken({
                            grant_type: 'refresh_token',
                            refresh_token: refreshToken
                        }).then(function(response) {
                            var accessToken = response.access_token;
                            $cookies.put(APP_CONSTANTS.OAUTH.ACCESS_TOKEN_NAME, accessToken);
                            refreshPromise = null
                            return accessToken;
                        });

                        return refreshPromise

                    } else {
                        return accessToken;
                    }
                }],
            whiteListedDomains: APP_CONSTANTS.OAUTH.WHITE_LISTED_DOMAINS
        });
        $httpProvider.interceptors.push('jwtInterceptor');
    }]);

app.run([
    "$http",
    "$rootScope",
    "$translate",
    "AdminOrganizationsService",
    "ConfigurationService",
    "PermissionsService",
    "UserService",
    "AuthService",
    "PlansService",
    "PaymentService",
    "stripe",
    function($http, $rootScope, $translate, AdminOrganizationsService, ConfigurationService, PermissionsService, UserService, AuthService, PlansService, PaymentService, stripe) {

        App.init();

        $rootScope.appName = "Home";
        $rootScope.org = {};
        $rootScope.user = {};
        $rootScope.initApp = function() {
            UserService.getUser().then(function(user) {
                $rootScope.currentUser = user.plain();
                return AdminOrganizationsService.getOrganizationConfiguration(user.organization.uuid);
            }).then(function(orgConfiguration) {
                if (orgConfiguration && orgConfiguration.conf.numShifts) {
                    $rootScope.org.configuration = orgConfiguration.plain();
                } else {
                    $rootScope.org.configuration = {
                        organizationUuid: $rootScope.currentUser.organization.uuid,
                        conf: {
                            numShifts: 3
                        }
                    }
                }
                return ConfigurationService.getConfigurations({
                    resourceUuid: $rootScope.currentUser.uuid
                });
            }).then(function(userConfiguration) {
                $rootScope.user.configuration = userConfiguration[0];
                $rootScope.appReady = true;
            });

            PaymentService.getStripeKey().then(function(key) {
                stripe.setPublishableKey(key.key);
            });

            PlansService.refreshActiveSubscription();
        }
        if (AuthService.isAuthenticated())
            $rootScope.initApp();

    }
])
;

app.config(["uiSelectConfig", function(uiSelectConfig) {
    uiSelectConfig.theme = 'bootstrap';
}]);

app.config(['growlProvider', '$httpProvider', function(growlProvider, $httpProvider) {
    growlProvider.globalTimeToLive(5000);
    growlProvider.globalReversedOrder(true);
    growlProvider.onlyUniqueMessages(false);
    $httpProvider.interceptors.push(growlProvider.serverMessagesInterceptor);
}]);

app.config(['$logProvider', function($logProvider) {
    $logProvider.debugEnabled(false);
}]);

app.config(['cfpLoadingBarProvider', function(cfpLoadingBarProvider) {
    cfpLoadingBarProvider.parentSelector = '#header';
}]);

// app.config(['$locationProvider', function($locationProvider) {
//     $locationProvider.html5Mode({
//         enabled: true,
//         //requireBase: false
//     });
// }])

app.run(['$rootScope', '$state', '$window', '$cookies', '$location', "AuthService",
    function($rootScope, $state, $window, $cookies, $location, AuthService) {
        $rootScope.$on('$stateChangeSuccess', function() {
            document.body.scrollTop = document.documentElement.scrollTop = 0;
        });

        $rootScope.$on('$stateChangeStart', function(evt, to, params) {
            if ($location.$$search.passwordReset) {
                AuthService.revokeToken();
            }
            if (!AuthService.isAuthenticated() && $location.path().indexOf("/public") == -11) {
                evt.preventDefault();
                AuthService.revokeToken();
                $location.path("/public");
                $state.go('Landing');
                return;
            }
            // else if (AuthService.isAuthenticated() && $location.path().indexOf("/public") > -1) {
            //     evt.preventDefault();
            //     $location.path("/");
            //     $state.go('OEE');
            //     return;
            // }
            // else if (AuthService.isAuthenticated() && to.redirectTo) {
            //     evt.preventDefault();
            //     $state.go(to.redirectTo, params, {location: 'replace'})
            //     return;
            // }
            else if (to.redirectTo) {
                evt.preventDefault();
                $state.go(to.redirectTo, params, {location: 'replace'})
                return;
            }
        });
    }])

angular.module('app')

.constant('APP_CONSTANTS', {OAUTH:{CLIENT_ID:'improvidus',ACCESS_TOKEN_NAME:'access-token-improvidus',REFRESH_TOKEN_NAME:'refresh-token-improvidus',WHITE_LISTED_DOMAINS:['192.168.56.11','localhost','127.0.0.1','int.improvidus.com','www.improvidus.com']},WORDPRESS:{API_PATH:'https://public-api.wordpress.com/wp/v2/sites/improvidusdev.wordpress.com',ENVIRONMENT:'integration'},SSL_ENABLED:false,API_PATH:'http://127.0.0.1:8090'})

;
/*******************************************************************************
 * Copyright (c) 2015 Bosch Software Innovations GmbH. All rights reserved.
 ******************************************************************************/
app.config(["$stateProvider", "$urlRouterProvider", "$locationProvider", function($stateProvider, $urlRouterProvider, $locationProvider) {

    $urlRouterProvider.otherwise("/");

    $stateProvider.state('OEE', {
        url: "/",
        templateUrl: "app/main/oee/views/oee.html",
        controller: "OEECtrl",
        reloadOnSearch: false
    });

    $stateProvider.state('Services', {
        url: "/services",
        reloadOnSearch: false,
        templateUrl: "app/main/services/services.html",
        redirectTo: 'Services.Consulting'
    }).state('Services.Consulting', {
        url: "/consulting",
        templateUrl: "app/main/services/views/consulting.html",
        controller: "ConsultingServicesCtrl",
        reloadOnSearch: false
    });

    //     .state('Login', {
    //     url: "/login?passwordReset&userUuid&token",
    //     templateUrl: "app/main/login/views/login.html",
    //     controller: "LandingCtrl",
    //     reloadOnSearch: false,
    //     css: ['assets/css/login/style.min.css']
    // });

}]);

app.config(["$httpProvider", function($httpProvider) {
    var csrf_header = $('meta[name=_csrf-header]').attr('content');
    var csrf_token = $('meta[name=_csrf-token]').attr('content');
    $httpProvider.defaults.headers.common[csrf_header] = csrf_token;
    $httpProvider.defaults.headers.common['Content-Type']
        = 'application/json;charset=UTF-8';
}]);

app.config(["RestangularProvider", "APP_CONSTANTS", function(RestangularProvider, APP_CONSTANTS) {
    RestangularProvider.setBaseUrl(APP_CONSTANTS.API_PATH + '/api/v1');
}]);

app.factory('RestangularV2', ['Restangular', 'APP_CONSTANTS', function RestangularV2(Restangular, APP_CONSTANTS) {
    return Restangular.withConfig(function(RestangularConfigurer) {
        RestangularConfigurer.setBaseUrl(APP_CONSTANTS.API_PATH + '/api/v2');
    });
}]);

app.factory('CachedRestangular', ['Restangular', 'APP_CONSTANTS', function CachedRestangular(Restangular, APP_CONSTANTS) {
    return Restangular.withConfig(function(RestangularConfigurer) {
        RestangularConfigurer.setBaseUrl(APP_CONSTANTS.API_PATH + '/api/v2');
        RestangularConfigurer.setDefaultHttpFields({
            cache: true
        });
    });
}]);


var handleRequest = function(response, growl) {
    growl.error(response.data.message, {
        title: response.data.error
    });
    return true;
}

app.run(["Restangular", "growl", function(Restangular, growl) {
    Restangular.setErrorInterceptor(function(response) {
        handleRequest(response, growl)
    });
}])

app.run(["RestangularV2", "growl", function(RestangularV2, growl) {
    RestangularV2.setErrorInterceptor(function(response) {
        handleRequest(response, growl)
    });
}]);

app.run(["CachedRestangular", "growl", function(CachedRestangular, growl) {
    CachedRestangular.setErrorInterceptor(function(response) {
        handleRequest(response, growl)
    });
}])
app.config(["$translateProvider", function($translateProvider) {
    $translateProvider.useStaticFilesLoader({
        prefix: 'app/shared/translations/dictionaries/dictionary/languages/',
        suffix: '.json'
    });
    $translateProvider.preferredLanguage('en_US');
    $translateProvider.useSanitizeValueStrategy('escape');
}]);
var loginApp = angular.module('app.controllers.login', ['ui.router']);

loginApp.controller("LandingCtrl", ["$rootScope", "$scope", "$state", "$location", "$stateParams", "$modal", "$modalStack",
    "$http", "$log", "$window", "$timeout", "$cookies", "AuthService", "WordPressService", "APP_CONSTANTS",
    function($rootScope, $scope, $state, $location, $stateParams, $modal, $modalStack, $http, $log, $window, $timeout, $cookies,
             AuthService, WordPressService, APP_CONSTANTS) {

        $(document).ready(function() {
            App.init();
        });

        $scope.loginData = {
            username: null,
            password: null
        }

        $scope.registration = {};
        $scope.passwordReset = {};

        $scope.registrationStatus = 200;
        $scope.registrationMessage = {};

        $scope.controls = {
            registrationView: 'registration',
            processing: false,
            registrationState: 'filling',
            emailFormat: /^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,5}$/,
            passwordFormat: /^(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/
        };

        $scope.getWordPressCategoryId = function(category) {
            return WordPressService.getCategory({
                slug: category
            }).then(function(categories) {
                if (!categories || categories.length == 0) return;
                return categories[0].id
            });
        };

        $scope.getWordPressPostsByCategoryId = function(id) {
            return WordPressService.getPosts({
                categories: id
            }).then(function(posts) {
                if (!posts || posts.length == 0) return;
                var allPosts = [];
                posts.forEach(function(post) {
                    var plainText = post.content.rendered;
                    var sanitizedText = plainText.replace(/<[^>]+>/gm, '');
                    var jsonPost = JSON.parse(sanitizedText);
                    allPosts.unshift(jsonPost);
                });
                return allPosts;
            });
        };

        $scope.passwordReset = function() {
            $modalStack.dismissAll();
            $scope.controls.registrationState = 'filling';
            var opts = {
                templateUrl: "app/main/login/views/password-reset.html",
                scope: $scope,
                size: 'sm'
            };
            var instance = $modal.open(opts);
        }

        if ($location.$$search.passwordReset) $scope.passwordReset();

        $scope.passwordRecovery = function() {
            $modalStack.dismissAll();
            $scope.controls.registrationState = 'filling';
            var opts = {
                templateUrl: "app/main/login/views/password-recovery.html",
                scope: $scope,
                size: 'sm'
            };
            var instance = $modal.open(opts);
        }

        $scope.resetPassword = function() {
            $scope.controls.processing = true;
            $http({
                method: 'POST',
                url: APP_CONSTANTS.API_PATH + '/api/public/v2/passwordreset',
                params: {
                    fgUserName: $scope.passwordReset.email
                }
            }).then(function successCallback(response) {
                $scope.updateRegistrationStatus(response)
            }, function errorCallback(response) {
                $scope.updateRegistrationStatus(response);
                $log.error(response);
            });
        }

        $scope.submitResetPassword = function() {
            $scope.controls.processing = true;
            $http({
                method: 'POST',
                url: APP_CONSTANTS.API_PATH + '/api/public/v2/users/' + $location.$$search.userUuid + "/resetpassword",
                params: {
                    token: $location.$$search.token
                },
                data: {
                    password: $scope.passwordReset.password,
                    password2: $scope.passwordReset.password2
                }
            }).then(function successCallback(response) {
                $scope.updateRegistrationStatus(response)
            }, function errorCallback(response) {
                $scope.updateRegistrationStatus(response);
                $log.error(response);
            });
        }

        $scope.login = function() {
            $modalStack.dismissAll();
            $scope.controls.registrationState = 'filling';
            var opts = {
                templateUrl: "app/main/login/views/logon.html",
                scope: $scope,
                size: 'sm'
            };
            var instance = $modal.open(opts);
            instance.result.then(function() {
                // Success
            }, function() {
                $scope.loginError = false;
            })
        }


        $scope.orgForm = {};
        $scope.register = function() {
            $modalStack.dismissAll();
            $scope.controls.registrationState = 'filling';
            var opts = {
                templateUrl: "app/main/login/views/register.html",
                scope: $scope,
                size: 'md'
            };
            var instance = $modal.open(opts);
            instance.result.then(function() {
                // Success
            }, function() {
                $scope.loginError = false;
            })
        }


        $scope.updateRegistrationStatus = function(response) {
            $scope.controls.processing = false;
            switch (response.data.exception) {
                case "com.bosch.im.service.InvalidPasswordException":
                    $scope.controls.registrationState = 'duplicatePasswordError'
                    break;
                case "com.bosch.improvidus.passwordreset.ExpiredTokenException":
                    $scope.controls.registrationState = 'expiredTokenError'
                    break;
                case "com.bosch.improvidus.web.exceptions.DuplicatedEntityException":
                    $scope.controls.registrationState = 'duplicateOrganizationError'
                    $scope.controls.errorMessage = "An Organization with the same name already exists.";
                    break;
                case "com.bosch.improvidus.user.DuplicatedUsernameException":
                    $scope.controls.registrationState = 'duplicateUserError'
                    $scope.controls.errorMessage = "Email address is already in use.";
                    break;
                default:
                    $scope.controls.registrationState = 'success'
            }
        }

        $scope.registerSubmit = function() {
            $scope.orgForm.user.username = $scope.orgForm.user.email;
            $scope.orgForm.org.name = $scope.orgForm.org.displayName.toUpperCase().split(" ")
                .join("_");
            $scope.controls.processing = true;
            $http({
                method: 'POST',
                url: APP_CONSTANTS.API_PATH + '/api/public/v2/organization',
                data: $scope.orgForm,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function successCallback(response) {
                $scope.controls.processing = false;
                $scope.updateRegistrationStatus(response);
                $scope.orgForm = {};
            }, function errorCallback(response) {
                $scope.controls.processing = false;
                $scope.updateRegistrationStatus(response);
                $log.error(response);
            });

        }

        function delayLogin() {
            $scope.controls.delay = $scope.controls.delay - 1;
            if ($scope.controls.delay <= 0) {
                $scope.controls.error = undefined;
                return;
            }
            $timeout(delayLogin, 1000);
            $scope.controls.error = "Exceeded maximum login attempts. Please wait "
                + $scope.controls.delay + " seconds before next attempt.";
        }

        $scope.close = function() {
            $modalStack.dismissAll();
        }

        function showCookieWarning() {
            $modalStack.dismissAll();
            var opts = {
                templateUrl: "app/main/login/views/cookie-warning.html",
                scope: $scope,
                size: 'md'
            };
            var instance = $modal.open(opts);
        }

        $scope.doLogin = function() {
            if ($scope.controls.delay > 0) return;

            AuthService.getNewToken({
                username: $scope.loginData.username,
                password: $scope.loginData.password
            }).then(function(token) {
                $modalStack.dismissAll();
                // console.log(response);
                // var tokenPayload = jwtHelper.decodeToken(response.data.access_token);
                // console.log(tokenPayload);
                $cookies.put(APP_CONSTANTS.OAUTH.ACCESS_TOKEN_NAME, token.access_token);
                $cookies.put(APP_CONSTANTS.OAUTH.REFRESH_TOKEN_NAME, token.refresh_token);
                if (!$cookies.get(APP_CONSTANTS.OAUTH.ACCESS_TOKEN_NAME))
                    showCookieWarning();
                else {
                    $rootScope.initApp();
                    $scope.selectState('/');
                }
            }, function(error) {
                var description = error.data.error_description;
                if (description && description.indexOf("The login attempts exceed the maximum limit") > -1) {
                    $scope.controls.delay = parseInt(description.replace(/^\D+|\D+$/g, "")) + 2;
                    delayLogin();
                }
                else
                    $scope.controls.error = "Invalid Username or Password."
            });
        }
    }]);

loginApp.directive('passMatch', function() {
    return {
        require: 'ngModel',
        link: function(scope, elm, attrs, ctrl) {
            var originPass = '#' + attrs.passMatch;
            elm.add(originPass).on('keyup', function() {
                scope.$apply(function() {
                    var v = elm.val() === $(originPass).val();
                    ctrl.$setValidity('passMatch', v);
                });
            });
        }
    }
})
// /*******************************************************************************
//  * Copyright (c) 2015 Bosch Software Innovations GmbH. All rights reserved.
//  ******************************************************************************/
app.config(["$stateProvider", function($stateProvider) {
    $stateProvider.state('Landing', {
        url: "/public",
        templateUrl: "app/main/login/views/login.html",
        reloadOnSearch: false,
        redirectTo: 'Landing.Home'
    });

    $stateProvider.state('Landing.Home', {
        url: "/home?passwordReset&userUuid&token",
        templateUrl: "app/main/login/home/views/home.html",
        controller: "LandingHomeCtrl",
        reloadOnSearch: false,
        css: ['assets/css/login/style.min.css', 'assets/css/landing/landing.css']
    }).state('Landing.Services', {
        url: "/services",
        reloadOnSearch: false,
        templateUrl: "app/main/login/services/views/services.html",
        controller: "LandingCtrl",
        css: ['assets/css/login/style.min.css', 'assets/css/landing/landing.css']
    }).state('Landing.Trainings', {
        url: "/services/trainings?category",
        reloadOnSearch: false,
        templateUrl: "app/main/login/services/views/trainings.html",
        controller: "LandingServicesCtrl",
        css: ['assets/css/login/style.min.css', 'assets/css/landing/landing.css']
    }).state('Landing.Solutions', {
        url: "/solutions",
        reloadOnSearch: false,
        templateUrl: "app/main/login/solutions/views/solutions.html",
        controller: "LandingSolutionsCtrl",
        css: ['assets/css/login/style.min.css', 'assets/css/landing/landing.css']
    }).state('Landing.OEE', {
        url: "/oee",
        reloadOnSearch: false,
        templateUrl: "app/main/login/oee/views/oee.html",
        controller: "LandingOEECtrl",
        css: ['assets/css/login/style.min.css', 'assets/css/landing/landing.css']
    }).state('Landing.Company', {
        url: "/company",
        reloadOnSearch: false,
        templateUrl: "app/main/login/company/views/company.html",
        controller: "LandingCompanyCtrl",
        css: ['assets/css/login/style.min.css', 'assets/css/landing/landing.css']
    })
    //     .state('Landing.Team', {
    //     url: "/company/team",
    //     reloadOnSearch: false,
    //     templateUrl: "app/main/login/company/views/team.html",
    //     controller: "LandingCompanyCtrl",
    //     css: ['assets/css/login/style.min.css', 'assets/css/landing/landing.css']
    // })
        .state('Landing.Contact', {
            url: "/contact",
            reloadOnSearch: false,
            templateUrl: "app/main/login/contact/views/contact.html",
            controller: "LandingContactCtrl",
            css: ['assets/css/login/style.min.css', 'assets/css/landing/landing.css']
        })
}]);

loginApp.factory('AuthService', ['$cookies', "RestangularV2", "APP_CONSTANTS",
    function AuthService($cookies, RestangularV2, APP_CONSTANTS) {
        return {
            isAuthenticated: function() {
                return $cookies.get(APP_CONSTANTS.OAUTH.ACCESS_TOKEN_NAME) && $cookies.get(APP_CONSTANTS.OAUTH.REFRESH_TOKEN_NAME);
            },
            revokeToken: function() {
                $cookies.remove(APP_CONSTANTS.OAUTH.ACCESS_TOKEN_NAME);
                $cookies.remove(APP_CONSTANTS.OAUTH.REFRESH_TOKEN_NAME);
            },
            getNewToken: function(options) {
                var data = $.param(options);
                return RestangularV2.one('oauth').one('token').customPOST(data, '', {
                    "grant_type": "password"
                }, {
                    "Authorization": "Basic " + btoa(APP_CONSTANTS.OAUTH.CLIENT_ID + ":"),
                    'Content-Type': "application/x-www-form-urlencoded"
                })
            },
            getRefreshToken: function(options) {
                var data = $.param(options);
                return RestangularV2.one('oauth').one('token').withHttpConfig({skipAuthorization: true}).customPOST(data, '', {}, {
                    "Authorization": "Basic " + btoa(APP_CONSTANTS.OAUTH.CLIENT_ID + ":"),
                    'Content-Type': "application/x-www-form-urlencoded"
                })
            }
        };
    }]);

loginApp.factory('EmailService', ["Restangular", "APP_CONSTANTS",
    function EmailService(Restangular, APP_CONSTANTS) {
        return {
            sendContactForm: function(email, options) {
                return Restangular.withConfig(function(RestangularConfigurer) {
                    RestangularConfigurer.setBaseUrl(APP_CONSTANTS.API_PATH + '/api/public');
                }).one('contact').customPOST(email, "", options, {});
            }
        };
    }]);

loginApp.factory('YouTubeService', ["Restangular",
    function YouTubeService(Restangular) {
        return {
            getVideoInfo: function(options) {
                return Restangular.withConfig(function(RestangularConfigurer) {
                    RestangularConfigurer.setBaseUrl('https://content.googleapis.com/youtube/v3');
                }).one('videos').customGET("", options);
            }
        };
    }]);

loginApp.factory('WordPressService', ["$q", "CachedRestangular", "APP_CONSTANTS",
    function WordPressService($q, CachedRestangular, APP_CONSTANTS) {
        var environmentId;

        function refreshEnvironmentId() {
            var deferred = $q.defer();
            if (environmentId) deferred.resolve();
            else {
                CachedRestangular.withConfig(function(RestangularConfigurer) {
                    RestangularConfigurer.setBaseUrl(APP_CONSTANTS.WORDPRESS.API_PATH);
                }).one('tags').customGET("", {
                    slug: APP_CONSTANTS.WORDPRESS.ENVIRONMENT
                }).then(function(tags) {
                    environmentId = tags[0].id;
                    deferred.resolve();
                })
            }

            return deferred.promise;
        }

        return {
            getPosts: function(options) {
                return refreshEnvironmentId().then(function() {
                    options.tags = environmentId;
                    options.per_page = 100;
                    return CachedRestangular.withConfig(function(RestangularConfigurer) {
                        RestangularConfigurer.setBaseUrl(APP_CONSTANTS.WORDPRESS.API_PATH);
                    }).one('posts').customGET("", options);
                })
            },
            getCategory: function(options) {
                return CachedRestangular.withConfig(function(RestangularConfigurer) {
                    RestangularConfigurer.setBaseUrl(APP_CONSTANTS.WORDPRESS.API_PATH);
                }).one('categories').customGET("", options);
            }
        };
    }]);
loginApp.controller("LandingHomeCtrl", ["$rootScope", "$scope", "$controller",
    function($rootScope, $scope, $controller) {
        angular.extend(this, $controller('LandingCtrl', {
            $scope: $scope
        }));

        $scope.data = {
            slides: []
        };

        $scope.getWordPressCategoryId("carousel").then(function(id) {
            $scope.getWordPressPostsByCategoryId(id).then(function(posts) {
                $scope.data.slides = posts;
            })
        });
    }]);
loginApp.controller("LandingServicesCtrl", ["$rootScope", "$scope", "$controller", "$http", "$window", "$stateParams",
    function($rootScope, $scope, $controller, $http, $window, $stateParams) {
        angular.extend(this, $controller('LandingCtrl', {
            $scope: $scope
        }));

        $scope.data = {
            selectedTraining: null,
            trainings: []
        };

        $scope.controls.showAllTrainings = false;

        $scope.scrollToTop = function() {
            $('#content').animate({
                scrollTop: $('#content').offset().top + 300
            }, 0);
        };

        $scope.isArray = function(array) {
            return Array.isArray(array);
        }

        $scope.loadTrainings = function(listName) {
            $scope.getWordPressCategoryId(listName).then(function(id) {
                $scope.getWordPressPostsByCategoryId(id).then(function(posts) {
                    $scope.data.trainings = posts;
                })
            });
        };

        $scope.initTrainings = function() {
            switch ($stateParams.category) {
                case "leadership":
                    $scope.loadTrainings('leadership');
                    $scope.data.trainingCategory = "Leadership Trainings";
                    break;
                case "lean":
                    $scope.loadTrainings('lean');
                    $scope.data.trainingCategory = "Lean Trainings";
                    break;
                case "manufacturing":
                default :
                    $scope.loadTrainings('manufacturing');
                    $scope.data.trainingCategory = "Manufacturing Trainings";
                    break;
            }
        };
        $scope.initTrainings();
    }]);
loginApp.controller("LandingSolutionsCtrl", ["$rootScope", "$scope", "$controller", "$stateParams",
    function($rootScope, $scope, $controller, $stateParams) {
        angular.extend(this, $controller('LandingCtrl', {
            $scope: $scope
        }));

        var solutions = {
            paretos: {
                images: ["mmparetos.png"],
                title: "Multi-Metric Pareto and Recommendations",
                description: "Allows for easy visualization, filtering, and drill down for your deviation data set. These tools highlight opportunities, guide the user and provide recommendations by looking for filtering options within the data that give a \"line of best fit\" following pareto rules."
            },
            correlations: {
                images: ["correlations.png"],
                title: "Event Correlation Tool",
                description: "Provides insights into correlating events and time between those events. This enables users to address the root cause of an issue that is further upstream in the process rather than focusing on the symptoms that show up downstream. This powerful tool will open up new and improved transparency to pinpoint and drive Connected Industry investments and improvements using the most efficient approach."
            },
            wordcloud: {
                images: ["wordcloud.png"],
                title: "Word Cloud",
                description: "Associates word usage with impact to help find systemic issues and is especially useful if you have access to free text inputs from operators/technicians and impacts (hours, cost, etc.), but do not have highly organized deviation data."
            },
            oee: {
                images: ["tutorials.jpg"],
                title: "Overall Equipment Effectiveness (OEE) Visualization Tool",
                description: "Shows you equipment availability, production output, and deviation information. Users are able to filter and navigate based on their specific areas of responsibility. This is an outstanding tool for making sound decisions for Connected Industry investments. The OEE tool is available at no cost."
            }
        };

        // $scope.selectSolution = function(solution) {
        //     switch (solution) {
        //         case "paretos":
        //             $scope.data.selectedSolution = solutions.paretos;
        //             break;
        //         case "correlations":
        //             $scope.data.selectedSolution = solutions.correlations;
        //             break;
        //         case "wordcloud":
        //             $scope.data.selectedSolution = solutions.wordcloud;
        //             break;
        //         case "oee":
        //             $scope.data.selectedSolution = solutions.oee;
        //             break;
        //         default :
        //             $scope.data.selectedSolution = null;
        //             break;
        //     }
        // };
        // $scope.selectSolution($stateParams.solution);
    }]);
loginApp.controller("LandingOEECtrl", ["$rootScope", "$scope", "$controller", "$sce", "$http", "YouTubeService",
    function($rootScope, $scope, $controller, $sce, $http, YouTubeService) {
        angular.extend(this, $controller('LandingCtrl', {
            $scope: $scope
        }));

        var videoBase = "https://www.youtube.com/embed/";
        var youtubeKey = "AIzaSyDsNWtmh8rw5czPIo58lp2UuH_OKkjEuFA";

        $scope.controls.showAllVideos = false;

        $scope.data = {
            embedLinks: [],
            videos: []
        };

        $scope.getWordPressCategoryId("videos").then(function(id) {
            $scope.getWordPressPostsByCategoryId(id).then(function(posts) {
                $scope.data.videos = posts;
                var videoIds = $scope.data.videos.map(function(video) {
                    return video.id;
                }).join();
                $scope.getYoutubeInfo(videoIds);
            })
        });

        $scope.getYoutubeInfo = function(videoIds) {
            YouTubeService.getVideoInfo({
                id: videoIds,
                part: 'snippet,statistics',
                key: youtubeKey
            }).then(function(youtubeVideos) {
                if (!youtubeVideos) return;
                for (var i = 0; i < $scope.data.videos.length; i++) {
                    $scope.data.videos[i].youtube = youtubeVideos.items[i];
                    $scope.data.embedLinks.push($sce.trustAsResourceUrl(videoBase + $scope.data.videos[i].id));
                }
            });
        }
    }]);
loginApp.controller("LandingCompanyCtrl", ["$rootScope", "$scope", "$controller",
    function($rootScope, $scope, $controller) {
        angular.extend(this, $controller('LandingCtrl', {
            $scope: $scope
        }));
    }]);
loginApp.controller("LandingContactCtrl", ["$rootScope", "$scope", "$controller", "$timeout", "growl", "EmailService",
    function($rootScope, $scope, $controller, $timeout, growl, EmailService) {
        angular.extend(this, $controller('LandingCtrl', {
            $scope: $scope
        }));

        $scope.controls.processing = false;

        $scope.sendMessage = function(form) {
            $scope.controls.processing = true;

            EmailService.sendContactForm({
                from: form.email,
                to: "",
                body: form.message,
                subject: "Improvidus Contact Form"
            }, {
                company: form.company,
                email: form.email,
                message: form.message,
                username: form.username,
                phone: form.phone,
                website: form.website
            }).then(function() {
                growl.info('Your message has been sent successfully.', {
                    title: 'Message Sent'
                });
                $scope.contactForm = {};
                $scope.contactUs.$setPristine(true);
                $scope.controls.processing = false;
            }, function(error) {
                growl.error('There was a problem sending your message. Please try again.', {
                    title: 'Message Not Sent'
                });
                $scope.controls.processing = false;
            });
        }

    }]);
app.factory('TranslationService', ['Restangular',
    function TranslationService(Restangular) {

      var languages = [{
        name: 'en_US',
        countryName: 'United States',
        languageName: 'English',
        flagCode: 'us'
      }, {
        name: 'es_ES',
        countryName: 'España',
        languageName: 'Español',
        flagCode: 'es'
      }];

      return {
        getAvailableLocales: function() {
          return languages;
        }
      };

    }]);
app.factory('ConfigurationService', [
    'RestangularV2',
    function ConfigurationService(RestangularV2) {
      return {
        getConfiguration: function(uuid) {
          return RestangularV2.one('configurations', uuid).get();
        },
        getConfigurations: function(options) {
          return RestangularV2.one('configurations').get(options);
        },
        addConfiguration: function(configuration) {
          return RestangularV2.one('configurations').customPOST(configuration, '', {}, {});
        },
        updateConfiguration: function(configuration, resourceUuid) {
          if (configuration.uuid) {
            return RestangularV2.one('configurations', configuration.uuid).customPUT(configuration,
                '', {}, {});
          } else {
            configuration.resourceUuid = resourceUuid;
            return this.addConfiguration(configuration);
          }
        },
        deleteConfiguration: function(uuid) {
          return RestangularV2.one('configurations', uuid).remove();
        },
        deleteConfiguration2: function(uuid) {
          return RestangularV2.one('configurations', uuid).remove();
        }
      };
    }]);
app.factory('UsersAdministrationService', ['$q', 'RestangularV2', 'PermissionsService',
    function UsersAdministrationService($q, RestangularV2, PermissionsService) {

      var users = [];

      return {
        addUser: function(user, options) {
          return RestangularV2.one('users').customPOST(user, '', options);
        },
        getUser: function(user) {
          return RestangularV2.one('user').get();
        },
        getUsers: function() {
          return RestangularV2.one('users').get();
        },
        getOrganizationUsers: function(uuid) {
          return RestangularV2.one('organization', uuid).one('users').get();
        },
        getUserGroupUsers: function(uuid) {
          return RestangularV2.one('usergroups', uuid).one('users').get();
        },
        updateUser: function(uuid, user) {
          return RestangularV2.one('users', uuid).customPUT(user);
        },
        deleteUser: function(uuid) {
          return RestangularV2.one('users', uuid).remove();
        },
        assignPermissions: function(userUuid, uuids) {
          return RestangularV2.one('users', userUuid).one('permissions').put({
            "uuids": uuids
          });
        },
        unassignPermissions: function(userUuid, uuids) {
          return RestangularV2.one('users', userUuid).one('permissions').remove({
            "uuids": uuids
          });
        },
        assignRoles: function(userUuid, uuids) {
          return RestangularV2.one('users', userUuid).one('roles').put({
            "uuids": uuids
          });
        },
        unassignRoles: function(userUuid, uuids) {
          return RestangularV2.one('users', userUuid).one('roles').remove({
            "uuids": uuids
          });
        }
      };
    }]);
app.factory('PermissionsService', ['$rootScope', '$q', 'RestangularV2', 'ACLEntriesService',
    function PermissionsService($rootScope, $q, RestangularV2, ACLEntriesService) {
      return {
        deferred: null,

        getPermissions: function() {
          return RestangularV2.one('permissions').get();
        },
        getUserPermissions: function(uuid) {
          return RestangularV2.one('users', uuid).one('permissions').get();
        },

        checkEntityPermissions: function(uuid, validPermissions) {
          var deferred = $q.defer();
          ACLEntriesService.getUserResourceACLEntries($rootScope.currentUser.uuid, {
            resourceUuid: uuid
          }).then(function(aclEntries) {
            aclEntries.forEach(function(entry) {
              if ($.inArray(entry.action, validPermissions) > -1) deferred.resolve();
            });
            deferred.reject();
          }, function(err) {
            deferred.reject();
            $log.error(err);
          });
          return deferred.promise;
        },

        checkPermissions: function(permission) {
          if (!$rootScope.user.permissions) {
            this.deferred = $q.defer();
            this.deferred.permission = permission;
            return this.deferred.promise;
          }
          if ($.inArray("ROLE_ADMIN", $rootScope.currentUser.authorities) > -1) return true;
          return this.intersect($rootScope.user.permissions, permission).length > 0;
        },

        intersect: function intersect(a, b) {
          var t;
          if (b.length > a.length) t = b, b = a, a = t;
          return a.filter(function(e) {
            if (b.indexOf(e.uuid) !== -1) return true;
          });
        }
      };
    }]);

app.factory('ACLEntriesService', ['RestangularV2', function ACLEntriesService(RestangularV2) {
  return {
    getUserResourceACLEntries: function(uuid, options) {
      return RestangularV2.one('users', uuid).one('aclentries').get(options);
    },
    getResourceACLEntries: function(options) {
      return RestangularV2.one('aclentries').get(options);
    },
    addResourceACLEntry: function(aclEntry) {
      return RestangularV2.one('aclentries').customPOST(aclEntry, '', {}, {});
    },
    updateResourceACLEntry: function(uuid, aclEntry) {
      return RestangularV2.one('aclentries', uuid).customPUT(aclEntry);
    },
    removeResourceACLEntry: function(uuid) {
      return RestangularV2.one('aclentries', uuid).remove();
    },
    removeResourceACLEntryBySubject: function(subjectUuid, objectUuid) {
      return RestangularV2.one('aclentries').remove({
        subjectUuid: subjectUuid,
        objectUuid: objectUuid
      });
    },
  };
}]);
app.factory('UserService', ['RestangularV2', function UserService(RestangularV2) {
  return {
    getUser: function(uuid) {
      if (uuid)
        return RestangularV2.one('users', uuid).get();
      else
        return RestangularV2.one('user').get();
    },
    getUsers: function() {
      return RestangularV2.one('users').get();
    }
  };
}]);
/*******************************************************************************
 * Copyright (c) 2015 Bosch Software Innovations GmbH. All rights reserved.
 ******************************************************************************/
app.directive('dashboardDraggable', function() {
  return {
    restrict: 'A',
    link: function(scope, element, attrs) {
      element.draggable({
        cursor: "move",
        handle: "div",
        stop: function(event, ui) {
          scope[attrs.xpos] = ui.position.left;
          scope[attrs.ypos] = ui.position.top;
          scope.$apply();
        }
      });
    }
  };
});

app
    .directive(
        'dropdownMultiselect',
        function() {
          return {
            restrict: 'E',
            scope: {
              model: '=',
              options: '=',
              pre_selected: '=preSelected'
            },
            template: "<div class='btn-group columns-selector' data-ng-class='{open: open}'>"
                + "<button class='btn btn-xs btn-default no-background'>Select Columns</button>"
                + "<button class='btn btn-xs btn-default no-background dropdown-toggle' data-toggle='dropdown'><span class='caret'></span></button>"
                + "<ul class='dropdown-menu' aria-labelledby='dropdownMenu'>"
                + "<li><a data-ng-click='selectAll()'><i class='icon-ok-sign'></i>  Check All</a></li>"
                + "<li><a data-ng-click='deselectAll();'><i class='icon-remove-sign'></i>  Uncheck All</a></li>"
                + "<li class='divider'></li>"
                + "<li data-ng-repeat='option in options'> <a data-ng-click='setSelectedItem()'>{{option.title}}<span data-ng-class='isChecked(option.field)'></span></a></li>"
                + "</ul>" + "</div>",
            controller: ["$scope", function($scope) {

              $scope.openDropdown = function() {
                $scope.selected_items = [];
                for (var i = 0; i < $scope.pre_selected.length; i++) {
                  $scope.selected_items.push($scope.pre_selected[i].field2);
                }
              };

              $scope.selectAll = function() {
                $scope.model = _.pluck($scope.options, 'field');
              };

              $scope.selectAll();

              $scope.deselectAll = function() {
                $scope.model = [];
              };

              $scope.setSelectedItem = function() {
                var field = this.option.field;
                if (_.contains($scope.model, field)) {
                  $scope.model = _.without($scope.model, field);
                } else {
                  $scope.model.push(field);
                }
                return false;
              };
              $scope.isChecked = function(field) {
                if (_.contains($scope.model, field)) { return 'glyphicon glyphicon-ok pull-right'; }
                return false;
              };
            }]
          };
        });

app.directive('fileModel', ['$parse', function($parse) {
  return {
    restrict: 'A',
    link: function(scope, element, attrs) {
      var model = $parse(attrs.fileModel);
      var modelSetter = model.assign;

      element.bind('change', function() {
        scope.$apply(function() {
          modelSetter(scope, element[0].files[0]);
        });
      });
    }
  };
}]);

app.service('fileUpload', ['$http', function($http) {
  this.uploadFileToUrl = function(file, uploadUrl) {
    var fd = new FormData();
    fd.append('file', file);
    $http.post(uploadUrl, fd, {
      transformRequest: angular.identity,
      headers: {
        'Content-Type': undefined
      }
    }).success(function() {
    }).error(function() {
    });
  }
}]);

app.directive('file', function() {
  return {
    scope: {
      file: '='
    },
    link: function(scope, el, attrs) {
      el.bind('change', function(event) {
        var files = event.target.files;
        var file = files[0];
        scope.file = file ? file : undefined;
        scope.$apply();
      });
    }
  };
});

app
    .directive(
        'pageSelect',
        function() {
          return {
            restrict: 'E',
            template: '<input type="text" class="select-page input-xs" ng-model="inputPage" ng-change="selectPage(inputPage)">',
            link: function(scope, element, attrs) {
              scope.$watch('currentPage', function(c) {
                scope.inputPage = c;
              });
            }
          }
        });

app.directive('dashboardDraggable', function() {
  return {
    restrict: 'A',
    link: function(scope, element, attrs) {
      element.draggable({
        cursor: "move",
        handle: "div",
        stop: function(event, ui) {
          scope[attrs.xpos] = ui.position.left;
          scope[attrs.ypos] = ui.position.top;
          scope.$apply();
        }
      });
    }
  };
});

app.directive('csSelect', function() {
  return {
    require: '^stTable',
    template: '<input type="checkbox"/>',
    scope: {
      row: '=csSelect'
    },
    link: function(scope, element, attr, ctrl) {

      element.bind('change', function(evt) {
        scope.$apply(function() {
          ctrl.select(scope.row, 'multiple');
        });
      });

      scope.$watch('row.isSelected', function(newValue, oldValue) {
        if (newValue === true) {
          element.parent().addClass('st-selected');
        } else {
          element.parent().removeClass('st-selected');
        }
      });
    }
  };
});

app.directive('stSelectAll', function() {
  return {
    restrict: 'E',
    template: '<input type="checkbox" ng-model="isAllSelected" />',
    scope: {
      all: '='
    },
    link: function(scope, element, attr) {
      scope.$watch('isAllSelected', function() {
        if (!scope.all) return;
        scope.all.forEach(function(val) {
          val.isSelected = scope.isAllSelected;
        });
      });
      scope.$watch('all', function(newVal, oldVal) {
        if (oldVal) {
          oldVal.forEach(function(val) {
            val.isSelected = false;
          });
        }
        scope.isAllSelected = false;
      });
    }
  }
});

app.directive('stSelection', function() {
  return {
    scope: {
      selection: '=stSelection',
      collection: '=stTable'
    },
    link: function(scope, element, attr) {
      scope.$watch('collection', function() {
        scope.selection = $.grep(scope.collection || [], function(item) {
          return item.isSelected;
        })
      }, true);
    }
  }
});

app.directive('compile', ['$compile', '$state', function($compile, $state) {
  return function(scope, element, attrs) {
    scope.loggMsg = function(uuid) {
      $state.go('Notifications', {
        uuid: uuid
      });
    };
    scope.$watch(function(scope) {
      // console.log(scope.$eval(attrs.compile).toString());
      return scope.$eval(attrs.compile).toString();
    }, function(value) {
      element.html(value);
      $compile(element.contents())(scope);
    })
  };
}]);

app.directive('bindHtmlCompile', ['$compile', function($compile) {
  return {
    restrict: 'A',
    link: function(scope, element, attrs) {
      scope.$watch(function() {
        return scope.$eval(attrs.bindHtmlCompile);
      }, function(value) {
        // In case value is a TrustedValueHolderType, sometimes it
        // needs to be explicitly called into a string in order to
        // get the HTML string.
        element.html(value && value.toString());
        // If scope is provided use it, otherwise use parent scope
        var compileScope = scope;
        if (attrs.bindHtmlScope) {
          compileScope = scope.$eval(attrs.bindHtmlScope);
        }
        $compile(element.contents())(compileScope);
      });

      scope.select = function() {
        console.log("Hello")
      }

    }
  };
}]);

app.directive('idWatch', [function() {
  return {
    scope: {
      id: '@',
    },
    controller: function($scope, $element, $attrs) {
      $attrs.$observe('id', function(id) {
        if ($scope.$parent.onElementIdChange) $scope.$parent.onElementIdChange($element);
      });
    }
  }
}]);

app.directive('tooltip', [function() {
  return {
    compile: function(scope, elem, attrs) {
      $(document).ready(function() {
        $(elem).tooltip();
      });
    }
  }
}]);

app.directive('ionslider', ['$timeout', function($timeout) {
  return {
    restrict: 'EA',
    require: '^ngModel',
    scope: {
      min: '=',
      max: '=',
      type: '@',
      prefix: '@',
      maxPostfix: '@',
      prettify: '=',
      grid: '@',
      gridMargin: '@',
      postfix: '@',
      step: '@',
      hideMinMax: '@',
      hideFromTo: '@',
      from: '=',
      disable: '=',
      onChange: '=',
      onFinish: '=',
      ngModel: '='
    },
    template: '<div></div>',
    replace: true,
    link: function(scope, element, attrs, ngModel) {
      (function init() {
        $(element).ionRangeSlider({
          min: scope.min,
          max: scope.max,
          type: scope.type,
          prefix: scope.prefix,
          maxPostfix: scope.maxPostfix,
          prettify: scope.prettify,
          grid: scope.grid,
          gridMargin: scope.gridMargin,
          postfix: scope.postfix,
          step: scope.step,
          hideMinMax: scope.hideMinMax,
          hideFromTo: scope.hideFromTo,
          from: scope.from,
          to: scope.to,
          disable: scope.disable,
          onChange: scope.onChange,
          onFinish: setSalaryRange
        });
      })();

      function setSalaryRange() {
        scope.$evalAsync(function() {
          var values = $(element).prop('value');
          ngModel.$setViewValue(values);
        });
      }

      scope.$watch('min', function(value) {
        $timeout(function() {
          $(element).data("ionRangeSlider").update({
            min: value
          });
        });
      }, true);
      scope.$watch('max', function(value) {
        $timeout(function() {
          $(element).data("ionRangeSlider").update({
            max: value
          });
        });
      });
      scope.$watch('from', function(value) {
        $timeout(function() {
          $(element).data("ionRangeSlider").update({
            from: value
          });
        });
      });
      scope.$watch('to', function(value) {
        $timeout(function() {
          $(element).data("ionRangeSlider").update({
            to: value
          });
        });
      });
      scope.$watch('disable', function(value) {
        $timeout(function() {
          $(element).data("ionRangeSlider").update({
            disable: value
          });
        });
      });
      ngModel.$render = function() {
        var options = {
          from: ngModel.$viewValue
        };
        $(element).data("ionRangeSlider").update(options);
      };
    }
  };
}]);

app.directive('ionslider2', [
    '$timeout',
    function($timeout) {
      return {
        restrict: 'EA',
        require: '^ngModel',
        scope: {
          min: '=',
          max: '=',
          type: '@',
          to: '=',
          prefix: '@',
          maxPostfix: '@',
          prettify: '=',
          grid: '@',
          gridMargin: '@',
          postfix: '@',
          step: '@',
          hideMinMax: '@',
          hideFromTo: '@',
          from: '=',
          disable: '=',
          onChange: '=',
          onFinish: '=',
          value: '=ngModel'

        },
        template: '<div></div>',
        replace: true,
        link: function(scope, element, attrs, modelCtrl) {
          var $slider;

          (function init() {
            $(element).ionRangeSlider({
              min: scope.min,
              max: scope.max,
              type: scope.type,
              prefix: scope.prefix,
              maxPostfix: scope.maxPostfix,
              prettify: scope.prettify,
              grid: scope.grid,
              gridMargin: scope.gridMargin,
              postfix: scope.postfix,
              step: scope.step,
              hideMinMax: scope.hideMinMax,
              hideFromTo: scope.hideFromTo,
              to: scope.to,
              from: scope.from,
              disable: scope.disable,
              onChange: scope.onChange,
              onFinish: setSalaryRange
            });
          })();

          function setSalaryRange() {
            scope.$evalAsync(function() {
              var values = (attrs.type == 'single') ? $(element).prop('value') : $(element).prop(
                  'value').split(';');
              modelCtrl.$setViewValue(values);
            });
          }

          scope.$watch('value', function(value) {
            if (attrs.type == 'single') {
              $(element).ionRangeSlider("update", {
                from: value
              });
            } else { // double
              if (value[0] == scope.max && value[0] > 0) {
                // avoid getting stuck with both from & to == max
                value[0]--;
              }
              $(element).ionRangeSlider("update", {
                from: value[0],
                to: value[1]
              });
            }
          }, true);

          scope.$watch('min', function(value) {
            $(element).ionRangeSlider("update", {
              min: value
            });
          }, true);
          scope.$watch('max', function(value) {
            $timeout(function() {
              $(element).ionRangeSlider("update", {
                max: value
              });
            });
          });
          scope.$watch('disable', function(value) {
            $timeout(function() {
              $(element).ionRangeSlider("update", {
                disable: value
              });
            });
          });
        }
      };
    }]);

app.directive('uiSwitch', ['$window', '$timeout', '$log', '$parse',
    function($window, $timeout, $log, $parse) {

      function linkSwitchery(scope, elem, attrs, ngModel) {
        if (!ngModel) return false;
        var options = {};
        try {
          options = $parse(attrs.uiSwitch)(scope);
        } catch (e) {
        }

        var switcher;

        attrs.$observe('disabled', function(value) {
          if (!switcher) { return; }

          if (value) {
            switcher.disable();
          } else {
            switcher.enable();
          }
        });

        function initializeSwitch() {
          $timeout(function() {
            // Remove any old switcher
            if (switcher) {
              angular.element(switcher.switcher).remove();
            }
            // (re)create switcher to reflect latest state of the checkbox
            // element
            switcher = new $window.Switchery(elem[0], options);
            var element = switcher.element;
            element.checked = scope.initValue;
            if (attrs.disabled) {
              switcher.disable();
            }

            switcher.setPosition(false);
            element.addEventListener('change', function(evt) {
              scope.$apply(function() {
                ngModel.$setViewValue(element.checked);
              })
            })

            scope.$watch("initValue", function(newValue) {
              ngModel.$setViewValue(newValue);
            });
          }, 0);
        }
        initializeSwitch();
      }

      return {
        require: 'ngModel',
        restrict: 'AE',
        scope: {
          initValue: '=ngModel'
        },
        link: linkSwitchery
      }
    }]);

app.directive('switch', function() {
  return {
    restrict: 'AE',
    replace: true,
    transclude: true,
    template: function(element, attrs) {
      var html = '';
      html += '<span';
      html += ' class="switch' + (attrs.class ? ' ' + attrs.class : '') + '"';
      html += attrs.ngModel ? ' ng-click="' + attrs.disabled + ' ? ' + attrs.ngModel + ' : '
          + attrs.ngModel + '=!' + attrs.ngModel
          + (attrs.ngChange ? '; ' + attrs.ngChange + '()"' : '"') : '';
      html += ' ng-class="{ checked:' + attrs.ngModel + ', disabled:' + attrs.disabled + ' }"';
      html += '>';
      html += '<small></small>';
      html += '<input type="checkbox"';
      html += attrs.id ? ' id="' + attrs.id + '"' : '';
      html += attrs.name ? ' name="' + attrs.name + '"' : '';
      html += attrs.ngModel ? ' ng-model="' + attrs.ngModel + '"' : '';
      html += ' style="display:none" />';
      html += '<span class="switch-text">';
      html += attrs.on ? '<span class="on">' + attrs.on + '</span>' : '';
      html += attrs.off ? '<span class="off">' + attrs.off + '</span>' : ' ';
      html += '</span>';
      return html;
    }
  }
});

app.directive('ngEnter', function() {
  return function(scope, element, attrs) {
    element.bind("keydown keypress", function(event) {
      if (attrs.validated) {
        if (event.which === 13 && scope.$eval(attrs.validated)) {
          scope.$apply(function() {
            scope.$eval(attrs.ngEnter);
          });

          event.preventDefault();
        }
      } else {
        if (event.which === 13) {
          scope.$apply(function() {
            scope.$eval(attrs.ngEnter);
          });

          event.preventDefault();
        }
      }
    });
  };
});

app.directive('ngLeft', function() {
  return function(scope, element, attrs) {
    element.bind("keydown keypress", function(event) {
      if (attrs.validated) {
        if (event.which === 37 && scope.$eval(attrs.validated)) {
          scope.$apply(function() {
            scope.$eval(attrs.ngLeft);
          });

          event.preventDefault();
        }
      } else {
        if (event.which === 37) {
          scope.$apply(function() {
            scope.$eval(attrs.ngLeft);
          });

          event.preventDefault();
        }
      }
    });
  };
});

app.directive('ngRight', function() {
  return function(scope, element, attrs) {
    element.bind("keydown keypress", function(event) {
      if (attrs.validated) {
        if (event.which === 39 && scope.$eval(attrs.validated)) {
          scope.$apply(function() {
            scope.$eval(attrs.ngRight);
          });

          event.preventDefault();
        }
      } else {
        if (event.which === 39) {
          scope.$apply(function() {
            scope.$eval(attrs.ngRight);
          });

          event.preventDefault();
        }
      }
    });
  };
});

app.directive('userImage', ["$http", 'Restangular', function($http, Restangular) {
    return {
        restrict: 'A',
        scope: {
            user: '='
        },
        link: function(scope, element, attrs) {
            function getUserPicture() {
                Restangular.one('media').withHttpConfig({responseType: 'blob', cache: true}).get({
                    'name': 'user-profile',
                    'ownerUuid': scope.user.uuid
                }).then(function(data) {
                    var fr = new FileReader();
                    fr.onload = function(e) {
                        if (!e.target.result || !e.target.result.split(',')[1])
                            element.attr('src', 'images/user.png');
                        else
                            element.attr('src', 'data:image/jpeg;base64,' + e.target.result.split(',')[1])
                    };
                    fr.readAsDataURL(data);
                }, function(error) {
                    element.attr('src', 'images/user.png');
                });
            }

            scope.$watch('user', function(newObj) {
                if (scope.user && scope.user.uuid) getUserPicture();
            })

        }
    };
}]);

app.directive('userMedia', [function() {
  return {
    restrict: 'E',
    scope: {
      user: "="
    },
    template: '<div class="media media-user media-xs"><a class="media-left" href="javascript:;">'
    + '<img user-image src="" user="user" alt="" class="media-object"></a>'
    + '<div class="media-body"><p class="media-heading">{{user.displayName}}</p>'
    + '</div></div>'
  }}]);

app.directive('dndMatchHeight', function() {
  function link(scope, element, attrs) {
    scope.$watch(function() {
      var maxItems = -1;
      var longestList = null;

      for (var i = 0; i < scope.models.length; ++i) {
        if (scope.models[i].items.length > maxItems) {
          maxItems = scope.models[i].items.length;
          longestList = scope.models[i];
        }
      }

      if (longestList.listName == element[0].attributes['dnd-match-height'].value) {
        for (var i = 0; i < scope.models.length; ++i) {
          if (scope.models[i].listName == longestList.listName)
            scope.models[i].style.height = null;
          else
            scope.models[i].style.height = element[0].offsetHeight;
        }
      }
    });
  }
  return {
    restrict: 'AE',
    link: link
  };
});

/*
 * ! ngclipboard - v1.1.1 - 2016-02-26
 * https://github.com/sachinchoolur/ngclipboard Copyright (c) 2016 Sachin;
 * Licensed MIT
 */
app.directive('ngclipboard', function() {
  return {
    restrict: 'A',
    scope: {
      ngclipboardSuccess: '&',
      ngclipboardError: '&'
    },
    link: function(scope, element) {
      var clipboard = new Clipboard(element[0]);

      clipboard.on('success', function(e) {
        scope.$apply(function() {
          scope.ngclipboardSuccess({
            e: e
          });
        });
      });

      clipboard.on('error', function(e) {
        scope.$apply(function() {
          scope.ngclipboardError({
            e: e
          });
        });
      });

    }
  };
});

app.directive('dragOverZone', function() {
  return {
    restrict: 'A',
    link: function(scope, element, attrs) {
      var mousex, mousey = [];
      var dropZoneCoordinates = {};

      var continueDragging = function(e) {
        if (mousex >= dropZoneCoordinates.left && mousex <= dropZoneCoordinates.right
            && mousey >= dropZoneCoordinates.top && mousey <= dropZoneCoordinates.bottom) {
          dropZoneCoordinates.dom.addClass(attrs.dropZoneClass);
        } else {
          dropZoneCoordinates.dom.removeClass(attrs.dropZoneClass);
        }

        mousex = e.pageX;
        mousey = e.pageY;
      }

      var endDragging = function(e) {
        $(document).unbind("mousemove", continueDragging);
        $(document).unbind("mouseup", endDragging);
        mousex = 0;
        mousey = 0;
        dropZoneCoordinates = {};
      }

      var startDragging = function(e) {
        var lefttop = $("#" + attrs.dragOverZone).offset();
        dropZoneCoordinates = {
          dom: $("#" + attrs.dragOverZone),
          left: lefttop.left,
          top: lefttop.top,
          right: lefttop.left + $("#" + attrs.dragOverZone).width(),
          bottom: lefttop.top + $("#" + attrs.dragOverZone).height()
        };

        if (e.type == "mousedown") {
          $(document).bind("mousemove", continueDragging);
          $(document).bind("mouseup", endDragging);
        }
      }

      element.bind("mousedown", startDragging);
    }
  }
});

app.directive('topMenuItem', function() {
  return {
    restrict: 'A',
    scope: true,
    link: function($scope, $element) {
      $scope.toggleDashboard = function(uuid) {
        if ($scope.getUuid() == uuid)
          $scope.selectState('/dashboards', 'uuid', $scope.dashboards[0].uuid);
        else
          $scope.selectState('/dashboards', 'uuid', uuid);
      }
      $scope.toggleState = function(defaultState, newState, param1, param2, param3, param4) {
        if ($scope.getState() == newState)
          $scope.selectState(defaultState, param1, param2, param3, param4);
        else
          $scope.selectState(newState, param1, param2, param3, param4);
      }
    }
  }
});
app.filter('customFilter', ['$parse', function($parse) {
  return function(items, filters) {
    var itemsLeft = items.slice();

    Object.keys(filters).forEach(function(model) {
      var value = filters[model], getter = $parse(model);

      itemsLeft = itemsLeft.filter(function(item) {
        return getter(item).toLowerCase().indexOf(value.toLowerCase()) === 0;
      });
    });

    return itemsLeft;
  };
}])

app.filter('splitcharacters', function() {
  return function(input, chars) {
    if (chars.length < 1) return input;
    if (chars[0] <= 0 || chars[0] <= 0) return '';
    if (input && input.length > (chars[0] + chars[1])) {
      var prefix = input.substring(0, chars[0] / 2);
      var postfix = input.substring(input.length - chars[1], input.length);
      return prefix + '...' + postfix;
    }
    return input;
  };
});

app.filter('words', function() {
  return function(input, words) {
    if (isNaN(words)) return input;
    if (words <= 0) return '';
    if (input) {
      var inputWords = input.split(/\s+/);
      if (inputWords.length > words) {
        input = inputWords.slice(0, words).join(' ') + '…';
      }
    }
    return input;
  };
});

app.filter('split', function() {
  return function(input, splitChar, splitIndex) {
    return input.split(splitChar)[splitIndex];
  };
});

app.filter('fieldArray', function() {
  return function(input, field) {
    var newArray = [];
    input.forEach(function(obj) {
      newArray.push(obj[field]);
    });
    return newArray;
  };
});

app.filter('keysSet', function() {
  return function(input) {
    return Object.keys(input);
  };
});

app.filter('lastPathRoute', function() {
  return function(input) {
    return input ? input.substr(input.lastIndexOf("/") + 1) : "";
  };
});

app.filter('characters', function() {
  return function(input, chars, breakOnWord) {
    if (isNaN(chars)) return input;
    if (chars <= 0) return '';
    if (input && input.length > chars) {
      input = input.substring(0, chars);

      if (!breakOnWord) {
        var lastspace = input.lastIndexOf(' ');
        // get last space
        if (lastspace !== -1) {
          input = input.substr(0, lastspace);
        }
      } else {
        while (input.charAt(input.length - 1) === ' ') {
          input = input.substr(0, input.length - 1);
        }
      }
      return input + '…';
    }
    return input;
  };
});

app.directive('stBooleanFilter', function() {
  return {
    restrict: 'E',
    require: '^stTable',
    template: '<input type="checkbox" ng-model="checked">',
    scope: true,
    link: function(scope, element, attr, ctrl) {
      var tableState = ctrl.tableState();
      scope.$watch('checked', function(value) {
        ctrl.search(value || false, 'active');
      })
    }
  };
});

app.filter('reverse', function() {
  return function(items) {
    return items.slice().reverse();
  };
});

app.filter('sliceLast', function() {
  return function(items) {
    return items.splice(0, items.length - 1);
  };
});

app.filter('unique', function() {
  return function(collection, keyname) {
    var output = [], keys = [];

    angular.forEach(collection, function(item) {
      var key = item[keyname];
      if (keys.indexOf(key) === -1) {
        keys.push(key);
        output.push(item);
      }
    });

    return output;
  };
});

app.filter('multipleSelectionFilter', function() {
  return function(allSelected) {
    var DGLength = allSelected.deviceGroups.length;
    var DLength = allSelected.devices.length;
    if (DGLength == 0 && DLength == 0) return "No Current Selection";
    if (DGLength == 1 && DLength == 0) return allSelected.deviceGroups[0].displayName;
    if (DGLength == 0 && DLength == 1) return allSelected.devices[0].displayName;
    if (DGLength > 0 && DLength > 0)
      return DGLength + " Device Groups + " + DLength + " Devices Selected";
    if (DGLength > 1 && DLength == 0) return DGLength + " Device Groups Selected";
    if (DGLength == 0 && DLength > 1) return DLength + " Devices Selected";
  };
});

app.filter('bytes', function() {
  return function(bytes, precision) {
    if (isNaN(parseFloat(bytes)) || !isFinite(bytes)) return '-';
    if (typeof precision === 'undefined') precision = 1;
    var units = ['bytes', 'kB', 'MB', 'GB', 'TB', 'PB'], number = Math.floor(Math.log(bytes)
        / Math.log(1024));
    return (bytes / Math.pow(1024, Math.floor(number))).toFixed(precision) + ' ' + units[number];
  }
});

/*******************************************************************************
 * Copyright (c) 2015 Bosch Software Innovations GmbH. All rights reserved.
 ******************************************************************************/
app.factory('Constants', ['growl', function Constants(growl) {
  var units = {
    'temperature': [{
      name: 'C',
      displayName: '°C',
      description: 'Celsius'
    }, {
      name: 'F',
      displayName: '°F',
      description: 'Farenheit'
    }],
    'relativeHumidity': [{
      name: '%RH',
      displayName: '%RH',
      description: 'Relative Humidity'
    }],
    'humidity': [{
      name: '%rh',
      displayName: '%RH',
      description: 'Relative Humidity'
    }],
    'airFlow': [{
      name: 'SCCM',
      displayName: 'SCCM',
      description: 'Standard Cubic Centimeters per Minute'
    }],
    'voltage': [{
      name: 'V',
      displayName: 'V',
      description: 'Voltage'
    }],
    'light': [{
      name: 'lux',
      displayName: 'Lux',
      description: 'Lumen'
    }],
    'pressure': [{
      name: 'hPa',
      displayName: 'hPa',
      description: 'hPascal'
    }]
  };

  var colors = [{
    displayName: 'Blue',
    name: 'blue',
    value: '#7CB5EC'
  }, {
    displayName: 'Black',
    name: 'black',
    value: '#434348'
  }, {
    displayName: 'Orange',
    name: 'orange',
    value: '#f7a35c'
  }, {
    displayName: 'Green',
    name: 'green',
    value: '#85E992'
  }, {
    displayName: 'Brown',
    name: 'brown',
    value: '#800a0a'
  }, {
    displayName: 'Yellow',
    name: 'yellow',
    value: '#E4D314'
  }];

  return {
    getAvailableUnits: function(topic) {
      var result = units[topic];
      if (!result || result.length == 0) {
        result = [];
        for ( var topic in units) {
          result = result.concat(units[topic]);
        }
      }
      return result;
    },
    getAvailableColors: function() {
      return colors;
    }
  };
}]);

app.factory('DateHelper', ['Restangular', function DateHelper(Restangular) {
  var timeZoneOffset = new Date().getTimezoneOffset() * 60 * 1000;

  var minuteMillis = 60000;
  var hourMillis = 60 * minuteMillis;
  var dayMillis = 24 * hourMillis;
  var weekMillis = 7 * dayMillis;

  return {
    getTimestamp: function(dataTs) {
      var dataTimestamp;
      if (typeof dataTs == 'string' || dataTs instanceof String) {
        dataTimestamp = moment(dataTs);
      } else {
        dataTimestamp = parseInt(dataTs);
      }
      return dataTimestamp;
    },
    getTimeZoneOffset: function() {
      return timeZoneOffset;
    },
    getDateFromUnix: function(unix, offset) {
      var a = moment(new Date(unix));
      return a.format("ddd, MMM Do YYYY, HH:mm:ss");
    },
    getShortDateFromUnix: function(unix) {
      var a = moment(new Date(unix));
      return a.format("MM-DD-YYYY HH:mm:ss");
    },
    isActive: function(from, to) {
      var current = new Date().getTime();
      if (current >= from && current <= to) { return true; }
    },
    getMillis: function(unit) {
      switch (unit) {
      case 'm':
        return minuteMillis;
      case 'h':
        return hourMillis;
      case 'd':
        return dayMillis;
      case 'w':
        return weekMillis;
      default:
        return 0;
      }
    }
  };
}]);

app.factory('FileService', ['Restangular', 'DeviceService',
    function FileUploadService(Restangular, DeviceService) {
      return {
        downloadFile: function(deviceUuid, fileName, version) {
          DeviceService.getAttachment(deviceUuid, version).then(function(result) {
            var blob = new Blob([result], {
              type: 'text/plain'
            });
            var url = window.URL || window.webkitURL;
            var link = document.createElementNS("http://www.w3.org/1999/xhtml", "a");
            link.href = url.createObjectURL(blob);
            link.download = fileName;

            var event = document.createEvent("MouseEvents");
            event.initEvent("click", true, false);
            link.dispatchEvent(event);
          });
        },
        exportResources: function(resources, filename) {
            var zip = new JSZip();
        	  var json = JSON.stringify(resources);
        	  zip.file(filename, json);
        	  var content = zip.generate();
        	  var link = document.createElement('a');
        	  link.download = "notifications.mer";
        	  link.href="data:application/zip;base64,"+content;
        	  link.click();
          }
      };
    }]);

app.factory('GrowlService', ['growl', function GrowlService(growl) {
  return {
    checkNotNullNorBlank: function(input, err, msg) {
      if (!input || input == null || input == '') {
        growl.warning(err, {
          title: msg
        });
        return true;
      } else {
        return false;
      }
    }
  };
}]);

app.factory('LocationService', [function LocationService() {
  return {
    formatLocation: function(lat, lng) {
      var convertLat = Math.abs(lat);
      var LatDeg = Math.floor(convertLat);
      var LatSec = ((convertLat - LatDeg) * 60).toFixed(3);
      var LatCardinal = ((lat > 0) ? "N" : "S");

      var convertLng = Math.abs(lng);
      var LngDeg = Math.floor(convertLng);
      var LngSec = ((convertLng - LngDeg) * 60).toFixed(3);
      var LngCardinal = ((lng > 0) ? "E" : "W");

      return LatDeg + "° " + LatSec + "' " + LatCardinal + ", " + LngDeg + "° " + LngSec + "' "
          + LngCardinal;
    }
  };
}]);

app.factory('UUIDGenerator', [
    'Restangular',
    function UUIDGenerator(Restangular) {
      return {
        guid: function() {
          var self = this;
          return self.s4() + self.s4() + '-' + self.s4() + '-' + self.s4() + '-' + self.s4() + '-'
              + self.s4() + self.s4() + self.s4();
        },
        s4: function() {
          return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
        }
      };
    }]);

app.directive("fileread", [function() {
    return {
        scope: {
            fileread: "="
        },
        link: function(scope, element) {
            element.bind("change", function(changeEvent) {
                scope.fileread = changeEvent.target.files[0];
            });
        }
    }
}]);
app.directive('checkList', [function() {
    return {
        scope: {
            list: '=checkList',
            value: '@'
        },
        link: function(scope, elem, attrs) {
            var handler = function() {
                var checked = elem.prop('checked');
                var index = scope.list.indexOf(scope.value);
                if (index == -1)
                    index = scope.list.indexOf(parseInt(scope.value));

                if (checked && index == -1) {
                    scope.list.push(parseInt(scope.value));
                } else if (!checked && index != -1) {
                    scope.list.splice(index, 1);
                }
            };

            var changeHandler = handler.bind(null);

            elem.bind('change', function() {
                scope.$apply(changeHandler);
            });
            scope.$watch('list', changeHandler, true);
        }
    };
}]);
app.directive('autoFocus', ["$timeout", function($timeout) {
    return {
        scope: {trigger: '=autoFocus'},
        link: function(scope, element) {
            scope.$watch('trigger', function(value) {
                if (value === true) {
                    $timeout(function() {
                        element[0].focus();
                    }, 50);
                }
            });
        }
    };
}]);
app.controller('multiSelectController', ['$scope', '$parse', '$filter', '$attrs', function MultiSelectController($scope, $parse, $filter, $attrs) {
    var propertyName = $attrs.multiSelectTable;
    var displayGetter = $parse(propertyName);
    var lastSelected;

    function copyRefs(src) {
        return src ? [].concat(src) : [];
    }

    function wipeSelection(rows) {
        rows.forEach(function(row) {
            row.isSelected = false;
        })
    }

    function areMultipleRowsSelected(rows) {
        return $.grep(rows, function(row) {
            return row.isSelected;
        }).length > 1;
    }

    this.selectSingle = function select(row) {
        var rows = copyRefs(displayGetter($scope));
        var index = rows.indexOf(row);
        if (index !== -1) {
            var selection = areMultipleRowsSelected(rows) ? true : row.isSelected !== true;
            wipeSelection(rows);
            row.isSelected = selection;
            lastSelected = row;
        }
    };

    this.ctrlSelect = function select(row) {
        var rows = copyRefs(displayGetter($scope));
        var index = rows.indexOf(row);
        if (index !== -1) {
            row.isSelected = row.isSelected !== true;
            lastSelected = row;
        }
    };

    this.shiftSelect = function select(row, ctrl) {
        var rows = copyRefs(displayGetter($scope));
        var index = rows.indexOf(row);
        lastSelected = lastSelected ? lastSelected : rows[0];
        var anchorIndex = rows.indexOf(lastSelected);

        if (index !== -1 && anchorIndex !== -1) {
            if (!ctrl)
                wipeSelection(rows);
            var min = index >= anchorIndex ? anchorIndex : index;
            var max = index >= anchorIndex ? index : anchorIndex;

            for (var i = min; i <= max; i++) {
                rows[i].isSelected = true;
            }
        }
    };
}]);


app.directive('multiSelectTable', [function() {
    return {
        controller: 'multiSelectController',
    }
}]);

app.directive('multiSelectRow', [function() {
    return {
        restrict: 'A',
        require: '^multiSelectTable',
        scope: {
            row: '=multiSelectRow'
        },
        link: function(scope, element, attr, ctrl) {
            element.bind('click', function(event) {
                if (event.shiftKey) {
                    scope.$apply(function() {
                        ctrl.shiftSelect(scope.row, event.ctrlKey);
                    });
                }
                else if (event.ctrlKey) {
                    scope.$apply(function() {
                        ctrl.ctrlSelect(scope.row);
                    });
                }
                else {
                    scope.$apply(function() {
                        ctrl.selectSingle(scope.row);
                    });
                }
            });

            scope.$watch('row.isSelected', function(newValue) {
                if (newValue === true) {
                    element.addClass('st-selected');
                } else {
                    element.removeClass('st-selected');
                }
            });
        }
    };
}]);
app.constant('Configuration', (function() {
    return {
        notificationsDefaultPeriod: {
            value: '14',
            units: 'days'
        }
    }
})());
profile.factory('ProfileService', ['Restangular', 'RestangularV2',
    function ProfileService(Restangular, RestangularV2) {
        return {
            addProfile: function(name, file) {
                return Restangular.one('media').post({}, '', {}, {
                    file: file,
                    name: name
                })
            },
            getProfile: function(options) {
                return Restangular.one('media').get(options);
            },
            deleteProfile: function(uuid) {
                return Restangular.one('profile', uuid).remove();
            },
            changePassword: function(currentPassword, newPassword) {
                return RestangularV2.one('users').one('changepassword').customPUT({
                    currentPassword: currentPassword,
                    newPassword: newPassword
                });
            }
        };
    }]);

datasources.factory('DatasourceService', ['RestangularV2', 'CachedRestangular', '$http',
    function DatasourceService(RestangularV2, CachedRestangular, $http) {
        return {
            getDatasources: function(options) {
                return RestangularV2.one('datasources').get(options);
            },
            getDatasource: function(uuid) {
                return CachedRestangular.one('datasources', uuid).get();
            },
            getDatasourceMedia: function(uuid, options) {
                // return $http.get('api/v2/datasources/' + uuid + '/media', {
                // cache: true,
                // params: options
                // });
                return CachedRestangular.one('datasources', uuid).one('media').get(options);
            },
            removeDatasource: function(uuid) {
                return RestangularV2.one('datasources', uuid).remove();
            }
        };
    }]);

datasources.factory('OEEEntryService', ['RestangularV2', 'CachedRestangular', '$http',
    function OEEEntryService(RestangularV2, CachedRestangular, $http) {
        return {
            addOeeEntries: function(entries) {
                return RestangularV2.one('oeeentries').customPOST(entries, '', {}, {});
            },
            getOeeEntries: function(options) {
                if (!options) options = {};
                if (!options.from) options.from = moment().subtract(2, 'y').valueOf();
                if (!options.to) options.to = moment().endOf('day').valueOf();
                return RestangularV2.one('oeeentries').get(options);
            },
            getOEEEntriesStatistics: function(options) {
                if (!options) options = {};
                if (!options.from) options.from = moment().subtract(2, 'y').valueOf();
                if (!options.to) options.to = moment().valueOf();
                return RestangularV2.one('oeeentries').one('statistics').get(options);
            },
            updateOeeEntry: function(uuid, entry) {
                return RestangularV2.one('oeeentries', uuid).customPUT(entry, '', {}, {});
            },
            deleteOeeEntry: function(uuid) {
                return RestangularV2.one('oeeentries', uuid).remove();
            }

        };
    }]);

datasources.factory('DeviationEntryService', ['RestangularV2', 'CachedRestangular', '$http',
    function DeviationEntryService(RestangularV2, CachedRestangular, $http) {
        return {
            addDeviationEntries: function(entries) {
                return RestangularV2.one('deviationentries').customPOST(entries, '', {}, {});
            },
            getDeviationEntries: function(options) {
                if (!options) options = {};
                if (!options.from) options.from = moment().subtract(2, 'y').valueOf();
                if (!options.to) options.to = moment().valueOf();
                return RestangularV2.one('deviationentries').get(options);
            },
            getDeviationEntriesStatistics: function(options) {
                if (!options) options = {};
                if (!options.from) options.from = moment().subtract(2, 'y').valueOf();
                if (!options.to) options.to = moment().valueOf();
                return RestangularV2.one('deviationentries').one('statistics').get(options);
            },
            getDeviationEntriesWorkCenters: function() {
                return RestangularV2.one('deviationentries').one('workcenters').get();
            },
            updateDeviationEntry: function(uuid, entry) {
                return RestangularV2.one('deviationentries', uuid).customPUT(entry, '', {}, {});
            },
            deleteDeviationEntry: function(uuid) {
                return RestangularV2.one('deviationentries', uuid).remove();
            },
            analyze: function(options) {
                if (!options) options = {};
//                if (!options.from) options.from = moment().subtract(2, 'y').valueOf();
//                if (!options.to) options.to = moment().valueOf();
                return RestangularV2.one('deviationentries').one('analyze').customPOST(options, '', {}, {});
            }
        };
    }]);

datasources.factory('FilterService', [
    function FilterService() {
        var filter = null;
        return {
            saveFilter: function(newFilter) {
                filter = newFilter;
            },
            getFilter: function() {
                return filter;
            }
        }
    }]);

datasources.factory('DisplayService', [
    function DisplayService() {
        var display = {};
        var entry = {};
        return {
            saveDisplay: function(newDisplay) {
                display = newDisplay;
            },
            getDisplay: function() {
                return display;
            },
            saveEntry: function(newEntry) {
                entry = newEntry;
            },
            getEntry: function() {
                return entry;
            }
        }
    }]);
administration.factory('AdminOrganizationsService', ['Restangular', 'RestangularV2',
    function AdminOrganizationsService(Restangular, RestangularV2) {
        return {
            addOrganization: function(form) {
                return RestangularV2.one('organization').customPOST(form, '', {}, {})
            },
            getOrganizations: function(options) {
                return RestangularV2.one('organization').get(options);
            },
            getOrganization: function(uuid) {
                return RestangularV2.one('organization', uuid).get();
            },
            saveOrganizationConfiguration: function(uuid, configuration) {
                return RestangularV2.one('organization', uuid).one('configuration')
                    .customPUT(configuration, '', {}, {});
            },
            getOrganizationConfiguration: function(uuid) {
                return RestangularV2.one('organization', uuid).one('configuration').get();
            },
            getOrganizationStatistics: function(options) {
                return RestangularV2.one('organization').one('statistics').get(options);
            }
        };
    }]);
administration.factory('AdminUsersService', ['CachedRestangular',
    function AdminUsersService(CachedRestangular) {
        return {
            addUser: function(user, options) {
                return CachedRestangular.one('users').customPOST(user, '', options, {})
            },
            deleteUser: function(uuid) {
                return CachedRestangular.one('users', uuid).remove();
            },
            getUser: function(organizationUuid, uuid) {
                if (uuid)
                    return CachedRestangular.one('organization', organizationUuid)
                        .one('users', uuid).get();
                else
                    return CachedRestangular.one('user').get();
            },
            assignUser: function(organizationUuid, options) {
                return CachedRestangular.one('organization', organizationUuid).one('users').customPOST('', '', options, {});
            },
            unassignUser: function(organizationUuid, userUuid) {
                return CachedRestangular.one('organization', organizationUuid).one('users', userUuid).remove();
            },
            updateUserOrganization: function(user, uuid) {
                return CachedRestangular.one('organization', uuid).one('users').one(user.uuid)
                    .customPUT(user, '', {}, {})
            },
            assignOrganizationUserRole: function(user, uuid, options) {
                return CachedRestangular.one('organization', uuid).one('users').one(user.uuid).one('roles')
                    .customPUT(user, '', options, {})
            }
        };
    }]);
administration.factory('AdminSchedulesService', ['Restangular',
    function AdminSchedulesService(Restangular) {
        return {
            addSchedule: function(schedule) {
                return Restangular.one('schedules').customPOST(schedule, '', {}, {})
            },
            updateSchedule: function(uuid, schedule) {
                return Restangular.one('schedules', uuid).customPUT(schedule, '', {}, {});
            },
            deleteSchedule: function(uuid) {
                return Restangular.one('schedules', uuid).remove();
            },
            getSchedule: function(uuid) {
                return Restangular.one('schedules', uuid).get();
            },
            getSchedules: function() {
                return Restangular.one('schedules').get();
            },
        };
    }]);
administration.factory('AdminPlantsService', ['Restangular', 'RestangularV2',
    function AdminPlantsService(Restangular, RestangularV2) {
        return {
            addPlant: function(plant) {
                return Restangular.one('plants').customPOST(plant, '', {}, {})
            },
            getPlant: function(uuid) {
                return Restangular.one('plants', uuid).get();
            },
            getPlants: function(options) {
                return Restangular.one('plants').get(options);
            },
            getUsers: function(uuid, options) {
                return Restangular.one('plants', uuid).one('users').get(options);
            },
            assignUser: function(uuid, userUuid) {
                return Restangular.one('plants', uuid).one('users', userUuid).put();
            },
            unassingUser: function(uuid, userUuid) {
                return Restangular.one('plants', uuid).one('users', userUuid).remove();
            },
            deletePlant: function(uuid) {
                return Restangular.one('plants', uuid).remove();
            },
            updatePlant: function(uuid, plant) {
                return Restangular.one('plants', uuid).customPUT(plant, '', {}, {});
            }
        };
    }]);

administration.factory('DataService', [
    function DataService() {
        var plants = {};
        var costCenters = {};
        return {
            savePlants: function(newPlants) {
                plants = newPlants;
            },
            getPlants: function() {
                return plants;
            },
            saveCostCenters: function(newCostCenters) {
                costCenters = newCostCenters;
            },
            getCostCenters: function() {
                return costCenters;
            }
        }
    }]);
administration.factory('AdminCostCenterService', ['Restangular', 'RestangularV2',
    function AdminCostCenterService(Restangular, RestangularV2) {
        return {
            addCostCenter: function(uuid, costcenter) {
                return RestangularV2.one('plants', uuid)
                    .customPOST(costcenter, 'costcenters', {}, {})
            },
            getCostCenter: function(uuid, costCenterUuid) {
                return RestangularV2.one('plants', uuid).one('costcenters', costCenterUuid).get();
            },
            getCostCenters: function(uuid, options) {
                return RestangularV2.one('plants', uuid).one('costcenters').get(options);
            },
            getUsers: function(uuid, costCenterUuid, options) {
                return RestangularV2.one('plants', uuid).one('costcenters', costCenterUuid)
                    .one('users').get(options);
            },
            assignUser: function(uuid, costCenterUuid, userUuid) {
                return RestangularV2.one('plants', uuid).one('costcenters', costCenterUuid)
                    .one('users', userUuid).put();
            },
            unassignUser: function(uuid, costCenterUuid, userUuid) {
                return RestangularV2.one('plants', uuid).one('costcenters', costCenterUuid)
                    .one('users', userUuid).remove();
            },
            deleteCostCenter: function(uuid, costCenterUuid) {
                return RestangularV2.one('plants', uuid).one('costcenters', costCenterUuid)
                    .remove();
            },
            updateCostCenter: function(uuid, costCenterUuid, costCenter) {
                return RestangularV2.one('plants', uuid).one('costcenters', costCenterUuid)
                    .customPUT(costCenter, '', {}, {});
            }

        };
    }]);
administration.factory('AdminWorkCenterService', ['Restangular', 'RestangularV2',
    function AdminWorkCenterService(Restangular, RestangularV2) {
        return {
            addWorkCenter: function(uuid, costCenterUuid, workcenter) {
                return Restangular.one('plants', uuid).one('costcenters', costCenterUuid).customPOST(workcenter, 'workcenters', {}, {})
            },
            getWorkCenter: function(uuid, costCenterUuid, workCenterUuid, options) {
                return Restangular.one('plants', uuid).one('costcenters', costCenterUuid).one('workcenters', workCenterUuid).get(options);
            },
            getWorkCenters: function(uuid, costCenterUuid, options) {
                return Restangular.one('plants', uuid).one('costcenters', costCenterUuid).one('workcenters').get(options);
            },
            getUsers: function(uuid, costCenterUuid, workCenterUuid, options) {
                return Restangular.one('plants', uuid).one('costcenters', costCenterUuid).one('workcenters', workCenterUuid).one('users').get(options);
            },
            assignUser: function(uuid, costCenterUuid, workCenterUuid, userUuid) {
                return Restangular.one('plants', uuid).one('costcenters', costCenterUuid).one('workcenters', workCenterUuid).one('users', userUuid).put();
            },
            unassignUser: function(uuid, costCenterUuid, workCenterUuid, userUuid) {
                return Restangular.one('plants', uuid).one('costcenters', costCenterUuid).one('workcenters', workCenterUuid).one('users', userUuid).remove();
            },
            deleteWorkCenter: function(uuid, costCenterUuid, workCenterUuid) {
                return Restangular.one('plants', uuid).one('costcenters', costCenterUuid).one('workcenters', workCenterUuid).remove();
            },
            updateWorkCenter: function(uuid, costCenterUuid, workCenterUuid, workCenter) {
                return Restangular.one('plants', uuid).one('costcenters', costCenterUuid).one('workcenters', workCenterUuid).customPUT(workCenter, '', {}, {});
            }
        };
    }]);
analysis.factory('AnalysisResultService', ['RestangularV2', 'CachedRestangular', '$http',
    function AnalysisResultService(RestangularV2, CachedRestangular, $http) {
        return {
//            getAnalysisResults: function (options) {
//                if (!options) options = {};
//                if (!options.from) options.from = moment().subtract(2, 'y').valueOf();
//                if (!options.to) options.to = moment().valueOf();
//                return RestangularV2.one('deviationentries').one('analysisresults').get(options);
//            },
            getAnalysisGroups: function (options) {
                if (!options) options = {};
                if (!options.from) options.from = moment().subtract(2, 'y').valueOf();
                if (!options.to) options.to = moment().valueOf();
                return RestangularV2.one('deviationentries').one('analysisgroups').get(options);
            },
//            getAnalysisResult: function (uuid) {
//                return RestangularV2.one('deviationentries').one('analysisresults', uuid).get();
//            },
            getAnalysisGroup: function (uuid) {
                return RestangularV2.one('deviationentries').one('analysisgroups', uuid).get();
            },
            reanalyze: function (options) {
                if (!options) options = {};
//                if (!options.from) options.from = moment().subtract(2, 'y').valueOf();
//                if (!options.to) options.to = moment().valueOf();
//                return RestangularV2.one('deviationentries').one('analyze', uuid).get(options);
                return RestangularV2.one('deviationentries').one('analyze').customPOST(options, '', {}, {});
            },
//            deleteAnalysisResult: function (uuid,ver) {
//                return RestangularV2.one('deviationentries', ver).one('analysisresults', uuid).remove();
//            },
            updateAnalysisGroup: function (uuid, analysisGroup) {
                return RestangularV2.one('deviationentries').one('analysisgroups', uuid).customPUT(analysisGroup, '', {}, {});
            },
            deleteAnalysisGroup: function (uuid) {
                return RestangularV2.one('deviationentries').one('analysisgroups', uuid).remove();
            },
            deleteAnalysisResults: function (uuid) {
                return RestangularV2.one('deviationentries').one('analysisresults', uuid).remove();
            }

        };
    }]);

analysis.factory('CurrentAnalysisGroupService', [function CurrentAnalysisGroupService() {
    var currentAnalysisGroup = null;

    return {
        getCurrentAnalysisGroup: function () {
            return currentAnalysisGroup;
        },
        setCurrentAnalysisGroup: function (analysisGroup) {
            currentAnalysisGroup = analysisGroup;
        }
    };
}]);
subscription.factory('PaymentService', ['RestangularV2', 'CachedRestangular', '$http',
    function PaymentService(RestangularV2, CachedRestangular, $http) {
        return {
            addPaymentMethod: function(payment) {
                return RestangularV2.one('paymentmethods').customPOST(payment, '', {}, {});
            },
            getPaymentMethods: function(options) {
                return RestangularV2.one('paymentmethods').get(options);
            },
            updatePaymentMethod: function(payment) {
                return RestangularV2.one('paymentmethods', payment.uuid)
                    .customPUT(payment, '', {}, {});
            },
            removePaymentMethod: function(uuid) {
                return RestangularV2.one('paymentmethods', uuid).remove();
            },
            getStripeKey: function() {
                return RestangularV2.one('stripe').one('key').get();
            }
        };
    }]);
subscription.factory('PlansService', ['RestangularV2', 'CachedRestangular', '$http', '$q',
    function PlansService(RestangularV2, CachedRestangular, $http, $q) {

        var activePlan;

        return {
            getSubscriptions: function() {
                return RestangularV2.one('accountsubscriptions').get();
            },
            getActiveSubscription: function() {
                return activePlan;
            },
            refreshActiveSubscription: function() {
                var deferred = $q.defer();
                RestangularV2.one('accountsubscriptions').get({"active": true})
                    .then(function(newActivePlan) {
                        activePlan = newActivePlan;
                        deferred.resolve();
                    }, function() {
                        deferred.reject();
                    });
                return deferred.promise;
            },
            changeActiveSubscription: function(plan) {
                return RestangularV2.one('accountsubscriptions').customPOST(plan, '', {}, {});
            },
            cancelActiveSubscription: function(providerId) {
                return RestangularV2.one('accountsubscriptions', providerId).remove();
            }
        };
    }]);
var jsTreeModule = angular.module('ng-tree', []);

jsTreeModule.directive('jsTree', [
    '$http',
    'AdminPlantsService',
    'AdminCostCenterService',
    'AdminWorkCenterService',
    function($http, AdminPlantsService, AdminCostCenterService, AdminWorkCenterService) {

        var typesConfig = {
            "plant": {
                "icon": "fa fa-industry text-primary fa-lg"
            },
            "costCenter": {
                "icon": "fa fa-usd text-primary fa-lg"
            },
            "workCenter": {
                "icon": "fa fa-gear text-primary fa-lg"
            }
        }

        var sort = function(a, b) {
            var nodeA = this.get_node(a);
            var nodeB = this.get_node(b);
            var aType = nodeA.type;
            var bType = nodeB.type;
            var aName = nodeA.displayName;
            var bName = nodeB.displayName;
            if (aType == bType) {
                return (aName < bName) ? -1 : (aName > bName) ? 1 : 0;
            } else {
                return (aType < bType) ? 1 : -1;
            }
        }

        var treeDir = {
            restrict: 'EA',
            fetchResource: function(url, cb) {
                return $http.get(url).then(function(data) {
                    if (cb) cb(data.data);
                });
            },

            managePlugins: function(s, e, a, config) {
                if (a.treePlugins) {
                    config.plugins = a.treePlugins.split(',');
                    config.core = config.core || {};
                    config.core.check_callback = config.core.check_callback || true;

                    if (config.plugins.indexOf('state') >= 0) {
                        config.state = config.state || {};
                        config.state.key = a.treeStateKey;
                    }

                    if (config.plugins.indexOf('checkbox') >= 0) {
                        config.checkbox = config.checkbox || {};
                        config.checkbox.keep_selected_style = false;
                    }

                    if (config.plugins.indexOf('contextmenu') >= 0) {
                        if (a.treeContextmenu) {
                            config.contextmenu = s[a.treeContextmenu];
                        }
                    }

                    if (config.plugins.indexOf('types') >= 0) {
                        if (a.treeTypes) {
                            config.types = s[a.treeTypes] ? s[a.treeTypes] : typesConfig;
                        }
                    }

                    if (config.plugins.indexOf('dnd') >= 0) {
                        if (a.treeDnd) {
                            config.dnd = s[a.treeDnd];
                        }
                    }

                    if (config.plugins.indexOf('sort') >= 0) {
                        if (a.treeSort) {
                            config.sort = s[a.treeSort] ? s[a.treeSort].sort : sort;
                        }
                    }

                }
                return config;
            },
            manageEvents: function(s, e, a) {
                if (a.treeEvents) {
                    var evMap = a.treeEvents.split(';');
                    for (var i = 0; i < evMap.length; i++) {
                        if (evMap[i].length > 0) {
                            // plugins could have events with suffixes other than '.jstree'
                            var evt = evMap[i].split(':')[0];
                            if (evt.indexOf('.') < 0) {
                                evt = evt + '.jstree';
                            }
                            var cb = evMap[i].split(':')[1];
                            treeDir.tree.on(evt, s[cb]);
                        }
                    }
                }
            },
            link: function(s, e, a) { // scope, element, attribute \O/
                $(function() {
                    var config = {};

                    // users can define 'core'
                    config.core = {};
                    if (a.treeCore) {
                        config.core = $.extend(config.core, s[a.treeCore]);
                    }

                    var deviceGroup2Node = function(deviceGroup) {
                        deviceGroup.id = deviceGroup.uuid;
                        deviceGroup.parent = deviceGroup.parentUuid == '-1' ? '#' :
                            deviceGroup.parentUuid;
                        deviceGroup.text = deviceGroup.displayName;
                        deviceGroup.type = "deviceGroup";
                        deviceGroup.children = true;
                        return deviceGroup;
                    }

                    // var device2Node = function(device, parentUuid) {
                    //     device.id = device.uuid;
                    //     device.parent = parentUuid;
                    //     device.text = device.displayName;
                    //     device.type = "device";
                    //     return device;
                    // }

                    var plant2Node = function(plant) {
                        plant.id = plant.uuid;
                        plant.parent = "#";
                        plant.text = plant.displayName;
                        plant.type = "plant";
                        plant.children = true;
                        return plant;
                    };

                    var costCenter2Node = function(costCenter, parentUuid) {
                        costCenter.id = costCenter.uuid;
                        costCenter.parent = parentUuid;
                        costCenter.text = costCenter.displayName;
                        costCenter.type = "costCenter";
                        costCenter.children = true;
                        return costCenter;
                    };

                    var workCenter2Node = function(workCenter, parentUuid) {
                        workCenter.id = workCenter.uuid;
                        workCenter.parent = parentUuid;
                        workCenter.text = workCenter.displayName;
                        workCenter.type = "workCenter";
                        return workCenter;
                    };

                    // clean Case
                    a.treeData = a.treeData ? a.treeData.toLowerCase() : '';
                    a.treeSrc = a.treeSrc ? a.treeSrc.toLowerCase() : '';

                    if (a.treeData == 'html') {
                        treeDir.fetchResource(a.treeSrc, function(data) {
                            e.html(data);
                            treeDir.init(s, e, a, config);
                        });
                    } else if (a.treeData == 'json') {
                        treeDir.fetchResource(a.treeSrc, function(data) {
                            config.core.data = data;
                            treeDir.init(s, e, a, config);
                        });
                    } else if (a.treeData == 'scope') {
                        s.$watch(a.treeModel, function(n, o) {
                            if (n) {
                                config.core.data = s[a.treeModel];
                                $(e).jstree('destroy');
                                treeDir.init(s, e, a, config);
                            }
                        }, true);
                        // Trigger it initally
                        // Fix issue #13
                        config.core.data = s[a.treeModel];
                        treeDir.init(s, e, a, config);
                    } else if (a.treeAjax) {
                        config.core.data = function(node, cb) {
                            var uuid = node.id === "#" ? '-1' : node.id;
                            var nodes = [];

                            if (uuid == '-1') {
                                AdminPlantsService.getPlants({}).then(function(plants) {
                                    for (var i = 0; i < plants.length; i++) {
                                        nodes.push(plant2Node(plants[i]));
                                    }
                                    cb(nodes);
                                }, function(err) {
                                    cb([]);
                                })
                            }
                            else {
                                if (node.type == "plant") {
                                    AdminCostCenterService.getCostCenters(uuid, {}).then(function(costCenters) {
                                        for (var i = 0; i < costCenters.length; i++) {
                                            nodes.push(costCenter2Node(costCenters[i], uuid));
                                        }
                                        cb(nodes);
                                    }, function(err) {
                                        cb([]);
                                    })
                                }
                                else {
                                    AdminWorkCenterService.getWorkCenters(node.parent, uuid, {}).then(function(workCenters) {
                                        for (var i = 0; i < workCenters.length; i++) {
                                            nodes.push(workCenter2Node(workCenters[i], uuid));
                                        }
                                        cb(nodes);
                                    }, function(err) {
                                        cb([]);
                                    })
                                }
                            }
                        };
                        treeDir.init(s, e, a, config);
                    }
                });

            },
            init: function(s, e, a, config) {
                treeDir.managePlugins(s, e, a, config);
                this.tree = $(e).jstree(config);
                s.$parent.$parent[a.treeInstance] = this.tree;
                treeDir.manageEvents(s, e, a);
            }
        };

        return treeDir;

    }]);
