module.exports = function(grunt) {

    var port = grunt.option('PORT') || '9091';
    var apiPath = grunt.option('API_PATH') || 'http://127.0.0.1:8090';
    var wpApiPath = grunt.option('WORDPRESS_API_PATH') || 'https://public-api.wordpress.com/wp/v2/sites/improvidusdev.wordpress.com';
    var wpEnvironment = grunt.option('WORDPRESS_ENVIRONMENT') || 'integration';
    var clientId = grunt.option('CLIENT_ID') || 'improvidus';
    var sslEnabled = grunt.option('SSL_ENABLED') || false;
    var accessToken = grunt.option('ACCESS_TOKEN_NAME') || 'access-token-improvidus';
    var refreshToken = grunt.option('REFRESH_TOKEN_NAME') || 'refresh-token-improvidus';
    var domains = grunt.option('WHITE_LISTED_DOMAINS') || ['192.168.56.11', 'localhost', '127.0.0.1', 'int.improvidus.com', 'www.improvidus.com'];

    // Project configuration
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        ngconstant: {
            options: {
                name: 'app',
                dest: 'src/main/resources/static/app/main/app.constants.js',
                constants: {
                    APP_CONSTANTS: {
                        OAUTH: {
                            CLIENT_ID: clientId,
                            ACCESS_TOKEN_NAME: accessToken,
                            REFRESH_TOKEN_NAME: refreshToken,
                            WHITE_LISTED_DOMAINS: domains
                        },
                        WORDPRESS: {
                            API_PATH: wpApiPath,
                            ENVIRONMENT: wpEnvironment
                        },
                        SSL_ENABLED: sslEnabled,
                        API_PATH: apiPath
                    }
                },
                deps: false
            },
            build: {}
        },
        shell: {
            options: {
                stderr: false
            },
            server: "http-server ./src/main/resources/static -s -d false -p " + port
        },
        copy: {
            images: {
                files: [{
                    expand: true,
                    cwd: 'src/main/resources/static/assets/',
                    src: ['**/*.{png,jpg,svg}'],
                    dest: 'src/main/resources/static/min/css/',
                    flatten: true
                }]
            },
            fonts: {
                files: [{
                    expand: true,
                    cwd: 'src/main/resources/static/assets/',
                    src: ['**/*.{woff,woff2,ttf,svg,eot,otf}'],
                    dest: 'src/main/resources/static/min/fonts/',
                    flatten: true
                }]
            }
        },

        // Concat + Uglify according to HTML usemin blocks
        useminPrepare: {
            all: {
                src: 'src/main/resources/static/**.html',
            },
            appJS: {
                src: ['src/main/resources/templates/fragments/js/app.js.html', 'src/main/resources/templates/fragments/css/app.css.html'],
            },
            options: {
                root: 'src/main/resources/static',
                dest: 'src/main/resources/static'
            }
        },

        usemin: {
            html: 'src/main/resources/static/**.html',
            options: {
                blockReplacements: {
                    css: function(block) {
                        return '<link rel="stylesheet" href="' + block.dest + '" />';
                    }
                }
            }
        },
        //Inject dependency blocks into html
        includes: {
            files: {
                src: ['index.html', '404.html'],
                dest: 'src/main/resources/static', // Destination directory
                cwd: 'src/main/resources/templates',
                flatten: true,
                options: {
                    silent: true
                }
            }
        }
    });

    // Load NPM tasks
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-usemin');
    grunt.loadNpmTasks('grunt-shell');
    grunt.loadNpmTasks('grunt-ng-constant');
    grunt.loadNpmTasks('grunt-includes');

    // Default task
    grunt.registerTask('default', ['includes', 'ngconstant', 'useminPrepare:all', 'concat:generated', 'cssmin:generated', 'uglify:generated', 'usemin']);

    grunt.registerTask('minappJS', ['includes', 'ngconstant', 'copy:images', 'copy:fonts', 'useminPrepare:appJS', 'concat:generated', 'cssmin:generated', 'uglify:generated', 'usemin']);

    grunt.registerTask('devserver', ['includes', 'ngconstant', 'shell:server']);
    grunt.registerTask('prodserver', ['minappJS', 'shell:server']);
};