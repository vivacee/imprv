/*******************************************************************************
 * Copyright (c) 2015 Bosch Software Innovations GmbH. All rights reserved.
 ******************************************************************************/
angular.module('app.controllers.base', []).controller(
    "BaseCtrl",
    ["$rootScope", "$scope", "$location", "screenSize",
        function($rootScope, $scope, $location, screenSize) {

          screenSize.when('sm', function() {
            $scope.desktop = screenSize.is('md, lg');
          });

          screenSize.when('xs', function() {
            $scope.desktop = screenSize.is('md, lg');
          });

          screenSize.when('md', function() {
            $scope.desktop = screenSize.is('md, lg');
          });

          screenSize.when('lg', function() {
            $scope.desktop = screenSize.is('md, lg');
          });

          $scope.desktop = screenSize.is('md, lg');

          $scope.select = function(state, uuid) {
            if ($location.$$path.endsWith(state)) {
              $location.search('uuid', uuid);
            } else {
              $location.path(state).search('uuid', uuid);
            }
          }

          $scope.obj2List = function(obj) {
            var list = [];
            for ( var index in obj) {
              list.push({
                key: index,
                value: obj[index]
              });
            }
            return list;
          }

        }]);