/*******************************************************************************
 * Copyright (c) 2015 Bosch Software Innovations GmbH. All rights reserved.
 ******************************************************************************/
angular.module('app.controllers.profile', ['ngFileUpload', 'angular-international'])//, 'angular-stripe'])
//.config(['stripeProvider', function (stripeProvider) {
//    stripeProvider.setPublishableKey('pk_test_KU2W7iv2ofe5ufG7VMa5n8O0');
//}])
.controller(
    "ProfileCtrl",
    [
        "$http",
        "$scope",
        "$rootScope",
        "$log",
        "growl",
        "countries",
        "languages",
        "ConfigurationService",
        "ProfileService",
        "Upload",
        "TranslationService",
        '$uibModal',
        '$modalStack',
        //'PaymentService',
        function($http, $scope, $rootScope, $log, growl, countries, languages,
            ConfigurationService, ProfileService, Upload, TranslationService,
                 $uibModal) {//, PaymentService) {

          console.log("Profile State");

          $scope.images = {};
          $scope.data = {};
          $scope.controls = {};
          $scope.payment = {};
          $scope.data = {
        		  show: false,
        		  exec: false,
        		  word: false,
        		  metric: false,
        		  corr: false,
        		  disable: false
          };
          $scope.add = {};

          $scope.getCountries = function() {
            return countries;
          }

          $scope.getLanguages = function() {
            return languages;
          }

          $scope.saveProfile = function() {
            $scope.saveProfilePicture();
            $scope.saveUserSettings();
          }

          $scope.saveUserSettings = function() {
            ConfigurationService.updateConfiguration($scope.user.configuration,
                $scope.currentUser.uuid).then(function(configuration) {
              $rootScope.user.configuration = configuration;
              growl.success("PROFILE_HAS_BEEN_SUCCESSFULLY_SAVED", {
                title: 'SUCCESS'
              });
              $scope.controls.countryEdit = false;
            });
          }

          $scope.saveProfilePicture = function() {
            if (!$scope.picFile) return;
            var fd = new FormData();
            fd.append('file', $scope.picFile);
            $http.post("api/v1/media", fd, {
              transformRequest: angular.identity,
              headers: {
                'Content-Type': undefined,
                'name': 'user-profile'
              }
            }).success(function(result) {
              growl.success("PROFILE_HAS_BEEN_SUCCESSFULLY_SAVED", {
                title: 'SUCCESS'
              });
              $log.info("Profile picture successfully saved")
            }).error(function(err) {
              growl.error("There is been a problem updating your profile picture", {
                title: 'ERROR'
              });
              console.log(err);
            });
          }

          var exception = "com.bosch.im.authentication.AuthenticationDeniedRuntimeException";

          $scope.savePassword = function() {
            if ($scope.password.newPassErrors.length == 0
                && $scope.password.newPass2Errors.length == 0) {
              ProfileService.changePassword($scope.password.current, $scope.password.newPass).then(
                  function(result) {
                    $scope.passwordEdit = false;
                    growl.success("Successfully changed password", {
                      title: "SUCCESS"
                    });
                  }, function(error) {
                    if (error.data.exception == exception) {
                      growl.warning("Current password incorrect", {
                        title: 'ERROR'
                      });
                    }
                  });
            }
          }

          $http.get("api/v1/media", {
            responseType: "blob",
            params: {
              'name': 'user-profile'
            }
          }).success(function(data, status, headers, config) {
            var fr = new FileReader();
            fr.onload = function(e) {
              $scope.updateProfilePicture(e.target.result.split(',')[1]);
            };
            fr.readAsDataURL(data);
          }).error(function(data, status, headers, config) {
            console.log(data);
          });

          $scope.updateProfilePicture = function(data) {
            $scope.data.profilePic = 'data:image/jpeg;base64,' + data;
            $scope.$apply();
          }

          $scope.$watch('password.newPass', function() {
            if (!$scope.password) return;
            $scope.password.newPassErrors = [];

            if (/[a-z]/.test($scope.password.newPass) == false) {
              $scope.password.newPassErrors
                  .push('Your password should have at least one lower case letter.');
            }
            if (/[A-Z]/.test($scope.password.newPass) == false) {
              $scope.password.newPassErrors
                  .push('Your password should have at least one upper case letter.');
            }
            if (/\d/.test($scope.password.newPass) == false) {
              $scope.password.newPassErrors.push('Your password should have at least one number.');
            }
            if (/\W+/.test($scope.password.newPass) == false) {
              $scope.password.newPassErrors
                  .push('Your password should have at least one special character.');
            }

          })

          $scope.$watch('password.newPass2', function() {
            if (!$scope.password) return;
            $scope.password.newPass2Errors = [];
            if ($scope.password.newPass2 !== $scope.password.newPass) {
              $scope.password.newPass2Errors.push('Passwords should match');
            }
          })

        }]);
