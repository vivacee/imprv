profile.config(["$stateProvider", function($stateProvider) {

    $stateProvider.state('Account.Profile', {
        url: "/profile",
        templateUrl: "app/main/account/profile/views/user.html",
        controller: "ProfileCtrl",
        reloadOnSearch: false
    });

}]);