profile.factory('ProfileService', ['Restangular', 'RestangularV2',
    function ProfileService(Restangular, RestangularV2) {
        return {
            addProfile: function(name, file) {
                return Restangular.one('media').post({}, '', {}, {
                    file: file,
                    name: name
                })
            },
            getProfile: function(options) {
                return Restangular.one('media').get(options);
            },
            deleteProfile: function(uuid) {
                return Restangular.one('profile', uuid).remove();
            },
            changePassword: function(currentPassword, newPassword) {
                return RestangularV2.one('users').one('changepassword').customPUT({
                    currentPassword: currentPassword,
                    newPassword: newPassword
                });
            }
        };
    }]);
