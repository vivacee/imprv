subscription.factory('PlansService', ['RestangularV2', 'CachedRestangular', '$http', '$q',
    function PlansService(RestangularV2, CachedRestangular, $http, $q) {

        var activePlan;

        return {
            getSubscriptions: function() {
                return RestangularV2.one('accountsubscriptions').get();
            },
            getActiveSubscription: function() {
                return activePlan;
            },
            refreshActiveSubscription: function() {
                var deferred = $q.defer();
                RestangularV2.one('accountsubscriptions').get({"active": true})
                    .then(function(newActivePlan) {
                        activePlan = newActivePlan;
                        deferred.resolve();
                    }, function() {
                        deferred.reject();
                    });
                return deferred.promise;
            },
            changeActiveSubscription: function(plan) {
                return RestangularV2.one('accountsubscriptions').customPOST(plan, '', {}, {});
            },
            cancelActiveSubscription: function(providerId) {
                return RestangularV2.one('accountsubscriptions', providerId).remove();
            }
        };
    }]);