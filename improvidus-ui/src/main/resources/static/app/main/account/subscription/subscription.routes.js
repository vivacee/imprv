subscription.config(["$stateProvider", function($stateProvider) {

    $stateProvider.state('Account.Subscription', {
        url: "/subscription",
        templateUrl: "app/main/account/subscription/views/subscription.html",
        reloadOnSearch: false,
        data: {
            permissions: {
                only: ['ADMIN', 'GENERAL_MANAGER'],
                redirectTo: 'OEE'
            }
        }
    });
}]);