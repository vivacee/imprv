subscription.factory('PaymentService', ['RestangularV2', 'CachedRestangular', '$http',
    function PaymentService(RestangularV2, CachedRestangular, $http) {
        return {
            addPaymentMethod: function(payment) {
                return RestangularV2.one('paymentmethods').customPOST(payment, '', {}, {});
            },
            getPaymentMethods: function(options) {
                return RestangularV2.one('paymentmethods').get(options);
            },
            updatePaymentMethod: function(payment) {
                return RestangularV2.one('paymentmethods', payment.uuid)
                    .customPUT(payment, '', {}, {});
            },
            removePaymentMethod: function(uuid) {
                return RestangularV2.one('paymentmethods', uuid).remove();
            },
            getStripeKey: function() {
                return RestangularV2.one('stripe').one('key').get();
            }
        };
    }]);