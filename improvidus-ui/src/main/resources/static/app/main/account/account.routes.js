account.config(["$stateProvider", function($stateProvider) {

    $stateProvider.state('Account', {
        url: "/account",
        templateUrl: "app/main/account/views/account.html",
        reloadOnSearch: false,
        redirectTo: 'Account.Profile'
    });

}]);