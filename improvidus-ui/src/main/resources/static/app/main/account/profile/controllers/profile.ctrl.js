profile.controller(
    "ProfileCtrl",
    [
        "$http",
        "$scope",
        "$rootScope",
        "$log",
        "$state",
        "growl",
        "countries",
        "languages",
        "ConfigurationService",
        "ProfileService",
        "Upload",
        "TranslationService",
        '$modal',
        '$modalStack',
        'PaymentService',
        'AdminUsersService',
        'APP_CONSTANTS',
        function($http, $scope, $rootScope, $log, $state, growl, countries, languages,
                 ConfigurationService, ProfileService, Upload, TranslationService,
                 $modal, $modalStack, PaymentService, AdminUsersService, APP_CONSTANTS) {

            $rootScope.state = "Profile";

            $scope.images = {};
            $scope.data = {};
            $scope.controls = {};
            $scope.payment = {};
            $scope.data = {
                show: false,
                exec: false,
                word: false,
                metric: false,
                corr: false,
                disable: false
            };
            $scope.add = {};

            $scope.getCountries = function() {
                return countries;
            }

            $scope.getLanguages = function() {
                return languages;
            }

            $scope.saveProfile = function() {
                $scope.saveProfilePicture();
                $scope.saveUserSettings();
            }

            $scope.saveUserSettings = function() {
                ConfigurationService.updateConfiguration($scope.user.configuration,
                    $scope.currentUser.uuid).then(function(configuration) {
                    $rootScope.user.configuration = configuration;
                    growl.success("PROFILE_HAS_BEEN_SUCCESSFULLY_SAVED", {
                        title: 'SUCCESS'
                    });
                    $scope.controls.countryEdit = false;
                });
            }

            $scope.$watch('data.picFile', function() {
                if (!$scope.data.picFile) return;
                $scope.saveProfilePicture();
            })

            $scope.saveProfilePicture = function() {
                if (!$scope.data.picFile) return;
                // $scope.data.disableControls = true;
                var fd = new FormData();
                fd.append('file', $scope.data.picFile);
                fd.append('name', "user-profile");
                $http.post(APP_CONSTANTS.API_PATH + "/api/v1/media", fd, {
                    transformRequest: angular.identity,
                    headers: {
                        'Content-Type': undefined,
                        // 'name': 'user-profile'
                    }
                }).success(function(result) {
                    // $scope.data.disableControls = false;
                    growl.success("PROFILE_HAS_BEEN_SUCCESSFULLY_SAVED", {
                        title: 'SUCCESS'
                    });
                    $log.info("Profile picture successfully saved")
                }).error(function(err) {
                    // $scope.data.disableControls = false;
                    growl.error("There is been a problem updating your profile picture", {
                        title: 'ERROR'
                    });
                    console.log(err);
                });
            }

            var exception = "com.bosch.im.authentication.AuthenticationDeniedRuntimeException";

            $scope.savePassword = function() {
                if ($scope.password.newPassErrors.length == 0
                    && $scope.password.newPass2Errors.length == 0) {
                    ProfileService.changePassword($scope.password.current, $scope.password.newPass)
                        .then(
                            function(result) {
                                $scope.passwordEdit = false;
                                growl.success("Successfully changed password", {
                                    title: "SUCCESS"
                                });
                            }, function(error) {
                                if (error.data.exception == exception) {
                                    growl.warning("Current password incorrect", {
                                        title: 'ERROR'
                                    });
                                }
                            });
                }
            }

            $http.get(APP_CONSTANTS.API_PATH + "/api/v1/media", {
                responseType: "blob",
                params: {
                    'name': 'user-profile'
                }
            }).success(function(data, status, headers, config) {
                var fr = new FileReader();
                fr.onload = function(e) {
                    $scope.updateProfilePicture(e.target.result.split(',')[1]);
                };
                fr.readAsDataURL(data);
            }).error(function(data, status, headers, config) {
                console.log(data);
            });

            $scope.updateProfilePicture = function(data) {
                $scope.data.profilePic = 'data:image/jpeg;base64,' + data;
                $scope.$apply();
            }

            $scope.$watch('password.newPass', function() {
                if (!$scope.password) return;
                $scope.password.newPassErrors = [];

                if (/[a-z]/.test($scope.password.newPass) == false) {
                    $scope.password.newPassErrors
                        .push('Your password should have at least one lower case letter.');
                }
                if (/[A-Z]/.test($scope.password.newPass) == false) {
                    $scope.password.newPassErrors
                        .push('Your password should have at least one upper case letter.');
                }
                if (/\d/.test($scope.password.newPass) == false) {
                    $scope.password.newPassErrors.push(
                        'Your password should have at least one number.');
                }
                if (/\W+/.test($scope.password.newPass) == false) {
                    $scope.password.newPassErrors
                        .push('Your password should have at least one special character.');
                }

            })

            $scope.$watch('password.newPass2', function() {
                if (!$scope.password) return;
                $scope.password.newPass2Errors = [];
                if ($scope.password.newPass2 !== $scope.password.newPass) {
                    $scope.password.newPass2Errors.push('Passwords should match');
                }
            })

            $scope.$watch('add.card', function() {
                if ($scope.add.card) {
                    var cardSize = $scope.add.card.length;
                    if (cardSize < 5) {
                        if (cardSize % 4 == 0) {
                            $scope.add.card += ' ';
                            cardSize++;
                        }
                    } else if (cardSize !== 5) {
                        if (cardSize % 5 == 4) {
                            $scope.add.card += ' ';
                            cardSize++;
                        }
                    }
                    $scope.add.number = $scope.add.card;
                    $scope.add.number = $scope.add.number.replace(/[\s]/g, '');
                }
            })

            $scope.choosePlan = function(type) {
                $scope.data.disable = true
                var plan = {plan: type};

                PaymentService.paymentPlan(plan).then(function(response) {
                    if (response.status) {
                        growl.info(response.response, {
                            title: 'Info'
                        });
                        switch (type) {
                            case 'exec':
                                $scope.data.exec = true;
                                break;
                            case 'word':
                                $scope.data.word = true;
                                break;
                            case 'correlation':
                                $scope.data.corr = true;
                                break;
                            case 'metric':
                                $scope.data.metric = true;
                                break;
                            default :
                                break;
                        }
                    } else {
                        growl.error(response.response, {
                            title: 'Error'
                        });
                    }
                    $scope.data.disable = false;
                });
            }

            $scope.close = function() {
                $scope.data.disable = false;
                $modalStack.dismissAll();
            }

            $scope.cancelPlan = function(type) {
                $scope.refresh();
                $scope.data.disable = true;
                $scope.data.current = type;
                $modalStack.dismissAll();
                var opts = {
                    templateUrl: "app/main/profile/views/cancel-modal.html",
                    scope: $scope,
                    size: 'md'
                };
                var instance = $modal.open(opts);
            }

            $scope.deleteSub = function() {
                var row;
                $scope.data.displayedPaymentPlans.forEach(function(entry) {
                    if (entry.planId == $scope.data.current)
                        row = entry;
                });
                PaymentService.deleteSubscription(row.subscriptionId).then(function(result) {
                    if (result.status) {
                        switch ($scope.data.current) {
                            case 'exec':
                                $scope.data.exec = false;
                                break;
                            case 'word':
                                $scope.data.word = false;
                                break;
                            case 'correlation':
                                $scope.data.corr = false;
                                break;
                            case 'metric':
                                $scope.data.metric = false;
                                break;
                            default :
                                break;
                        }
                        growl.info(result.response, {
                            title: 'Info'
                        });
                    } else {
                        growl.error(result.response, {
                            title: 'Error'
                        });
                    }
                    $scope.refresh();
                    $scope.data.disable = false;
                });
            }

            $scope.showCancel = function() {
                $scope.data.disable = true;
                var entries = $.grep($scope.data.displayedPayments, function(entry) {
                    return entry.isSelected;
                });
                $scope.data.row = entries[0];
                $modalStack.dismissAll();
                var opts = {
                    templateUrl: "app/main/profile/views/cancel-card-modal.html",
                    scope: $scope,
                    size: 'md'
                };
                var instance = $modal.open(opts);
            }

            $scope.deleteCard = function() {
                PaymentService.deleteCard($scope.data.row.cardId).then(function(result) {
                    if (result.status) {
                        growl.info(result.response, {
                            title: 'Info'
                        });
                    } else {
                        growl.error(result.response, {
                            title: 'Error'
                        });
                    }
                    $scope.refresh();
                    $scope.data.disable = false;
                    $scope.data.show = false;
                });
            };

        }]);