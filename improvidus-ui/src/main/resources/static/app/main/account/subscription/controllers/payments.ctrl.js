subscription.controller("PaymentsCtrl", ["$rootScope",
    "$scope", "$log", "$modal", "$modalStack", "growl", "PaymentService",
    function($rootScope, $scope, $log, $modal, $modalStack, growl, PaymentService) {

        $rootScope.state = "Subscription";

        $scope.data = {
            selectedPayments: [],
            disableButtons: false
        };
        $scope.card = {};

        $scope.cardPlaceholders = {
            name: 'Your Full Name',
            number: 'xxxx xxxx xxxx xxxx',
            expiry: 'MM/YY',
            cvc: 'xxx'
        };

        $scope.cardMessages = {
            validDate: 'valid\nthru',
            monthYear: 'MM/YY',
        };

        $scope.cardOptions = {
            debug: false,
            formatting: true
        };

        $scope.$on('addCreditCard', function() {
            $scope.addCard();
        });

        $scope.addCard = function() {
            $modalStack.dismissAll();
            var opts = {
                templateUrl: "app/main/account/subscription/views/add-card-modal.html",
                scope: $scope,
                size: 'sm'
            };
            var instance = $modal.open(opts);
        }

        function setPrimary(payment, payments) {
            payments.forEach(function(p) {
                p.primary = p.uuid !== payment.uuid ? false : true;
            })
        }

        function onTokenCreated(response) {
            var card = {
                token: response.id,
                created: response.created,
                type: response.card.brand,
                last4: response.card.last4
            }
            var active = $scope.data.payments.length > 0 ? $scope.card.primary : true;
            var paymentMethod = {
                accountUuid: $scope.currentUser.organization.uuid,
                displayName: $scope.card.displayName,
                primary: active,
                provider: 'Stripe',
                hash: response.id,
                type: 'CREDIT_CARD',
                params: card
            }
            PaymentService.addPaymentMethod(paymentMethod).then(function(payment) {
                $scope.data.disableButtons = false;
                $modalStack.dismissAll();
                $scope.data.payments.push(payment);
                if (payment.primary == true)
                    setPrimary(payment, $scope.data.payments);
                $scope.formReset();
                $scope.refresh()
            }, function() {
                $scope.data.disableButtons = false;
                growl.error("There was an error with your request.", {
                    title: 'Error'
                });
            });
        }

        $scope.submit = function() {
            if (!$scope.card.displayName || $scope.card.displayName == '') {
                growl.error("Please, provide an alias for this card", {
                    title: 'Error'
                });
                return;
            }
            var card = {
                number: $scope.card.number,
                exp: $scope.card.exp,
                cvc: $scope.card.cvc
            }
            $scope.data.disableButtons = true;
            Stripe.card.createToken(card, function(status, response) {
                if (!response.error)
                    onTokenCreated(response);
                else {
                    $scope.data.disableButtons = false;
                    growl.error(response.error.message, {
                        title: 'Error'
                    });
                }
            });
        }

        $scope.getPayments = function() {
            PaymentService.getPaymentMethods().then(function(payments) {
                $scope.data.payments = payments
            });
        };

        $scope.formReset = function() {
            $scope.card = {};
            $scope.data.disableButtons = false;
        }

        $scope.refresh = function() {
            $scope.getPayments();
        }
        $scope.refresh();

        function getPrimary(newPayment) {
            var bckPrimary = $.grep($scope.data.payments, function(payment) {
                return payment.primary && (newPayment.uuid != payment.uuid);
            });
            if (bckPrimary.length == 1)
                return bckPrimary[0];
        }

        $scope.makeDefault = function(payment) {
            if ($scope.data.disableControls == true) return;
            if (payment.primary == false) {
                payment.primary = true;
                growl.warning("You should have always a card associated with your account.", {
                    title: 'Warning'
                });
                return;
            }
            $scope.backUp = angular.copy(getPrimary(payment));
            if (payment.primary == true)
                setPrimary(payment, $scope.data.payments);
            $scope.data.disableControls = true;
            PaymentService.updatePaymentMethod(payment)
                .then(function(updatedPayment) {
                    growl.success("Successfully changed your primary Payment Method.", {
                        title: 'Success'
                    });
                    $scope.data.disableControls = false;
                    $scope.refresh()
                }, function() {
                    $scope.data.disableControls = false;
                    setPrimary($scope.backUp, $scope.data.payments);
                });
        }

        $scope.remove = function() {
            var selection = $.grep($scope.data.payments, function(payment) {
                return payment.isSelected;
            })
            if (selection.length != 1 || selection[0].primary) {
                growl.warning(
                    "You cannot remove the Payment Method set as Primary. Please, " +
                    "mark another Payment Method as Primary before performing this operation.", {
                        title: 'Invalid Selection'
                    });
                return;
            }
            $scope.data.selectedPayment = selection[0];
            $modalStack.dismissAll();
            var opts = {
                templateUrl: "app/main/account/subscription/views/remove-card-modal.html",
                scope: $scope,
                size: 'sm'
            };
            var instance = $modal.open(opts);
        }

        $scope.removePaymentMethod = function() {
            $scope.data.disableControls = true;
            PaymentService.removePaymentMethod($scope.data.selectedPayment.uuid).then(function() {
                var index = $scope.data.payments.indexOf($scope.data.selectedPayment);
                $scope.data.payments.splice(index, 1);
                $modalStack.dismissAll();
                $scope.data.disableControls = false;
            }, function() {
                $scope.data.disableControls = false;
            })
        }

        $scope.close = function() {
            $modalStack.dismissAll();
        }

    }]);
