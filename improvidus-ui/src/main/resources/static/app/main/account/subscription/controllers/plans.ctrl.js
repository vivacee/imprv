subscription.controller("PlansCtrl", ["$rootScope",
    "$scope", "$log", "growl", "PlansService",
    function($rootScope, $scope, $log, growl, PlansService) {

        $rootScope.state = "Subscription";

        $scope.data.plans = [
            //     {
            //     displayName: "Started",
            //     providerId: "started",
            //     amount: "FREE",
            //     interval: '',
            //     content: [{
            //         strong: 1,
            //         small: "user"
            //     }, {
            //         strong: 10,
            //         small: "analysis"
            //     }]
            // },
            {
                displayName: "Team",
                providerId: "team",
                amount: "999",
                interval: 'month',
                content: [{
                    strong: 10,
                    small: "users"
                }, {
                    strong: "∞",
                    small: "analysis"
                }]
            }, {
                displayName: "Premium",
                providerId: "premium",
                amount: "2,999",
                interval: 'month',
                content: [{
                    strong: 50,
                    small: "users"
                }, {
                    strong: "∞",
                    small: "analysis"
                }]
            }
            // , {
            //     displayName: "Premium Plus",
            //     providerId: "premiumPlus",
            //     amount: "Contact for Pricing",
            //     interval: '',
            //     content: [{
            //         strong: "50+",
            //         small: "users"
            //     }, {
            //         strong: "∞",
            //         small: "analysis"
            //     }]
            // }
        ]

        PlansService.getSubscriptions().then(function(activePlans) {
            activePlans.forEach(function(activePlan) {
                var selection = $.grep($scope.data.plans, function(plan) {
                    return plan.providerId === activePlan.plan.providerId;
                });
                if (selection.length == 1)
                    selection[0].active = true;
            })
        })

        function setActivePlan(activePlan) {
            $scope.data.plans.forEach(function(plan) {
                if (!activePlan || plan.providerId != activePlan.providerId)
                    plan.active = false;
                else
                    plan.active = true;
            })
        }

        function getActivePlan() {
            var selection = $.grep($scope.data.plans, function(plan) {
                return plan.active;
            });
            if (selection.length == 1)
                return selection[0];
        }

        $scope.selectPlan = function(activePlan) {
            $scope.backUp = getActivePlan();
            setActivePlan(activePlan);
            $scope.data.disableButtons = true;
            PlansService.changeActiveSubscription(activePlan).then(function() {
                PlansService.refreshActiveSubscription();
                $scope.data.disableButtons = false;
                growl.success("Successfully changed Plan Subscription.", {
                    title: 'Success'
                });
            }, function(error) {
                $log.error(error);
                $scope.data.disableButtons = false;
                setActivePlan($scope.backUp);


                var description = error.data.message;
                if (description && description.indexOf("There is no primary payment method selected") > -1) {
                    $rootScope.$broadcast('addCreditCard');
                }
            })
        }

        $scope.cancelPlan = function(canceledPlan) {
            canceledPlan.active = false;
            $scope.data.disableButtons = true;
            PlansService.cancelActiveSubscription(canceledPlan.providerId).then(function() {
                PlansService.refreshActiveSubscription();
                $scope.data.disableButtons = false;
                growl.info(
                    "You have successfully canceled your subscription. You plan will still be " +
                    "valid until the end of the billing cycle.", {
                        title: 'Info',
                        ttl: 10000
                    });
            }, function(err) {
                $log.error(err);
                $scope.data.disableButtons = false;
                canceledPlan.active = true;
            })
        }


    }]);
