app.config(['IdleProvider', 'KeepaliveProvider', function(IdleProvider, KeepaliveProvider) {
    IdleProvider.idle(1120);
    IdleProvider.timeout(5);
    KeepaliveProvider.interval(10);
}]).run(['$rootScope', '$modal', 'Idle', function($rootScope, $modal, Idle) {
    Idle.watch();
    $rootScope.started = false;

    function closeModals() {
        if ($rootScope.warning) {
            $rootScope.warning.close();
            $rootScope.warning = null;
        }

        if ($rootScope.timedout) {
            $rootScope.timedout.close();
            $rootScope.timedout = null;
        }
    }

    $rootScope.$on('IdleStart', function() {
        closeModals();

        $rootScope.warning = $modal.open({
            templateUrl: 'app/shared/idle/warning-dialog.html',
            windowClass: 'modal-danger'
        });
    });

    $rootScope.$on('IdleEnd', function() {
        closeModals();
    });

    $rootScope.$on('IdleTimeout', function() {
        closeModals();
        $rootScope.timedout = $modal.open({
            templateUrl: 'app/shared/idle/timedout-dialog.html',
            windowClass: 'modal-danger'
        });
    });

    $rootScope.start = function() {
        closeModals();
        Idle.watch();
        $rootScope.started = true;
    };

    $rootScope.stop = function() {
        closeModals();
        Idle.unwatch();
        $rootScope.started = false;

    };
}]);
