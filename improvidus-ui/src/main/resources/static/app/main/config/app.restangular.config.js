app.config(["$httpProvider", function($httpProvider) {
    var csrf_header = $('meta[name=_csrf-header]').attr('content');
    var csrf_token = $('meta[name=_csrf-token]').attr('content');
    $httpProvider.defaults.headers.common[csrf_header] = csrf_token;
    $httpProvider.defaults.headers.common['Content-Type']
        = 'application/json;charset=UTF-8';
}]);

app.config(["RestangularProvider", "APP_CONSTANTS", function(RestangularProvider, APP_CONSTANTS) {
    RestangularProvider.setBaseUrl(APP_CONSTANTS.API_PATH + '/api/v1');
}]);

app.factory('RestangularV2', ['Restangular', 'APP_CONSTANTS', function RestangularV2(Restangular, APP_CONSTANTS) {
    return Restangular.withConfig(function(RestangularConfigurer) {
        RestangularConfigurer.setBaseUrl(APP_CONSTANTS.API_PATH + '/api/v2');
    });
}]);

app.factory('CachedRestangular', ['Restangular', 'APP_CONSTANTS', function CachedRestangular(Restangular, APP_CONSTANTS) {
    return Restangular.withConfig(function(RestangularConfigurer) {
        RestangularConfigurer.setBaseUrl(APP_CONSTANTS.API_PATH + '/api/v2');
        RestangularConfigurer.setDefaultHttpFields({
            cache: true
        });
    });
}]);


var handleRequest = function(response, growl) {
    growl.error(response.data.message, {
        title: response.data.error
    });
    return true;
}

app.run(["Restangular", "growl", function(Restangular, growl) {
    Restangular.setErrorInterceptor(function(response) {
        handleRequest(response, growl)
    });
}])

app.run(["RestangularV2", "growl", function(RestangularV2, growl) {
    RestangularV2.setErrorInterceptor(function(response) {
        handleRequest(response, growl)
    });
}]);

app.run(["CachedRestangular", "growl", function(CachedRestangular, growl) {
    CachedRestangular.setErrorInterceptor(function(response) {
        handleRequest(response, growl)
    });
}])