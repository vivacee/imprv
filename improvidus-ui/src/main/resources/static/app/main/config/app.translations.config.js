app.config(["$translateProvider", function($translateProvider) {
    $translateProvider.useStaticFilesLoader({
        prefix: 'app/shared/translations/dictionaries/dictionary/languages/',
        suffix: '.json'
    });
    $translateProvider.preferredLanguage('en_US');
    $translateProvider.useSanitizeValueStrategy('escape');
}]);