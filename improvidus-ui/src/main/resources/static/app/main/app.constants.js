angular.module('app')

.constant('APP_CONSTANTS', {OAUTH:{CLIENT_ID:'improvidus',ACCESS_TOKEN_NAME:'access-token-improvidus',REFRESH_TOKEN_NAME:'refresh-token-improvidus',WHITE_LISTED_DOMAINS:['192.168.56.11','localhost','127.0.0.1','int.improvidus.com','www.improvidus.com']},WORDPRESS:{API_PATH:'https://public-api.wordpress.com/wp/v2/sites/improvidusdev.wordpress.com',ENVIRONMENT:'integration'},SSL_ENABLED:false,API_PATH:'http://127.0.0.1:8090'})

;