app.factory('UserService', ['RestangularV2', function UserService(RestangularV2) {
  return {
    getUser: function(uuid) {
      if (uuid)
        return RestangularV2.one('users', uuid).get();
      else
        return RestangularV2.one('user').get();
    },
    getUsers: function() {
      return RestangularV2.one('users').get();
    }
  };
}]);