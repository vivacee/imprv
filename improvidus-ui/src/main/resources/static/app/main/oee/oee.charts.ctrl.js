angular.module('app.controllers.oee.charts', []).controller("OEEChartCtrl",
    ["$scope", "$controller", "$state", "$interval", "$timeout", "$q", "DeviationEntryService", "OEEEntryService", "FilterService", function($scope, $controller, $state, $interval, $timeout, $q, DeviationEntryService, OEEEntryService, FilterService) {


        angular.extend(this, $controller('DataFilterCtrl', {
            $scope: $scope
        }));

        var data = {
            chart: {
                type: 'multiBarChart',
                height: 400,
                margin: {
                    top: 20,
                    right: 20,
                    bottom: 45,
                    left: 45
                },
                multibar: {
                    dispatch: { //container of event handlers
                        elementClick: function(e) {
                            var query = FilterService.getFilter();

                            query.dateRange.startDate = e.data.from;
                            query.dateRange.endDate = e.data.to;
                            query.workCenters = $scope.filter.query.workCenters;
                            query.shifts = $scope.filter.query.shifts.map(function(shift) {
                                return $scope.filter.shifts[parseInt(shift) - 1];
                            });

                            FilterService.saveFilter(query);
                            if (e.data.series == 0)
                                $state.go("Datasource.OEE");
                            else if (e.data.series == 2)
                                $state.go("Datasource.Deviation");
                        }
                    }
                },
                clipEdge: true,
                duration: 500,
                stacked: true,
                xAxis: {
                    showMaxMin: false,
                    tickFormat: function(d) {
                        return d3.time.format('%Y-%m')(new Date(d))
                    }
                },
                yAxis: {
                    axisLabel: 'OEE',
                    axisLabelDistance: -20,
                    tickFormat: function(d) {
                        return d3.format(',.0f')(d * 100) + '%';
                    }
                },
                color: ['#348fe2', '#b6c2c9', '#f59c1a']
            }
        };

        var monthlyData = angular.copy(data);
        var dailyData = angular.copy(data);
        dailyData.chart.xAxis.tickFormat = function(d) {
            return d3.time.format('%Y-%m-%d')(new Date(d))
        };


        $scope.data.monthly = angular.copy(monthlyData);
        $scope.data.monthly.title = {
            enable: true,
            text: 'OEE Monthly Analytics'
        };

        $scope.data.daily = angular.copy(dailyData);
        $scope.data.daily.title = {
            enable: true,
            text: 'OEE Daily Analytics'
        };

        $scope.data.monthlyData = [];
        $scope.data.dailyData = [];

        $scope.getMonthlyOEE = function(number) {
            var now = moment($scope.filter.query.date).endOf('day');
            var endMonth = now.endOf('months');
            var from = angular.copy(endMonth).subtract(number, 'months').endOf('month');
            var to = angular.copy(endMonth);
            $scope.getOEE(from, to);
        };

        $scope.getDailyOEE = function(number) {
            var now = moment($scope.filter.query.date).endOf('day');
            var endOfDay = now.endOf('day');
            var from = angular.copy(endOfDay).subtract(number, 'days');
            var to = angular.copy(endOfDay);
            $scope.getOEE(from, to, 'days');
        };

        function pad(num, size) {
            var s = num + "";
            while (s.length < size) s = "0" + s;
            return s;
        }

        function fill(oeeData, deviationData, from, to, freq) {
            var intervalSize = Math.ceil(to.diff(from, freq, true));
            var oee = [];
            var referenceDate = moment($scope.filter.query.date).add(1, freq);
            for (var i = 0; i < intervalSize; i++) {
                var from = angular.copy(referenceDate).subtract(intervalSize - i, freq).startOf(freq);
                var to = angular.copy(referenceDate).subtract(intervalSize - i - 1, freq).startOf(freq).subtract(1, 'days').endOf('day');
                oee[i] = {
                    x: angular.copy(from).valueOf(),
                    y: 0,
                }
                oee[i].from = from;
                oee[i].to = to;
            }
            var deviation = angular.copy(oee);
            var missing = angular.copy(oee);
            for (var i = 0; i < oeeData.length; i++) {
                var entry = oeeData[i];
                var dateStr = entry['_id'].year + '-' + pad(entry['_id'].month, 2) + '-' + pad((freq == 'days' ? entry['_id'].day : 1), 2);
                var date = moment(dateStr);
                var index = intervalSize - 1 - Math.floor(to.diff(date, freq, true));
                if (!oee[index]) oee[index] = {};
                oee[index].y = entry.sumPlannedOperatingTime == 0 ? 0 : entry.sumUptime / entry.sumPlannedOperatingTime;
                oee[index].sumPlannedOperatingTime = entry.sumPlannedOperatingTime;
            }
            for (var i = 0; i < deviationData.length; i++) {
                var entry = deviationData[i];
                var dateStr = entry['_id'].year + '-' + pad(entry['_id'].month, 2) + '-' + pad((freq == 'days' ? entry['_id'].day : 1), 2);
                var date = moment(dateStr);
                var index = intervalSize - 1 - Math.floor(to.diff(date, freq, true));
                if (oee[index] && oee[index].sumPlannedOperatingTime > 0 && deviation[index])
                    deviation[index].y = oee[index] ? 60 * entry.sumDuration / oee[index].sumPlannedOperatingTime : 0;
            }

            return [{
                key: "OEE",
                values: angular.copy(oee)
            }, {
                key: "Missing",
                values: angular.copy(missing).map(function(entry, i) {
                    return {
                        x: entry.x,
                        y: 1 - (oee[i].y ? oee[i].y : 0) - (deviation[i].y ? deviation[i].y : 0)
                    }
                })
            }, {
                key: "Deviation",
                values: angular.copy(deviation)
            }];
        }


        $scope.data.dailyQueries = [{
            displayName: '2m',
            number: 60
        }, {
            displayName: '1m',
            number: 30
        }, {
            displayName: '2w',
            number: 14
        }, {
            displayName: '1w',
            number: 7
        }];

        $scope.data.activeDailyQuery = $scope.data.dailyQueries.length - 1;

        $scope.data.monthlyQueries = [{
            displayName: '5y',
            number: 60
        }, {
            displayName: '3y',
            number: 36
        }, {
            displayName: '1y',
            number: 12
        }, {
            displayName: '6m',
            number: 6
        }];

        $scope.data.activeMonthlyQuery = $scope.data.monthlyQueries.length - 1;

        $scope.getGraphOEE = function(index, query, type) {
            if (type == 'days') {
                $scope.data.activeDailyQuery = index;
                $scope.getDailyOEE(query.number)
            }
            else {
                $scope.data.activeMonthlyQuery = index;
                $scope.getMonthlyOEE(query.number)
            }
        };

        $scope.store = {};

        function fillCharts(oeeData, deviationData, from, to, freq) {
            if (freq === 'days') {
                $scope.data.dailyData = [];
                $scope.data.dailyData = fill(oeeData, deviationData, from, to, 'days');
            }
            else {
                $scope.data.monthlyData = [];
                $scope.data.monthlyData = fill(oeeData, deviationData, from, to, 'months');
            }
            $timeout(function() {
                d3.selectAll('.nv-legendWrap').attr('transform', 'translate(180 340)');
            }, 1)
        }

        $scope.getOEE = function(from, to, freq) {

            var workCenters = $scope.filter.query.workCenters.map(function(workCenter) {
                if ($scope.filter.workCenters[workCenter.id]) return $scope.filter.workCenters[workCenter.id].label;
            }).filter(function(workCenter) {
                return workCenter !== undefined;
            });

            if (workCenters.length == 0 || $scope.filter.query.shifts.length == 0) {
                fillCharts([], [], from, to, freq);
                return;
            }

            var devPromise = DeviationEntryService.getDeviationEntriesStatistics({
                from: from.valueOf(),
                to: to.valueOf(),
                offset: moment().utcOffset(),
                workCenters: workCenters,
                shifts: $scope.filter.query.shifts,
                freq: freq
            });
            var oeePromise = OEEEntryService.getOEEEntriesStatistics({
                from: from.valueOf(),
                to: to.valueOf(),
                offset: moment().utcOffset(),
                workCenters: workCenters,
                shifts: $scope.filter.query.shifts,
                freq: freq
            });

            if (!$scope.store[freq]) $scope.store[freq] = {};
            $scope.store[freq].from = from;
            $scope.store[freq].to = to;

            $q.all([devPromise, oeePromise])
                .then(function(res) {
                    var deviation = res[0];
                    var oee = res[1];
                    var deviationData = deviation.reverse();
                    var oeeData = oee.reverse();
                    fillCharts(oeeData, deviationData, from, to, freq);
                });
        }

        $scope.$on('$refresh', function() {
            $scope.refresh();
        });

        $scope.refresh = function() {
            var query = FilterService.getFilter();
            if (query) $scope.filter.query = query;
            $scope.getMonthlyOEE($scope.data.monthlyQueries[$scope.data.activeMonthlyQuery].number);
            $scope.getDailyOEE($scope.data.dailyQueries[$scope.data.activeDailyQuery].number);
        }

    }]);