angular.module('app.controllers.oee', ['nvd3', 'app.controllers.oee.charts']).controller(
    "OEECtrl",
    ["$rootScope", "$scope", "$q", "$state", "growl", "$interval", "$timeout", "$controller", "OEEEntryService",
        "DeviationEntryService", "FilterService", "PlansService",
        function($rootScope, $scope, $q, $state, growl, $interval, $timeout, $controller, OEEEntryService,
                 DeviationEntryService, FilterService, PlansService) {

            $rootScope.state = 'OEE';

            angular.extend(this, $controller('DataFilterCtrl', {
                $scope: $scope
            }));

            $scope.controls = {
                topDownOEE: false,
                topDownTEEP: false
            };

            var MINUTES_IN_DAY = 24 * 60;

            $scope.clear = function() {
                $scope.filter.query.date = null;
            };
            $scope.maxDate = new Date();
            $scope.toggleMin = function() {
                $scope.minDate = $scope.minDate ? null : new Date();
            };
            $scope.open = function($event) {
                $event.preventDefault();
                $event.stopPropagation();
                $scope.filter.date.opened = $scope.filter.date.opened ? false : true;
            };
            $scope.dateOptions = {
                formatYear: 'yyyy',
                startingDay: 1
            };

            var tomorrow = new Date();
            tomorrow.setDate(tomorrow.getDate() + 1);
            var afterTomorrow = new Date();
            afterTomorrow.setDate(tomorrow.getDate() + 2);
            $scope.events = [{
                date: tomorrow,
                status: 'full'
            }, {
                date: afterTomorrow,
                status: 'partially'
            }];
            $scope.getDayClass = function(date, mode) {
                if (mode === 'day') {
                    var dayToCheck = new Date(date).setHours(0, 0, 0, 0);
                    for (var i = 0; i < $scope.events.length; i++) {
                        var currentDay = new Date($scope.events[i].date).setHours(0, 0, 0, 0);
                        if (dayToCheck === currentDay) {
                            return $scope.events[i].status;
                        }
                    }
                }
                return '';
            };

            function query() {
                if (!$scope.filter.query.workCenters) return;
                var workCenters = $scope.filter.query.workCenters.map(function(workCenter) {
                    if ($scope.filter.workCenters[workCenter.id]) return $scope.filter.workCenters[workCenter.id].label;
                }).filter(function(workCenter) {
                    return workCenter !== undefined;
                });

                var from = moment($scope.filter.query.date).startOf('day');
                var to = moment($scope.filter.query.date).endOf('day');

                if (workCenters.length == 0 || $scope.filter.query.shifts.length == 0) {
                    $scope.getTopBottomOEE([], []);
                    $scope.getBottomUpOEE([]);
                    return;
                }

                var devPromise = DeviationEntryService.getDeviationEntries({
                    from: from.valueOf(),
                    to: to.valueOf(),
                    workCenters: workCenters,
                    shifts: $scope.filter.query.shifts
                });

                var oeePromise = OEEEntryService.getOeeEntries({
                    from: from.valueOf(),
                    to: to.valueOf(),
                    workCenters: workCenters,
                    shifts: $scope.filter.query.shifts
                });

                $q.all([devPromise, oeePromise])
                    .then(function(res) {
                        var deviation = res[0].content;
                        var oee = res[1].content;
                        $scope.getTopBottomOEE(deviation, oee);
                        $scope.getBottomUpOEE(oee);
                    });
            }

            $scope.filter.options = {
                scrollableHeight: '200px',
                scrollable: true,
                smartButtonMaxItems: 3
            };

            $scope.getBottomUpOEE = function(oeeEntries) {
                $scope.data.bottomUpOee = undefined;
                $scope.data.oeeData = undefined;
                $scope.data.totalEffectivenessBottomUp = undefined;
                if (!oeeEntries || oeeEntries.length == 0) return;
                $scope.data.oeeData = {
                    plannedOperatingTime: 0,
                    weightedAvgCycleTime: 0,
                    goodPiecesCompleted: 0
                }
                var sumTop = 0;
                var sumBottom = 0;
                var workCenters = [];
                oeeEntries.forEach(function(entry) {
                    $scope.data.oeeData.plannedOperatingTime += entry.plannedOperatingTime;
                    $scope.data.oeeData.weightedAvgCycleTime += entry.weightedAvgCycleTime;
                    $scope.data.oeeData.goodPiecesCompleted += entry.goodPiecesCompleted;
                    if (entry.plannedOperatingTime > 0) {
                        sumTop += entry.goodPiecesCompleted * entry.weightedAvgCycleTime;
                        sumBottom += entry.plannedOperatingTime;
                    }
                    if (workCenters.indexOf(entry.workCenter) == -1)
                        workCenters.push(entry.workCenter);
                });
                $scope.data.oeeData.plannedOperatingTime = $scope.data.oeeData.plannedOperatingTime;
                $scope.data.oeeData.weightedAvgCycleTime = $scope.data.oeeData.weightedAvgCycleTime / oeeEntries.length;
                $scope.data.oeeData.goodPiecesCompleted = $scope.data.oeeData.goodPiecesCompleted;
                $scope.data.bottomUpOee = 100 * sumTop / sumBottom;
                $scope.data.totalEffectivenessBottomUp = 100 * sumTop / (MINUTES_IN_DAY * workCenters.length);
            };

            $scope.getTopBottomOEE = function(deviationEntries, oeeEntries) {
                $scope.data.topBottomOee = 0;
                $scope.data.totalEffectivenessTopDown = 0;
                if (!oeeEntries || oeeEntries.length == 0)
                    return;

                var sumTop = 0;
                var sumBottom = 0;
                var workCenters = [];
                oeeEntries.forEach(function(oeeEntry) {
                    var deviationMatches = $.grep(deviationEntries, function(entry) {
                        return entry.deviation1Level === 'OEE Data' && entry.deviation1Level !== 'Planned Downtime' &&
                            entry.shift === oeeEntry.shift && entry.workCenter === oeeEntry.workCenter;
                    });
                    var sumDuration = 0;
                    deviationMatches.forEach(function(entry) {
                        if (entry.duration > 0)
                            sumDuration += entry.duration;
                    });

                    sumTop += (oeeEntry.plannedOperatingTime - 60 * sumDuration);
                    sumBottom += oeeEntry.plannedOperatingTime;
                    if (workCenters.indexOf(oeeEntry.workCenter) == -1)
                        workCenters.push(oeeEntry.workCenter);
                });
                $scope.data.topBottomOee = 100 * (sumTop / sumBottom);
                $scope.data.totalEffectivenessTopDown = 100 * sumTop / (MINUTES_IN_DAY * workCenters.length);
            };

            $scope.goToAnalysis = function() {
                if ($rootScope.currentUser.roles[0] && $rootScope.currentUser.roles[0].name === "SuperAdmin") {
                    $scope.selectState('/analysis');
                    return;
                }

                PlansService.refreshActiveSubscription().then(function() {
                    var subscriptionPlan = PlansService.getActiveSubscription();
                    if (subscriptionPlan.providerId === 'started') {
                        growl.error("The Starter Subscription Plan does not include access to the Analysis Tools", {
                            title: 'Insufficient Subscription'
                        });
                    }
                    else if ($rootScope.currentUser.organization.nusers > subscriptionPlan.properties.nUsers) {
                        growl.info("You have exceeded the maximum number of users allowed for your Subscription Plan. You " +
                            "must remove " + ($rootScope.currentUser.organization.nusers - subscriptionPlan.properties.nUsers) +
                            " user(s) before accessing the Analysis Tools", {
                            title: 'Too Many Users'
                        });
                    }
                    else
                        $scope.selectState('/analysis');
                });
            };

            var timer = $interval(query, 20000);

            $scope.$on('$refresh', function() {
                query();
            });

            $scope.$on('$destroy', function() {
                $interval.cancel(timer);
            });

        }]);