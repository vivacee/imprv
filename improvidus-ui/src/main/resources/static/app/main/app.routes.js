/*******************************************************************************
 * Copyright (c) 2015 Bosch Software Innovations GmbH. All rights reserved.
 ******************************************************************************/
app.config(["$stateProvider", "$urlRouterProvider", "$locationProvider", function($stateProvider, $urlRouterProvider, $locationProvider) {

    $urlRouterProvider.otherwise("/");

    $stateProvider.state('OEE', {
        url: "/",
        templateUrl: "app/main/oee/views/oee.html",
        controller: "OEECtrl",
        reloadOnSearch: false
    });

    $stateProvider.state('Services', {
        url: "/services",
        reloadOnSearch: false,
        templateUrl: "app/main/services/services.html",
        redirectTo: 'Services.Consulting'
    }).state('Services.Consulting', {
        url: "/consulting",
        templateUrl: "app/main/services/views/consulting.html",
        controller: "ConsultingServicesCtrl",
        reloadOnSearch: false
    });

    //     .state('Login', {
    //     url: "/login?passwordReset&userUuid&token",
    //     templateUrl: "app/main/login/views/login.html",
    //     controller: "LandingCtrl",
    //     reloadOnSearch: false,
    //     css: ['assets/css/login/style.min.css']
    // });

}]);
