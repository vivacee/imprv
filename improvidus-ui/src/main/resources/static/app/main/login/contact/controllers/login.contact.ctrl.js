loginApp.controller("LandingContactCtrl", ["$rootScope", "$scope", "$controller", "$timeout", "growl", "EmailService",
    function($rootScope, $scope, $controller, $timeout, growl, EmailService) {
        angular.extend(this, $controller('LandingCtrl', {
            $scope: $scope
        }));

        $scope.controls.processing = false;

        $scope.sendMessage = function(form) {
            $scope.controls.processing = true;

            EmailService.sendContactForm({
                from: form.email,
                to: "",
                body: form.message,
                subject: "Improvidus Contact Form"
            }, {
                company: form.company,
                email: form.email,
                message: form.message,
                username: form.username,
                phone: form.phone,
                website: form.website
            }).then(function() {
                growl.info('Your message has been sent successfully.', {
                    title: 'Message Sent'
                });
                $scope.contactForm = {};
                $scope.contactUs.$setPristine(true);
                $scope.controls.processing = false;
            }, function(error) {
                growl.error('There was a problem sending your message. Please try again.', {
                    title: 'Message Not Sent'
                });
                $scope.controls.processing = false;
            });
        }

    }]);