loginApp.controller("LandingOEECtrl", ["$rootScope", "$scope", "$controller", "$sce", "$http", "YouTubeService",
    function($rootScope, $scope, $controller, $sce, $http, YouTubeService) {
        angular.extend(this, $controller('LandingCtrl', {
            $scope: $scope
        }));

        var videoBase = "https://www.youtube.com/embed/";
        var youtubeKey = "AIzaSyDsNWtmh8rw5czPIo58lp2UuH_OKkjEuFA";

        $scope.controls.showAllVideos = false;

        $scope.data = {
            embedLinks: [],
            videos: []
        };

        $scope.getWordPressCategoryId("videos").then(function(id) {
            $scope.getWordPressPostsByCategoryId(id).then(function(posts) {
                $scope.data.videos = posts;
                var videoIds = $scope.data.videos.map(function(video) {
                    return video.id;
                }).join();
                $scope.getYoutubeInfo(videoIds);
            })
        });

        $scope.getYoutubeInfo = function(videoIds) {
            YouTubeService.getVideoInfo({
                id: videoIds,
                part: 'snippet,statistics',
                key: youtubeKey
            }).then(function(youtubeVideos) {
                if (!youtubeVideos) return;
                for (var i = 0; i < $scope.data.videos.length; i++) {
                    $scope.data.videos[i].youtube = youtubeVideos.items[i];
                    $scope.data.embedLinks.push($sce.trustAsResourceUrl(videoBase + $scope.data.videos[i].id));
                }
            });
        }
    }]);