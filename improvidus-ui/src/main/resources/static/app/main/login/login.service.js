loginApp.factory('AuthService', ['$cookies', "RestangularV2", "APP_CONSTANTS",
    function AuthService($cookies, RestangularV2, APP_CONSTANTS) {
        return {
            isAuthenticated: function() {
                return $cookies.get(APP_CONSTANTS.OAUTH.ACCESS_TOKEN_NAME) && $cookies.get(APP_CONSTANTS.OAUTH.REFRESH_TOKEN_NAME);
            },
            revokeToken: function() {
                $cookies.remove(APP_CONSTANTS.OAUTH.ACCESS_TOKEN_NAME);
                $cookies.remove(APP_CONSTANTS.OAUTH.REFRESH_TOKEN_NAME);
            },
            getNewToken: function(options) {
                var data = $.param(options);
                return RestangularV2.one('oauth').one('token').customPOST(data, '', {
                    "grant_type": "password"
                }, {
                    "Authorization": "Basic " + btoa(APP_CONSTANTS.OAUTH.CLIENT_ID + ":"),
                    'Content-Type': "application/x-www-form-urlencoded"
                })
            },
            getRefreshToken: function(options) {
                var data = $.param(options);
                return RestangularV2.one('oauth').one('token').withHttpConfig({skipAuthorization: true}).customPOST(data, '', {}, {
                    "Authorization": "Basic " + btoa(APP_CONSTANTS.OAUTH.CLIENT_ID + ":"),
                    'Content-Type': "application/x-www-form-urlencoded"
                })
            }
        };
    }]);

loginApp.factory('EmailService', ["Restangular", "APP_CONSTANTS",
    function EmailService(Restangular, APP_CONSTANTS) {
        return {
            sendContactForm: function(email, options) {
                return Restangular.withConfig(function(RestangularConfigurer) {
                    RestangularConfigurer.setBaseUrl(APP_CONSTANTS.API_PATH + '/api/public');
                }).one('contact').customPOST(email, "", options, {});
            }
        };
    }]);

loginApp.factory('YouTubeService', ["Restangular",
    function YouTubeService(Restangular) {
        return {
            getVideoInfo: function(options) {
                return Restangular.withConfig(function(RestangularConfigurer) {
                    RestangularConfigurer.setBaseUrl('https://content.googleapis.com/youtube/v3');
                }).one('videos').customGET("", options);
            }
        };
    }]);

loginApp.factory('WordPressService', ["$q", "CachedRestangular", "APP_CONSTANTS",
    function WordPressService($q, CachedRestangular, APP_CONSTANTS) {
        var environmentId;

        function refreshEnvironmentId() {
            var deferred = $q.defer();
            if (environmentId) deferred.resolve();
            else {
                CachedRestangular.withConfig(function(RestangularConfigurer) {
                    RestangularConfigurer.setBaseUrl(APP_CONSTANTS.WORDPRESS.API_PATH);
                }).one('tags').customGET("", {
                    slug: APP_CONSTANTS.WORDPRESS.ENVIRONMENT
                }).then(function(tags) {
                    environmentId = tags[0].id;
                    deferred.resolve();
                })
            }

            return deferred.promise;
        }

        return {
            getPosts: function(options) {
                return refreshEnvironmentId().then(function() {
                    options.tags = environmentId;
                    options.per_page = 100;
                    return CachedRestangular.withConfig(function(RestangularConfigurer) {
                        RestangularConfigurer.setBaseUrl(APP_CONSTANTS.WORDPRESS.API_PATH);
                    }).one('posts').customGET("", options);
                })
            },
            getCategory: function(options) {
                return CachedRestangular.withConfig(function(RestangularConfigurer) {
                    RestangularConfigurer.setBaseUrl(APP_CONSTANTS.WORDPRESS.API_PATH);
                }).one('categories').customGET("", options);
            }
        };
    }]);