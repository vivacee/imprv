// /*******************************************************************************
//  * Copyright (c) 2015 Bosch Software Innovations GmbH. All rights reserved.
//  ******************************************************************************/
app.config(["$stateProvider", function($stateProvider) {
    $stateProvider.state('Landing', {
        url: "/public",
        templateUrl: "app/main/login/views/login.html",
        reloadOnSearch: false,
        redirectTo: 'Landing.Home'
    });

    $stateProvider.state('Landing.Home', {
        url: "/home?passwordReset&userUuid&token",
        templateUrl: "app/main/login/home/views/home.html",
        controller: "LandingHomeCtrl",
        reloadOnSearch: false,
        css: ['assets/css/login/style.min.css', 'assets/css/landing/landing.css']
    }).state('Landing.Services', {
        url: "/services",
        reloadOnSearch: false,
        templateUrl: "app/main/login/services/views/services.html",
        controller: "LandingCtrl",
        css: ['assets/css/login/style.min.css', 'assets/css/landing/landing.css']
    }).state('Landing.Trainings', {
        url: "/services/trainings?category",
        reloadOnSearch: false,
        templateUrl: "app/main/login/services/views/trainings.html",
        controller: "LandingServicesCtrl",
        css: ['assets/css/login/style.min.css', 'assets/css/landing/landing.css']
    }).state('Landing.Solutions', {
        url: "/solutions",
        reloadOnSearch: false,
        templateUrl: "app/main/login/solutions/views/solutions.html",
        controller: "LandingSolutionsCtrl",
        css: ['assets/css/login/style.min.css', 'assets/css/landing/landing.css']
    }).state('Landing.OEE', {
        url: "/oee",
        reloadOnSearch: false,
        templateUrl: "app/main/login/oee/views/oee.html",
        controller: "LandingOEECtrl",
        css: ['assets/css/login/style.min.css', 'assets/css/landing/landing.css']
    }).state('Landing.Company', {
        url: "/company",
        reloadOnSearch: false,
        templateUrl: "app/main/login/company/views/company.html",
        controller: "LandingCompanyCtrl",
        css: ['assets/css/login/style.min.css', 'assets/css/landing/landing.css']
    })
    //     .state('Landing.Team', {
    //     url: "/company/team",
    //     reloadOnSearch: false,
    //     templateUrl: "app/main/login/company/views/team.html",
    //     controller: "LandingCompanyCtrl",
    //     css: ['assets/css/login/style.min.css', 'assets/css/landing/landing.css']
    // })
        .state('Landing.Contact', {
            url: "/contact",
            reloadOnSearch: false,
            templateUrl: "app/main/login/contact/views/contact.html",
            controller: "LandingContactCtrl",
            css: ['assets/css/login/style.min.css', 'assets/css/landing/landing.css']
        })
}]);
