loginApp.controller("LandingHomeCtrl", ["$rootScope", "$scope", "$controller",
    function($rootScope, $scope, $controller) {
        angular.extend(this, $controller('LandingCtrl', {
            $scope: $scope
        }));

        $scope.data = {
            slides: []
        };

        $scope.getWordPressCategoryId("carousel").then(function(id) {
            $scope.getWordPressPostsByCategoryId(id).then(function(posts) {
                $scope.data.slides = posts;
            })
        });
    }]);