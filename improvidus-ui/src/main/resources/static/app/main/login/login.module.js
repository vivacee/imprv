var loginApp = angular.module('app.controllers.login', ['ui.router']);

loginApp.controller("LandingCtrl", ["$rootScope", "$scope", "$state", "$location", "$stateParams", "$modal", "$modalStack",
    "$http", "$log", "$window", "$timeout", "$cookies", "AuthService", "WordPressService", "APP_CONSTANTS",
    function($rootScope, $scope, $state, $location, $stateParams, $modal, $modalStack, $http, $log, $window, $timeout, $cookies,
             AuthService, WordPressService, APP_CONSTANTS) {

        $(document).ready(function() {
            App.init();
        });

        $scope.loginData = {
            username: null,
            password: null
        }

        $scope.registration = {};
        $scope.passwordReset = {};

        $scope.registrationStatus = 200;
        $scope.registrationMessage = {};

        $scope.controls = {
            registrationView: 'registration',
            processing: false,
            registrationState: 'filling',
            emailFormat: /^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,5}$/,
            passwordFormat: /^(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/
        };

        $scope.getWordPressCategoryId = function(category) {
            return WordPressService.getCategory({
                slug: category
            }).then(function(categories) {
                if (!categories || categories.length == 0) return;
                return categories[0].id
            });
        };

        $scope.getWordPressPostsByCategoryId = function(id) {
            return WordPressService.getPosts({
                categories: id
            }).then(function(posts) {
                if (!posts || posts.length == 0) return;
                var allPosts = [];
                posts.forEach(function(post) {
                    var plainText = post.content.rendered;
                    var sanitizedText = plainText.replace(/<[^>]+>/gm, '');
                    var jsonPost = JSON.parse(sanitizedText);
                    allPosts.unshift(jsonPost);
                });
                return allPosts;
            });
        };

        $scope.passwordReset = function() {
            $modalStack.dismissAll();
            $scope.controls.registrationState = 'filling';
            var opts = {
                templateUrl: "app/main/login/views/password-reset.html",
                scope: $scope,
                size: 'sm'
            };
            var instance = $modal.open(opts);
        }

        if ($location.$$search.passwordReset) $scope.passwordReset();

        $scope.passwordRecovery = function() {
            $modalStack.dismissAll();
            $scope.controls.registrationState = 'filling';
            var opts = {
                templateUrl: "app/main/login/views/password-recovery.html",
                scope: $scope,
                size: 'sm'
            };
            var instance = $modal.open(opts);
        }

        $scope.resetPassword = function() {
            $scope.controls.processing = true;
            $http({
                method: 'POST',
                url: APP_CONSTANTS.API_PATH + '/api/public/v2/passwordreset',
                params: {
                    fgUserName: $scope.passwordReset.email
                }
            }).then(function successCallback(response) {
                $scope.updateRegistrationStatus(response)
            }, function errorCallback(response) {
                $scope.updateRegistrationStatus(response);
                $log.error(response);
            });
        }

        $scope.submitResetPassword = function() {
            $scope.controls.processing = true;
            $http({
                method: 'POST',
                url: APP_CONSTANTS.API_PATH + '/api/public/v2/users/' + $location.$$search.userUuid + "/resetpassword",
                params: {
                    token: $location.$$search.token
                },
                data: {
                    password: $scope.passwordReset.password,
                    password2: $scope.passwordReset.password2
                }
            }).then(function successCallback(response) {
                $scope.updateRegistrationStatus(response)
            }, function errorCallback(response) {
                $scope.updateRegistrationStatus(response);
                $log.error(response);
            });
        }

        $scope.login = function() {
            $modalStack.dismissAll();
            $scope.controls.registrationState = 'filling';
            var opts = {
                templateUrl: "app/main/login/views/logon.html",
                scope: $scope,
                size: 'sm'
            };
            var instance = $modal.open(opts);
            instance.result.then(function() {
                // Success
            }, function() {
                $scope.loginError = false;
            })
        }


        $scope.orgForm = {};
        $scope.register = function() {
            $modalStack.dismissAll();
            $scope.controls.registrationState = 'filling';
            var opts = {
                templateUrl: "app/main/login/views/register.html",
                scope: $scope,
                size: 'md'
            };
            var instance = $modal.open(opts);
            instance.result.then(function() {
                // Success
            }, function() {
                $scope.loginError = false;
            })
        }


        $scope.updateRegistrationStatus = function(response) {
            $scope.controls.processing = false;
            switch (response.data.exception) {
                case "com.bosch.im.service.InvalidPasswordException":
                    $scope.controls.registrationState = 'duplicatePasswordError'
                    break;
                case "com.bosch.improvidus.passwordreset.ExpiredTokenException":
                    $scope.controls.registrationState = 'expiredTokenError'
                    break;
                case "com.bosch.improvidus.web.exceptions.DuplicatedEntityException":
                    $scope.controls.registrationState = 'duplicateOrganizationError'
                    $scope.controls.errorMessage = "An Organization with the same name already exists.";
                    break;
                case "com.bosch.improvidus.user.DuplicatedUsernameException":
                    $scope.controls.registrationState = 'duplicateUserError'
                    $scope.controls.errorMessage = "Email address is already in use.";
                    break;
                default:
                    $scope.controls.registrationState = 'success'
            }
        }

        $scope.registerSubmit = function() {
            $scope.orgForm.user.username = $scope.orgForm.user.email;
            $scope.orgForm.org.name = $scope.orgForm.org.displayName.toUpperCase().split(" ")
                .join("_");
            $scope.controls.processing = true;
            $http({
                method: 'POST',
                url: APP_CONSTANTS.API_PATH + '/api/public/v2/organization',
                data: $scope.orgForm,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function successCallback(response) {
                $scope.controls.processing = false;
                $scope.updateRegistrationStatus(response);
                $scope.orgForm = {};
            }, function errorCallback(response) {
                $scope.controls.processing = false;
                $scope.updateRegistrationStatus(response);
                $log.error(response);
            });

        }

        function delayLogin() {
            $scope.controls.delay = $scope.controls.delay - 1;
            if ($scope.controls.delay <= 0) {
                $scope.controls.error = undefined;
                return;
            }
            $timeout(delayLogin, 1000);
            $scope.controls.error = "Exceeded maximum login attempts. Please wait "
                + $scope.controls.delay + " seconds before next attempt.";
        }

        $scope.close = function() {
            $modalStack.dismissAll();
        }

        function showCookieWarning() {
            $modalStack.dismissAll();
            var opts = {
                templateUrl: "app/main/login/views/cookie-warning.html",
                scope: $scope,
                size: 'md'
            };
            var instance = $modal.open(opts);
        }

        $scope.doLogin = function() {
            if ($scope.controls.delay > 0) return;

            AuthService.getNewToken({
                username: $scope.loginData.username,
                password: $scope.loginData.password
            }).then(function(token) {
                $modalStack.dismissAll();
                // console.log(response);
                // var tokenPayload = jwtHelper.decodeToken(response.data.access_token);
                // console.log(tokenPayload);
                $cookies.put(APP_CONSTANTS.OAUTH.ACCESS_TOKEN_NAME, token.access_token);
                $cookies.put(APP_CONSTANTS.OAUTH.REFRESH_TOKEN_NAME, token.refresh_token);
                if (!$cookies.get(APP_CONSTANTS.OAUTH.ACCESS_TOKEN_NAME))
                    showCookieWarning();
                else {
                    $rootScope.initApp();
                    $scope.selectState('/');
                }
            }, function(error) {
                var description = error.data.error_description;
                if (description && description.indexOf("The login attempts exceed the maximum limit") > -1) {
                    $scope.controls.delay = parseInt(description.replace(/^\D+|\D+$/g, "")) + 2;
                    delayLogin();
                }
                else
                    $scope.controls.error = "Invalid Username or Password."
            });
        }
    }]);

loginApp.directive('passMatch', function() {
    return {
        require: 'ngModel',
        link: function(scope, elm, attrs, ctrl) {
            var originPass = '#' + attrs.passMatch;
            elm.add(originPass).on('keyup', function() {
                scope.$apply(function() {
                    var v = elm.val() === $(originPass).val();
                    ctrl.$setValidity('passMatch', v);
                });
            });
        }
    }
})