loginApp.controller("LandingSolutionsCtrl", ["$rootScope", "$scope", "$controller", "$stateParams",
    function($rootScope, $scope, $controller, $stateParams) {
        angular.extend(this, $controller('LandingCtrl', {
            $scope: $scope
        }));

        var solutions = {
            paretos: {
                images: ["mmparetos.png"],
                title: "Multi-Metric Pareto and Recommendations",
                description: "Allows for easy visualization, filtering, and drill down for your deviation data set. These tools highlight opportunities, guide the user and provide recommendations by looking for filtering options within the data that give a \"line of best fit\" following pareto rules."
            },
            correlations: {
                images: ["correlations.png"],
                title: "Event Correlation Tool",
                description: "Provides insights into correlating events and time between those events. This enables users to address the root cause of an issue that is further upstream in the process rather than focusing on the symptoms that show up downstream. This powerful tool will open up new and improved transparency to pinpoint and drive Connected Industry investments and improvements using the most efficient approach."
            },
            wordcloud: {
                images: ["wordcloud.png"],
                title: "Word Cloud",
                description: "Associates word usage with impact to help find systemic issues and is especially useful if you have access to free text inputs from operators/technicians and impacts (hours, cost, etc.), but do not have highly organized deviation data."
            },
            oee: {
                images: ["tutorials.jpg"],
                title: "Overall Equipment Effectiveness (OEE) Visualization Tool",
                description: "Shows you equipment availability, production output, and deviation information. Users are able to filter and navigate based on their specific areas of responsibility. This is an outstanding tool for making sound decisions for Connected Industry investments. The OEE tool is available at no cost."
            }
        };

        // $scope.selectSolution = function(solution) {
        //     switch (solution) {
        //         case "paretos":
        //             $scope.data.selectedSolution = solutions.paretos;
        //             break;
        //         case "correlations":
        //             $scope.data.selectedSolution = solutions.correlations;
        //             break;
        //         case "wordcloud":
        //             $scope.data.selectedSolution = solutions.wordcloud;
        //             break;
        //         case "oee":
        //             $scope.data.selectedSolution = solutions.oee;
        //             break;
        //         default :
        //             $scope.data.selectedSolution = null;
        //             break;
        //     }
        // };
        // $scope.selectSolution($stateParams.solution);
    }]);