loginApp.controller("LandingServicesCtrl", ["$rootScope", "$scope", "$controller", "$http", "$window", "$stateParams",
    function($rootScope, $scope, $controller, $http, $window, $stateParams) {
        angular.extend(this, $controller('LandingCtrl', {
            $scope: $scope
        }));

        $scope.data = {
            selectedTraining: null,
            trainings: []
        };

        $scope.controls.showAllTrainings = false;

        $scope.scrollToTop = function() {
            $('#content').animate({
                scrollTop: $('#content').offset().top + 300
            }, 0);
        };

        $scope.isArray = function(array) {
            return Array.isArray(array);
        }

        $scope.loadTrainings = function(listName) {
            $scope.getWordPressCategoryId(listName).then(function(id) {
                $scope.getWordPressPostsByCategoryId(id).then(function(posts) {
                    $scope.data.trainings = posts;
                })
            });
        };

        $scope.initTrainings = function() {
            switch ($stateParams.category) {
                case "leadership":
                    $scope.loadTrainings('leadership');
                    $scope.data.trainingCategory = "Leadership Trainings";
                    break;
                case "lean":
                    $scope.loadTrainings('lean');
                    $scope.data.trainingCategory = "Lean Trainings";
                    break;
                case "manufacturing":
                default :
                    $scope.loadTrainings('manufacturing');
                    $scope.data.trainingCategory = "Manufacturing Trainings";
                    break;
            }
        };
        $scope.initTrainings();
    }]);