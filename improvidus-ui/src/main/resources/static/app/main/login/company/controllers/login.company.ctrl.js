loginApp.controller("LandingCompanyCtrl", ["$rootScope", "$scope", "$controller",
    function($rootScope, $scope, $controller) {
        angular.extend(this, $controller('LandingCtrl', {
            $scope: $scope
        }));
    }]);