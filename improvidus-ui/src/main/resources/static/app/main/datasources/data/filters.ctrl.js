angular.module('app.controllers.datasources.filters', []).controller(
    'DataFilterCtrl',
    ['$rootScope', '$scope', 'DeviationEntryService', 'FilterService',
        function($rootScope, $scope, DeviationEntryService, FilterService) {

            var querySaved = false;

            var getQuery = function() {
                var query = FilterService.getFilter();
                if (query) {
                    querySaved = true;
                    query.date = moment(query.date).format('YYYY-MM-DD');
                    return angular.copy(query);
                }
                else
                    return {
                        workCenters: [],
                        shifts: [1, 2, 3, 4, 5, 6, 7, 8],
                        date: moment().format('YYYY-MM-DD'),
                        dateRange: {
                            startDate: moment().subtract(2, 'years'),
                            endDate: moment()
                        }
                    };
            };

            $scope.filter = {
                date: {
                    opened: false
                },
                dateOptions: {
                    opens: 'left',
                    ranges: {
                        'Today': [moment().startOf('day'), moment()],
                        'Last Week': [moment().subtract(7, 'days'), moment()],
                        'Last Month': [moment().subtract(1, 'months'), moment()],
                        'Last 3 Months': [moment().subtract(3, 'months'), moment()],
                        'Last 6 Months': [moment().subtract(6, 'months'), moment()]
                    },
                },
                shifts: [1, 2, 3, 4, 5, 6, 7, 8],
                workCenters: [],
                query: getQuery(),
                options: {
                    scrollableHeight: '200px',
                    scrollable: true,
                    smartButtonMaxItems: 3
                }
            };

            $scope.getNumber = function(num) {
                return new Array(num);
            };

            $scope.getQueryOptions = function(tableState) {
                if (!$scope.tableState && tableState)
                    $scope.tableState = tableState;

                var pagination = tableState.pagination;

                var start = pagination.start || 0;     // This is NOT the page number, but the index of item in the list that you want to use to display the table.
                var number = pagination.number || 10;  // Number of entries showed per page.

                var workCenters = $scope.filter.query.workCenters.map(function(workCenter) {
                    if ($scope.filter.workCenters[workCenter.id]) return $scope.filter.workCenters[workCenter.id].label;
                }).filter(function(workCenter) {
                    return workCenter !== undefined;
                });

                var shifts = $scope.filter.query.shifts.map(Number);

                var from = moment($scope.filter.query.dateRange.startDate).startOf('day');
                var to = moment($scope.filter.query.dateRange.endDate).endOf('day');

                return {
                    from: from.valueOf(),
                    to: to.valueOf(),
                    workCenters: workCenters,
                    shifts: shifts,
                    start: start / number,
                    number: number
                }
            };

            $scope.areItemsSelected = function(items) {
                if (!items) return false;
                return $.grep(items, function(entry) {
                    return entry.isSelected;
                }).length > 0;
            };

            $scope.validateDateFormat = function(dateString) {
                if (!/^\d{1,2}\/\d{1,2}\/\d{4}$/.test(dateString))
                    return false;
                var parts = dateString.split("/");
                var day = parseInt(parts[1], 10);
                var month = parseInt(parts[0], 10);
                var year = parseInt(parts[2], 10);
                if (year < 1000 || year > 3000 || month == 0 || month > 12)
                    return false;
                var monthLength = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
                if (year % 400 == 0 || (year % 100 != 0 && year % 4 == 0))
                    monthLength[1] = 29;
                return day > 0 && day <= monthLength[month - 1];
            };

            $scope.validateTimeFormat = function(timeString) {
                var utcPattern = /^(1[012]|[1-9]):([0-5][0-9]):([0-5][0-9]) (am|pm|AM|PM)$/; //9:44:56 PM
                var timezonePattern = /^(2[0123]|1[0-9]|0[1-9]):([0-5][0-9]):([0-5][0-9])(-|\+)(1[012]|0[0-9]):([03]0)$/; //15:44:56-06:00
                return utcPattern.test(timeString) || timezonePattern.test(timeString);
            };

            $scope.validateShift = function(shiftString) {
                var f = /^([12345678])$/;
                return f.test(shiftString);
            };

            $scope.validateString = function(str) {
                return typeof str === 'string';
            };

            $scope.validateNumber = function(number) {
                if (!number) return false;
                number = number.replace(/[",]+/g, '');
                return (number - 0) == number && ('' + number).trim().length > 0;
            };

            $scope.downloadSample = function(type) {
                if (window.navigator.msSaveOrOpenBlob && window.Blob) {
                    var blob = new Blob([$scope.data.samplePreview], {type: "text/csv"});
                    navigator.msSaveOrOpenBlob(blob, type + ".csv");
                } else {
                    var csvContent = "data:text/csv;charset=utf-8," + $scope.data.samplePreview;
                    var encodedUri = encodeURI(csvContent);
                    var link = document.createElement("a");
                    link.setAttribute("href", encodedUri);
                    link.setAttribute("download", type + ".csv");
                    document.body.appendChild(link);
                    link.click();
                }
            };

            var sortByName = function(a, b) {
                var nameA = a.toUpperCase();
                var nameB = b.toUpperCase();
                if (nameA < nameB) {
                    return -1;
                }
                if (nameA > nameB) {
                    return 1;
                }

                return 0;
            }

            var getDeviationEntriesWorkCenters = function() {
                return DeviationEntryService.getDeviationEntriesWorkCenters().then(function(workCenters) {
                    var index = 0;
                    workCenters = workCenters.sort(sortByName);
                    $scope.filter.workCenters = workCenters.map(function(workCenter) {
                        return {
                            id: index++,
                            label: workCenter
                        }
                    });
                });
            };

            $scope.$watch('filter.query', function() {
                if (!$scope.filter.query) return;
                if (!$scope.filter.query.date || !$scope.filter.query.workCenters || !$scope.filter.query.shifts || !$scope.filter.workCenters) return;
                FilterService.saveFilter($scope.filter.query);
                $rootScope.$broadcast('$refresh');
            }, true);

            $scope.init = function() {
                getDeviationEntriesWorkCenters().then(function() {
                    if (!querySaved)
                        $scope.filter.query.workCenters = $scope.filter.workCenters.map(function(workCenter) {
                            return {
                                id: workCenter.id
                            }
                        });
                    $rootScope.$broadcast('$refresh');
                })
            };
            $scope.init();
        }
    ]);




