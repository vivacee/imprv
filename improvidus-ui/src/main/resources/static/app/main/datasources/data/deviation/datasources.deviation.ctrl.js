angular.module('app.controllers.datasources.deviation', ['app.controllers.datasources.filters']).controller(
    'DatasourcesDeviationCtrl',
    ['$scope', '$controller', '$http', '$log', '$timeout', 'growl', '$modal', '$modalStack', '$location', 'DeviationEntryService', 'APP_CONSTANTS',
        function($scope, $controller, $http, $log, $timeout, growl, $modal, $modalStack, $location, DeviationEntryService, APP_CONSTANTS) {

            $scope.data = {
                deviationEntries: [],
                newEntry: {},
                process: false,
                selectedShifts: []
            };

            $scope.buttons = {
                confirmed: false,
                isCollapsed: false
            }

            $scope.controls = {};

            angular.extend(this, $controller('DataFilterCtrl', {
                $scope: $scope
            }));

            // $scope.selectAll = function() {
            //     $scope.data.displayedDeviationEntries.forEach(function(entry) {
            //         entry.isSelected = true;
            //     })
            // }

            $scope.showDeviationCodes = function() {
                var opts = {
                    templateUrl: "app/main/datasources/data/deviation/views/deviations.html",
                    scope: $scope,
                    size: 'md'
                };
                var instance = $modal.open(opts);
            }

            $scope.copyDeviations = function() {
                var text = '';
                if ($scope.data.selectedDeviation)
                    text += format($scope.data.selectedDeviation.displayName) + ',';
                if ($scope.data.selectedType)
                    text += format($scope.data.selectedType.displayName) + ',';
                if ($scope.data.selectedGroup)
                    text += format($scope.data.selectedGroup.displayName) + ',';
                if ($scope.data.selectedGroupType)
                    text += format($scope.data.selectedGroupType.displayName);
                copyTextToClipboard(text);
            }

            function format(text) {
                if (text.indexOf(",") > -1)
                    return "\"" + text + "\"";
                else
                    return text;
            }

            $scope.copy = function(text) {
                copyTextToClipboard(text)
            }

            $scope.close = function() {
                $modalStack.dismissAll();
                if (!$scope.data.selectedDeviation) return;
                $scope.data.newEntry.deviation1Level = $scope.data.selectedDeviation.displayName;
                $scope.data.newEntry.deviation2Level = $scope.data.selectedType.displayName;
                $scope.data.newEntry.deviation3Level = $scope.data.selectedGroup.displayName;
                $scope.data.newEntry.deviation4Level = $scope.data.selectedGroupType.displayName;
            }


            $scope.data.samplePreview = "Date,Time,Shift,Plant,Cost Center,Work Center,Deviation 1st level,Deviation 2nd Level,Deviation 3rd Level,Deviation 4th level,Duration,Amount,Partnumber,Comment\n" +
                "11/30/2014,9:00:00 PM,3,US10,10155541,MCF12CP,OEE Data,Technical,Mechanical Failure,Breakdown machine,4.72,177.84,N/A,\"Spindle Failure, should be fixed\"\n" +
                "11/30/2014,15:00:00-06:00,3,US10,10155541,MCF12CP,OEE Data,Technical,Mechanical Failure,Breakdown machine,4.72,177.84,N/A,\"Spindle Failure, should be fixed\""

            function copyTextToClipboard(text) {
                var textArea = document.createElement("textarea");
                textArea.style.position = 'fixed';
                textArea.style.top = 0;
                textArea.style.left = 0;
                textArea.style.width = '2em';
                textArea.style.height = '2em';
                textArea.style.padding = 0;
                textArea.style.border = 'none';
                textArea.style.outline = 'none';
                textArea.style.boxShadow = 'none';
                textArea.style.background = 'transparent';
                textArea.value = text;
                document.body.appendChild(textArea);
                textArea.select();
                try {
                    var successful = document.execCommand('copy');
                    var msg = successful ? 'successful' : 'unsuccessful';
                    console.log('Copying text command was ' + msg);
                } catch (err) {
                    console.log('Oops, unable to copy');
                }
                document.body.removeChild(textArea);
            }

            $http.get('app/main/datasources/data/deviation/data/fields.json')
                .then(function(res) {
                    $scope.data.fieldsDescription = res.data;
                });

            $http.get('app/main/datasources/data/deviation/data/deviations.json')
                .then(function(res) {
                    $scope.data.deviationData = res.data;
                });

            $scope.initDate = function() {
                $scope.data.newEntry.timestamp = $scope.latestDate ? $scope.latestDate : new Date();
            };
            $scope.initDate();
            $scope.clear = function() {
                $scope.data.newEntry.timestamp = null;
            };
            $scope.maxDate = new Date();
            $scope.toggleMin = function() {
                $scope.minDate = $scope.minDate ? null : new Date();
            };
            $scope.open = function($event) {
                $event.preventDefault();
                $event.stopPropagation();
                $scope.data.opened = $scope.data.opened ? false : true;
            };
            $scope.dateOptions = {
                formatYear: 'yy',
                startingDay: 1
            };
            var tomorrow = new Date();
            tomorrow.setDate(tomorrow.getDate() + 1);
            var afterTomorrow = new Date();
            afterTomorrow.setDate(tomorrow.getDate() + 2);
            $scope.events = [{
                date: tomorrow,
                status: 'full'
            }, {
                date: afterTomorrow,
                status: 'partially'
            }];
            $scope.getDayClass = function(date, mode) {
                if (mode === 'day') {
                    var dayToCheck = new Date(date).setHours(0, 0, 0, 0);
                    for (var i = 0; i < $scope.events.length; i++) {
                        var currentDay = new Date($scope.events[i].date).setHours(0, 0, 0, 0);
                        if (dayToCheck === currentDay) {
                            return $scope.events[i].status;
                        }
                    }
                }
                return '';
            };

            $scope.add = function() {
                $scope.data.newEntry = {};
                $scope.initDate();
            };

            function processLine(text) {
                var re_valid = /^\s*(?:'[^'\\]*(?:\\[\S\s][^'\\]*)*'|"[^"\\]*(?:\\[\S\s][^"\\]*)*"|[^,'"\s\\]*(?:\s+[^,'"\s\\]+)*)\s*(?:,\s*(?:'[^'\\]*(?:\\[\S\s][^'\\]*)*'|"[^"\\]*(?:\\[\S\s][^"\\]*)*"|[^,'"\s\\]*(?:\s+[^,'"\s\\]+)*)\s*)*$/;
                var re_value = /(?!\s*$)\s*(?:'([^'\\]*(?:\\[\S\s][^'\\]*)*)'|"([^"\\]*(?:\\[\S\s][^"\\]*)*)"|([^,'"\s\\]*(?:\s+[^,'"\s\\]+)*))\s*(?:,|$)/g;
                if (!re_valid.test(text)) return null;
                var a = [];
                text.replace(re_value,
                    function(m0, m1, m2, m3) {
                        if (m1 !== undefined) a.push(m1.replace(/\\'/g, "'"));
                        else if (m2 !== undefined) a.push(m2.replace(/\\"/g, '"'));
                        else if (m3 !== undefined) a.push(m3);
                        return '';
                    });
                if (/,\s*$/.test(text)) a.push('');
                return a;
            };


            function processData(allText) {
                var allTextLines = allText.split(/\r\n|\n|\r/);
                allTextLines = allTextLines.filter(function(line) {
                    return line != ""
                });
                var lines = [];
                while (allTextLines.length > 0)
                    lines.push(processLine(allTextLines.shift()));
                return lines;
            }

            $scope.upload = function() {
                $timeout(function() {
                    $("#input-1a").fileinput({
                        showUpload: false,
                        mainClass: "input-group-sm"
                    });

                    $('#input-1a').on('fileloaded', function(event, file, previewId, index, reader) {
                        $scope.data.file = file;
                        $scope.$apply();
                    });

                    $('#input-1a').on('fileclear', function(event, file, previewId, index, reader) {
                        $scope.data.file = null;
                        $scope.controls.analysis = false;
                        $scope.$apply();
                    });
                }, 1)
            }

            $scope.preAnalyze = function() {
                if ($scope.data.file.size > 25000) {
                    growl.warning("File is too large for pre-validation.", {
                        title: 'Error'
                    });
                    return;
                }
                $scope.controls.analysis = true;
                var reader = new FileReader();
                reader.onload = function(e) {
                    var data = processData(e.target.result);
                    data.shift()
                    $scope.data.filePreview = data;
                    $scope.validateData();
                };
                reader.readAsText($scope.data.file);
            }

            $scope.validateData = function() {
                $scope.data.errors = [];
                $scope.data.filePreview.forEach(function(row, idx) {
                    if (!row.length) return;
                    var errors = [];
                    for (var i = 0; i < 14; i++) {
                        switch (i) {
                            case 0:
                                if (!$scope.validateDateFormat(row[i]))
                                    errors.push(i);
                                break;
                            case 1:
                                if (!$scope.validateTimeFormat(row[i]))
                                    errors.push(i);
                                break;
                            case 2:
                                if (!$scope.validateShift(row[i]))
                                    errors.push(i);
                                break;
                            case 3:
                            case 4:
                            case 6:
                            case 7:
                            case 8:
                            case 9:
                            case 12:
                            case 13:
                                if (row[i] == undefined || !$scope.validateString(row[i]))
                                    errors.push(i);
                                break;
                            case 5:
                                if (!row[i] || !$scope.validateString(row[i]))
                                    errors.push(i);
                                break;
                            case 10:
                            case 11:
                                if (row[i] == undefined || (row[i] && !$scope.validateNumber(row[i])))
                                    errors.push(i);
                                break;
                            default:
                                break;
                        }
                    }
                    row.errors = errors;
                })
            }

            $scope.save = function() {
                if (!$scope.data.newEntry.uuid) {
                    DeviationEntryService.addDeviationEntries([$scope.data.newEntry]).then(function(entries) {
                        if (entries.length == 0) {
                            growl.error("Entry was not saved. Please validate the format of each entry and " +
                                "create matching Work Centers before uploading data. Please also make sure you have " +
                                "permission to upload data for the specified Work Center", {
                                title: 'Warning'
                            });
                            $scope.edit($scope.data.newEntry);
                        }
                        else {
                            growl.info("Entry added successfully.", {
                                title: 'Info'
                            });
                            $scope.refresh();
                        }
                    });
                } else {
                    DeviationEntryService.updateDeviationEntry($scope.data.newEntry.uuid, $scope.data.newEntry).then(function(entry) {
                        $scope.buttons.isCollapsed = false;
                    }, function(err) {
                        $scope.buttons.isCollapsed = true;
                        $scope.data.newEntry = angular.copy($scope.data.backUp);
                    });
                }
            }

            $scope.edit = function(entry) {
                $scope.data.backUp = angular.copy(entry);
                $scope.buttons.isCollapsed = true;
                $scope.data.newEntry = entry;
            }

            $scope.remove = function() {
                var entries = $.grep($scope.data.displayedDeviationEntries, function(entry) {
                    return entry.isSelected;
                })
                entries.forEach(function(entry) {
                    DeviationEntryService.deleteDeviationEntry(entry.uuid).then(function() {
                        $scope.getDeviationEntries($scope.tableState);
                    });
                });
            }

            $scope.submitFile = function() {
                var reader = new FileReader();
                reader.onload = function(e) {
                    var data = processData(e.target.result);
                    var numSubmitted = data.length - 1;
                    var fd = new FormData();
                    fd.append('file', $scope.data.file);

                    $http.post(APP_CONSTANTS.API_PATH + "/api/v2/deviationentries/source", fd, {
                        transformRequest: angular.identity,
                        headers: {
                            'Content-Type': undefined
                        }
                    }).then(function(result) {
                        $scope.buttons.upload = false;
                        $scope.refresh();
                        growl.info(result.data.length + " entries have been added.", {
                            title: 'Info'
                        });
                        if (result.data.length < numSubmitted) {
                            growl.warning(numSubmitted - result.data.length + " entries have not been saved. Please " +
                                "validate the format of each entry and create matching Work Centers before uploading " +
                                "data. Please also make sure you have permission to upload data for each Work Center", {
                                title: 'Warning'
                            });
                        }
                    }, function(err) {
                        growl.error("There has been an error processing your request.", {
                            title: 'Error'
                        });
                    }, function(err) {
                        $scope.controls.initFlag = false;
                    });
                };
                reader.readAsText($scope.data.file);
            }

            $scope.refresh = function() {
                $scope.getDeviationEntries($scope.tableState);
            }

            $scope.$watch('filter', function() {
                if (!$scope.tableState) return;
                $scope.getDeviationEntries($scope.tableState);
            }, true);

            $scope.getDeviationEntries = function(tableState) {
                var options = $scope.getQueryOptions(tableState);
                if (options.shifts.length == 0 || options.workCenters.length == 0) {
                    $scope.data.displayedDeviationEntries = [];
                    tableState.pagination.numberOfPages = 0;
                    return;
                }
                DeviationEntryService.getDeviationEntries(options).then(function(result) {
                    $scope.data.displayedDeviationEntries = result.content;
                    tableState.pagination.numberOfPages = result.totalPages;
                });

            };
        }
    ]);

