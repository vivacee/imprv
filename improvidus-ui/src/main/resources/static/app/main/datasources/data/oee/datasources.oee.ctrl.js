angular.module('app.controllers.datasources.oee', []).controller(
    'DatasourcesOEECtrl',
    ['$rootScope', '$scope', '$controller', '$http', '$log', '$timeout', 'growl', 'OEEEntryService', 'APP_CONSTANTS',
        function($rootScope, $scope, $controller, $http, $log, $timeout, growl, OEEEntryService, APP_CONSTANTS) {

            $rootScope.state = 'OEE Data';

            $scope.data = {
                oeeEntries: [],
                newEntry: {}
            };

            $scope.buttons = {
                confirmed: false,
                isCollapsed: false
            };

            $scope.controls = {};

            angular.extend(this, $controller('DataFilterCtrl', {
                $scope: $scope
            }));

            $scope.data.samplePreview = "Date,Time,Work Center,Shift,Planned Operating Time,Weighted Average Cycle Time (min),Good Pieces Completed\n" +
                "4/30/2015,9:00:00 PM,MCF45CP,1,420,3.24,138\n" +
                "4/30/2015,15:00:00-06:00,MCF45CP,2,420,3.24,138\n" +
                "4/30/2015,MCF45CP,3,420,3.24,138";


            $scope.initDate = function() {
                $scope.data.newEntry.timestamp = $scope.latestDate ? $scope.latestDate : new Date();
            };
            $scope.initDate();
            $scope.clear = function() {
                $scope.data.newEntry.timestamp = null;
            };
            $scope.maxDate = new Date();
            $scope.toggleMin = function() {
                $scope.minDate = $scope.minDate ? null : new Date();
            };
            $scope.open = function($event) {
                $event.preventDefault();
                $event.stopPropagation();
                $scope.data.opened = $scope.data.opened ? false : true;
            };
            $scope.dateOptions = {
                formatYear: 'yy',
                startingDay: 1
            };
            var tomorrow = new Date();
            tomorrow.setDate(tomorrow.getDate() + 1);
            var afterTomorrow = new Date();
            afterTomorrow.setDate(tomorrow.getDate() + 2);
            $scope.events = [{
                date: tomorrow,
                status: 'full'
            }, {
                date: afterTomorrow,
                status: 'partially'
            }];
            $scope.getDayClass = function(date, mode) {
                if (mode === 'day') {
                    var dayToCheck = new Date(date).setHours(0, 0, 0, 0);
                    for (var i = 0; i < $scope.events.length; i++) {
                        var currentDay = new Date($scope.events[i].date).setHours(0, 0, 0, 0);
                        if (dayToCheck === currentDay) {
                            return $scope.events[i].status;
                        }
                    }
                }
                return '';
            };

            function processLine(text) {
                var re_valid = /^\s*(?:'[^'\\]*(?:\\[\S\s][^'\\]*)*'|"[^"\\]*(?:\\[\S\s][^"\\]*)*"|[^,'"\s\\]*(?:\s+[^,'"\s\\]+)*)\s*(?:,\s*(?:'[^'\\]*(?:\\[\S\s][^'\\]*)*'|"[^"\\]*(?:\\[\S\s][^"\\]*)*"|[^,'"\s\\]*(?:\s+[^,'"\s\\]+)*)\s*)*$/;
                var re_value = /(?!\s*$)\s*(?:'([^'\\]*(?:\\[\S\s][^'\\]*)*)'|"([^"\\]*(?:\\[\S\s][^"\\]*)*)"|([^,'"\s\\]*(?:\s+[^,'"\s\\]+)*))\s*(?:,|$)/g;
                if (!re_valid.test(text)) return null;
                var a = [];
                text.replace(re_value,
                    function(m0, m1, m2, m3) {
                        if (m1 !== undefined) a.push(m1.replace(/\\'/g, "'"));
                        else if (m2 !== undefined) a.push(m2.replace(/\\"/g, '"'));
                        else if (m3 !== undefined) a.push(m3);
                        return '';
                    });
                if (/,\s*$/.test(text)) a.push('');
                return a;
            }


            function processData(allText) {
                var allTextLines = allText.split(/\r\n|\n|\r/);
                allTextLines = allTextLines.filter(function(line) {
                    return line != ""
                });
                var lines = [];
                while (allTextLines.length > 0)
                    lines.push(processLine(allTextLines.shift()));
                return lines;
            }

            $scope.validateData = function() {
                $scope.data.errors = [];
                $scope.data.filePreview.forEach(function(row, idx) {
                    if (!row.length) return;
                    //Needed for backwards compatibility: if no time specified, auto fill 12PM UTC
                    if (row.length < 7 && !$scope.validateTimeFormat(row[1]))
                        row.splice(1, 0, "12:00:00 PM");
                    var errors = [];
                    for (var i = 0; i < 7; i++) {
                        switch (i) {
                            case 0:
                                if (!$scope.validateDateFormat(row[i]))
                                    errors.push(i);
                                break;
                            case 1:
                                if (!$scope.validateTimeFormat(row[i]))
                                    errors.push(i);
                                break;
                            case 2:
                                if (!$scope.validateString(row[i]))
                                    errors.push(i);
                                break;
                            case 3:
                            case 4:
                            case 5:
                            case 6:
                                if (row[i] == undefined || (row[i] && !$scope.validateNumber(row[i])))
                                    errors.push(i);
                                break;
                            default:
                                break;
                        }
                    }
                    row.errors = errors;
                })
            };

            $http.get('app/main/datasources/data/oee/data/fields.json')
                .then(function(res) {
                    $scope.data.fieldsDescription = res.data;
                });

            $scope.copy = function(text) {
                copyTextToClipboard(text)
            };

            function copyTextToClipboard(text) {
                var textArea = document.createElement("textarea");
                textArea.style.position = 'fixed';
                textArea.style.top = 0;
                textArea.style.left = 0;
                textArea.style.width = '2em';
                textArea.style.height = '2em';
                textArea.style.padding = 0;
                textArea.style.border = 'none';
                textArea.style.outline = 'none';
                textArea.style.boxShadow = 'none';
                textArea.style.background = 'transparent';
                textArea.value = text;
                document.body.appendChild(textArea);
                textArea.select();
                try {
                    var successful = document.execCommand('copy');
                    var msg = successful ? 'successful' : 'unsuccessful';
                    console.log('Copying text command was ' + msg);
                } catch (err) {
                    console.log('Oops, unable to copy');
                }
                document.body.removeChild(textArea);
            }

            $scope.add = function() {
                $scope.data.newEntry = {};
                $scope.initDate();
            };

            $scope.upload = function() {
                $timeout(function() {
                    $("#input-1a").fileinput({
                        showUpload: false,
                        mainClass: "input-group-sm"
                    });

                    $('#input-1a').on('fileloaded', function(event, file, previewId, index, reader) {
                        $scope.data.file = file;
                        $scope.$apply();
                    });

                    $('#input-1a').on('fileclear', function(event, file, previewId, index, reader) {
                        $scope.data.file = null;
                        $scope.controls.analysis = false;
                        $scope.$apply();
                    });
                }, 1)
            };

            $scope.preAnalyze = function() {
                if ($scope.data.file.size > 25000) {
                    growl.warning("File is too large for pre-validation.", {
                        title: 'Error'
                    });
                    return;
                }
                $scope.controls.analysis = true;
                var reader = new FileReader();
                reader.onload = function(e) {
                    var data = processData(e.target.result);
                    data.shift();
                    $scope.data.filePreview = data;
                    $scope.validateData();
                };
                reader.readAsText($scope.data.file);
            };

            $scope.save = function() {
                if (!$scope.data.newEntry.uuid) {
                    OEEEntryService.addOeeEntries([$scope.data.newEntry]).then(function(entries) {
                        if (entries.length == 0) {
                            growl.error("Entry was not saved. Please validate the format of each entry and " +
                                "create matching Work Centers before uploading data. Please also make sure you have " +
                                "permission to upload data for the specified Work Center", {
                                title: 'Warning'
                            });
                            $scope.edit($scope.data.newEntry);
                        }
                        else {
                            growl.info("Entry added successfully.", {
                                title: 'Info'
                            });
                            $scope.refresh();
                        }
                    });
                } else {
                    OEEEntryService.updateOeeEntry($scope.data.newEntry.uuid, $scope.data.newEntry).then(function(entry) {
                        $scope.buttons.isCollapsed = false;
                    }, function(err) {
                        $scope.buttons.isCollapsed = true;
                        $scope.data.newEntry = angular.copy($scope.data.backUp);
                    });
                }
            };

            $scope.edit = function(entry) {
                $scope.data.backUp = angular.copy(entry);
                $scope.buttons.isCollapsed = true;
                $scope.data.newEntry = entry;
            };

            $scope.remove = function() {
                var entries = $.grep($scope.data.displayedOeeEntries, function(entry) {
                    return entry.isSelected;
                });
                entries.forEach(function(entry) {
                    OEEEntryService.deleteOeeEntry(entry.uuid).then(function() {
                        $scope.getOEEEntries($scope.tableState);
                    });
                });
            };

            $scope.submitFile = function() {
                var reader = new FileReader();
                reader.onload = function(e) {
                    var data = processData(e.target.result);
                    var numSubmitted = data.length - 1;
                    var fd = new FormData();
                    fd.append('file', $scope.data.file);

                    $http.post(APP_CONSTANTS.API_PATH + "/api/v2/oeeentries/source", fd, {
                        transformRequest: angular.identity,
                        headers: {
                            'Content-Type': undefined
                        }
                    }).then(function(result) {
                        $scope.buttons.upload = false;
                        $scope.refresh();
                        growl.info(result.data.length + " entries have been added.", {
                            title: 'Info'
                        });
                        if (result.data.length < numSubmitted) {
                            growl.warning(numSubmitted - result.data.length + " entries have not been saved. Please " +
                                "validate the format of each entry and create matching Work Centers before uploading " +
                                "data. Please also make sure you have permission to upload data for each Work Center", {
                                title: 'Warning'
                            });
                        }
                    }, function(err) {
                        growl.error("There has been an error processing your request.", {
                            title: 'Error'
                        });
                    }, function(err) {
                        $scope.controls.initFlag = false;
                    });
                };
                reader.readAsText($scope.data.file);
            };
            $scope.tableState;
            $scope.refresh = function() {
                $scope.getOEEEntries($scope.tableState);
            };

            $scope.$watch('filter', function() {
                if (!$scope.tableState) return;
                $scope.getOEEEntries($scope.tableState);
            }, true);


            $scope.getOEEEntries = function(tableState) {
                var options = $scope.getQueryOptions(tableState);
                if (options.shifts.length == 0 || options.workCenters.length == 0) {
                    $scope.data.displayedOeeEntries = [];
                    tableState.pagination.numberOfPages = 0;
                    return;
                }
                OEEEntryService.getOeeEntries(options).then(function(result) {
                    $scope.data.displayedOeeEntries = result.content;
                    tableState.pagination.numberOfPages = result.totalPages;
                });
            };
        }]);
