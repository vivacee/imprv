datasources.factory('DatasourceService', ['RestangularV2', 'CachedRestangular', '$http',
    function DatasourceService(RestangularV2, CachedRestangular, $http) {
        return {
            getDatasources: function(options) {
                return RestangularV2.one('datasources').get(options);
            },
            getDatasource: function(uuid) {
                return CachedRestangular.one('datasources', uuid).get();
            },
            getDatasourceMedia: function(uuid, options) {
                // return $http.get('api/v2/datasources/' + uuid + '/media', {
                // cache: true,
                // params: options
                // });
                return CachedRestangular.one('datasources', uuid).one('media').get(options);
            },
            removeDatasource: function(uuid) {
                return RestangularV2.one('datasources', uuid).remove();
            }
        };
    }]);

datasources.factory('OEEEntryService', ['RestangularV2', 'CachedRestangular', '$http',
    function OEEEntryService(RestangularV2, CachedRestangular, $http) {
        return {
            addOeeEntries: function(entries) {
                return RestangularV2.one('oeeentries').customPOST(entries, '', {}, {});
            },
            getOeeEntries: function(options) {
                if (!options) options = {};
                if (!options.from) options.from = moment().subtract(2, 'y').valueOf();
                if (!options.to) options.to = moment().endOf('day').valueOf();
                return RestangularV2.one('oeeentries').get(options);
            },
            getOEEEntriesStatistics: function(options) {
                if (!options) options = {};
                if (!options.from) options.from = moment().subtract(2, 'y').valueOf();
                if (!options.to) options.to = moment().valueOf();
                return RestangularV2.one('oeeentries').one('statistics').get(options);
            },
            updateOeeEntry: function(uuid, entry) {
                return RestangularV2.one('oeeentries', uuid).customPUT(entry, '', {}, {});
            },
            deleteOeeEntry: function(uuid) {
                return RestangularV2.one('oeeentries', uuid).remove();
            }

        };
    }]);

datasources.factory('DeviationEntryService', ['RestangularV2', 'CachedRestangular', '$http',
    function DeviationEntryService(RestangularV2, CachedRestangular, $http) {
        return {
            addDeviationEntries: function(entries) {
                return RestangularV2.one('deviationentries').customPOST(entries, '', {}, {});
            },
            getDeviationEntries: function(options) {
                if (!options) options = {};
                if (!options.from) options.from = moment().subtract(2, 'y').valueOf();
                if (!options.to) options.to = moment().valueOf();
                return RestangularV2.one('deviationentries').get(options);
            },
            getDeviationEntriesStatistics: function(options) {
                if (!options) options = {};
                if (!options.from) options.from = moment().subtract(2, 'y').valueOf();
                if (!options.to) options.to = moment().valueOf();
                return RestangularV2.one('deviationentries').one('statistics').get(options);
            },
            getDeviationEntriesWorkCenters: function() {
                return RestangularV2.one('deviationentries').one('workcenters').get();
            },
            updateDeviationEntry: function(uuid, entry) {
                return RestangularV2.one('deviationentries', uuid).customPUT(entry, '', {}, {});
            },
            deleteDeviationEntry: function(uuid) {
                return RestangularV2.one('deviationentries', uuid).remove();
            },
            analyze: function(options) {
                if (!options) options = {};
//                if (!options.from) options.from = moment().subtract(2, 'y').valueOf();
//                if (!options.to) options.to = moment().valueOf();
                return RestangularV2.one('deviationentries').one('analyze').customPOST(options, '', {}, {});
            }
        };
    }]);

datasources.factory('FilterService', [
    function FilterService() {
        var filter = null;
        return {
            saveFilter: function(newFilter) {
                filter = newFilter;
            },
            getFilter: function() {
                return filter;
            }
        }
    }]);

datasources.factory('DisplayService', [
    function DisplayService() {
        var display = {};
        var entry = {};
        return {
            saveDisplay: function(newDisplay) {
                display = newDisplay;
            },
            getDisplay: function() {
                return display;
            },
            saveEntry: function(newEntry) {
                entry = newEntry;
            },
            getEntry: function() {
                return entry;
            }
        }
    }]);