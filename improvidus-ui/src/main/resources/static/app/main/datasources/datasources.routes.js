datasources.config(["$stateProvider", function($stateProvider) {

    $stateProvider.state('Datasource', {
        url: "/datasources",
        templateUrl: "app/main/datasources/datasource.html",
        redirectTo: 'Datasource.Deviation'
    }).state('Datasource.Deviation', {
        url: "/deviation",
        templateUrl: "app/main/datasources/data/deviation/deviation.html",
        controller: "DatasourcesDeviationCtrl",
        reloadOnSearch: false
    }).state('Datasource.OEE', {
        url: "/oee",
        templateUrl: "app/main/datasources/data/oee/oee.html",
        controller: "DatasourcesOEECtrl",
        reloadOnSearch: false
    });

}]);