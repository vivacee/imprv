/*******************************************************************************
 * Copyright (c) 2015 Bosch Software Innovations GmbH. All rights reserved.
 ******************************************************************************/
var app = angular.module("app", ['angular-cache', 'ngAnimate',
    'angular-growl', 'angular-loading-bar', 'app.controllers.login', 'app.controllers.administration',
    'app.controllers.datasources.filters',
    'app.controllers.links', 'app.controllers.oee', 'app.controllers.services',
    'app.controllers.datasources.oee',
    'app.controllers.datasources.deviation', 'app.controllers.profile', 'colorpicker.module',
    'jkuri.touchspin', 'ngIdle', 'ngSanitize', 'isteven-multi-select',
    'oc.lazyLoad', 'pascalprecht.translate', 'permission', 'restangular', 'smart-table',
    'daterangepicker',
    'ui.bootstrap', 'ui.layout', 'ui.router', 'ui.select', 'improvidus-multimetric',
    'improvidus-wordcloud', 'improvidus-correlations', 'improvidus-recommendations',
    'improvidus-filters', 'ngFitText', 'angularjs-dropdown-multiselect',
    'com.bosch.shared', 'com.bosch.account',
    'com.bosch.administration', 'com.bosch.datasources', 'com.bosch.analysis', 'ngCookies', 'angular-jwt',
    'ng-tree', 'angularCSS']);

// app.config(["$httpProvider", "$locationProvider", function($httpProvider, $locationProvider) {
//     var csrf_header = $('meta[name=_csrf-header]').attr('content');
//     var csrf_token = $('meta[name=_csrf-token]').attr('content');
//     $httpProvider.defaults.headers.common[csrf_header] = csrf_token;
//     $httpProvider.defaults.headers.common['Content-Type'] = 'application/json;charset=UTF-8';
//
// }]);

app.config(['$httpProvider', '$cookiesProvider', 'jwtOptionsProvider', 'APP_CONSTANTS',
    function Config($httpProvider, $cookiesProvider, jwtOptionsProvider, APP_CONSTANTS) {
        $cookiesProvider.defaults.expires = new Date(moment().add(30, 'day'));
        $cookiesProvider.defaults.secure = APP_CONSTANTS.SSL_ENABLED;
        var refreshPromise;
        jwtOptionsProvider.config({
            tokenGetter: ['$cookies', 'APP_CONSTANTS', 'jwtHelper', 'AuthService', '$http', '$location', '$state',
                function($cookies, APP_CONSTANTS, jwtHelper, AuthService, $http, $location, $state) {
                    if (refreshPromise) {
                        return refreshPromise;
                    }

                    var accessToken = $cookies.get(APP_CONSTANTS.OAUTH.ACCESS_TOKEN_NAME);
                    //TODO: increase offset time
                    if (!accessToken || jwtHelper.isTokenExpired(accessToken, 5)) {
                        var refreshToken = $cookies.get(APP_CONSTANTS.OAUTH.REFRESH_TOKEN_NAME);
                        if (refreshToken == null) {
                            AuthService.revokeToken();
                            if ($location.path().indexOf("/public") == -1 && $location.path().indexOf("/landing") == -1) {
                                $location.path("/public");
                                $state.go('Landing');
                            }
                            return undefined;
                        }
                        refreshPromise = AuthService.getRefreshToken({
                            grant_type: 'refresh_token',
                            refresh_token: refreshToken
                        }).then(function(response) {
                            var accessToken = response.access_token;
                            $cookies.put(APP_CONSTANTS.OAUTH.ACCESS_TOKEN_NAME, accessToken);
                            refreshPromise = null
                            return accessToken;
                        });

                        return refreshPromise

                    } else {
                        return accessToken;
                    }
                }],
            whiteListedDomains: APP_CONSTANTS.OAUTH.WHITE_LISTED_DOMAINS
        });
        $httpProvider.interceptors.push('jwtInterceptor');
    }]);

app.run([
    "$http",
    "$rootScope",
    "$translate",
    "AdminOrganizationsService",
    "ConfigurationService",
    "PermissionsService",
    "UserService",
    "AuthService",
    "PlansService",
    "PaymentService",
    "stripe",
    function($http, $rootScope, $translate, AdminOrganizationsService, ConfigurationService, PermissionsService, UserService, AuthService, PlansService, PaymentService, stripe) {

        App.init();

        $rootScope.appName = "Home";
        $rootScope.org = {};
        $rootScope.user = {};
        $rootScope.initApp = function() {
            UserService.getUser().then(function(user) {
                $rootScope.currentUser = user.plain();
                return AdminOrganizationsService.getOrganizationConfiguration(user.organization.uuid);
            }).then(function(orgConfiguration) {
                if (orgConfiguration && orgConfiguration.conf.numShifts) {
                    $rootScope.org.configuration = orgConfiguration.plain();
                } else {
                    $rootScope.org.configuration = {
                        organizationUuid: $rootScope.currentUser.organization.uuid,
                        conf: {
                            numShifts: 3
                        }
                    }
                }
                return ConfigurationService.getConfigurations({
                    resourceUuid: $rootScope.currentUser.uuid
                });
            }).then(function(userConfiguration) {
                $rootScope.user.configuration = userConfiguration[0];
                $rootScope.appReady = true;
            });

            PaymentService.getStripeKey().then(function(key) {
                stripe.setPublishableKey(key.key);
            });

            PlansService.refreshActiveSubscription();
        }
        if (AuthService.isAuthenticated())
            $rootScope.initApp();

    }
])
;

app.config(["uiSelectConfig", function(uiSelectConfig) {
    uiSelectConfig.theme = 'bootstrap';
}]);

app.config(['growlProvider', '$httpProvider', function(growlProvider, $httpProvider) {
    growlProvider.globalTimeToLive(5000);
    growlProvider.globalReversedOrder(true);
    growlProvider.onlyUniqueMessages(false);
    $httpProvider.interceptors.push(growlProvider.serverMessagesInterceptor);
}]);

app.config(['$logProvider', function($logProvider) {
    $logProvider.debugEnabled(false);
}]);

app.config(['cfpLoadingBarProvider', function(cfpLoadingBarProvider) {
    cfpLoadingBarProvider.parentSelector = '#header';
}]);

// app.config(['$locationProvider', function($locationProvider) {
//     $locationProvider.html5Mode({
//         enabled: true,
//         //requireBase: false
//     });
// }])

app.run(['$rootScope', '$state', '$window', '$cookies', '$location', "AuthService",
    function($rootScope, $state, $window, $cookies, $location, AuthService) {
        $rootScope.$on('$stateChangeSuccess', function() {
            document.body.scrollTop = document.documentElement.scrollTop = 0;
        });

        $rootScope.$on('$stateChangeStart', function(evt, to, params) {
            if ($location.$$search.passwordReset) {
                AuthService.revokeToken();
            }
            if (!AuthService.isAuthenticated() && $location.path().indexOf("/public") == -11) {
                evt.preventDefault();
                AuthService.revokeToken();
                $location.path("/public");
                $state.go('Landing');
                return;
            }
            // else if (AuthService.isAuthenticated() && $location.path().indexOf("/public") > -1) {
            //     evt.preventDefault();
            //     $location.path("/");
            //     $state.go('OEE');
            //     return;
            // }
            // else if (AuthService.isAuthenticated() && to.redirectTo) {
            //     evt.preventDefault();
            //     $state.go(to.redirectTo, params, {location: 'replace'})
            //     return;
            // }
            else if (to.redirectTo) {
                evt.preventDefault();
                $state.go(to.redirectTo, params, {location: 'replace'})
                return;
            }
        });
    }])
