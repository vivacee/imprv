app.factory('PermissionsService', ['$rootScope', '$q', 'RestangularV2', 'ACLEntriesService',
    function PermissionsService($rootScope, $q, RestangularV2, ACLEntriesService) {
      return {
        deferred: null,

        getPermissions: function() {
          return RestangularV2.one('permissions').get();
        },
        getUserPermissions: function(uuid) {
          return RestangularV2.one('users', uuid).one('permissions').get();
        },

        checkEntityPermissions: function(uuid, validPermissions) {
          var deferred = $q.defer();
          ACLEntriesService.getUserResourceACLEntries($rootScope.currentUser.uuid, {
            resourceUuid: uuid
          }).then(function(aclEntries) {
            aclEntries.forEach(function(entry) {
              if ($.inArray(entry.action, validPermissions) > -1) deferred.resolve();
            });
            deferred.reject();
          }, function(err) {
            deferred.reject();
            $log.error(err);
          });
          return deferred.promise;
        },

        checkPermissions: function(permission) {
          if (!$rootScope.user.permissions) {
            this.deferred = $q.defer();
            this.deferred.permission = permission;
            return this.deferred.promise;
          }
          if ($.inArray("ROLE_ADMIN", $rootScope.currentUser.authorities) > -1) return true;
          return this.intersect($rootScope.user.permissions, permission).length > 0;
        },

        intersect: function intersect(a, b) {
          var t;
          if (b.length > a.length) t = b, b = a, a = t;
          return a.filter(function(e) {
            if (b.indexOf(e.uuid) !== -1) return true;
          });
        }
      };
    }]);

app.factory('ACLEntriesService', ['RestangularV2', function ACLEntriesService(RestangularV2) {
  return {
    getUserResourceACLEntries: function(uuid, options) {
      return RestangularV2.one('users', uuid).one('aclentries').get(options);
    },
    getResourceACLEntries: function(options) {
      return RestangularV2.one('aclentries').get(options);
    },
    addResourceACLEntry: function(aclEntry) {
      return RestangularV2.one('aclentries').customPOST(aclEntry, '', {}, {});
    },
    updateResourceACLEntry: function(uuid, aclEntry) {
      return RestangularV2.one('aclentries', uuid).customPUT(aclEntry);
    },
    removeResourceACLEntry: function(uuid) {
      return RestangularV2.one('aclentries', uuid).remove();
    },
    removeResourceACLEntryBySubject: function(subjectUuid, objectUuid) {
      return RestangularV2.one('aclentries').remove({
        subjectUuid: subjectUuid,
        objectUuid: objectUuid
      });
    },
  };
}]);