administration.factory('AdminUsersService', ['CachedRestangular',
    function AdminUsersService(CachedRestangular) {
        return {
            addUser: function(user, options) {
                return CachedRestangular.one('users').customPOST(user, '', options, {})
            },
            deleteUser: function(uuid) {
                return CachedRestangular.one('users', uuid).remove();
            },
            getUser: function(organizationUuid, uuid) {
                if (uuid)
                    return CachedRestangular.one('organization', organizationUuid)
                        .one('users', uuid).get();
                else
                    return CachedRestangular.one('user').get();
            },
            assignUser: function(organizationUuid, options) {
                return CachedRestangular.one('organization', organizationUuid).one('users').customPOST('', '', options, {});
            },
            unassignUser: function(organizationUuid, userUuid) {
                return CachedRestangular.one('organization', organizationUuid).one('users', userUuid).remove();
            },
            updateUserOrganization: function(user, uuid) {
                return CachedRestangular.one('organization', uuid).one('users').one(user.uuid)
                    .customPUT(user, '', {}, {})
            },
            assignOrganizationUserRole: function(user, uuid, options) {
                return CachedRestangular.one('organization', uuid).one('users').one(user.uuid).one('roles')
                    .customPUT(user, '', options, {})
            }
        };
    }]);