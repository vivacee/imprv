administration.controller("adminWorkCentersCtrl",
    ["$rootScope", "$scope", "$timeout", "$stateParams", "$controller", "$modalStack", "$modal", "$log", "growl",
        "AdminOrganizationsService",
        "AdminPlantsService", "AdminCostCenterService", "AdminWorkCenterService",
        function($rootScope, $scope, $timeout, $stateParams, $controller, $modalStack, $modal, $log, growl,
                 AdminOrganizationsService, AdminPlantsService, AdminCostCenterService,
                 AdminWorkCenterService) {

            $rootScope.state = 'Work Centers';

            $scope.data = {
                workCenters: [],
                displayedWorkCenters: [],
                selectedWorkCenters: [],
                users: [],
                displayedUsers: [],
                tableState: undefined
            }

            angular.extend(this, $controller('adminBaseCtrl', {
                $scope: $scope
            }));

            if ($stateParams.plantUuid && $stateParams.costCenterUuid) {
                AdminPlantsService.getPlant($stateParams.plantUuid).then(function(plant) {
                    $scope.data.plant = plant;
                    return AdminCostCenterService.getCostCenter(plant.uuid,
                        $stateParams.costCenterUuid);
                }).then(function(costCenter) {
                    $scope.data.costCenter = costCenter;
                    $scope.getWorkCenters();
                })
            }

            var manualSchedule = {
                displayName: "**Manual Schedule**",
                uuid: null
            };

            var resetWorkCenterForm = function() {
                $scope.workCenterForm = {
                    "users": [],
                    "workCenter": {
                        scheduleTemplate: manualSchedule
                    },
                    "newWorkCenter": {},
                    step: 1
                };
                $scope.data.selectAll = false;
            }
            resetWorkCenterForm();

            $scope.buttons = {
                isCollapsed: false,
                invalid: false
            }

            $scope.add = function() {
                $modalStack.dismissAll();
                var opts = {
                    templateUrl: "app/main/administration/workcenters/views/add.html",
                    scope: $scope,
                    size: 'lg'
                };
                $scope.data.displayedUsers = [];
                $scope.data.plants = [$scope.data.plant];
                $scope.workCenterForm.plant = $scope.data.plant;

                $scope.data.costCenters = [$scope.data.costCenter];
                $scope.workCenterForm.costCenter = $scope.data.costCenter;

                $scope.loadScheduleTemplates();
                AdminCostCenterService.getUsers($scope.data.plant.uuid, $scope.data.costCenter.uuid)
                    .then(function(users) {
                        $scope.data.users = users.plain();
                        $scope.data.displayedUsers = users.plain();
                    });
                var instance = $modal.open(opts);
            }


            $scope.save = function() {
                $scope.workCenterForm.workCenter.name
                    = $scope.workCenterForm.workCenter.displayName;
                if ($scope.workCenterForm.workCenter.scheduleTemplate.uuid == null)
                    $scope.workCenterForm.workCenter.scheduleTemplate = null;
                AdminWorkCenterService.addWorkCenter(
                    $scope.workCenterForm.plant.uuid, $scope.workCenterForm.costCenter.uuid,
                    $scope.workCenterForm.workCenter).then(function(workCenter) {
                    var users = $.grep($scope.data.displayedUsers, function(user) {
                        if (user) return user.selected;
                    });
                    $scope.assignUsers($scope.workCenterForm.plant,
                        $scope.workCenterForm.costCenter, workCenter, users)
                    $scope.refresh();
                    growl.info("Created " + workCenter.displayName, {
                        title: 'Info'
                    });
                    $modalStack.dismissAll();
                    resetWorkCenterForm();
                }, function(err) {
                    $log.error(err);
                    if (err.status == 409) {
                        growl.error("Work Center with the name " + $scope.workCenterForm.workCenter.displayName +
                            " already exists within the Organization. Please choose a new name.", {
                            title: 'Duplicate Work Center Name'
                        });
                        $scope.workCenterForm.step = 1;
                        $scope.workCenterForm.workCenter.scheduleTemplate = $scope.workCenterForm.workCenter.scheduleTemplate == null ? manualSchedule : $scope.workCenterForm.workCenter.scheduleTemplate;
                    }
                    else
                        resetWorkCenterForm();
                })
            }

            $scope.assignUsers = function(plant, costCenter, workCenter, users) {
                users.forEach(function(user) {
                    AdminWorkCenterService.assignUser(plant.uuid, costCenter.uuid, workCenter.uuid,
                        user.uuid).then(function() {
                        $log.info('Assigned User ' + user.displayName + ' to Work Center ' +
                            workCenter.displayName);
                        //$scope.data.users.push(user);
                    });
                })
            }

            $scope.getWorkCenters = function() {
                $scope.data.workCenters = [];
                AdminWorkCenterService.getWorkCenters(
                    $scope.data.plant.uuid, $scope.data.costCenter.uuid, {})
                    .then(function(workCenters) {
                        if (!workCenters) return;
                        $scope.data.workCenters = workCenters.plain().sort($scope.sortByDisplayName);
                    }, function(err) {
                        $log.error(err);
                    });
            }

            $scope.refresh = function() {
                $scope.getWorkCenters();
            }

            $scope.selectedUsers = function() {
                return $.grep($scope.data.users, function(user) {
                    return user.selected;
                }).length;
            }

            $scope.remove = function() {
                $modalStack.dismissAll();
                var opts = {
                    templateUrl: "app/main/administration/workcenters/views/remove-modal.html",
                    scope: $scope,
                    size: 'md'
                };
                var instance = $modal.open(opts);
            }

            $scope.close = function() {
                $modalStack.dismissAll();
            }

            $scope.removeWorkCenter = function() {
                $scope.data.plants.forEach(function(plant) {
                    for (var i = 0; i < plant.costCenters.length; i++)
                        for (var x = 0; x < plant.costCenters[i].workCenters.length; x++)
                            for (var j = 0; j < $scope.data.selectedWorkCenters.length; j++)
                                if (plant.costCenters[i].workCenters[x].uuid ==
                                    $scope.data.selectedWorkCenters[j].uuid) {
                                    AdminWorkCenterService.deleteWorkCenter(plant.uuid,
                                        plant.costCenters[i].uuid,
                                        $scope.data.selectedWorkCenters[j].uuid)
                                        .then(function() {
                                            $scope.refresh();
                                        })
                                }
                })
            }

            $scope.confirmEdit = function() {
                var workCenter;
                for (var i = 0; i < $scope.data.displayedWorkCenters.length; i++)
                    if ($scope.data.displayedWorkCenters[i].edit == true)
                        workCenter = $scope.data.displayedWorkCenters[i];

                $scope.data.plants.forEach(function(plant) {
                    for (var i = 0; i < plant.costCenters.length; i++)
                        for (var j = 0; j < plant.costCenters[i].workCenters.length; j++)
                            if (plant.costCenters[i].workCenters[j].uuid == workCenter.uuid) {
                                $scope.workCenterForm.newWorkCenter.name
                                    = $scope.data.newWorkCenterName
                                AdminWorkCenterService.updateWorkCenter(plant.uuid,
                                    plant.costCenters[i].uuid, workCenter.uuid,
                                    $scope.workCenterForm.newWorkCenter).then(function() {
                                    $scope.refresh();
                                });
                            }
                });
            }

            // $scope.getPlants = function() {
            //     AdminPlantsService.getPlants({}).then(function(plants) {
            //         $scope.data.plants = plants;
            //         $scope.getWorkCenters();
            //     }, function(err) {
            //         $log.error(err);
            //     })
            // }
            //
            // $scope.data.plants = DataService.getPlants();
            // if ($scope.data.plants.length)
            //     $scope.getWorkCenters();
            // else
            //     $scope.getPlants();

        }]);