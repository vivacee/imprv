administration.controller("adminSchedulesCtrl",
    ["$rootScope", "$scope", "$state", "$modalStack", "$modal", "$log", "growl", "AdminSchedulesService", "AdminOrganizationsService",
        function($rootScope, $scope, $state, $modalStack, $modal, $log, growl, AdminSchedulesService, AdminOrganizationsService) {

            $rootScope.state = 'Schedules';

            $scope.data = {
                scheduleTemplates: [],
                displayedScheduleTemplates: [],
                selectedScheduleTemplates: []
            };

            var blankShift = [null, null, null, null, null, null, null];
            var blankSchedule = [angular.copy(blankShift), angular.copy(blankShift), angular.copy(blankShift)];

            $scope.scheduleForm = {
                step: 1,
                scheduleTemplate: {
                    name: null,
                    description: null,
                    schedule: blankSchedule
                }
            };

            $scope.buttons = {
                isCollapsed: false,
                invalid: false
            };

            $scope.controls = {
                processing: false
            };

            $scope.insertShift = function() {
                var blankShift = [null, null, null, null, null, null, null];
                $scope.scheduleForm.scheduleTemplate.schedule.push(blankShift);
            };

            $scope.removeShift = function(index) {
                $scope.scheduleForm.scheduleTemplate.schedule.splice(index, 1);
            };

            $scope.addScheduleTemplate = function() {
                $modalStack.dismissAll();
                var opts = {
                    templateUrl: "app/main/administration/schedules/views/add.html",
                    scope: $scope,
                    size: 'lg'
                };
                $modal.open(opts);
            };

            $scope.save = function() {
                AdminSchedulesService.addSchedule($scope.scheduleForm.scheduleTemplate).then(function() {
                    growl.info("Schedule Template created successfully", {
                        title: 'Info'
                    });
                    $modalStack.dismissAll();
                    $scope.refresh();
                }, function(error) {
                    growl.error("Error occurred while saving schedule template.", {
                        title: 'Creation Failed'
                    });
                });
            };

            $scope.close = function() {
                $modalStack.dismissAll();
            };

            $scope.removeSchedules = function() {
                $scope.data.selectedSchedules.forEach(function(schedule) {
                    AdminSchedulesService.deleteSchedule(schedule.uuid).then(function() {
                        growl.info("Schedule deleted successfully", {
                            title: 'Info'
                        });
                        $scope.refresh();
                    }, function(error) {
                        growl.error("Schedule was not removed", {
                            title: 'Schedule Removal Error'
                        });
                    });
                });
            };

            $scope.getSchedules = function() {
                AdminSchedulesService.getSchedules()
                    .then(function(schedules) {
                        $scope.data.scheduleTemplates = schedules.plain();
                    });
            };

            $scope.refresh = function() {
                $scope.getSchedules();
                $scope.scheduleForm = {
                    step: 1,
                    scheduleTemplate: {
                        name: null,
                        description: null,
                        schedule: blankSchedule
                    }
                };
            };

            $scope.getSchedules();

        }]);