administration.controller("adminWorkCenterDetailsCtrl",
    ["$rootScope", "$scope", "$log", "$q", "$stateParams", "$controller", "$modalStack", "$modal", "$location",
        "$state", "growl", "AdminPlantsService", "AdminCostCenterService", "AdminWorkCenterService", "AdminSchedulesService",
        function($rootScope, $scope, $log, $q, $stateParams, $controller, $modalStack, $modal, $location, $state, growl,
                 AdminPlantsService, AdminCostCenterService, AdminWorkCenterService, AdminSchedulesService) {

            $scope.data = {
                users: [],
                scheduleTemplates: [],
                displayedUsers: [],
                onlyMembers: false,
                updateAll: false,
                tableState: undefined
            };

            var manualSchedule = {
                displayName: "**Manual Schedule**",
                uuid: null
            };

            angular.extend(this, $controller('adminBaseCtrl', {
                $scope: $scope
            }));

            $scope.loadUsers = function() {
                $scope.data.users = [];
                AdminCostCenterService.getUsers($scope.data.plant.uuid, $scope.data.costCenter.uuid)
                    .then(function(costCenterUsers) {
                        $scope.data.users = costCenterUsers;
                        return AdminWorkCenterService.getUsers($scope.data.plant.uuid,
                            $scope.data.costCenter.uuid, $scope.data.workCenter.uuid);
                    }).then(function(workCenterUsers) {
                    var uuids = workCenterUsers.map(function(user) {
                        if (user) return user.uuid;
                    })
                    $scope.data.users.forEach(function(user) {
                        if (user)
                            user.selected = uuids.indexOf(user.uuid) > -1 ? true : false;
                    });
                    $scope.getUsers($scope.data.tableState);
                })
            };

            $scope.updateWorkCenterSchedule = function(workCenter) {
                if (workCenter.scheduleTemplate && workCenter.scheduleTemplate.uuid == null)
                    workCenter.scheduleTemplate = null;
                AdminWorkCenterService.updateWorkCenter($scope.data.plant.uuid, $scope.data.costCenter.uuid, workCenter.uuid, workCenter).then(function(workCenter) {
                    $scope.data.workCenter = workCenter.plain();
                    if ($scope.data.workCenter.scheduleTemplate == null)
                        $scope.data.workCenter.scheduleTemplate = angular.copy(manualSchedule);
                    growl.info('Assigned Schedule ' + $scope.data.workCenter.scheduleTemplate.displayName + ' to Work Center ' +
                        $scope.data.workCenter.displayName, {
                        title: 'Info'
                    });
                    $log.info('Assigned Schedule ' + $scope.data.workCenter.scheduleTemplate.displayName + ' to Work Center ' +
                        $scope.data.workCenter.displayName);
                })
            };

            $scope.updateEntry = function(user) {
                if (user.selected == true) {
                    AdminWorkCenterService.assignUser($scope.data.plant.uuid,
                        $scope.data.costCenter.uuid, $scope.data.workCenter.uuid, user.uuid)
                        .then(function() {
                            $log.info('Assigned User ' + user.displayName + ' to Work Center ' +
                                $scope.data.workCenter.displayName);
                        });
                } else if (user.selected == false) {
                    AdminWorkCenterService.unassignUser($scope.data.plant.uuid,
                        $scope.data.costCenter.uuid, $scope.data.workCenter.uuid, user.uuid)
                        .then(function() {
                            $log.info('Unassigned User ' + user.displayName + ' to Work Center ' +
                                $scope.data.workCenter.displayName);
                        });
                }
                $scope.getUsers($scope.data.tableState);
            }

            $scope.remove = function() {
                $modalStack.dismissAll();
                var opts = {
                    templateUrl: "app/main/administration/workcenters/views/remove-modal.html",
                    scope: $scope,
                    size: 'md'
                };
                var instance = $modal.open(opts);
            }

            $scope.removeWorkCenter = function() {
                AdminWorkCenterService.deleteWorkCenter($scope.data.plant.uuid,
                    $scope.data.costCenter.uuid, $scope.data.workCenter.uuid).then(function() {
                    $modalStack.dismissAll();
                    growl.info("Removed " + $scope.data.workCenter.displayName, {
                        title: 'Info'
                    });
                    $location.path("admin/plants/" + $scope.data.plant.uuid + "/costcenters/" +
                        $scope.data.costCenter.uuid);
                }, function(err) {
                    $log.error(err);
                })
            }

            $scope.close = function() {
                $modalStack.dismissAll();
            }

            if ($stateParams.plantUuid) {
                if ($stateParams.costCenterUuid) {
                    if ($stateParams.workCenterUuid) {
                        AdminPlantsService.getPlant($stateParams.plantUuid).then(function(plant) {
                            $scope.data.plant = plant;
                            return AdminCostCenterService.getCostCenter($stateParams.plantUuid,
                                $stateParams.costCenterUuid);
                        }).then(function(costCenter) {
                            $scope.data.costCenter = costCenter;
                            return AdminWorkCenterService.getWorkCenter($stateParams.plantUuid,
                                $stateParams.costCenterUuid, $stateParams.workCenterUuid);
                        }).then(function(workCenter) {
                            $scope.data.workCenter = workCenter.plain();
                            if ($scope.data.workCenter.scheduleTemplate == null)
                                $scope.data.workCenter.scheduleTemplate = angular.copy(manualSchedule);
                            $rootScope.state = workCenter.displayName;
                            $scope.loadUsers();
                            $scope.loadScheduleTemplates();
                        });
                    }
                    else {
                        $location.path("admin/plants/" + $stateParams.plantUuid + "/costcenters/" +
                            $stateParams.costCenterUuid + "/workcenters");
                    }
                }
                else {
                    $location.path("admin/plants/" + $stateParams.plantUuid + "/costcenters");
                }
            }
            else {
                $state.go("Admin.Plants");
            }


        }])
;