administration.config(["$stateProvider", function($stateProvider) {

    var all = ['SUPER_ADMIN', 'ADMIN', 'GENERAL_MANAGER', 'PLANT_MANAGER',
        'PRODUCTION_MANAGER'];

    var def = {
        permissions: {
            only: all,
            redirectTo: 'OEE'
        }
    }

    $stateProvider.state('Admin', {
        url: "/admin",
        templateUrl: "app/main/administration/admin.html",
        // controller: "adminCtrl",
        redirectTo: 'Admin.Organizations'
    });

    $stateProvider.state('Admin.Organizations', {
        url: "/organizations",
        templateUrl: "app/main/administration/organizations/views/organizations.html",
        controller: "adminOrganizationsCtrl",
        data: {
            permissions: {
                only: ['SUPER_ADMIN'],
                redirectTo: 'Admin.OrganizationDetails'
            }
        },
        reloadOnSearch: false
    });

    $stateProvider.state('Admin.OrganizationDetails', {
        url: "/organizations/:uuid",
        templateUrl: "app/main/administration/organizations/views/details.html",
        controller: "adminOrganizationDetailsCtrl",
        data: {
            permissions: {
                only: ['SUPER_ADMIN', 'ADMIN'],
                redirectTo: 'Admin.Users'
            }
        },
        reloadOnSearch: false
    });

    $stateProvider.state('Admin.Users', {
        url: "/users",
        templateUrl: "app/main/administration/users/views/users.html",
        controller: "adminUsersCtrl",
        reloadOnSearch: false,
        data: {
            permissions: {
                only: ['SUPER_ADMIN', 'ADMIN', 'GENERAL_MANAGER'],
                redirectTo: 'Admin.Plants'
            }
        },
    });

    $stateProvider.state('Admin.ScheduleTemplates', {
        url: "/schedules",
        templateUrl: "app/main/administration/schedules/views/schedules.html",
        controller: "adminSchedulesCtrl",
        reloadOnSearch: false,
        data: def,
    });

    $stateProvider.state('Admin.ScheduleTemplateDetails', {
        url: "/schedules/:scheduleTemplateUuid",
        templateUrl: "app/main/administration/schedules/views/details.html",
        controller: "adminScheduleDetailsCtrl",
        reloadOnSearch: false,
        data: def,
    });

    $stateProvider.state('Admin.Plants', {
        url: "/plants",
        templateUrl: "app/main/administration/plants/views/plants.html",
        controller: "adminPlantsCtrl",
        reloadOnSearch: false,
        data: def,
    });

    $stateProvider
        .state('Admin.PlantDetails', {
            url: "/plants/:plantUuid",
            templateUrl: "app/main/administration/plants/views/details.html",
            controller: "adminPlantDetailsCtrl",
            reloadOnSearch: false,
            data: def,
        });

    $stateProvider.state('Admin.PlantCostCenters', {
        url: "/plants/:plantUuid/costcenters",
        templateUrl: "app/main/administration/costcenters/views/costcenters.html",
        controller: "adminCostCentersCtrl",
        reloadOnSearch: false,
        data: def,
    });

    $stateProvider.state('Admin.CostCenterDetails', {
        url: "/plants/:plantUuid/costcenters/:costCenterUuid",
        templateUrl: "app/main/administration/costcenters/views/details.html",
        controller: "adminCostCenterDetailsCtrl",
        reloadOnSearch: false,
        data: def,
    });

    $stateProvider.state('Admin.CostCenterWorkCenters', {
        url: "/plants/:plantUuid/costcenters/:costCenterUuid/workcenters",
        templateUrl: "app/main/administration/workcenters/views/workcenters.html",
        controller: "adminWorkCentersCtrl",
        reloadOnSearch: false,
        data: def,
    });

    $stateProvider.state('Admin.WorkCenterDetails', {
        url: "/plants/:plantUuid/costcenters/:costCenterUuid/workcenters/:workCenterUuid",
        templateUrl: "app/main/administration/workcenters/views/details.html",
        controller: "adminWorkCenterDetailsCtrl",
        reloadOnSearch: false,
        data: def,
    });

}]);