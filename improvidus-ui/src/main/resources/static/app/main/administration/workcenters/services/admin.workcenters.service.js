administration.factory('AdminWorkCenterService', ['Restangular', 'RestangularV2',
    function AdminWorkCenterService(Restangular, RestangularV2) {
        return {
            addWorkCenter: function(uuid, costCenterUuid, workcenter) {
                return Restangular.one('plants', uuid).one('costcenters', costCenterUuid).customPOST(workcenter, 'workcenters', {}, {})
            },
            getWorkCenter: function(uuid, costCenterUuid, workCenterUuid, options) {
                return Restangular.one('plants', uuid).one('costcenters', costCenterUuid).one('workcenters', workCenterUuid).get(options);
            },
            getWorkCenters: function(uuid, costCenterUuid, options) {
                return Restangular.one('plants', uuid).one('costcenters', costCenterUuid).one('workcenters').get(options);
            },
            getUsers: function(uuid, costCenterUuid, workCenterUuid, options) {
                return Restangular.one('plants', uuid).one('costcenters', costCenterUuid).one('workcenters', workCenterUuid).one('users').get(options);
            },
            assignUser: function(uuid, costCenterUuid, workCenterUuid, userUuid) {
                return Restangular.one('plants', uuid).one('costcenters', costCenterUuid).one('workcenters', workCenterUuid).one('users', userUuid).put();
            },
            unassignUser: function(uuid, costCenterUuid, workCenterUuid, userUuid) {
                return Restangular.one('plants', uuid).one('costcenters', costCenterUuid).one('workcenters', workCenterUuid).one('users', userUuid).remove();
            },
            deleteWorkCenter: function(uuid, costCenterUuid, workCenterUuid) {
                return Restangular.one('plants', uuid).one('costcenters', costCenterUuid).one('workcenters', workCenterUuid).remove();
            },
            updateWorkCenter: function(uuid, costCenterUuid, workCenterUuid, workCenter) {
                return Restangular.one('plants', uuid).one('costcenters', costCenterUuid).one('workcenters', workCenterUuid).customPUT(workCenter, '', {}, {});
            }
        };
    }]);