administration.component('plantItem', {
    bindings: {
        plant: '='
    },
    template: '<span>{{$ctrl.plant.count || 0}}</span>',
    controller: ['AdminPlantsService', function(AdminPlantsService) {
        var that = this;
        AdminPlantsService.getUsers(this.plant.uuid, {count: true}).then(function(count) {
            that.plant.count = count.length;
        });
    }]
});

administration.component('plantCostCentersCount', {
    bindings: {
        plant: '='
    },
    template: '<span>{{$ctrl.plant.costCenterCount || 0}}</span>',
    controller: ['AdminCostCenterService', function(AdminCostCenterService) {
        var that = this;
        AdminCostCenterService.getCostCenters(this.plant.uuid).then(function(costCenters) {
            that.plant.costCenterCount = costCenters.length;
        });
    }]
});


// administration.component('userIcon', {
//     bindings: {
//         user: '='
//     },
//     template: `<span>{{$ctrl.plant.count}}</span>`,
//     controller: function (AdminPlantsService) {
//         var that = this;
//         AdminPlantsService.getUsers(this.plant.uuid, {count: true}).then(function (count) {
//             var newCount = count ? count.length : 0;
//             that.plant.count = newCount;
//         });
//     }
// });
//
//
// $http.get("api/v1/media", {
//     responseType: "blob",
//     cache: true,
//     params: {
//         'name': 'user-profile',
//         'ownerUuid': scope.user.uuid
//     }
// }).success(function(data, status, headers, config) {
//     var fr = new FileReader();
//     fr.onload = function(e) {
//         if (!e.target.result || !e.target.result.split(',')[1])
//             element.attr('src', 'images/user.png');
//         else
//             element.attr('src', 'data:image/jpeg;base64,' + e.target.result.split(',')[1])
//     };
//     fr.readAsDataURL(data);
// }).error(function(data, status, headers, config) {
//     element.attr('src', 'images/user.png');
// });