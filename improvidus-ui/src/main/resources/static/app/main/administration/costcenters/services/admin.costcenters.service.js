administration.factory('AdminCostCenterService', ['Restangular', 'RestangularV2',
    function AdminCostCenterService(Restangular, RestangularV2) {
        return {
            addCostCenter: function(uuid, costcenter) {
                return RestangularV2.one('plants', uuid)
                    .customPOST(costcenter, 'costcenters', {}, {})
            },
            getCostCenter: function(uuid, costCenterUuid) {
                return RestangularV2.one('plants', uuid).one('costcenters', costCenterUuid).get();
            },
            getCostCenters: function(uuid, options) {
                return RestangularV2.one('plants', uuid).one('costcenters').get(options);
            },
            getUsers: function(uuid, costCenterUuid, options) {
                return RestangularV2.one('plants', uuid).one('costcenters', costCenterUuid)
                    .one('users').get(options);
            },
            assignUser: function(uuid, costCenterUuid, userUuid) {
                return RestangularV2.one('plants', uuid).one('costcenters', costCenterUuid)
                    .one('users', userUuid).put();
            },
            unassignUser: function(uuid, costCenterUuid, userUuid) {
                return RestangularV2.one('plants', uuid).one('costcenters', costCenterUuid)
                    .one('users', userUuid).remove();
            },
            deleteCostCenter: function(uuid, costCenterUuid) {
                return RestangularV2.one('plants', uuid).one('costcenters', costCenterUuid)
                    .remove();
            },
            updateCostCenter: function(uuid, costCenterUuid, costCenter) {
                return RestangularV2.one('plants', uuid).one('costcenters', costCenterUuid)
                    .customPUT(costCenter, '', {}, {});
            }

        };
    }]);