app.factory('UsersAdministrationService', ['$q', 'RestangularV2', 'PermissionsService',
    function UsersAdministrationService($q, RestangularV2, PermissionsService) {

      var users = [];

      return {
        addUser: function(user, options) {
          return RestangularV2.one('users').customPOST(user, '', options);
        },
        getUser: function(user) {
          return RestangularV2.one('user').get();
        },
        getUsers: function() {
          return RestangularV2.one('users').get();
        },
        getOrganizationUsers: function(uuid) {
          return RestangularV2.one('organization', uuid).one('users').get();
        },
        getUserGroupUsers: function(uuid) {
          return RestangularV2.one('usergroups', uuid).one('users').get();
        },
        updateUser: function(uuid, user) {
          return RestangularV2.one('users', uuid).customPUT(user);
        },
        deleteUser: function(uuid) {
          return RestangularV2.one('users', uuid).remove();
        },
        assignPermissions: function(userUuid, uuids) {
          return RestangularV2.one('users', userUuid).one('permissions').put({
            "uuids": uuids
          });
        },
        unassignPermissions: function(userUuid, uuids) {
          return RestangularV2.one('users', userUuid).one('permissions').remove({
            "uuids": uuids
          });
        },
        assignRoles: function(userUuid, uuids) {
          return RestangularV2.one('users', userUuid).one('roles').put({
            "uuids": uuids
          });
        },
        unassignRoles: function(userUuid, uuids) {
          return RestangularV2.one('users', userUuid).one('roles').remove({
            "uuids": uuids
          });
        }
      };
    }]);