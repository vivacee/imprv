administration.controller("adminCostCenterDetailsCtrl",
    ["$rootScope", "$scope", "$log", "$q", "$stateParams", "$controller", "$modalStack", "$modal", "$location",
        "$state", "growl", "AdminPlantsService", "AdminCostCenterService", "AdminUsersService",
        function($rootScope, $scope, $log, $q, $stateParams, $controller, $modalStack, $modal, $location, $state, growl,
                 AdminPlantsService, AdminCostCenterService) {

            $scope.data = {
                users: [],
                displayedUsers: [],
                onlyMembers: false,
                updateAll: false,
                tableState: undefined
            }

            angular.extend(this, $controller('adminBaseCtrl', {
                $scope: $scope
            }));

            $scope.loadUsers = function() {
                $scope.data.users = [];
                AdminPlantsService.getUsers($scope.data.plant.uuid)
                    .then(function(users) {
                        $scope.data.users = users;
                        return AdminCostCenterService.getUsers($scope.data.plant.uuid,
                            $scope.data.costCenter.uuid);
                    }).then(function(costCenterUsers) {
                    var uuids = costCenterUsers.map(function(user) {
                        if (user) return user.uuid;
                    })
                    $scope.data.users.forEach(function(user) {
                        if (user)
                            user.selected = uuids.indexOf(user.uuid) > -1 ? true : false;
                    });
                });
            }

            $scope.updateEntry = function(user) {
                if (user.selected == true) {
                    AdminCostCenterService.assignUser($scope.data.plant.uuid,
                        $scope.data.costCenter.uuid, user.uuid)
                        .then(function() {
                            $log.info('Assigned User ' + user.displayName + ' to Cost Center ' +
                                $scope.data.costCenter.displayName);
                        });
                } else if (user.selected == false) {
                    AdminCostCenterService.unassignUser($scope.data.plant.uuid,
                        $scope.data.costCenter.uuid, user.uuid)
                        .then(function() {
                            $log.info('Unassigned User ' + user.displayName + ' to Cost Center ' +
                                $scope.data.costCenter.displayName);
                        });
                }
                $scope.getUsers($scope.data.tableState);
            }

            $scope.remove = function() {
                $modalStack.dismissAll();
                var opts = {
                    templateUrl: "app/main/administration/costcenters/views/remove-modal.html",
                    scope: $scope,
                    size: 'md'
                };
                var instance = $modal.open(opts);
            }

            $scope.removeCostCenter = function() {
                AdminCostCenterService.deleteCostCenter($scope.data.plant.uuid,
                    $scope.data.costCenter.uuid).then(function() {
                    $modalStack.dismissAll();
                    growl.info("Removed " + $scope.data.costCenter.displayName, {
                        title: 'Info'
                    });
                    $location.path("admin/plants/" + $scope.data.plant.uuid);
                }, function(err) {
                    $log.error(err);
                })
            }

            $scope.close = function() {
                $modalStack.dismissAll();
            }

            if ($stateParams.plantUuid) {
                if ($stateParams.costCenterUuid) {
                    AdminPlantsService.getPlant($stateParams.plantUuid).then(function(plant) {
                        $scope.data.plant = plant;
                        return AdminCostCenterService.getCostCenter($stateParams.plantUuid,
                            $stateParams.costCenterUuid);
                    }).then(function(costCenter) {
                        $scope.data.costCenter = costCenter;
                        $rootScope.state = costCenter.displayName;
                        $scope.loadUsers();
                        $scope.loadScheduleTemplates();
                    })
                }
                else {
                    $location.path("admin/plants/" + $stateParams.plantUuid + "/costcenters");
                }
            }
            else {
                $state.go("Admin.Plants");
            }


        }])
;