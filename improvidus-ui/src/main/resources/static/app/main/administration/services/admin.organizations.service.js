administration.factory('AdminOrganizationsService', ['Restangular', 'RestangularV2',
    function AdminOrganizationsService(Restangular, RestangularV2) {
        return {
            addOrganization: function(form) {
                return RestangularV2.one('organization').customPOST(form, '', {}, {})
            },
            getOrganizations: function(options) {
                return RestangularV2.one('organization').get(options);
            },
            getPlantsCount: function(uuid, options) {
                options = options ? options : {}
                angular.extend(options, {
                    count: true
                });
                return RestangularV2.one('organization', uuid).one('plants').get(options);
            },
        };
    }]);