administration.factory('AdminPlantsService', ['Restangular', 'RestangularV2',
    function AdminPlantsService(Restangular, RestangularV2) {
        return {
            addPlant: function(plant) {
                return Restangular.one('plants').customPOST(plant, '', {}, {})
            },
            getPlant: function(uuid) {
                return Restangular.one('plants', uuid).get();
            },
            getPlants: function(options) {
                return Restangular.one('plants').get(options);
            },
            getUsers: function(uuid, options) {
                return Restangular.one('plants', uuid).one('users').get(options);
            },
            assignUser: function(uuid, userUuid) {
                return Restangular.one('plants', uuid).one('users', userUuid).put();
            },
            unassingUser: function(uuid, userUuid) {
                return Restangular.one('plants', uuid).one('users', userUuid).remove();
            },
            deletePlant: function(uuid) {
                return Restangular.one('plants', uuid).remove();
            },
            updatePlant: function(uuid, plant) {
                return Restangular.one('plants', uuid).customPUT(plant, '', {}, {});
            }
        };
    }]);

administration.factory('DataService', [
    function DataService() {
        var plants = {};
        var costCenters = {};
        return {
            savePlants: function(newPlants) {
                plants = newPlants;
            },
            getPlants: function() {
                return plants;
            },
            saveCostCenters: function(newCostCenters) {
                costCenters = newCostCenters;
            },
            getCostCenters: function() {
                return costCenters;
            }
        }
    }]);