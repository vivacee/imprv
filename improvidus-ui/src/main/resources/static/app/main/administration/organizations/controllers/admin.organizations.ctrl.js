administration.controller("adminOrganizationsCtrl",
    ["$rootScope", "$scope", "PermissionsService", "$modalStack", "$modal", "$log", "AdminOrganizationsService",
        function($rootScope, $scope, PermissionsService, $modalStack, $modal, $log, AdminOrganizationsService) {

            $rootScope.state = 'Organizations';

            $scope.data = {
                organizations: [],
                displayedOrganizations: [],
                selectedOrganizations: []
            }

            $scope.orgForm = {
                "user": {
                    "lastName": "",
                },
                "org": {
                    "address": {},
                    "contacts": [{}]
                },
                step: 1
            }

            $scope.buttons = {
                isCollapsed: false,
                invalid: false,
                submitted: false
            }

            $scope.add = function() {
                $modalStack.dismissAll();
                var opts = {
                    templateUrl: "app/main/administration/organizations/views/add.html",
                    scope: $scope,
                    size: 'md'
                };
                var instance = $modal.open(opts);
            }

            $scope.save = function() {
                $scope.orgForm.user.username = $scope.orgForm.user.email;
                $scope.orgForm.org.name = $scope.orgForm.org.displayName.toUpperCase().split(" ")
                    .join("_");
                $scope.buttons.submitted = true;
                AdminOrganizationsService.addOrganization($scope.orgForm).then(function(org) {
                    $scope.getOrganizations();
                    $modalStack.dismissAll();
                    $scope.buttons.submitted = false;
                }, function(err) {
                    $log.error(err);
                    $scope.buttons.submitted = false;
                })
            }

            $scope.getOrganizations = function() {
                AdminOrganizationsService.getOrganizations({}).then(function(organizations) {
                    $scope.data.organizations = organizations.plain();
                    var organizationUuids = organizations.map(function(org) {
                        return org.uuid;
                    });
                    AdminOrganizationsService.getOrganizationStatistics({
                        organizationUuids: organizationUuids
                    }).then(function(statistics) {
                        if (statistics == null || statistics.length == 0) return;
                        $scope.data.totalStatistics = statistics.pop();
                        $scope.data.orgStatistics = statistics.plain();
                    })
                }, function(err) {
                    $log.error(err);
                })
            }

            $scope.refresh = function() {
                $scope.getOrganizations();
            };

            $scope.getOrganizations();

        }]);