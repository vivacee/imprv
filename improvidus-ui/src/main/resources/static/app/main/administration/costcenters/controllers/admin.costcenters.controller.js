administration.controller("adminCostCentersCtrl",
    ["$rootScope", "$scope", "$timeout", "$stateParams", "$controller",
        "$modalStack", "$modal", "$log", "$filter", "growl", "AdminPlantsService",
        "AdminCostCenterService", "AdminUsersService", "DataService",
        function($rootScope, $scope, $timeout, $stateParams, $controller,
                 $modalStack, $modal, $log, $filter, growl, AdminPlantsService, AdminCostCenterService) {

            $rootScope.state = 'Cost Centers';

            $scope.data = {
                costCenters: [],
                displayedCostCenters: [],
                selectedCostCenters: [],
                users: [],
                displayedUsers: [],
                tableState: undefined
            }

            angular.extend(this, $controller('adminBaseCtrl', {
                $scope: $scope
            }));

            if ($stateParams.plantUuid) {
                AdminPlantsService.getPlant($stateParams.plantUuid).then(function(plant) {
                    $scope.data.plant = plant;
                    $scope.getCostCenters([plant]);
                })
            }

            $scope.costCenterForm = {
                costCenter: {},
                newCostCenter: {},
                step: 1
            }

            $scope.buttons = {
                isCollapsed: false,
                invalid: false
            }

            $scope.add = function() {
                $modalStack.dismissAll();
                var opts = {
                    templateUrl: "app/main/administration/costcenters/views/add.html",
                    scope: $scope,
                    size: 'lg'
                };
                $scope.data.plants = [$scope.data.plant];
                $scope.costCenterForm.plant = $scope.data.plant;
                $scope.data.plantSelectionDisabled = true;
                $scope.data.users = [];
                $scope.data.displayedUsers = [];
                $scope.data.selectAll = false;
                AdminPlantsService.getUsers($scope.data.plant.uuid).then(function(users) {
                    $scope.data.users = users;
                    $scope.data.displayedUsers = users.plain();
                })
                var instance = $modal.open(opts);
            }


            $scope.save = function() {
                AdminCostCenterService.addCostCenter(
                    $scope.costCenterForm.plant.uuid, $scope.costCenterForm.costCenter)
                    .then(function(costCenter) {
                        var users = $.grep($scope.data.displayedUsers, function(user) {
                            return user.selected;
                        });
                        $scope.assignUsers($scope.costCenterForm.plant, costCenter, users)
                        $scope.refresh();
                        growl.info("Created " + costCenter.displayName, {
                            title: 'Info'
                        });
                        $modalStack.dismissAll();
                    }, function(err) {
                        $log.error(err);
                    });
            }

            $scope.assignUsers = function(plant, costCenter, users) {
                users.forEach(function(user) {
                    AdminCostCenterService.assignUser(plant.uuid, costCenter.uuid, user.uuid)
                        .then(function() {
                            $log.info('Assigned User ' + user.displayName + ' to Cost Center ' +
                                costCenter.displayName);
                        });
                })
            }

            $scope.getCostCenters = function(plants) {
                $scope.data.costCenters = [];
                for (var i = 0; i < plants.length; i++) {
                    AdminCostCenterService.getCostCenters(plants[i].uuid, {})
                        .then(function(costCenters) {
                            if (!costCenters) return;
                            $scope.data.costCenters = costCenters.plain().sort($scope.sortByDisplayName);
                        }, function(err) {
                            $log.error(err);
                        })
                }
                // $timeout(function() {
                //     var obj = [];
                //     $scope.data.all.forEach(function(a) {
                //         for (i = 0; i < a.length; i++)
                //             obj.push(a[i]);
                //     })
                //     $scope.data.costCenters = obj;
                //     $scope.countWorkCenters();
                //     DataService.saveCostCenters(obj);
                // }, 75);
            }

            $scope.refresh = function() {
                $scope.getCostCenters([$scope.data.plant]);
                $scope.data.selectAll = false;
                $scope.costCenterForm.costCenter = {};
            }

            $scope.countWorkCenters = function() {
                $scope.data.costCenters.forEach(function(cost) {
                    var count = 0;
                    cost.workCenterCount = cost.workCenters.length;
                })
            }

            $scope.selectedUsers = function() {
                return $.grep($scope.data.users, function(user) {
                    return user.selected;
                }).length;
            }

            $scope.remove = function() {
                $modalStack.dismissAll();
                var opts = {
                    templateUrl: "app/main/administration/costcenters/views/remove-modal.html",
                    scope: $scope,
                    size: 'md'
                };
                var instance = $modal.open(opts);
            }

            $scope.close = function() {
                $modalStack.dismissAll();
            }

            $scope.removeCostCenter = function() {
                $scope.data.plants.forEach(function(plant) {
                    for (var i = 0; i < plant.costCenters.length; i++)
                        for (var j = 0; j < $scope.data.selectedCostCenters.length; j++)
                            if (plant.costCenters[i].uuid ==
                                $scope.data.selectedCostCenters[j].uuid) {
                                AdminCostCenterService.deleteCostCenter(plant.uuid,
                                    plant.costCenters[i].uuid).then(function() {
                                    $scope.refresh();
                                }, function(err) {
                                    $log.error(err);
                                })
                            }
                })
            }

            $scope.confirmEdit = function() {
                var costCenter;
                for (var i = 0; i < $scope.data.displayedCostCenters.length; i++)
                    if ($scope.data.displayedCostCenters[i].edit == true)
                        costCenter = $scope.data.displayedCostCenters[i];

                $scope.data.plants.forEach(function(plant) {
                    for (var i = 0; i < plant.costCenters.length; i++)
                        if (plant.costCenters[i].uuid == costCenter.uuid) {
                            $scope.costCenterForm.newCostCenter.name = $scope.data.newCostCenterName
                            AdminCostCenterService.updateCostCenter(plant.uuid, costCenter.uuid,
                                $scope.costCenterForm.newCostCenter).then(function() {
                                $scope.refresh();
                            });
                        }
                });
            };
        }]);