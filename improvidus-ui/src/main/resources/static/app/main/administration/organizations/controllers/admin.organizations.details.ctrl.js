administration.controller("adminOrganizationDetailsCtrl",
    ["$rootScope", "$scope", "$stateParams", "growl", "AdminOrganizationsService",
        function($rootScope, $scope, $stateParams, growl, AdminOrganizationsService) {

            $scope.data = {
                configuration: null,
                copyConfiguration: null,
                organization: null
            };

            $scope.controls = {
                edit: false
            };

            $scope.saveOrganizationConfiguration = function() {
                AdminOrganizationsService.saveOrganizationConfiguration($rootScope.currentUser.organization.uuid, $scope.data.configuration).then(function(configuration) {
                    growl.info("Configuration saved", {
                        title: 'Info'
                    });
                    $rootScope.org.configuration = configuration.plain();
                }, function(error) {
                    growl.error("Configuration failed to update", {
                        title: 'Error'
                    });
                    $scope.data.configuration = $scope.data.copyConfiguration;
                })
            };

            $scope.editConfiguration = function() {
                $scope.data.copyConfiguration = angular.copy($scope.data.configuration)
            };

            $scope.cancel = function() {
                $scope.data.configuration = $scope.data.copyConfiguration;
            };

            function getOrganizationConfiguration(uuid) {
                var defaultConfig = {
                    organizationUuid: uuid,
                    conf: {
                        numShifts: 3
                    }
                };

                if (uuid == $rootScope.currentUser.organization.uuid) {
                    AdminOrganizationsService.getOrganizationConfiguration(uuid).then(function(configuration) {
                        if (configuration != null)
                            $scope.data.configuration = configuration.plain();
                        else
                            $scope.data.configuration = defaultConfig;
                    })
                }
            }

            function getOrganizationStatistics(uuid) {
                AdminOrganizationsService.getOrganizationStatistics({
                    organizationUuids: uuid
                }).then(function(statistics) {
                    if (statistics == null || statistics.length == 0) return;
                    $scope.data.organization.statistics = statistics[0];
                })
            }

            $scope.load = function(uuid) {
                AdminOrganizationsService.getOrganization(uuid).then(function(organization) {
                    if (organization == null) {
                        growl.error("Organization does not exist", {
                            title: 'Error'
                        });
                        $scope.selectState('/admin/organizations');
                        return;
                    }
                    $scope.data.organization = organization.plain();
                    getOrganizationConfiguration(uuid);
                    getOrganizationStatistics(uuid);
                }, function(error) {
                    growl.error("Organization does not exist or User does not have sufficient permissions", {
                        title: 'Error'
                    });
                    $scope.selectState('/admin/organizations');
                });
            };

            if ($stateParams.uuid) {
                $scope.load($stateParams.uuid);
            }
            else {
                $scope.selectState('/admin/organizations/' + $rootScope.currentUser.organization.uuid);
            }
        }]);