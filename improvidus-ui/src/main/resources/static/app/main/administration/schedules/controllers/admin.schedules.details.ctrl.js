administration.controller("adminScheduleDetailsCtrl",
    ["$rootScope", "$scope", "$state", "$modalStack", "$modal", "$log", "$stateParams", "growl", "AdminSchedulesService",
        function($rootScope, $scope, $state, $modalStack, $modal, $log, $stateParams, growl, AdminSchedulesService) {

            $rootScope.state = 'ScheduleDetails';

            $scope.data = {
                scheduleTemplate: null,
                scheduleTemplateCopy: null
            };

            $scope.controls = {
                edit: false
            };

            $scope.close = function() {
                $modalStack.dismissAll();
            };

            $scope.editScheduleTemplate = function() {
                $scope.data.scheduleTemplateCopy = angular.copy($scope.data.scheduleTemplate);
            };

            $scope.cancel = function() {
                $scope.data.scheduleTemplate = angular.copy($scope.data.scheduleTemplateCopy);
            };

            $scope.insertShift = function() {
                var blankShift = [null, null, null, null, null, null, null];
                $scope.data.scheduleTemplate.schedule.push(blankShift);
            };

            $scope.removeShift = function(index) {
                $scope.data.scheduleTemplate.schedule.splice(index, 1);
            };

            $scope.removeScheduleTemplate = function() {
                $modalStack.dismissAll();
                var opts = {
                    templateUrl: "app/main/administration/schedules/views/remove-modal.html",
                    scope: $scope,
                    size: 'md'
                };
                var instance = $modal.open(opts);
            };

            $scope.remove = function(scheduleTemplate) {
                AdminSchedulesService.deleteSchedule(scheduleTemplate.uuid).then(function(response) {
                    growl.info("Successfully deleted schedule template", {
                        title: 'Info'
                    });
                    $scope.selectState('/admin/schedules')
                });
            };

            $scope.saveScheduleTemplate = function(scheduleTemplate) {
                AdminSchedulesService.updateSchedule(scheduleTemplate.uuid, scheduleTemplate)
                    .then(function(scheduleTemplate) {
                        if (!scheduleTemplate) return;
                        growl.info("Updated " + scheduleTemplate.displayName, {
                            title: 'Info'
                        });
                        $scope.data.scheduleTemplate = scheduleTemplate.plain();
                    }, function(error) {
                        growl.error("Error occurred while updating schedule template.", {
                            title: 'Update Failed'
                        });
                    });
            };

            if ($stateParams.scheduleTemplateUuid) {
                AdminSchedulesService.getSchedule($stateParams.scheduleTemplateUuid).then(function(scheduleTemplate) {
                    $scope.data.scheduleTemplate = scheduleTemplate.plain();
                    $rootScope.state = scheduleTemplate.displayName;
                }, function(err) {
                    if (err.status == 403)
                        $location.path("admin/schedules");
                })
            }
            else {
                $state.go("Admin.ScheduleTemplates");
            }

        }]);