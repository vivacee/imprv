administration.factory('AdminSchedulesService', ['Restangular',
    function AdminSchedulesService(Restangular) {
        return {
            addSchedule: function(schedule) {
                return Restangular.one('schedules').customPOST(schedule, '', {}, {})
            },
            updateSchedule: function(uuid, schedule) {
                return Restangular.one('schedules', uuid).customPUT(schedule, '', {}, {});
            },
            deleteSchedule: function(uuid) {
                return Restangular.one('schedules', uuid).remove();
            },
            getSchedule: function(uuid) {
                return Restangular.one('schedules', uuid).get();
            },
            getSchedules: function() {
                return Restangular.one('schedules').get();
            },
        };
    }]);