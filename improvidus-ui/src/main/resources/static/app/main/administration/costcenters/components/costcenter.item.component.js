administration.component('costCenterItem', {
    bindings: {
        plant: '=',
        costCenter: '='
    },
    templateUrl: 'app/main/administration/costcenters/components/costcenter.item.component.html',
    controller: ['AdminCostCenterService', 'AdminWorkCenterService',
        function(AdminCostCenterService, AdminWorkCenterService) {
            var that = this;
            AdminCostCenterService.getUsers(that.plant.uuid, that.costCenter.uuid, {count: true})
                .then(function(count) {
                    that.nUsers = count.length;
                });
            AdminWorkCenterService.getWorkCenters(that.plant.uuid, that.costCenter.uuid,
                {count: true})
                .then(function(count) {
                    that.nWorkCenters = count.length;
                });

        }]
});