administration.controller("adminBaseCtrl",
    ["$scope", "$filter", "AdminPlantsService", "AdminCostCenterService", "AdminSchedulesService",
        function($scope, $filter, AdminPlantsService, AdminCostCenterService, AdminSchedulesService) {

            $scope.selectAll = function(value) {
                $scope.data.displayedUsers.forEach(function(user) {
                    if (user) user.selected = value;
                })
            }

            var manualSchedule = {
                displayName: "**Manual Schedule**",
                uuid: null
            };

            $scope.sortByDisplayName = function(a, b) {
                var nameA = a.displayName.toUpperCase();
                var nameB = b.displayName.toUpperCase();
                if (nameA < nameB) {
                    return -1;
                }
                if (nameA > nameB) {
                    return 1;
                }

                return 0;
            }

            $scope.loadScheduleTemplates = function() {
                AdminSchedulesService.getSchedules().then(function(schedules) {
                    $scope.data.scheduleTemplates = schedules.plain().sort($scope.sortByDisplayName);
                    $scope.data.scheduleTemplates.unshift(angular.copy(manualSchedule));
                })
            };

            $scope.getUsers = function callServer(tableState) {

                if (!tableState) return;
                if (!$scope.data.tableState && tableState) $scope.data.tableState = tableState;

                var pagination = tableState.pagination;

                var start = pagination.start || 0;     // This is NOT the page number, but the index of item in the list that you want to use to display the table.
                var number = pagination.number || 10;  // Number of entries showed per page.

                var searchedUsers = $filter('filter')($scope.data.users,
                    tableState.search.predicateObject);

                var orderedUsers = $filter('orderBy')(searchedUsers, 'selected', true);

                var filteredUsers = $.grep(orderedUsers, function(user) {
                    return !$scope.data.onlyMembers ||
                        ($scope.data.onlyMembers && user.selected == true);
                })


                $scope.data.displayedUsers = filteredUsers.slice(start, start + number);
                tableState.pagination.numberOfPages = Math.ceil(filteredUsers.length / number);
            }

            $scope.$watch('data.onlyMembers', function(newObj) {
                if (newObj == undefined) return;
                $scope.getUsers($scope.data.tableState);
            });

            //JS Tree Config

            $scope.typesConfig = {
                "plant": {
                    "icon": "fa fa-industry text-primary fa-lg"
                },
                "costCenter": {
                    "icon": "fa fa-usd text-primary fa-lg"
                },
                "workCenter": {
                    "icon": "fa fa-gear text-primary fa-lg"
                }
            };

            $scope.dndConfig = {
                "is_draggable": function(node) {
                    return true;
                }
            };

            $scope.sortConfig = {
                sort: function(a, b) {
                    var nodeA = this.get_node(a);
                    var nodeB = this.get_node(b);
                    var aType = nodeA.original.className;
                    var bType = nodeB.original.className;
                    var aName = nodeA.original.displayName.toLowerCase();
                    var bName = nodeB.original.displayName.toLowerCase();
                    if (aType == bType) {
                        return (aName < bName) ? -1 : (aName > bName) ? 1 : 0;
                    } else {
                        return (aType < bType) ? 1 : -1;
                    }
                }
            };

            $scope.treeCore = {
                "check_callback": function(operation, node, node_parent, node_position, more) {
                    if (operation === 'move_node') {
                        if ((node.type == "costCenter" && node_parent.type == "plant") ||
                            (node.type == "workCenter" && node_parent.type == "costCenter"))
                            return true;
                        return false;
                    }
                }
            };

            $scope.moveNodeCB = function(e, data) {
                if (data.parent !== data.old_parent) {
                    if (data.node.type == "costCenter") {
                        var oldPlantNode = $scope.treeInstance.jstree('get_node', data.old_parent);
                        var newPlantNode = $scope.treeInstance.jstree('get_node', data.parent);
                        oldPlantNode.original.costCenters = oldPlantNode.original.costCenters.filter(function(costCenter) {
                            return costCenter.uuid !== data.node.id;
                        });
                        newPlantNode.original.costCenters.push(data.node.original);

                        AdminPlantsService.updatePlant(oldPlantNode.id, oldPlantNode.original).then(function() {
                            AdminPlantsService.updatePlant(newPlantNode.id, newPlantNode.original).then(function() {
                                growl.info("Successfully moved " + data.node.text + " to " + newPlantNode.text, {
                                    title: 'Info'
                                });
                            })
                        })
                    }
                    else {
                        var oldCostCenterNode = $scope.treeInstance.jstree('get_node', data.old_parent);
                        var newCostCenterNode = $scope.treeInstance.jstree('get_node', data.parent);
                        oldCostCenterNode.original.workCenters = oldCostCenterNode.original.workCenters.filter(function(workCenter) {
                            return workCenter.uuid !== data.node.id;
                        });
                        newCostCenterNode.original.workCenters.push(data.node.original);

                        AdminCostCenterService.updateCostCenter(oldCostCenterNode.parent, oldCostCenterNode.id, oldCostCenterNode.original).then(function() {
                            AdminCostCenterService.updateCostCenter(newCostCenterNode.parent, newCostCenterNode.id, newCostCenterNode.original).then(function() {
                                growl.info("Successfully moved " + data.node.text + " to " + newCostCenterNode.text, {
                                    title: 'Info'
                                });
                            })
                        })

                    }
                }
            };

        }]);