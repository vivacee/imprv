administration.controller("adminPlantsCtrl",
    ["$rootScope", "$scope", "$controller", "$modalStack", "$modal", "$log", "growl", "AdminPlantsService",
        "UsersAdministrationService",
        function($rootScope, $scope, $controller, $modalStack, $modal, $log, growl, AdminPlantsService,
                 UsersAdministrationService) {

            $rootScope.state = 'Plants';

            $scope.data = {
                users: [],
                plants: [],
                displayedPlants: [],
                selectedPlants: [],
                tableState: undefined
            };

            angular.extend(this, $controller('adminBaseCtrl', {
                $scope: $scope
            }));

            var resetPlantForm = function() {
                $scope.plantForm = {
                    "users": [],
                    "plant": {},
                    "newPlant": {},
                    step: 1
                };
                $scope.data.selectAll = false;
            };
            resetPlantForm();

            $scope.buttons = {
                isCollapsed: false,
                invalid: false,
                submitted: false
            };

            $scope.add = function() {
                $modalStack.dismissAll();
                var opts = {
                    templateUrl: "app/main/administration/plants/views/add.html",
                    scope: $scope,
                    size: 'lg'
                };
                resetPlantForm();
                $scope.data.users = [];
                $scope.data.displayedUsers = [];
                UsersAdministrationService.getOrganizationUsers(
                    $scope.currentUser.organization.uuid)
                    .then(function(users) {
                        $scope.data.users = users.plain();
                        $scope.data.displayedUsers = users.plain();
                    });
                var instance = $modal.open(opts);
            };

            $scope.save = function() {
                $scope.buttons.submitted = true;
                var plant = {attributes: $scope.plantForm.plant};
                AdminPlantsService.addPlant(plant).then(function(plant) {
                    $log.info(plant);
                    var users = $.grep($scope.data.displayedUsers, function(user) {
                        return user.selected;
                    });
                    $scope.assignUsers(plant, users)
                    $scope.refresh();
                    $scope.buttons.submitted = false;
                    growl.info("Created " + plant.displayName, {
                        title: 'Info'
                    });
                    $modalStack.dismissAll();
                }, function(err) {
                    $scope.buttons.submitted = false;
                    $log.error(err);
                })
            };

            $scope.assignUsers = function(plant, users) {
                users.forEach(function(user) {
                    AdminPlantsService.assignUser(plant.uuid, user.uuid).then(function() {
                        $log.info(
                            'Assigned User ' + user.displayName + ' to Plant ' + plant.displayName);
                    });
                })
            };

            $scope.getPlants = function() {
                AdminPlantsService.getPlants({}).then(function(plants) {
                    $scope.data.plants = plants.plain().sort($scope.sortByDisplayName);
                }, function(err) {
                    $log.error(err);
                })
            };

            $scope.refresh = function() {
                $scope.getPlants();
                //$scope.treeInstance.jstree('refresh');
            };

            $scope.getPlants();

        }]);