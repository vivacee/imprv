administration.run([
    '$rootScope',
    '$q',
    'PermissionStore',
    'UserService',
    'PlansService',
    function($rootScope, $q, PermissionStore, UserService, PlansService) {

        var currentUser;

        function hasRole(roleName, deferred) {
            var results = $.grep(currentUser.roles, function(role) {
                    return role.name === roleName && role.organization.uuid === currentUser.organization.uuid;
                }).length > 0;
            if (roleName == 'SUPER_ADMIN')
                deferred.resolve()
            else
                results && PlansService.getActiveSubscription() ? deferred.resolve() :
                    deferred.reject();
        }

        function resolveUser(roleName, deferred) {
            UserService.getUser().then(function(user) {
                currentUser = user.plain();
                return PlansService.refreshActiveSubscription()
            }).then(function() {
                hasRole(roleName, deferred);
            })
        }

        var permissions = [{
            name: 'SUPER_ADMIN',
            displayName: 'SuperAdmin'
        }, {
            name: 'ADMIN',
            displayName: 'Admin'
        }, {
            name: 'GENERAL_MANAGER',
            displayName: 'GeneralManager'
        }, {
            name: 'PLANT_MANAGER',
            displayName: 'PlantManager'
        }, {
            name: 'PRODUCTION_MANAGER',
            displayName: 'ProductionManager'
        }, {
            name: 'OPERATOR',
            displayName: 'Operator'
        }]

        permissions.forEach(function(permission) {
            PermissionStore.definePermission(permission.name, function() {
                var deferred = $q.defer();
                // return true;
                // if (!angular.equals(_.omit(currentUser, 'lastModifiedDate'), _.omit($rootScope.currentUser, 'lastModifiedDate')))
                //     console.log("NOT EQUAL");
                if (currentUser && angular.equals(_.omit(currentUser, 'lastModifiedDate'), _.omit($rootScope.currentUser, 'lastModifiedDate')))
                    hasRole(permission.displayName, deferred);
                else
                    resolveUser(permission.displayName, deferred);
                return deferred.promise;
            });
        });

        function hasAnalysisAccess(deferred) {
            var subscriptionPlan = PlansService.getActiveSubscription();
            var isSuperAdmin = $.grep(currentUser.roles, function(role) {
                    return role.name === "SuperAdmin";
                }).length > 0;
            if (isSuperAdmin) {
                deferred.resolve();
                return;
            }
            var validPlan = subscriptionPlan.providerId !== 'started';
            var validNUsers = (subscriptionPlan.properties.nUsers === -1 ||
            currentUser.organization.nusers <= subscriptionPlan.properties.nUsers);

            validNUsers && validPlan ? deferred.resolve() :
                deferred.reject();
        }

        PermissionStore.definePermission("ANALYSIS_ACCESS", function() {
            var deferred = $q.defer();
            UserService.getUser().then(function(user) {
                currentUser = user.plain();
                return PlansService.refreshActiveSubscription()
            }).then(function() {
                hasAnalysisAccess(deferred);
            })
            return deferred.promise;
        });


        // PlansService.refreshActiveSubscription();
    }]);