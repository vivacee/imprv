administration.factory('AdminOrganizationsService', ['Restangular', 'RestangularV2',
    function AdminOrganizationsService(Restangular, RestangularV2) {
        return {
            addOrganization: function(form) {
                return RestangularV2.one('organization').customPOST(form, '', {}, {})
            },
            getOrganizations: function(options) {
                return RestangularV2.one('organization').get(options);
            },
            getOrganization: function(uuid) {
                return RestangularV2.one('organization', uuid).get();
            },
            saveOrganizationConfiguration: function(uuid, configuration) {
                return RestangularV2.one('organization', uuid).one('configuration')
                    .customPUT(configuration, '', {}, {});
            },
            getOrganizationConfiguration: function(uuid) {
                return RestangularV2.one('organization', uuid).one('configuration').get();
            },
            getOrganizationStatistics: function(options) {
                return RestangularV2.one('organization').one('statistics').get(options);
            }
        };
    }]);