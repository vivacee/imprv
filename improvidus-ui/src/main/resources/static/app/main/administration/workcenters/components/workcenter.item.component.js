administration.component('workCenterItem', {
    bindings: {
        plant: '=',
        costCenter: '=',
        workCenter: '='
    },
    templateUrl: 'app/main/administration/workcenters/components/workcenter.item.component.html',
    controller: ['AdminWorkCenterService', function(AdminWorkCenterService) {
        var that = this;
        AdminWorkCenterService.getUsers(that.plant.uuid, that.costCenter.uuid, that.workCenter.uuid,
            {count: true})
            .then(function(count) {
                that.nUsers = count.length;
            });
    }]
});