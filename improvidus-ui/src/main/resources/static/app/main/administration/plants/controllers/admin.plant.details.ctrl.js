administration.controller("adminPlantDetailsCtrl",
    ["$rootScope", "$scope", "$log", "$stateParams", "$filter", "$modalStack", "$modal", "$location",
        "$controller",
        "$state",
        "growl",
        "UsersAdministrationService",
        "AdminPlantsService",
        function($rootScope, $scope, $log, $stateParams, $filter, $modalStack, $modal, $location, $controller,
                 $state, growl, UsersAdministrationService, AdminPlantsService) {

            $scope.data = {
                users: [],
                displayedUsers: [],
                onlyMembers: false,
                updateAll: false,
                tableState: undefined
            }

            angular.extend(this, $controller('adminBaseCtrl', {
                $scope: $scope
            }));

            $scope.updateAll = function() {
                $scope.data.users.forEach(function(user) {
                    if (!user.selected && $scope.data.updateAll == true) {
                        AdminPlantsService.assignUser($scope.data.plant.uuid, user.uuid)
                            .then(function() {
                                user.selected = true;
                                $log.info('Assigned User ' + user.displayName + ' to Plant ' +
                                    $scope.data.plant.displayName);
                            });
                    } else if (user.selected == true && $scope.data.updateAll == false) {
                        AdminPlantsService.unassingUser($scope.data.plant.uuid, user.uuid)
                            .then(function() {
                                user.selected = false;
                                $log.info('Assigned User ' + user.displayName + ' to Plant ' +
                                    $scope.data.plant.displayName);
                            });
                    }
                })
            }

            $scope.remove = function() {
                $modalStack.dismissAll();
                var opts = {
                    templateUrl: "app/main/administration/plants/views/remove-modal.html",
                    scope: $scope,
                    size: 'md'
                };
                var instance = $modal.open(opts);
            }

            $scope.removePlant = function() {
                AdminPlantsService.deletePlant($scope.data.plant.uuid).then(function(plant) {
                    $modalStack.dismissAll();
                    growl.info("Removed " + $scope.data.plant.displayName, {
                        title: 'Info'
                    });
                    $location.path("admin/plants");
                }, function(err) {
                    $log.error(err);
                })
            }

            $scope.close = function() {
                $modalStack.dismissAll();
            }


            $scope.loadUsers = function() {
                UsersAdministrationService.getOrganizationUsers(
                    $scope.currentUser.organization.uuid)
                    .then(function(users) {
                        $scope.data.users = users;
                        return AdminPlantsService.getUsers($scope.data.plant.uuid);
                    }).then(function(plantUsers) {
                    var uuids = plantUsers.map(function(user) {
                        if (user) return user.uuid;
                    })
                    $scope.data.users.forEach(function(user) {
                        if (user)
                            user.selected = uuids.indexOf(user.uuid) > -1 ? true : false;
                    });
                    $scope.getUsers($scope.data.tableState);
                });
            }

            $scope.updateEntry = function(user) {
                if (user.selected == true) {
                    AdminPlantsService.assignUser($scope.data.plant.uuid, user.uuid)
                        .then(function() {
                            $log.info('Assigned User ' + user.displayName + ' to Plant ' +
                                $scope.data.plant.displayName);
                        });
                } else if (user.selected == false) {
                    AdminPlantsService.unassingUser($scope.data.plant.uuid, user.uuid)
                        .then(function() {
                            $log.info('Unassigned User ' + user.displayName + ' to Plant ' +
                                $scope.data.plant.displayName);
                        });
                }
                $scope.getUsers($scope.data.tableState);
            }

            if ($stateParams.plantUuid) {
                AdminPlantsService.getPlant($stateParams.plantUuid).then(function(plant) {
                    $scope.data.plant = plant;
                    $rootScope.state = plant.displayName;
                    $scope.loadUsers();
                }, function(err) {
                    if (err.status == 403)
                        $location.path("admin/plants");
                })
            }
            else {
                $state.go("Admin.Plants");
            }

        }])
;