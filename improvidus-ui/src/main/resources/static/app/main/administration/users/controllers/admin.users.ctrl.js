administration.controller("adminUsersCtrl",
    ["$rootScope", "$scope", "$state", "PermissionsService", "$modalStack", "$modal", "$log", "growl", "UsersAdministrationService",
        "UserService", "AdminUsersService",
        function($rootScope, $scope, $state, PermissionsService, $modalStack, $modal, $log, growl, UsersAdministrationService,
                 UserService, AdminUsersService) {

            $rootScope.state = 'Users';

            $scope.data = {
                users: [],
                displayedUsers: [],
                selectedUsers: []
            }

            $scope.userForm = {
                role: "Operator",
                step: 1
            }

            $scope.userInviteForm = {
                role: "Operator",
                step: 1
            }

            $scope.buttons = {
                isCollapsed: false,
                invalid: false
            };

            $scope.controls = {
                processing: false
            }

            $scope.add = function() {
                if ($rootScope.currentUser.roles[0].displayName == "SuperAdmin")
                    $scope.userForm.role = "SuperAdmin";

                $modalStack.dismissAll();
                var opts = {
                    templateUrl: "app/main/administration/users/views/add.html",
                    scope: $scope,
                    size: 'md'
                };
                $modal.open(opts);
            };

            $scope.invite = function() {
                if ($rootScope.currentUser.roles[0].displayName == "SuperAdmin")
                    $scope.userInviteForm.role = "SuperAdmin";

                $modalStack.dismissAll();
                var opts = {
                    templateUrl: "app/main/administration/users/views/invite.html",
                    scope: $scope,
                    size: 'md'
                };
                $modal.open(opts);
            };

            $scope.inviteUser = function() {
                $scope.buttons.isCollapsed = false;
                $scope.controls.processing = true;
                AdminUsersService.assignUser($scope.currentUser.organization.uuid, {
                    roleName: $scope.userInviteForm.role,
                    email: $scope.userInviteForm.email
                }).then(function() {
                    growl.info("Invited " + $scope.userInviteForm.email + " to join the Organization", {
                        title: 'Info'
                    });
                    $scope.controls.processing = false;
                    $modalStack.dismissAll();
                    $scope.getUsers();
                }, function() {
                    $scope.controls.processing = false;
                });
            };

            $scope.edit = function(user) {
                $scope.data.updatedUser = angular.copy(user);
                for (var i = 0; i < $scope.data.updatedUser.roles.length; i++) {
                    if ($scope.data.updatedUser.roles[i].organization.uuid == $rootScope.currentUser.organization.uuid) {
                        $scope.data.newRoleName = $scope.data.updatedUser.roles[i].displayName.replace(/\s+/g, '');
                    }
                }
                if ($scope.data.newRoleName == "SuperAdmin")
                    return;
                $modalStack.dismissAll();
                var opts = {
                    templateUrl: "app/main/administration/users/views/edit.html",
                    scope: $scope,
                    size: 'md'
                };
                $modal.open(opts);
            }

            $scope.save = function() {
                $scope.userForm.user.username = $scope.userForm.user.email;
                $scope.buttons.isCollapsed = false;
                $scope.controls.processing = true;
                AdminUsersService.addUser($scope.userForm.user, {
                    roleName: $scope.userForm.role,
                    organizationUuid: $scope.currentUser.organization.uuid
                }).then(function() {
                    growl.info("User created successfully. A registration email has been sent to " + $scope.userForm.user.email, {
                        title: 'Info'
                    });
                    $scope.controls.processing = false;
                    $modalStack.dismissAll();
                    $scope.getUsers();
                }, function() {
                    $scope.controls.processing = false;
                });
            }

            $scope.close = function() {
                $modalStack.dismissAll();
            }

            $scope.updateUserRole = function(user, roleName) {
                user.authorities = null;
                AdminUsersService.assignOrganizationUserRole(user, $rootScope.currentUser.organization.uuid, {
                    roleName: roleName
                }).then(function() {
                    growl.info("User updated successfully", {
                        title: 'Info'
                    });
                    $scope.getUsers();
                }, function(error) {
                    growl.error("Cannot change your own user role", {
                        title: 'Update User Error'
                    });
                });
            }

            $scope.removeUsers = function() {
                $scope.data.selectedUsers.forEach(function(user) {
                    if (user.organizationMemberships.length == 1) {
                        //Delete User
                        AdminUsersService.deleteUser(user.uuid).then(function() {
                            growl.info("User deleted successfully", {
                                title: 'Info'
                            });
                            $scope.getUsers();
                        }, function(error) {
                            growl.error("Cannot delete user that is currently logged on", {
                                title: 'User Removal Error'
                            });
                        });
                    }
                    else {
                        //Remove User from organization
                        AdminUsersService.unassignUser($rootScope.currentUser.organization.uuid, user.uuid).then(function() {
                            growl.info("User removed successfully", {
                                title: 'Info'
                            });
                            if (user.uuid == $rootScope.currentUser.uuid) {
                                UserService.getUser().then(function(user) {
                                    $rootScope.currentUser = user.plain();
                                    $state.reload(true);
                                    $scope.selectState('/');
                                })
                            }
                            else
                                $scope.getUsers();

                        }, function(error) {
                            growl.error("Cannot remove user that is currently logged on", {
                                title: 'User Removal Error'
                            });
                        });
                    }
                });
            };

            // $scope.getUserRole = function(roles) {
            //     var userRole;
            //     roles.forEach(function(role) {
            //         if (role.organization.uuid == $rootScope.currentUser.organization.uuid) {
            //             userRole = role;
            //             return;
            //         }
            //     })
            //     return userRole;
            // };

            $scope.setActiveRoles = function() {
                $scope.data.users.forEach(function(user) {
                    user.roles.forEach(function(role) {
                        if (role.organization.uuid == $rootScope.currentUser.organization.uuid) {
                            user.activeRole = role;
                        }
                    })
                })
            };

            $scope.getUsers = function() {
                UsersAdministrationService.getOrganizationUsers(
                    $scope.currentUser.organization.uuid)
                    .then(function(users) {
                        $scope.data.users = users;
                        $rootScope.currentUser.organization.nusers = users.length;
                        $scope.setActiveRoles();
                    });
            };

            $scope.selectedUsers = function() {
                return $.grep($scope.data.users, function(user) {
                    return user.selected;
                }).length;
            }

            $scope.refresh = function() {
                $scope.getUsers();
            };

            $scope.getUsers();

        }]);