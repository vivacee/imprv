/*******************************************************************************
 * Copyright (c) 2015 Bosch Software Innovations GmbH. All rights reserved.
 ******************************************************************************/
angular.module("app.controllers.administration", ['app.controllers.base', 'dndLists'])
    .controller(
        "AdministrationCtrl",
        [
            "$rootScope",
            "$scope",
            "$controller",
            "$location",
            "$state",
            "$q",
            "growl",
            "UserService",
            "UsersAdministrationService",
            "UserGroupsAdministrationService",
            "RolesAdministrationService",
            "PermissionsService",
            function($rootScope, $scope, $controller, $location, $state, $q, growl, UserService,
                     UsersAdministrationService, UserGroupsAdministrationService,
                     RolesAdministrationService, PermissionsService) {

                angular.extend(this, $controller('BaseCtrl', {
                    $scope: $scope
                }));

                $scope.usersAdminControls = {
                    resourceView: 'usersView',
                    selectionView: ''
                }

                $scope.clearControls = function() {
                    $scope.buttons = {
                        collapsed: true,
                        confirm: false
                    }
                    $scope.errors = {};
                    $scope.newObject = {};
                }
                $scope.clearControls();

                $scope.firstSelected = false;
                $scope.firstSelectedRow = {};

                $scope.init = function(supressEvents) {
                    if ($location.$$search.resourceView) {
                        $scope.usersAdminControls.resourceView = $location.$$search.resourceView;
                        if ($location.$$search.uuid) {
                            switch ($location.$$search.resourceView) {
                                case 'usersView':
                                    $scope.loadAllUsers(function() {
                                        for (var i = 0; i < $scope.data.users.length; ++i) {
                                            if ($scope.data.users[i].uuid == $location.$$search.uuid)
                                                $scope.selectInitialRow($scope.data.users[i], supressEvents);
                                        }
                                    });
                                    break;
                                case 'userGroupsView':
                                    $scope.loadAllUserGroups(function() {
                                        for (var i = 0; i < $scope.data.userGroups.length; ++i) {
                                            if ($scope.data.userGroups[i].uuid == $location.$$search.uuid)
                                                $scope.selectInitialRow($scope.data.userGroups[i], supressEvents);
                                        }
                                    });
                                    break;
                                case 'rolesView':
                                    $scope.loadAllRoles(function() {
                                        for (var i = 0; i < $scope.data.roles.length; ++i) {
                                            if ($scope.data.roles[i].uuid == $location.$$search.uuid)
                                                $scope.selectInitialRow($scope.data.roles[i], supressEvents);
                                        }
                                    });
                                    break;
                            }
                        }
                    }
                }

                $scope.addUser = function() {
                    var user = {
                        username: $scope.newObject.username,
                        email: $scope.newObject.email,
                        firstName: $scope.newObject.firstName,
                        lastName: $scope.newObject.lastName,
                        organization: {
                            uuid: $scope.currentUser.organization.uuid
                        },
                        password: $scope.newObject.password,
                        userGroups: [],
                        roles: [],
                        permissions: [],
                        contacts: []
                    };
                    UsersAdministrationService.addUser(user).then(function(newUser) {
                        $scope.deselectAll(function() {
                            $location.search('uuid', newUser.uuid);
                            $scope.init();
                        });
                    });
                }

                $scope.addUserGroup = function() {
                    var userGroup = {
                        displayName: $scope.newObject.displayName,
                        name: $scope.newObject.displayName,
                        description: $scope.newObject.description,
                        organization: {
                            uuid: $scope.currentUser.organization.uuid
                        }
                    };
                    UserGroupsAdministrationService.addUserGroup(userGroup).then(
                        function(newUserGroup) {
                            $scope.deselectAll(function() {
                                $location.search('uuid', newUserGroup.uuid);
                                $scope.init();
                            });
                        });
                }

                $scope.addRole = function() {
                    var role = {
                        displayName: $scope.newObject.displayName,
                        name: $scope.newObject.displayName,
                        description: $scope.newObject.description,
                        organization: {
                            uuid: $scope.currentUser.organization.uuid
                        }
                    };
                    RolesAdministrationService.addRole(role).then(function(newRole) {
                        $scope.deselectAll(function() {
                            $location.search('uuid', newRole.uuid);
                            $scope.init();
                        });
                    });
                }

                $scope.removeUsers = function() {
                    for (var i = 0; i < $scope.data.selectedUsers.length; i++) {
                        $scope.removeUser($scope.data.selectedUsers[i]);
                    }
                }

                $scope.removeUser = function(user) {
                    UsersAdministrationService.deleteUser(user.uuid).then(function() {
                        $scope.$emit("elementDeSelectedEvent", user.uuid, user);
                        var index = $scope.data.users.indexOf(user);
                        $scope.data.users.splice(index, 1);
                    })
                }

                $scope.removeUserGroups = function() {
                    for (var i = 0; i < $scope.data.selectedUserGroups.length; i++) {
                        $scope.removeUserGroup($scope.data.selectedUserGroups[i]);
                    }
                }

                $scope.removeUserGroup = function(userGroup) {
                    UserGroupsAdministrationService.deleteUserGroup(userGroup.uuid).then(function() {
                        $scope.$emit("elementDeSelectedEvent", userGroup.uuid, userGroup);
                        var index = $scope.data.userGroups.indexOf(userGroup);
                        $scope.data.userGroups.splice(index, 1);
                    })
                }

                $scope.removeRoles = function() {
                    for (var i = 0; i < $scope.data.selectedRoles.length; i++) {
                        $scope.removeRole($scope.data.selectedRoles[i]);
                    }
                }

                $scope.removeRole = function(role) {
                    RolesAdministrationService.deleteRole(role.uuid).then(function() {
                        $scope.$emit("elementDeSelectedEvent", role.uuid, role);
                        var index = $scope.data.roles.indexOf(role);
                        $scope.data.roles.splice(index, 1);
                    })
                }

                $scope.$watch('newObject.password', function() {
                    if (!$scope.errors) return;
                    if (!$scope.newObject.password) return;
                    $scope.errors.password = [];
                    if (/[a-z]/.test($scope.newObject.password) == false) {
                        $scope.errors.password.push('Enter at least one lower case letter.');
                    }
                    if (/[A-Z]/.test($scope.newObject.password) == false) {
                        $scope.errors.password.push('Enter at least one upper case letter.');
                    }
                    if (/\d/.test($scope.newObject.password) == false) {
                        $scope.errors.password.push('Enter at least one number.');
                    }
                    if (/\W+/.test($scope.newObject.password) == false) {
                        $scope.errors.password.push('Enter at least one special character.');
                    }
                })

                $scope.$watch('newObject.password2', function() {
                    if (!$scope.errors) return;
                    if (!$scope.newObject.password2) return;
                    $scope.errors.password2 = [];
                    if ($scope.newObject.password2 !== $scope.newObject.password) {
                        $scope.errors.password2.push('Passwords should match');
                    }
                })

                $scope.$watch('usersAdminControls.resourceView', function(newView, oldView) {
                    if (newView !== oldView) $scope.deselectAll(function() {
                    });
                    $location.search('resourceView', newView);
                    switch (newView) {
                        case 'usersView':
                            if (!$location.$$search.uuid) {
                                $scope.loadAllUsers(function() {
                                    $scope.usersAdminControls.selectionView = 'userGroupsView';
                                    if ($scope.data.users.length > 0)
                                        $scope.selectInitialRow($scope.data.users[0]);
                                });
                            } else
                                $scope.usersAdminControls.selectionView = 'userGroupsView';
                            break;
                        case 'userGroupsView':
                            if (!$location.$$search.uuid) {
                                $scope.loadAllUserGroups(function() {
                                    $scope.usersAdminControls.selectionView = 'usersView';
                                    if ($scope.data.userGroups.length > 0)
                                        $scope.selectInitialRow($scope.data.userGroups[0]);
                                });
                            } else
                                $scope.usersAdminControls.selectionView = 'usersView';
                            break;
                        case 'rolesView':
                            if (!$location.$$search.uuid) {
                                $scope.loadAllRoles(function() {
                                    $scope.usersAdminControls.selectionView = 'permissionsView';
                                    if ($scope.data.roles.length > 0)
                                        $scope.selectInitialRow($scope.data.roles[0]);
                                });
                            } else
                                $scope.usersAdminControls.selectionView = 'permissionsView';
                            break;
                    }
                    $scope.firstSelected = true;
                }, true);

                $scope.$watch('usersAdminControls.selectionView', function(newView) {
                    if (newView && !jQuery.isEmptyObject($scope.currentSelection)) {
                        $scope.refreshDisplay(newView);
                    }
                }, true);

                $scope.loadAllUsers = function(callback) {
                    UsersAdministrationService.getUsers().then(function(users) {
                        $scope.data.users = users.plain();
                        if (callback) callback();
                    });
                }

                $scope.loadAllRoles = function(callback) {
                    RolesAdministrationService.getRoles().then(function(roles) {
                        $scope.data.roles = roles.plain();
                        if (callback) callback();
                    });
                }

                $scope.loadAllUserGroups = function(callback) {
                    UserGroupsAdministrationService.getUserGroups().then(function(userGroups) {
                        $scope.data.userGroups = userGroups.plain();
                        if (callback) callback();
                    });
                }

                $scope.loadAllPermissions = function(callback) {
                    PermissionsService.getPermissions().then(function(permissions) {
                        $scope.data.permissions = permissions.plain();
                        if (callback) callback();
                    });
                    /*
                     * TODO: Use when Permission Service is ready
                     * PermissionsAdministrationService.getPermissions().then(function(permissions) {
                     * $scope.data.permissions = permissions.plain(); callback();
                     * });
                     */
                }

                $scope.data = {
                    users: [],
                    roles: [],
                    userGroups: []
                };

                // Will be more useful if/when we enable multiple selections
                $scope.allSelected = {
                    users: [],
                    roles: [],
                    userGroups: []
                };

                $scope.currentSelection = {};

                $scope.existsSelection = false; // Used in administration.html to
                // update selection display

                $scope.updateSelectionFlag = function() {
                    if ($scope.allSelected.users.length > 0 || $scope.allSelected.roles.length > 0
                        || $scope.allSelected.userGroups.length > 0) {
                        $scope.existsSelection = true;
                    } else {
                        $scope.existsSelection = false;
                        $location.search('uuid', null);
                    }
                }

                $scope.selectInitialRow = function(row, supressEvents) {
                    row.isSelected = true;
                    $scope.firstSelectedRow = row;
                    if (supressEvents == undefined || supressEvents == false)
                        $scope.$emit("elementSelectedEvent", row.uuid, row);
                }

                $scope.deselectAll = function(callback) {
                    for (var i = 0; i < $scope.allSelected.users.length; ++i) {
                        $scope.allSelected.users[i].isSelected = false;
                    }
                    for (var i = 0; i < $scope.allSelected.roles.length; ++i) {
                        $scope.allSelected.roles[i].isSelected = false;
                    }
                    for (var i = 0; i < $scope.allSelected.userGroups.length; ++i) {
                        $scope.allSelected.userGroups[i].isSelected = false;
                    }
                    $scope.allSelected.users = [];
                    $scope.allSelected.roles = [];
                    $scope.allSelected.userGroups = [];
                    $scope.currentSelection = {};
                    $scope.clearControls();
                    $scope.updateSelectionFlag();
                    callback();
                }

                $scope.$on("elementSelectedEvent", function(e, uuid, row) {
                    // Must deselect initially selected row (if applicable)
                    if ($scope.firstSelected && $scope.firstSelectedRow !== row) {
                        $scope.firstSelectedRow.isSelected = false;
                        $scope.$emit("elementDeSelectedEvent", $scope.firstSelectedRow.uuid,
                            $scope.firstSelectedRow);
                        $scope.firstSelected = false;
                    }
                    $location.search('uuid', uuid);
                    switch (row.className) {
                        case 'User':
                            $scope.allSelected.users.push(row);
                            $scope.currentSelection = row;
                            break;
                        case 'UserGroup':
                            $scope.allSelected.userGroups.push(row);
                            $scope.currentSelection = row;
                            break;
                        case 'Role':
                            $scope.allSelected.roles.push(row);
                            $scope.currentSelection = row;
                            break;
                    }
                    $scope.refreshDisplay($scope.usersAdminControls.selectionView);
                    $scope.updateSelectionFlag();
                })

                $scope.$on("elementDeSelectedEvent", function(e, uuid, row) {
                    switch (row.className) {
                        case 'User':
                            var index = $scope.allSelected.users.indexOf(row);
                            $scope.allSelected.users.splice(index, 1);
                            break;
                        case 'UserGroup':
                            var index = $scope.allSelected.userGroups.indexOf(row);
                            $scope.allSelected.userGroups.splice(index, 1);
                            break;
                        case 'Role':
                            var index = $scope.allSelected.roles.indexOf(row);
                            $scope.allSelected.roles.splice(index, 1);
                            break;
                    }
                    $scope.updateSelectionFlag();
                });

                $scope.models = [{
                    listName: "Available",
                    items: [],
                    dragging: false,
                    style: {
                        height: null
                    }
                }, {
                    listName: "Current",
                    items: [],
                    dragging: false,
                    style: {
                        height: null
                    }
                }];

                $scope.refreshDisplay = function(view) {
                    switch (view) {
                        case 'usersView':
                            $scope.loadAllUsers(function() {
                                // $scope.updateSelectionDisplay($scope.data.users,
                                // $scope.currentSelection.users);

                                UsersAdministrationService.getUserGroupUsers($scope.currentSelection.uuid)
                                    .then(function(users) {
                                        $scope.updateSelectionDisplay($scope.data.users, users.plain());
                                    })

                            });
                            break;
                        case 'userGroupsView':
                            $scope.loadAllUserGroups(function() {
                                $scope.updateSelectionDisplay($scope.data.userGroups,
                                    $scope.currentSelection.userGroups);
                            });
                            break;
                        case 'rolesView':
                            $scope
                                .loadAllRoles(function() {
                                    $scope.updateSelectionDisplay($scope.data.roles,
                                        $scope.currentSelection.roles);
                                });
                            break;
                        case 'permissionsView':
                            $scope.loadAllPermissions(function() {
                                $scope.updateSelectionDisplay($scope.data.permissions,
                                    $scope.currentSelection.permissions);
                            });
                            break;
                    }
                }

                $scope.ArrayContainsUuid = function(array, uuid) {
                    for (var i = 0; i < array.length; ++i) {
                        if (array[i].uuid == uuid) return true;
                    }
                    return false;
                }

                $scope.updateSelectionDisplay = function(allItems, currentItems) {
                    $scope.models[0].items = [];
                    $scope.models[1].items = [];
                    for (var i = 0; i < allItems.length; ++i) {
                        if (!$scope.ArrayContainsUuid(currentItems, allItems[i].uuid)) {
                            $scope.models[0].items.push(allItems[i]);
                        }
                    }
                    for (var i = 0; i < currentItems.length; ++i) {
                        $scope.models[1].items.push(currentItems[i]);
                    }
                }

                /**
                 * dnd-dragging determines what data gets serialized and send to
                 * the receiver of the drop. While we usually just send a single
                 * object, we send the array of all selected items here.
                 */
                $scope.getSelectedItemsIncluding = function(list, item) {
                    item.selected = true;
                    return list.items.filter(function(item) {
                        return item.selected;
                    });
                };

                /**
                 * We set the list into dragging state, meaning the items that are
                 * being dragged are hidden. We also use the HTML5 API directly to
                 * set a custom image, since otherwise only the one item that the
                 * user actually dragged would be shown as drag image.
                 */
                $scope.onDragstart = function(list, event) {
                    list.dragging = true;
                    if (event.dataTransfer.setDragImage) {
                        var img = new Image();
                        // img.src =
                        // 'framework/vendor/ic_content_copy_black_24dp_2x.png';
                        event.dataTransfer.setDragImage(img, 0, 0);
                    }
                };

                /**
                 * In the dnd-drop callback, we now have to handle the data array
                 * that we sent above. We handle the insertion into the list
                 * ourselves. By returning true, the dnd-list directive won't do
                 * the insertion itself.
                 */
                $scope.onDrop = function(list, items, index) {
                    angular.forEach(items, function(item) {
                        item.selected = false;
                    });
                    list.items = list.items.slice(0, index).concat(items).concat(
                        list.items.slice(index));
                    return true;
                }

                $scope.assignItems = function(className, items) {
                    var defer = $q.defer();
                    switch ($scope.currentSelection.className) {
                        case 'User':
                            if (className == 'UserGroup')
                                for (var i = 0; i < items.length; ++i) {
                                    UserGroupsAdministrationService.assignUsers(items[i],
                                        $scope.currentSelection.uuid).then(function() {
                                        defer.resolve('true');
                                    });
                                }
                            if (className == 'Role')
                                UsersAdministrationService.assignRoles($scope.currentSelection.uuid, items)
                                    .then(function() {
                                        defer.resolve('true');
                                    });
                            if (className == 'Permission')
                                UsersAdministrationService.assignPermissions($scope.currentSelection.uuid,
                                    items).then(function() {
                                    defer.resolve('true');
                                });
                            break
                        case 'UserGroup':
                            if (className == 'User')
                                UserGroupsAdministrationService
                                    .assignUsers($scope.currentSelection.uuid, items).then(function() {
                                    defer.resolve('true');
                                });
                            if (className == 'Role')
                                UserGroupsAdministrationService
                                    .assignRoles($scope.currentSelection.uuid, items).then(function() {
                                    defer.resolve('true');
                                });
                            if (className == 'Permission')
                                UserGroupsAdministrationService.assignPermissions($scope.currentSelection.uuid,
                                    items).then(function() {
                                    defer.resolve('true');
                                });
                            break
                        case 'Role':
                            if (className == 'Permission')
                                RolesAdministrationService.assignPermissions($scope.currentSelection.uuid,
                                    items).then(function() {
                                    defer.resolve('true');
                                });
                            break
                    }
                    return defer.promise;
                }

                $scope.unassignItems = function(className, items) {
                    var defer = $q.defer();
                    switch ($scope.currentSelection.className) {
                        case 'User':
                            if (className == 'UserGroup')
                                for (var i = 0; i < items.length; ++i) {
                                    UserGroupsAdministrationService.unassignUsers(items[i],
                                        $scope.currentSelection.uuid).then(function() {
                                        defer.resolve('true');
                                    });
                                }
                            if (className == 'Role')
                                UsersAdministrationService.unassignRoles($scope.currentSelection.uuid, items)
                                    .then(function() {
                                        defer.resolve('true');
                                    });

                            if (className == 'Permission')
                                UsersAdministrationService.unassignPermissions($scope.currentSelection.uuid,
                                    items).then(function() {
                                    defer.resolve('true');
                                });
                            break
                        case 'UserGroup':
                            if (className == 'User')
                                UserGroupsAdministrationService.unassignUsers($scope.currentSelection.uuid,
                                    items).then(function() {
                                    defer.resolve('true');
                                });
                            if (className == 'Role')
                                UserGroupsAdministrationService.unassignRoles($scope.currentSelection.uuid,
                                    items).then(function() {
                                    defer.resolve('true');
                                });
                            if (className == 'Permission')
                                UserGroupsAdministrationService.unassignPermissions(
                                    $scope.currentSelection.uuid, items).then(function() {
                                    defer.resolve('true');
                                });
                            break
                        case 'Role':
                            if (className == 'Permission')
                                RolesAdministrationService.unassignPermissions($scope.currentSelection.uuid,
                                    items).then(function() {
                                    defer.resolve('true');
                                });
                            break
                    }
                    return defer.promise;
                }

                /**
                 * Last but not least, we have to remove the previously dragged
                 * items in the dnd-moved callback.
                 */
                $scope.onMoved = function(list) {
                    var movedItems = list.items.filter(function(item) {
                        return item.selected;
                    });

                    list.items = list.items.filter(function(item) {
                        return !item.selected;
                    })

                    var promises = [];

                    // Make sure items have actually been moved across lists
                    if (movedItems.length > 0
                        && !$scope.ArrayContainsUuid(list.items, movedItems[0].uuid)) {
                        var uuids = [];
                        var movedClass = '';
                        if (movedItems.length > 0) movedClass = movedItems[0].className;
                        for (var i = 0; i < movedItems.length; ++i) {
                            uuids.push(movedItems[i].uuid);
                            movedItems[i].selected = false;
                            if (list.listName == "Available") {
                                growl.success(movedItems[i].displayName + " added to "
                                    + $scope.currentSelection.displayName, {
                                    title: 'Updated Instance'
                                });
                            } else if (list.listName == "Current") {
                                growl.success(movedItems[i].displayName + " removed from "
                                    + $scope.currentSelection.displayName, {
                                    title: 'Updated Instance'
                                });
                            }
                        }
                        if (list.listName == "Available")
                            promises.push($scope.assignItems(movedClass, uuids));
                        if (list.listName == "Current")
                            promises.push($scope.unassignItems(movedClass, uuids));
                    }

                    $q.all(promises).then(function() {
                        $scope.init(true);
                    })
                };

                // $scope.checkInitialPermissions = function() {
                // if (!PermissionsService.checkPermissions(['VIEW_USERS']))
                // $location.path('/dashboards');
                // }
                // $scope.checkInitialPermissions();
            }]);