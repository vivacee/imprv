//import "./consulting.ctrl.js";

var servicesModule = angular.module('app.controllers.services', ['app.controllers.services.consulting']).controller("ServicesCtrl",
    ["$scope", "$controller", "$state", function($scope, $controller, $state) {


        angular.extend(this, $controller('DataFilterCtrl', {
            $scope: $scope
        }));


    }]);

servicesModule.directive('slideShow', function() {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            var nice = $(element).find(".slider-show").niceScroll({cursorcolor: "#d9e0e7", touchbehavior: true});
            $(element).find(".slider-show").scroll(function() {
                $(element).find(".slider-show").getNiceScroll().resize();
            });
        }
    };
});