analysis.controller(
    'AnalysisDetailsCtrl',
    ['$scope', '$location', '$stateParams', '$modal', '$modalStack', '$timeout', 'growl', 'AnalysisResultService',
        function($scope, $location, $stateParams, $modal, $modalStack, $timeout, growl, AnalysisResultService) {

            $scope.data = {};

            $scope.data = {
                analysisVersions: [],
                displayedAnalysisVersions: [],
                selectedAnalysisVersions: []
            }

            $scope.getAnalysisVersions = function() {
                if ($stateParams.uuid) {
                    AnalysisResultService.getAnalysisGroup($stateParams.uuid)
                        .then(function(analysisGroup) {
                            if (!analysisGroup) return;
                            $scope.data.analysisGroup = analysisGroup.plain();
                            $scope.data.analysisVersions = $scope.data.analysisGroup.versions;
                            $scope.checkResultsProgress($scope.data.analysisGroup.versions);
                        });
                }
            }

            $scope.checkResultsProgress = function(versions) {
                for (var i = 0; i < versions.length; i++) {
                    var results = Object.values(versions[i].results);
                    for (var j = 0; j < results.length; j++) {
                        if (results[j].status == 'IN_PROGRESS') {
                            $timeout($scope.getAnalysisVersions, 5000);
                            return;
                        }
                    }
                }
            }

            $scope.resultLabels = {
                "mmparetos": "Multi-Metric Pareto",
                "correlations": "Event Correlation",
                "wordcloud": "Word Cloud",
                "recommendations": "Recommendations"
            }

            $scope.getAnalysisVersions();

            $scope.showResult = function(type, version) {
                var status = $scope.data.analysisGroup.versions[version].results[type].status;
                if (status == 'COMPLETED')
                    $location.path('/analysis/' + $scope.data.analysisGroup.uuid + "/" + type + "/" + version);
                else if (status == 'FAIL')
                    growl.info(type + " analysis failed with cause: \n\n" + decodeURI($scope.data.analysisGroup.versions[version].results[type].comments), {
                        title: 'Info'
                    });
            }

            $scope.addVersion = function() {
                $modalStack.dismissAll();
                var opts = {
                    templateUrl: "app/main/analysis/views/new-version.html",
                    scope: $scope,
                    size: 'md'
                };
                var instance = $modal.open(opts);
            }

            $scope.close = function() {
                $modalStack.dismissAll();
            }

            $scope.reprocess = function(analysisGroup, versionDescription) {
                growl.info("Re-processing " + analysisGroup.displayName, {
                    title: 'Info'
                });
                analysisGroup.newVersionDescription = versionDescription;
                AnalysisResultService.reanalyze(analysisGroup).then(function(response) {
                    $scope.getAnalysisVersions();
                });
            }

            $scope.remove = function(analysisGroup) {
                growl.info("Deleting " + analysisGroup.displayName, {
                    title: 'Info'
                });
                AnalysisResultService.deleteAnalysisGroup(analysisGroup.uuid).then(function(response) {
                    $stateParams.uuid = null;
                    $scope.selectState('/analysis/list')
                });
            }

            $scope.removeAnalysisGroup = function() {
                $modalStack.dismissAll();
                var opts = {
                    templateUrl: "app/main/analysis/views/remove-group-modal.html",
                    scope: $scope,
                    size: 'md'
                };
                var instance = $modal.open(opts);
            }

            $scope.editAnalysisGroup = function() {
                $scope.data.copiedAnalysisGroup = angular.copy($scope.data.analysisGroup);
                $modalStack.dismissAll();
                var opts = {
                    templateUrl: "app/main/analysis/views/edit-group-modal.html",
                    scope: $scope,
                    size: 'md'
                };
                var instance = $modal.open(opts);
            }

            $scope.save = function(analysisGroup) {
                AnalysisResultService.updateAnalysisGroup(analysisGroup.uuid, analysisGroup)
                    .then(function(analysisGroup) {
                        growl.info("Updated " + analysisGroup.displayName, {
                            title: 'Info'
                        });
                        if (!analysisGroup) return;
                        $scope.data.analysisGroup = analysisGroup.plain();
                        $scope.data.analysisVersions = $scope.data.analysisGroup.versions;
                        $modalStack.dismissAll();
                    });
            }

        }]);
