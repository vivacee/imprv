/*******************************************************************************
 * Copyright (c) 2015 Bosch Software Innovations GmbH. All rights reserved.
 ******************************************************************************/

'use strict';

var wordcloud = angular.module('improvidus-wordcloud', []);

wordcloud.controller('WordcloudCtrl', [
    '$rootScope',
    '$scope',
    '$http',
    '$controller',
    '$stateParams',
    'FIELD',
    'CurrentAnalysisGroupService',
    'AnalysisResultService',
    'usSpinnerService',
    'DisplayService',
    'DatasourceService',
    function ($rootScope, $scope, $http, $controller, $stateParams, FIELD, CurrentAnalysisGroupService, AnalysisResultService, usSpinnerService, DisplayService, DatasourceService) {

        angular.extend(this, $controller('FiltersCtrl', {
            $scope: $scope
        }));

        $scope.initAnalysis('wordcloud');

        var fill = d3.scale.category20b();

        var w = window.innerWidth, h = window.innerHeight;

        var max, fontSize, scale;

        var layout = d3.layout.cloud().timeInterval(Infinity).size([w, h]).fontSize(function (d) {
            return fontSize(+d.value);
        }).text(function (d) {
            return d.key;
        }).on("end", draw);

        function getOffset(el) {
            var _x = 0;
            var _y = 0;
            while (el && !isNaN(el.offsetLeft) && !isNaN(el.offsetTop)) {
                _x += el.offsetLeft - el.scrollLeft;
                _y += el.offsetTop - el.scrollTop;
                el = el.offsetParent;
            }
            return {
                top: _y,
                left: _x
            };
        }

        var svg = d3.select("#wordcloud").append("svg").attr("width", w).attr("height", h).attr(
            "height", h);

        var vis = svg.append("g").attr("transform", "translate(" + [w >> 1, h >> 1] + ")").attr(
            "height", h);

        window.onresize = function (event) {
            update();
        };

        function draw(data, bounds) {

            var min, max;

            // data.forEach(function(line) {
            // if (line.Cor) {
            // if (!min)
            // min = line.Cor;
            // else
            // min = line.Cor < min ? line.Cor : min;
            // if (!max)
            // max = line.Cor;
            // else
            // max = line.Cor > max ? line.Cor : max;
            // }
            // })
            //
            // if (min === max) {
            // growl.error("Correlations could not be calculated. Select a different
            // Datasource", {
            // title: "Invalid Data"
            // });
            // return;
            // }
            //
            // var rainbow = new Rainbow();
            // rainbow.setNumberRange(min, max);
            // rainbow.setSpectrum("#539A85", "#DCCF47", "#C46F50");

            var w = $("#wordcloud").width(), h = $("#wordcloud").height();

            svg.attr("width", w).attr("height", h);

            scale = bounds ? Math.min(w / Math.abs(bounds[1].x - w / 2), w
                    / Math.abs(bounds[0].x - w / 2), h / Math.abs(bounds[1].y - h / 2), h
                    / Math.abs(bounds[0].y - h / 2)) / 1.5 : 1;

            var text = vis.selectAll("text").data(data, function (d) {
                return d.text.toLowerCase();
            });
            text.transition().duration(1000).attr("transform", function (d) {
                return "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")";
            }).style("font-size", function (d) {
                return d.size + "px";
            });
            text.enter().append("text").attr("text-anchor", "middle").attr("transform", function (d) {
                return "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")";
            }).style("font-size", function (d) {
                return d.size + "px";
            }).style("opacity", 1).transition().duration(1000).style("opacity", 1);
            text.style("font-family", function (d) {
                return d.font;
            }).style("fill", function (d) {
                return fill(d.text.toLowerCase());
            }).text(function (d) {
                return d.text;
            });

            vis.transition().attr("transform",
                "translate(" + [w >> 1, h >> 1] + ")scale(" + scale + ")");
        }

        function update() {
            d3.select("#wordcloud").selectAll("svg").remove();
            svg = d3.select("#wordcloud").append("svg").attr("width", w).attr("height", h).attr(
                "height", h);
            vis = svg.append("g").attr("transform", "translate(" + [w >> 1, h >> 1] + ")").attr(
                "height", h);
            layout = d3.layout.cloud().timeInterval(Infinity).size([w, h]).fontSize(function (d) {
                return fontSize(+d.value);
            }).text(function (d) {
                return d.key;
            }).on("end", draw);
            layout.font('impact').spiral('archimedean').rotate(function () {
                return 0;
            }).random(function () {
                return 0.5;
            });
            var tags = $scope.words;
            fontSize = d3.scale['sqrt']().range([10, 100]);
            if (tags.length) {
                fontSize.domain([+tags[tags.length - 1].value || 1, +tags[0].value]);
            }

            tags = tags.splice(0, 100);
            layout.stop().words(tags).start();

            usSpinnerService.stop('spinner-notifications');
        }

        function draw2(words) {
            update();
            d3.select("#wordcloud").append("svg").attr("width", width).attr("height", height).append(
                "g").selectAll("text").data(words).enter().append("text").style("font-size",
                function (d) {
                    return d.size + "px";
                }).style("font-family", "Impact").style("fill", function (d, i) {
                return fill(i);
            }).attr("text-anchor", "middle").attr("transform", function (d) {
                return "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")";
            }).text(function (d) {
                return d.text;
            });
        }

        $scope.paintGraph = function () {
            var lines = $scope.dateFilteredLines;

            var words = {};

            for (var i = 1; i < lines.length; i++) {
                var lineSize = lines[i].length - 1;
                if (lines[i][lineSize] && lines[i][lineSize] !== "") {
                    if (!words[lines[i][lineSize]]) {
                        words[lines[i][lineSize]] = {
                            count: 0,
                            value: 0,
                            duration: 0
                        }
                    }
                    if ($scope.controls.yAxis === 'Count')
                        words[lines[i][lineSize]].value++;
                    else if ($scope.controls.yAxis === 'Duration (h)')
                        words[lines[i][lineSize]].value += parseFloat(lines[i][FIELD.Duration]);
                    else
                        words[lines[i][lineSize]].value += parseFloat(lines[i][FIELD.Amount]);
                }
            }

            delete words["null"];

            var wordsList = [];
            var channel = "value"

            var min, max;

            for (var key in words) {
                var word = words[key];
                if (!min || word[channel] < min) {
                    min = word[channel];
                }
                if (!max || word[channel] > max) {
                    max = word[channel];
                }
            }

            for (var key in words) {
                wordsList.push({
                    key: key,
                    value: 10 + 150 * (words[key][channel] / max)
                });
            }

            var xScale = d3.scale.linear().domain([0, d3.max(wordsList, function (d) {
                return d.value;
            })]).range([10, 100]);

            wordsList.sort(function (a, b) {
                return (b.value - a.value);
            });

            $scope.words = wordsList;
            update();

        }

        $scope.name = {
            display: "N/A",
            version: 99
        }
        $scope.name = DisplayService.getDisplay();

    }]);
