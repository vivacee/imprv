analysis.controller(
    'AnalysisCtrl',
    ['$scope', '$controller', '$http', '$location', '$log', '$timeout', 'growl',
        'AnalysisResultService', '$modal', '$modalStack', 'CurrentAnalysisGroupService', 'DisplayService', 'DeviationEntryService',
        function($scope, $controller, $http, $location, $log, $timeout, growl,
                 AnalysisResultService, $modal, $modalStack, CurrentAnalysisGroupService, DisplayService, DeviationEntryService) {

            $scope.data = {
                oeeEntries: [],
                newEntry: {},
                count: [1, 2, 3, 4]
            };

            $scope.buttons = {
                confirmed: false,
                isCollapsed: false
            };

            $scope.analysisGroupForm = {
                step: 1
            };

            angular.extend(this, $controller('DataFilterCtrl', {
                $scope: $scope
            }));

            $scope.getNumber = function(num) {
                return new Array(num);
            };

            $scope.tableState;
            $scope.refresh = function(flag) {
                $scope.getAnalysisGroups($scope.tableState, flag);
            }

            $scope.getAnalysisGroups = function(tableState, flags) {
                var options = $scope.getQueryOptions(tableState);
                options = $scope.prepareAnalysisQuery(options);
                AnalysisResultService.getAnalysisGroups(options).then(function(result) {
                    result.content.sort(function(a, b) {
                        return a.createdDate >
                            b.createdDate;
                    });
                    $scope.data.displayedAnalysisGroups = result.content;
                    tableState.pagination.numberOfPages = result.totalPages;
                });

            };

            $scope.addAnalysis = function() {
                $modalStack.dismissAll();
                var opts = {
                    templateUrl: "app/main/analysis/views/add.html",
                    scope: $scope,
                    size: 'md'
                };
                var instance = $modal.open(opts);
            }

            $scope.process = function() {
                var options = $scope.getQueryOptions($scope.tableState);
                options = $scope.prepareAnalysisQuery(options);
                if ($scope.data.newEntry.displayName == null) {
                    growl.error("Please enter a name for the analysis.", {
                        title: 'Error'
                    });
                    return;
                }
                if (options.filters.shifts.params.shifts.length == 0) {
                    growl.error("Please select at least one shift.", {
                        title: 'Error'
                    });
                    return;
                }
                if (options.filters.workCenters.params.workCenters.length == 0) {
                    growl.error("Please select at least one work center.", {
                        title: 'Error'
                    });
                    return;
                }
                options.displayName = $scope.data.newEntry.displayName;
                options.description = $scope.data.newEntry.description;
                DeviationEntryService.analyze(options).then(function(result) {
                    if (!result) {
                        $modalStack.dismissAll();
                        $scope.refresh(false);
                        return;
                    }
                    if (result.status == 'BAD_REQUEST') {
                        growl.error("There has been an error processing your request.", {
                            title: 'Error'
                        });
                    }
                    else {
                        growl.info("Entries have been sent for analysis.", {
                            title: 'Info'
                        });
                        $modalStack.dismissAll();
                        $scope.refresh(false);
                    }
                }, function(err) {
                    growl.error("There has been an error processing your request.", {
                        title: 'Error'
                    });
                });
            }

            $scope.prepareAnalysisQuery = function(options) {
                return {
                    start: options.start,
                    number: options.number,
                    filters: {
                        time: {
                            params: {
                                from: options.from,
                                to: options.to
                            }
                        },
                        workCenters: {
                            params: {
                                workCenters: options.workCenters
                            }
                        },
                        shifts: {
                            params: {
                                shifts: options.shifts
                            }
                        }
                    }
                }
            }

            $scope.showDetails = function(analysis) {
                CurrentAnalysisGroupService.setCurrentAnalysisGroup(analysis);
                $location.path("/analysis/" + analysis.uuid);
            }
        }]);
