/*******************************************************************************
 * Copyright (c) 2015 Bosch Software Innovations GmbH. All rights reserved.
 ******************************************************************************/

'use strict';

var correlations = angular.module('improvidus-correlations', []);

correlations.controller('CorrelationsCtrl', [
    '$rootScope',
    '$scope',
    '$http',
    '$location',
    '$timeout',
    '$stateParams',
    'growl',
    'AnalysisResultService',
    'CurrentAnalysisGroupService',
    'DatasourceService',
    'DisplayService',
    function ($rootScope, $scope, $http, $location, $timeout, $stateParams, growl, AnalysisResultService, CurrentAnalysisGroupService, DatasourceService, DisplayService) {

        $scope.name = {};

        $scope.data = {};

        $scope.filters = {
            enableAll: true
        }

        $scope.$watch('filters.enableAll', function (newValue) {
            if ($scope.datasource) {
                $scope.datasource.forEach(function (event) {
                    event.selected = newValue;
                })
            }
        })

        $scope.flag = true;

        $scope.init = function (lines) {

            $scope.lines = lines;

            var map = {}
            var datasource = [];

            lines.forEach(function (line) {
                if (!map[line.Event1]) {
                    map[line.Event1] = 1;
                } else {
                    map[line.Event1] = map[line.Event1] + 1
                }
            })

            var length;
            var allTheSame = true;
            for (var i in map) {
                if (!length)
                    length = map[i];
                else {
                    if (map[i] !== length) {
                        allTheSame = false;
                        break;
                    }
                }
            }

            var datasource = [];
            var currentEvent;
            var dataPoint;
            for (var i = 0; i < lines.length; i++) {
                if (lines[i].Event1 !== currentEvent) {
                    if (currentEvent) {
                        dataPoint.selected = true;
                        datasource.push(dataPoint);
                    }
                    currentEvent = lines[i].Event1;
                    dataPoint = {
                        Event1: currentEvent
                    }
                }
                dataPoint['Event2-' + i % length] = lines[i].Event2;
                dataPoint['Cor-' + i % length] = lines[i].Cor;
                dataPoint['Lag-' + i % length] = lines[i].Lag;
            }
            dataPoint.selected = true;
            datasource.push(dataPoint);

            $scope.datasource = datasource;

            var min, max;

            lines.forEach(function (line) {
                if (line.Cor) {
                    if (!min)
                        min = line.Cor;
                    else
                        min = line.Cor < min ? line.Cor : min;
                    if (!max)
                        max = line.Cor;
                    else
                        max = line.Cor > max ? line.Cor : max;
                }
            })
            
            if (min === max) {
                growl.error("Correlations could not be calculated. Select a different Datasource", {
                    title: "Invalid Data"
                });
                return;
            }

            var colors = ["#FFFFFF", "#E7E7E7", "#CBCBCB", "#7070E4", "#0505FD"];

            var rainbow = new Rainbow();
            rainbow.setNumberRange(min, max);
            rainbow.setSpectrum("#FFFFFF", "#E7E7E7", "#CBCBCB", "#7070E4", "#0505FD");

            // now let's populate the source data with the colors based on the value
            // as well as replace the original value with 1
            for (i in datasource) {
                for (var h = 0; h <= length; h++) {
                    if (datasource[i]['Cor-' + h]) {
                        datasource[i]['Color-' + h] = '#' + rainbow.colourAt(datasource[i]['Cor-' + h]);
                        datasource[i]['Value-' + h] = datasource[i]['Event2-' + h];
                        datasource[i]['Event2-' + h] = 1;
                    }
                }
            }

            // define graph objects for each hour
            var graphs = [];
            for (var h = 0; h <= length; h++) {
                graphs.push({
                    "balloonFunction": function (item, graph) {
                        var result = graph.balloonText;
                        result = result.replace("[[Event1]]", item.category);
                        result = result.replace("[[Event2]]", item.dataContext[graph.eventField]);
                        result = result.replace("[[Cor]]", item.dataContext[graph.corField]);
                        result = result.replace("[[Lag]]", item.dataContext[graph.lagField]);
                        return result;
                    },
                    "balloonText": "<div style=\"text-align:left; font-size:13px\">"
                    + "Event1: <strong>[[Event1]]</strong><br />"
                    + "Event2: <strong>[[Event2]]</strong><br />"
                    + "Max Correlation (r): <strong>[[Cor]]</strong><br />"
                    + "Lag @ Max Correlation: <strong>[[Lag]] days</strong></div>",
                    "fillAlphas": 1,
                    "lineAlpha": 0,
                    "type": "column",
                    "colorField": "Color-" + h,
                    "valueField": "Event2-" + h,
                    "eventField": "Value-" + h,
                    "lagField": "Lag-" + h,
                    "corField": "Cor-" + h
                });
            }

            var chart = AmCharts.makeChart("chartdiv", {
                "type": "serial",
                "dataProvider": datasource,
                "valueAxes": [{
                    "stackType": "regular",
                    "axisAlpha": 0,
                    "gridAlpha": 0,
                    "autoGridCount": false,
                    "gridCount": length,
                    "labelsEnabled": false
                }],
                "graphs": graphs,
                "columnWidth": 1,
                "categoryField": "Event1",
                "categoryAxis": {
                    "gridPosition": "start",
                    "axisAlpha": 0,
                    "gridAlpha": 0,
                    "position": "left",
                    "labelsEnabled": false
                },
                "titles": [{
                    "size": 15,
                    "text": "Event Correlations",
                    "bold": false
                }, {
                    "text": "Positive lag means that Event1 happens after Event2, and vice versa"
                }]
            });

            chart.customLegend = document.getElementById('chartdiv');

            var width = 200, height = 40;

            d3.select('.heatmap-legend div').selectAll("svg").remove();
            var svg = d3.select('.heatmap-legend div').append('svg').attr('width', width).attr(
                'height', height);

            var title = svg.append('g').append('text').text("Max Correlations");

            var grad = svg.append('defs').append('linearGradient').attr('id', 'grad').attr('x1', '0%')
                .attr('x2', '100%').attr('y1', '0%').attr('y2', '0%');

            grad.selectAll('stop').data(colors).enter().append('stop').attr('offset', function (d, i) {
                return (i / colors.length) * 100 + '%';
            }).style('stop-color', function (d) {
                return d;
            }).style('stop-opacity', 0.9);

            svg.append('rect').attr('x', 0).attr('y', 0).attr('width', width)
                .attr('height', height / 2).attr('fill', 'url(#grad)');

            var g = svg.append('g').selectAll('.label').data(colors).enter();

            g.append('text').text(function (d, i) {
                if (i == 0) {
                    return min;
                } else if (i == colors.length - 1) {
                    return max;
                }
                return '';
            }).attr('transform', function (d, i) {
                return 'translate(' + (xPos(i) + 2) + ',' + ((height) - 7) + ')';
            })

            function xPos(i) {
                return (width / colors.length) * i;
            }

        }

        $scope.name = {
            display: "N/A",
            version: 99
        }
        $scope.name = DisplayService.getDisplay();

        $scope.initAnalysis = function () {
            $scope.data.analysisGroup = CurrentAnalysisGroupService.getCurrentAnalysisGroup();
            $scope.data.analysisType = 'correlations';

            if ($scope.data.analysisGroup == null) {
                AnalysisResultService.getAnalysisGroup($stateParams.uuid).then(function (analysisGroup) {
                    CurrentAnalysisGroupService.setCurrentAnalysisGroup(analysisGroup);
                    $scope.data.analysisGroup = analysisGroup;
                })
            }

            DatasourceService.getDatasourceMedia($stateParams.uuid, {
                type: 'correlations',
                version: $stateParams.version
            }).then(function (result) {
                $scope.init(result);
            });
        }
        $scope.initAnalysis('correlations');


        $scope.selectAnalysis = function (route, type) {
            var version = $location.$$search.version;
            var uuid = $location.$$search.uuid;
            $location.path(route).search("uuid", uuid).search("type", type).search("version", version);
        }

    }]);
