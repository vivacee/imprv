analysis.config(["$stateProvider", function ($stateProvider) {

    $stateProvider.state('Analysis', {
        url: "/analysis",
        reloadOnSearch: false,
        templateUrl: "app/main/analysis/views/analysis.html",
        redirectTo: 'Analysis.List'
    })

    $stateProvider.state('Analysis.List', {
        url: "/list",
        controller: "AnalysisCtrl",
        reloadOnSearch: false,
        templateUrl: "app/main/analysis/views/list.html",
        data: {
            permissions: {
                only: ['ANALYSIS_ACCESS'],
                redirectTo: 'OEE'
            }
        }
    })

    $stateProvider.state('Analysis.Details', {
        url: "/:uuid",
        controller: "AnalysisDetailsCtrl",
        reloadOnSearch: false,
        templateUrl: "app/main/analysis/views/details.html",
        data: {
            permissions: {
                only: ['ANALYSIS_ACCESS'],
                redirectTo: 'OEE'
            }
        }
    })

    $stateProvider.state('Analysis.Multimetric', {
        url: "/:uuid/mmparetos/:version",
        templateUrl: "app/main/analysis/multimetric/views/multimetric.html",
        controller: "MultimetricCtrl",
        reloadOnSearch: false,
        data: {
            permissions: {
                only: ['ANALYSIS_ACCESS'],
                redirectTo: 'OEE'
            }
        }
    }).state('Analysis.Wordcloud', {
        url: "/:uuid/wordcloud/:version",
        templateUrl: "app/main/analysis/wordcloud/views/wordcloud.html",
        controller: "WordcloudCtrl",
        reloadOnSearch: false,
        data: {
            permissions: {
                only: ['ANALYSIS_ACCESS'],
                redirectTo: 'OEE'
            }
        }
    }).state('Analysis.Correlations', {
        url: "/:uuid/correlations/:version",
        templateUrl: "app/main/analysis/correlations/views/correlations.html",
        controller: "CorrelationsCtrl",
        reloadOnSearch: false,
        data: {
            permissions: {
                only: ['ANALYSIS_ACCESS'],
                redirectTo: 'OEE'
            }
        }
    }).state('Analysis.Recommendations', {
        url: "/:uuid/recommendations/:version",
        templateUrl: "app/main/analysis/recommendations/views/recommendations.html",
        controller: "RecommendationsCtrl",
        reloadOnSearch: false,
        data: {
            permissions: {
                only: ['ANALYSIS_ACCESS'],
                redirectTo: 'OEE'
            }
        }
    });

}]);