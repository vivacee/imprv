/*******************************************************************************
 * Copyright (c) 2015 Bosch Software Innovations GmbH. All rights reserved.
 ******************************************************************************/

'use strict';

var multimetric = angular.module('improvidus-filters', []);

multimetric
    .constant('FIELD', {
        Date: 0,
        Time: 1,
        Shift: 2,
        Plant: 3,
        CostCenter: 4,
        WorkCenter: 5,
        Deviation1Level: 6,
        Deviation2Level: 7,
        Deviation3Level: 8,
        Deviation4Level: 9,
        Duration: 10,
        Amount: 11,
        Partnumber: 12,
        Comment: 13
    })
    .controller(
        'FiltersCtrl',
        [
            '$q',
            '$rootScope',
            '$scope',
            '$http',
            '$timeout',
            '$location',
            '$stateParams',
            'AnalysisResultService',
            'CurrentAnalysisGroupService',
            'DatasourceService',
            'DeviationEntryService',
            'DisplayService',
            'usSpinnerService',
            function ($q, $rootScope, $scope, $http, $timeout, $location, $stateParams, AnalysisResultService, CurrentAnalysisGroupService, DatasourceService, DeviationEntryService,
                      DisplayService, usSpinnerService) {

                // if ($stateParams.uuid)
                //     $location.search("uuid", $location.$$search.uuid);

                $scope.flag = false;
                // $scope.$on('datasource.selected', function (event, datasource) {
                //     if (!$scope.flag && datasource) {
                //         $scope.load(datasource);
                //         $scope.flag = true;
                //     } else {
                //         $scope.flag = false;
                //     }
                // })

                $scope.name = {};
                //
                // $scope.load = function (datasource) {
                //     DatasourceService.getDatasourceMedia(datasource.uuid, {
                //         type: "mmparetos"
                //     }).then(
                //         function (data) {
                //             $scope.rawData = data.slice(1, data.length);
                //             $scope.date.date = {
                //                 startDate: moment($scope.rawData[0][0] + " " + $scope.rawData[0][1],
                //                     "M/D/YYYY h:mm:ss a", true),
                //                 endDate: moment()
                //             }
                //             $scope.init($scope.rawData);
                //         }, function (err) {
                //             usSpinnerService.stop('spinner-notifications');
                //         })
                // }

                $scope.controls = {};
                $scope.chartOptions = {};

                var FIELD = {
                    Date: 0,
                    Time: 1,
                    Shift: 2,
                    Plant: 3,
                    CostCenter: 4,
                    WorkCenter: 5,
                    Deviation1Level: 6,
                    Deviation2Level: 7,
                    Deviation3Level: 8,
                    Deviation4Level: 9,
                    Duration: 10,
                    Amount: 11,
                    Partnumber: 12,
                    Comment: 13
                }

                $scope.data = {};
                $scope.data.deviationFilters = [{
                    displayName: "Deviation Level 1",
                    filterField: FIELD.Deviation1Level,
                    data: [],
                    options: [],
                    activeFilter: ''
                }, {
                    displayName: "Deviation Level 2",
                    filterField: FIELD.Deviation2Level,
                    data: [],
                    options: [],
                    activeFilter: ''
                }, {
                    displayName: "Deviation Level 3",
                    filterField: FIELD.Deviation3Level,
                    data: [],
                    options: [],
                    activeFilter: ''
                }, {
                    displayName: "Deviation Level 4",
                    filterField: FIELD.Deviation4Level,
                    data: [],
                    options: [],
                    activeFilter: ''
                }, {
                    data: []
                }]

                $scope.data.companyFilters = [{
                    displayName: "Plant",
                    filterField: FIELD.Plant,
                    data: [],
                    options: [],
                    activeFilter: ''
                }, {
                    displayName: "Cost Center",
                    filterField: FIELD.CostCenter,
                    data: [],
                    options: [],
                    activeFilter: ''
                }, {
                    displayName: "Work Center",
                    filterField: FIELD.WorkCenter,
                    data: [],
                    options: [],
                    activeFilters: ''
                }, {
                    data: []
                }]

                $scope.data.xAxis = ["Deviation", "Shift", "Partnumber", "Comment"]
                $scope.controls.xAxis = $scope.data.xAxis[0];

                $scope.$watch('controls.xAxis', function (newObj, oldObj) {
                    if (!newObj) return;
                    switch (newObj) {
                        case "Deviation":
                            $scope.chartOptions.xAxis = "Deviation";
                            break;
                        case "Shift":
                            $scope.chartOptions.xAxis = FIELD.Shift
                            break;
                        case "Partnumber":
                            $scope.chartOptions.xAxis = FIELD.Partnumber
                            break;
                        case "Comment":
                            $scope.chartOptions.xAxis = FIELD.Comment
                            break;
                        default:
                            $scope.chartOptions.xAxis = newObj;
                    }
                    if (newObj !== oldObj) $scope.paintGraph();
                }, true);

                $scope.data.yAxis = ["Amount ($)", "Duration (h)", "Count"]
                $scope.controls.yAxis = $scope.data.yAxis[0];

                $scope.$watch('controls.yAxis', function (newObj, oldObj) {
                    if (!newObj) return;
                    switch (newObj) {
                        case "Duration (h)":
                            $scope.chartOptions.yAxis = FIELD.Duration;
                            break;
                        case "Count":
                            $scope.chartOptions.yAxis = FIELD.Count
                            break;
                        default:
                            $scope.chartOptions.yAxis = FIELD.Amount
                    }
                    if (newObj !== oldObj) $scope.paintGraph();
                }, true);

                $scope.data.colorParameters = ["Year", "Month", "Month Year", "Week", "Weekday",
                    "Shift", "Plant", "Cost Center", "Work Center", "Deviation 1st Level",
                    "Deviation 2nd Level", "Deviation 3rd Level", "Deviation 4th Level",
                    "Partnumber", "Comment", "None"]
                $scope.$watch('controls.colorParameter', function (newObj, oldObj) {
                    switch (newObj) {
                        case "Shift":
                            $scope.chartOptions.colorParameter = FIELD.Shift;
                            break;
                        case "Plant":
                            $scope.chartOptions.colorParameter = FIELD.Shift;
                            break;
                        case "Cost Center":
                            $scope.chartOptions.colorParameter = FIELD.CostCenter;
                            break;
                        case "Deviation 1st Level":
                            $scope.chartOptions.colorParameter = FIELD.Deviation1Level;
                            break;
                        case "Deviation 2nd Level":
                            $scope.chartOptions.colorParameter = FIELD.Deviation2Level;
                            break;
                        case "Deviation 3rd Level":
                            $scope.chartOptions.colorParameter = FIELD.Deviation3Level;
                            break;
                        case "Deviation 4th Level":
                            $scope.chartOptions.colorParameter = FIELD.Deviation4Level;
                            break;
                        case "Partnumber":
                            $scope.chartOptions.colorParameter = FIELD.Partnumber;
                            break;
                        case "Comment":
                            $scope.chartOptions.colorParameter = FIELD.Comment;
                            break;
                        default:
                            $scope.chartOptions.colorParameter = FIELD.WorkCenter
                    }
                    if (newObj) $scope.paintGraph();
                }, true);

                $scope.date = {
                    date: {
                        startDate: moment().subtract(2, 'years'),
                        endDate: moment()
                    },
                    dateOptions: {
                        opens: 'left',
                        ranges: {
                            'Today': [moment().startOf('day'), moment()],
                            'Last Week': [moment().subtract(7, 'days'), moment()],
                            'Last Month': [moment().subtract(1, 'months'), moment()],
                            'Last 3 Months': [moment().subtract(3, 'months'), moment()],
                            'Last 6 Months': [moment().subtract(6, 'months'), moment()]
                        },
                        timePicker: true,
                        timePickerIncrement: 1,
                        format: 'M/D/YY H:mm'
                    }
                };

                function mapEntryToLine(entry) {
                    if (!entry) return [];
                    var line = [];
                    var time = moment(entry.timestamp)
                    line[0] = time.format('M/D/YYYY');
                    line[1] = time.format('h:mm:ss a');
                    line[2] = entry.shift;
                    line[3] = entry.plant;
                    line[4] = entry.costCenter;
                    line[5] = entry.workCenter;
                    line[6] = entry.deviation1Level;
                    line[7] = entry.deviation2Level;
                    line[8] = entry.deviation3Level;
                    line[9] = entry.deviation4Level;
                    line[10] = entry.duration;
                    line[11] = entry.amount;
                    line[12] = entry.partNumber;
                    line[13] = entry.comments;
                    return line;
                }

                $scope.$watch('date.date', function (newObj, oldObj) {
                    if (oldObj && newObj) {
                        usSpinnerService.spin('spinner-notifications');
                        $timeout($scope.loadGraphWithDateFilter, 100);
                        // DeviationEntryService.getDeviationEntries({
                        //     from: $scope.date.date.startDate.valueOf(),
                        //     to: $scope.date.date.endDate.valueOf()
                        // }).then(function (entries) {
                        //     var lines = entries.content.map(function (entry) {
                        //         return mapEntryToLine(entry);
                        //     });
                        //     $scope.init(lines);
                        // });
                    }
                });


                $scope.getOptions = function (matrix, field) {
                    var options = {};
                    for (var i = 1; i < matrix.length; i++) {
                        if (!matrix[i][field]) continue;
                        if (options[matrix[i][field]]) continue;
                        options[matrix[i][field]] = true;
                    }
                    return Object.keys(options);
                }

                $scope.lines = [];

                var fromTime, toTime;

                $scope.selectDeviationOption = function (index, option) {
                    usSpinnerService.spin('spinner-notifications');
                    for (var i = index + 1; i < $scope.data.deviationFilters.length; i++) {
                        $scope.data.deviationFilters[i].data = [];
                        $scope.data.deviationFilters[i].options = [];
                        $scope.data.deviationFilters[i].activeFilter = '';
                    }
                    if (index < $scope.data.deviationFilters.length) {
                        $scope.data.deviationFilters[index + 1].data = $.grep(
                            $scope.data.deviationFilters[index].data, function (line) {
                                var text = line[$scope.data.deviationFilters[index].filterField];
                                return option ? text === option : true;
                            })
                        $scope.data.deviationFilters[index + 1].options = $scope.getOptions(
                            $scope.data.deviationFilters[index + 1].data,
                            $scope.data.deviationFilters[index + 1].filterField);
                    }
                    if (option)
                        $scope.data.deviationFilters[index].activeFilter = option;
                    else
                        $scope.data.deviationFilters[index].activeFilter = $scope.data.deviationFilters[index].displayName;
                    $scope.currentDeviationFiltered = $scope.data.deviationFilters[index + 1].data;
                }

                $scope.loadByDeviation = function (index) {
                    $scope.data.xAxisLevel = $scope.data.deviationFilters[index].filterField;
                    $scope.loadGraph($scope.data.deviationFilters[index].data);
                }

                _.intersectionObjects = _.intersect = function (array) {
                    var slice = Array.prototype.slice;
                    var rest = slice.call(arguments, 1);
                    return _.filter(_.uniq(array), function (item) {
                        return _.every(rest, function (other) {
                            return _.any(other, function (element) {
                                return _.isEqual(element, item);
                            });
                        });
                    });
                };

                $scope.selectCompanyOption = function (index, option) {
                    usSpinnerService.spin('spinner-notifications');
                    for (var i = index + 1; i < $scope.data.companyFilters.length; i++) {
                        $scope.data.companyFilters[i].data = [];
                        $scope.data.companyFilters[i].options = [];
                        $scope.data.companyFilters[i].activeFilter = '';
                    }
                    if (index < $scope.data.companyFilters.length - 1) {
                        $scope.data.companyFilters[index + 1].data = $.grep(
                            $scope.data.companyFilters[index].data, function (line) {
                                var text = line[$scope.data.companyFilters[index].filterField];
                                return option ? text === option : true;
                            });
                        $scope.data.companyFilters[index + 1].options = $scope.getOptions(
                            $scope.data.companyFilters[index + 1].data,
                            $scope.data.companyFilters[index + 1].filterField);
                    }
                    if (option)
                        $scope.data.companyFilters[index].activeFilter = option;
                    else
                        $scope.data.companyFilters[index].activeFilter = $scope.data.companyFilters[index].displayName;
                    $scope.currentCompanyFiltered = $scope.data.companyFilters[index + 1].data;
                }

                $scope.$watch('currentDeviationFiltered', function (newObj) {
                    if ($scope.currentDeviationFiltered) {
                        setTimeout($scope.loadGraphWithFilters, 100);
                    }
                });
                $scope.$watch('currentCompanyFiltered', function (newObj) {
                    if ($scope.currentDeviationFiltered) {
                        setTimeout($scope.loadGraphWithFilters, 100);
                    }
                });

                $scope.isActive = function (activeOption, options) {
                    return options.indexOf(activeOption) > -1;
                }

                $scope.init = function (lines) {
                    if (!lines || lines.length == 0) return;
                    $scope.data.companyFilters[0].data = lines;
                    $scope.data.companyFilters[0].options = $scope.getOptions(lines, FIELD.Plant);

                    $scope.data.deviationFilters[0].data = lines;
                    $scope.data.deviationFilters[0].options = $scope.getOptions(lines,
                        FIELD.Deviation1Level);
                    $scope.loadGraph(lines);
                }

                $scope.loadGraph = function (lines) {
                    if (!lines || lines.length == 0) return;
                    $scope.currentLines = lines;
                    $scope.loadGraphWithFilters();
                }

                $scope.loadGraphWithFilters = function () {
                    $scope.filteredLines = $scope.currentDeviationFiltered || [];

                    if ($scope.currentDeviationFiltered && !$scope.currentCompanyFiltered) {
                        $scope.filteredLines = $scope.currentDeviationFiltered
                    } else if (!$scope.currentDeviationFiltered && $scope.currentCompanyFiltered) {
                        $scope.filteredLines = $scope.currentCompanyFiltered;
                    } else if (!$scope.currentDeviationFiltered && !$scope.currentCompanyFiltered) {
                        $scope.filteredLines = $scope.currentLines;
                    }

                    $scope.filteredLines = $
                        .grep(
                            $scope.filteredLines,
                            function (line) {
                                for (var i = 0; i < $scope.data.companyFilters.length - 1; i++) {
                                    if ($scope.data.companyFilters[i].activeFilter
                                        && $scope.data.companyFilters[i].displayName !== $scope.data.companyFilters[i].activeFilter
                                        && $scope.data.companyFilters[i].activeFilter !== line[$scope.data.companyFilters[i].filterField])
                                        return false;
                                }
                                return true;
                            });

                    // if ($scope.currentDeviationFiltered &&
                    // !$scope.currentCompanyFiltered) {
                    // $scope.filteredLines = $scope.currentDeviationFiltered
                    // } else if (!$scope.currentDeviationFiltered &&
                    // $scope.currentCompanyFiltered)
                    // {
                    // $scope.filteredLines = $scope.currentCompanyFiltered;
                    // } else if (!$scope.currentDeviationFiltered &&
                    // !$scope.currentCompanyFiltered) {
                    // $scope.filteredLines = $scope.currentLines;
                    // } else {
                    // $scope.filteredLines =
                    // _.intersectionObjects($scope.currentDeviationFiltered,
                    // $scope.currentCompanyFiltered);
                    // }
                    $scope.loadGraphWithDateFilter();
                }

                $scope.loadGraphWithDateFilter = function () {
                    if (!$scope.filteredLines) return;
                    $scope.dateFilteredLines = $.grep($scope.filteredLines, function (line) {
                        var date = moment(line[0] + " " + line[1], "M/D/YYYY h:mm:ss a", true);
                        var startDate = $scope.date.date.startDate;
                        var endDate = $scope.date.date.endDate;
                        return (date.isBefore(endDate) && date.isAfter(startDate))
                            || date.isSame(startDate) || date.isSame(endDate);
                    });

                    $scope.paintGraph();
                }

                $scope.initAnalysis = function (type) {
                    $scope.data.analysisGroup = CurrentAnalysisGroupService.getCurrentAnalysisGroup();
                    $scope.data.analysisType = type;

                    if ($scope.data.analysisGroup == null) {
                        AnalysisResultService.getAnalysisGroup($stateParams.uuid).then(function (analysisGroup) {
                            CurrentAnalysisGroupService.setCurrentAnalysisGroup(analysisGroup);
                            $scope.data.analysisGroup = analysisGroup;
                        })
                    }

                    DatasourceService.getDatasourceMedia($stateParams.uuid, {
                        type: type,
                        version: $stateParams.version
                    }).then(function (result) {
                        $scope.init(result);
                    });
                }

            }]);