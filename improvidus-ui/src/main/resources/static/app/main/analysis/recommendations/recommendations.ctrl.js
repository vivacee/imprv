/*******************************************************************************
 * Copyright (c) 2015 Bosch Software Innovations GmbH. All rights reserved.
 ******************************************************************************/

'use strict';

var recommendations = angular.module('improvidus-recommendations', []);

recommendations.controller('RecommendationsCtrl', [
    '$rootScope',
    '$scope',
    '$http',
    '$location',
    '$timeout',
    '$stateParams',
    'growl',
    'AnalysisResultService',
    'CurrentAnalysisGroupService',
    'DatasourceService',
    'usSpinnerService',
    'DisplayService',
    function($rootScope, $scope, $http, $location, $timeout, $stateParams, growl, AnalysisResultService, CurrentAnalysisGroupService, DatasourceService, usSpinnerService, DisplayService) {

        $scope.name = {};

        $scope.data = {};

        $scope.filters = {
            enableAll: true
        }

        var colors = ["#1F77B4", "#AEC7E8", "#FF7F0E", "#FFBB78", "#2CA02C", "#98DF8A", "#D62728",
            "#FF9896", "#9467BD", "#C5B0D5", "#8C564B", "#C49C94", "#E377C2", "#F7B6D2", "#7F7F7F",
            "#C7C7C7", "#BCBD22", "#DBDB8D", "#17BECF", "#9EDAE5"]

        $scope.$watch('filters.enableAll', function(newValue) {
            if ($scope.datasource) {
                $scope.datasource.forEach(function(event) {
                    event.selected = newValue;
                })
            }
        })

        $scope.flag = true;
        // $scope.$on('datasource.selected', function(event, datasource) {
        //   if ($scope.flag) {
        //     $scope.load(datasource);
        //     $scope.flag = false;
        //   } else {
        //     $scope.flag = true;
        //   }
        // })

        // $scope.load = function(datasource) {
        //   usSpinnerService.spin('spinner-notifications');
        //   DatasourceService.getDatasourceMedia(datasource.uuid, {
        //     type: "recommendations"
        //   }).then(function(data) {
        //     $scope.init(data);
        //   }, function(err) {
        //     usSpinnerService.stop('spinner-notifications');
        //   })
        // }

        // if ($rootScope.selections.datasource.uuid)
        //   $scope.load($rootScope.selections.datasource);
        // else
        //   $location.path('/datasources');

        $scope.options = {
            rank: 1
        }

        $scope.init = function(lines) {
            $("input[name='rank']").TouchSpin({
                min: 1,
                step: 1,
                max: lines[lines.length - 1].Rank
            });
            $scope.lines = lines;
            $scope.loadGraph();
        }

        $scope.$watch('options.rank', function(newValue) {
            if (!newValue || !$scope.lines) return;
            $scope.loadGraph();
        })

        AmCharts.exportCFG.menu[0].menu.splice(3, 1);

        $scope.loadGraph = function() {

            var datasource = $.grep($scope.lines, function(line) {
                return line.Rank === parseInt($scope.options.rank);
            });

            var chart = AmCharts.makeChart("chartdiv", {
                "type": "serial",
                "theme": "light",
                "dataProvider": datasource,
                "valueAxes": [{
                    "gridColor": "#FFFFFF",
                    "gridAlpha": 0.2,
                    "dashLength": 0,
                    "title": "Amount $",
                }],
                "gridAboveGraphs": true,
                "graphs": [{
                    "balloonText": datasource[0].X_Axis_Label + ": <b>[[category]]</b>"
                    + "<br/>Amount $: <b>[[value]]</b>",
                    "fillAlphas": 0.8,
                    "lineAlpha": 0.2,
                    "type": "column",
                    "fillColors": colors[($scope.options.rank - 1) % colors.length],
                    "valueField": "Frequency"
                }],
                "chartCursor": {
                    "categoryBalloonEnabled": false,
                    "cursorAlpha": 0,
                    "zoomable": false
                },
                "categoryField": "Category",
                "categoryAxis": {
                    "gridPosition": "start",
                    "gridAlpha": 0,
                    "tickPosition": "start",
                    "tickLength": 20,
                    "position": "left",
                    "labelRotation": 0,
                    "minHorizontalGap": 10,
                    "ignoreAxisWidth": true,
                    "autoWrap": true
                },
                "marginBottom": 75,
                "startDuration": 0,
                "titles": [
                    {
                        "text": "Pareto Rank: " + $scope.options.rank,
                        "size": 14
                    },
                    {
                        "text": "Filters: " + datasource[0].Filters,
                        "size": 14
                    },
                    {
                        "text": "% of Categories that cover 80% of the Data: "
                        + datasource[0].Score.toFixed(2),
                        "size": 14
                    }, {
                        "text": datasource[0].X_Axis_Label,
                        "size": 14,
                        "bold": false
                    }],
                "export": AmCharts.exportCFG

            });

            chart.addListener("dataUpdated", function() {
                usSpinnerService.stop('spinner-notifications');
            });

        }

        $scope.name = {
            display: "N/A",
            version: 99
        }
        $scope.name = DisplayService.getDisplay();

//      $scope.initAnalysis = function() {
//        if ($location.$$search.uuid && $location.$$search.type) {
//          DatasourceService.getDatasourceMedia($location.$$search.uuid, {
//            type: $location.$$search.type,
//            version: $location.$$search.version
//          }).then(function (result) {
//            $scope.init(result);
//          });
//          AnalysisResultService.getAnalysisResult($location.$$search.uuid).then(function(analysis) {
//        	$scope.name.display = analysis.displayName;
//          	$scope.name.version = $location.$$search.version;
//            DisplayService.saveDisplay($scope.name);
//            $scope.name.display = analysis.results[0].displayName;
//            $scope.name.version = $location.$$search.version;
//          })
//        }
//      }
//
//      $timeout($scope.initAnalysis, 100);

        $scope.initAnalysis = function() {
            $scope.data.analysisGroup = CurrentAnalysisGroupService.getCurrentAnalysisGroup();
            $scope.data.analysisType = 'recommendations';

            if ($scope.data.analysisGroup == null) {
                AnalysisResultService.getAnalysisGroup($stateParams.uuid).then(function(analysisGroup) {
                    CurrentAnalysisGroupService.setCurrentAnalysisGroup(analysisGroup);
                    $scope.data.analysisGroup = analysisGroup;
                })
            }

            DatasourceService.getDatasourceMedia($stateParams.uuid, {
                type: 'recommendations',
                version: $stateParams.version
            }).then(function(result) {
                $scope.init(result);
            });
        }
        $scope.initAnalysis();

        $scope.selectAnalysis = function(route, type) {
            var version = $location.$$search.version;
            var uuid = $location.$$search.uuid;
            $location.path(route).search("uuid", uuid).search("type", type).search("version", version);
        }

    }]);
