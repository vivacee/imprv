/*******************************************************************************
 * Copyright (c) 2015 Bosch Software Innovations GmbH. All rights reserved.
 ******************************************************************************/

'use strict';

var multimetric = angular.module('improvidus-multimetric', []);

multimetric
    .controller(
        'MultimetricCtrl',
        [
            '$rootScope',
            '$scope',
            '$http',
            '$controller',
            '$location',
            '$state',
            '$stateParams',
            'DatasourceService',
            'FIELD',
            'usSpinnerService',
            'DisplayService',
            function($rootScope, $scope, $http, $controller, $location, $state, $stateParams, DatasourceService, FIELD,
                     usSpinnerService, DisplayService) {

                angular.extend(this, $controller('FiltersCtrl', {
                    $scope: $scope
                }));

                $scope.initAnalysis('mmparetos');

                $scope.data.search = {
                    uuid: $location.$$search.uuid,
                    type: $location.$$search.type,
                    version: $location.$$search.version
                }

                setTimeout(function() {
                    if ($location.$$search.uuid)
                        $location.search("uuid", $scope.data.search.uuid);
                }, 1);


                function compare(a, b) {
                    if (a.deviation < b.deviation)
                        return -1;
                    if (a.deviation > b.deviation)
                        return 1;
                    return 0;
                }


                $scope.paintGraph = function() {
                    var lines = $scope.dateFilteredLines;

                    if (!lines) return;

                    var words = [];

                    var field = $scope.chartOptions.xAxis === 'Deviation'
                        ? ($scope.data.xAxisLevel || FIELD.Deviation1Level) : $scope.chartOptions.xAxis;
                    for (var i = 0; i < lines.length; i++) {
                        var lineSize = lines[i].length - 1;
                        words.push({
                            deviation: lines[i][field === FIELD.Comment ? lines[i].length - 1 : field],
                            color: lines[i][$scope.chartOptions.colorParameter],
                            value: parseFloat(lines[i][$scope.chartOptions.yAxis]) || 1
                        })
                    }

                    var wordsMap = {};

                    for (var i = 0; i < words.length; i++) {
                        if (!words[i].deviation) continue;
                        if (!wordsMap[words[i].deviation]) {
                            wordsMap[words[i].deviation] = {}
                            if (!words[i].color) continue;
                            if (!wordsMap[words[i].deviation][words[i].color]) {
                                wordsMap[words[i].deviation][words[i].color] = 0;
                            }
                            wordsMap[words[i].deviation][words[i].color] += words[i].value;
                        } else {
                            if (!words[i].color) continue;
                            if (!wordsMap[words[i].deviation][words[i].color]) {
                                wordsMap[words[i].deviation][words[i].color] = 0;
                            }
                            wordsMap[words[i].deviation][words[i].color] += words[i].value;
                        }
                    }

                    var dataProvider = [];
                    var graphs = [{
                        title: "All",
                        id: "all",
                        legendValueText: " ",
                        legendPeriodValueText: " "
                    }];

                    var series = {};

                    for (var category in wordsMap) {
                        var point = angular.copy(wordsMap[category]);
                        point.deviation = category;
                        dataProvider.push(point);
                        for (var serie in wordsMap[category]) {
                            if (!series[serie]) {
                                series[serie] = true
                                graphs
                                    .push({
                                        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
                                        "fillAlphas": 0.8,
                                        "lineAlpha": 0.3,
                                        "title": serie,
                                        "type": "column",
                                        "valueField": serie
                                    });
                            }
                        }
                    }

                    dataProvider.sort(compare);

                    AmCharts.exportCFG.menu[0].menu.splice(3, 1);

                    var chart = AmCharts.makeChart("chartdiv", {
                        "type": "serial",
                        "theme": "light",
                        "dataProvider": dataProvider,
                        "valueAxes": [{
                            "title": $scope.controls.yAxis,
                            "stackType": "regular",
                            "axisAlpha": 0.3,
                            "gridAlpha": 0
                        }],
                        "graphs": graphs,
                        "categoryField": "deviation",
                        "categoryAxis": {
                            "gridPosition": "start",
                            "gridAlpha": 0,
                            "tickPosition": "start",
                            "tickLength": 20,
                            "position": "left",
                            "labelRotation": 0,
                            "minHorizontalGap": 0,
                            "ignoreAxisWidth": true,
                            "autoWrap": true
                        },
                        "numberFormatter": {
                            "precision": 2,
                            "decimalSeparator": ",",
                            "thousandsSeparator": "."
                        },
                        "marginBottom": 75,
                        "startDuration": 0,
                        "export": AmCharts.exportCFG,
                        "colors": ["#1F77B4", "#AEC7E8", "#FF7F0E", "#FFBB78", "#2CA02C", "#98DF8A",
                            "#D62728", "#FF9896", "#9467BD", "#C5B0D5", "#8C564B", "#C49C94", "#E377C2",
                            "#F7B6D2", "#7F7F7F", "#C7C7C7", "#BCBD22", "#DBDB8D", "#17BECF", "#9EDAE5",
                            "#1F77B4", "#AEC7E8", "#FF7F0E", "#FFBB78", "#2CA02C", "#98DF8A", "#D62728",
                            "#FF9896", "#9467BD", "#C5B0D5", "#8C564B", "#C49C94", "#E377C2", "#F7B6D2",
                            "#7F7F7F", "#C7C7C7", "#BCBD22", "#DBDB8D", "#17BECF", "#9EDAE5", "#1F77B4",
                            "#AEC7E8", "#FF7F0E", "#FFBB78", "#2CA02C", "#98DF8A", "#D62728", "#FF9896",
                            "#9467BD", "#C5B0D5", "#8C564B", "#C49C94", "#E377C2", "#F7B6D2", "#7F7F7F",
                            "#C7C7C7", "#BCBD22", "#DBDB8D", "#17BECF", "#9EDAE5"]

                    });

                    function legendHandler(evt) {
                        var state = evt.dataItem.hidden;
                        if (evt.dataItem.id == 'all') {
                            for (var i1 in evt.chart.graphs) {
                                if (evt.chart.graphs[i1].id != 'all') {
                                    evt.chart[evt.dataItem.hidden ? 'hideGraph' : 'showGraph']
                                    (evt.chart.graphs[i1]);
                                }
                            }
                        }
                    }

                    chart.addListener("dataUpdated", function() {
                        usSpinnerService.stop('spinner-notifications');
                    });

                    var legend = new AmCharts.AmLegend();
                    chart.addLegend(legend, "legend");

                    chart.legend.addListener('hideItem', legendHandler);
                    chart.legend.addListener('showItem', legendHandler);

                    chart.validateData();

                }

                $scope.name = {
                    display: "N/A",
                    version: 99
                }
                $scope.name = DisplayService.getDisplay();

            }]);