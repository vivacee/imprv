analysis.factory('AnalysisResultService', ['RestangularV2', 'CachedRestangular', '$http',
    function AnalysisResultService(RestangularV2, CachedRestangular, $http) {
        return {
//            getAnalysisResults: function (options) {
//                if (!options) options = {};
//                if (!options.from) options.from = moment().subtract(2, 'y').valueOf();
//                if (!options.to) options.to = moment().valueOf();
//                return RestangularV2.one('deviationentries').one('analysisresults').get(options);
//            },
            getAnalysisGroups: function (options) {
                if (!options) options = {};
                if (!options.from) options.from = moment().subtract(2, 'y').valueOf();
                if (!options.to) options.to = moment().valueOf();
                return RestangularV2.one('deviationentries').one('analysisgroups').get(options);
            },
//            getAnalysisResult: function (uuid) {
//                return RestangularV2.one('deviationentries').one('analysisresults', uuid).get();
//            },
            getAnalysisGroup: function (uuid) {
                return RestangularV2.one('deviationentries').one('analysisgroups', uuid).get();
            },
            reanalyze: function (options) {
                if (!options) options = {};
//                if (!options.from) options.from = moment().subtract(2, 'y').valueOf();
//                if (!options.to) options.to = moment().valueOf();
//                return RestangularV2.one('deviationentries').one('analyze', uuid).get(options);
                return RestangularV2.one('deviationentries').one('analyze').customPOST(options, '', {}, {});
            },
//            deleteAnalysisResult: function (uuid,ver) {
//                return RestangularV2.one('deviationentries', ver).one('analysisresults', uuid).remove();
//            },
            updateAnalysisGroup: function (uuid, analysisGroup) {
                return RestangularV2.one('deviationentries').one('analysisgroups', uuid).customPUT(analysisGroup, '', {}, {});
            },
            deleteAnalysisGroup: function (uuid) {
                return RestangularV2.one('deviationentries').one('analysisgroups', uuid).remove();
            },
            deleteAnalysisResults: function (uuid) {
                return RestangularV2.one('deviationentries').one('analysisresults', uuid).remove();
            }

        };
    }]);

analysis.factory('CurrentAnalysisGroupService', [function CurrentAnalysisGroupService() {
    var currentAnalysisGroup = null;

    return {
        getCurrentAnalysisGroup: function () {
            return currentAnalysisGroup;
        },
        setCurrentAnalysisGroup: function (analysisGroup) {
            currentAnalysisGroup = analysisGroup;
        }
    };
}]);