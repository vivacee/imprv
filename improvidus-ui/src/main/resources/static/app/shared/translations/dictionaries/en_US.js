app
    .config([
        "$translateProvider",
        function($translateProvider) {
          $translateProvider
              .translations(
                  'en_US',
                  {
                    'ACTIVE': 'Active',
                    'ADD': 'Add',
                    'ADD_DEVICE_GROUP': 'Add Device Group',
                    'ADD_EVENT': 'Add Event',
                    'ADD_NEW_DASHBOARD': 'Add Dashboard',
                    'ADD_WIDGET': 'Add Widget',
                    'ADMIN': 'Admin',
                    'ALERT': 'Alert',
                    'ALERTS': 'Alerts',
                    'ALERT_ID': 'Alert ID',
                    'ARGUMENTS': 'Arguments',
                    'ATTACHMENTS': 'Attachments',
                    'AUTOMATIC': 'Automatic',
                    'BACK_TO_RESULTS': 'Back to Results',
                    'CANCEL': 'Cancel',
                    'CLEAR': 'Clear',
                    'CLOSE': 'Close',
                    'CLOSED': 'Closed',
                    'CODE': 'Code',
                    'COMMENTS': 'Comments',
                    'CONDITIONS': 'Conditions',
                    'CONFIRM': 'Confirm',
                    'CREATE_CONDITIONS': 'Create Conditions',
                    'CREATED_DATE': 'Created Time',
                    'CRON_EXPRESSION': 'Cron_Expression',
                    'DASHBOARD_EDIT_MODE_MESSAGE': 'Currently in edit mode. Save or undo your changes when you are done.',
                    'DAHSBOARDS': 'Dashboards',
                    'DATA_SET': 'Data Set',
                    'DATA_SETS': 'Data Sets',
                    'DATA_SET_KEY': 'Data Set Key',
                    'DATA_SET_TECHNICAL_NAME': 'Data Set Technical Name',
                    'DATA_SET_TEMPLATE': 'Data Set Template',
                    'DATE_RANGE': 'Date Range',
                    'DELETE': 'Delete',
                    'DETAILS': 'Details',
                    'DEVICE': 'Device',
                    'DEVICE_UUID': 'Device UUID',
                    'DEVICES': 'Devices',
                    'DEVICE_EXPLORER': 'Device Explorer',
                    'DEVICE_GROUP': 'Device Group',
                    'DEVICE_GROUPS': 'Device Groups',
                    'DEVICE_TEMPLATE': 'Device Template',
                    'DISPLAY_NAME': 'Display Name',
                    'EDIT': 'Edit',
                    'EDIT_DASHBOARD': 'Edit Dashboard',
                    'EVENT': 'Event',
                    'EVENTS': 'Events',
                    'EXPORT':'Export',
                    'EXPRESSION': 'Expression',
                    'FILE_GROUP_NAME': 'File Group Name',
                    'FILE_MANAGER': 'File Manager',
                    'FILES': 'Files',
                    'FLAT': 'Flat',
                    'FROM': 'From',
                    'GENERAL': 'General',
                    'HAS_BEEN_CREATED_FOR_DEVICE': 'has been created for Device',
                    'HAS_BEEN_UPDATED_FOR_DEVICE': 'has been updated for Device',
                    'HIGH': 'High',
                    'HOME': 'Home',
                    'IMPORT_EXPORT': 'Import/Export',
                    'INVALID_ACTION': 'Invalid Action',
                    'KEY': 'Key',
                    'LAST_MODIFIED': 'Last Modified',
                    'LATITUDE': 'Latitude',
                    'LOCATION': 'Location',
                    'LONGITUDE': 'Longitude',
                    'LOGOUT': 'Logout',
                    'MARK_AS_READ': 'Mark as Read',
                    'METADATA': 'Metadata',
                    'MODEL': 'Model',
                    'NAME': 'Name',
                    'NEXT': 'Next',
                    'NAVIGATION': 'Navigation',
                    'NO': 'No',
                    'NO_ATTACHMENTS_AVAILABLE': 'There are no File Groups',
                    'NO_ATTACHMENT_VERSIONS_AVAILABLE': 'There are no versions of the selected File Group',
                    'NO_DATA_AVAILABLE': 'There is no data available',
                    'NO_DATASETS_AVAILABLE': 'There are no Data Sets',
                    'NO_DEVICES_AVAILABLE': 'There are no Devices',
                    'NO_DEVICEGROUPS_AVAILABLE': 'There are no Device Groups',
                    'NO_EVENTS_AVAILABLE': 'There are no Events',
                    'NO_METADATA_AVAILABLE': 'There is no Metadata',
                    'NO_NOTIFICATIONS_AVAILABLE': 'There are no Notifications available',
                    'NOTIFICATIONS': 'Notifications',
                    'NOTIFICATION_DETAILS': 'Notification Details',
                    'ORGANIZATIONS': 'Organizations',
                    'PERMISSION': 'Permission',
                    'PERMISSIONS': 'Permissions',
                    'PERMISSION_TYPE': 'Permission Type',
                    'PLEASE_SELECT_MODULE': 'Please, select a Module',
                    'PLEASE_SELECT_A_NOTIFICATION': 'Please, select a Notification',
                    'PREVIOUS': 'Previous',
                    'REFRESH': 'Refresh',
                    'REMOVE': 'Remove',
                    'RENAME': 'Rename',
                    'RESOURCE': 'Resource',
                    'RESOURCE_MANAGEMENT': 'Resource Management',
                    'REVIEW': 'Review',
                    'ROLE': 'Role',
                    'ROLES': 'Roles',
                    'RULES': 'Rules',
                    'SAVE': 'Save',
                    'SCHEDULE': 'Schedule',
                    'SEARCH': 'Search',
                    'SELECT_A_DEVICEGROUP': 'Select a Device Group',
                    'SELECT_A_TEMPLATE': 'Select a Template',
                    'SELECT_A_CONDITION': 'Select Conditions',
                    'SELECT_DATE_RANGE': 'Select a Date Range',
                    'SELECT_DATE': 'Select a Date',
                    'SELECT_DATE_RANGE': 'Select a Date Range',
                    'SELECT_EVENT': 'Select Event',
                    'SELECT_PERMISSION': 'Select Permission',
                    'SELECT_STATUS': 'Select Status',
                    'SELECT_TOPIC_ID': 'Select Topic ID',
                    'SELECT_USER': 'Select User',
                    'SELECT_WEEK': 'Select a Week',
                    'SERIAL_NUMBER': 'Serial Number',
                    'SETTINGS': 'Settings',
                    'SEVERITY': 'Severity',
                    'SHOW_DETAILS': 'Show details',
                    'SHOW_PROPERTIES': 'Show properties',
                    'SYSTEM_RESOURCES': 'System Resources',
                    'STATUS': 'Status',
                    'SUBSCRIPTIONS': 'Subscriptions',
                    'SUCCESS': 'Sucess',
                    'TEMPLATE': 'Template',
                    'TECHNICAL_NAME': 'Technical Name',
                    'TIME': 'Time',
                    'TITLE': 'Title',
                    'TOPIC_ID': 'Topic ID',
                    'TREE': 'Tree',
                    'TRIGGER': 'Trigger',
                    'TRIGGERED': 'Triggered',
                    'TYPE': 'Type',
                    'TO': 'To',
                    'UNDO': 'Undo',
                    'UPLOAD': 'Upload',
                    'UPLOAD_SELECTED_FILES': 'Upload selected File',
                    'USER': 'User',
                    'USERS': 'Users',
                    'USER_GROUP': 'User Group',
                    'USER_GROUPS': 'User Groups',
                    'USER_RESOURCES': 'User Resources',
                    'VALUE': 'Value',
                    'VENDOR': 'Vendor',
                    'VERSION': 'Version',
                    'WIDGET_MODULE': 'Widget Module',
                    'WIDGET_MODULES': 'Widget Modules',
                    'WIDGET_MODULE_DETAILS': 'Widget Module Details',
                    'WIDGET_MODULE_FILES': 'Widget Module Files',
                    'YES': 'Yes',
                    'EMAIL':'Email',
                    'PHONE_NUMBER':'Phone Number'
                  });
        }]);