app.factory('TranslationService', ['Restangular',
    function TranslationService(Restangular) {

      var languages = [{
        name: 'en_US',
        countryName: 'United States',
        languageName: 'English',
        flagCode: 'us'
      }, {
        name: 'es_ES',
        countryName: 'España',
        languageName: 'Español',
        flagCode: 'es'
      }];

      return {
        getAvailableLocales: function() {
          return languages;
        }
      };

    }]);