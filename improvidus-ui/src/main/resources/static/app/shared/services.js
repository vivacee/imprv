/*******************************************************************************
 * Copyright (c) 2015 Bosch Software Innovations GmbH. All rights reserved.
 ******************************************************************************/
app.factory('Constants', ['growl', function Constants(growl) {
  var units = {
    'temperature': [{
      name: 'C',
      displayName: '°C',
      description: 'Celsius'
    }, {
      name: 'F',
      displayName: '°F',
      description: 'Farenheit'
    }],
    'relativeHumidity': [{
      name: '%RH',
      displayName: '%RH',
      description: 'Relative Humidity'
    }],
    'humidity': [{
      name: '%rh',
      displayName: '%RH',
      description: 'Relative Humidity'
    }],
    'airFlow': [{
      name: 'SCCM',
      displayName: 'SCCM',
      description: 'Standard Cubic Centimeters per Minute'
    }],
    'voltage': [{
      name: 'V',
      displayName: 'V',
      description: 'Voltage'
    }],
    'light': [{
      name: 'lux',
      displayName: 'Lux',
      description: 'Lumen'
    }],
    'pressure': [{
      name: 'hPa',
      displayName: 'hPa',
      description: 'hPascal'
    }]
  };

  var colors = [{
    displayName: 'Blue',
    name: 'blue',
    value: '#7CB5EC'
  }, {
    displayName: 'Black',
    name: 'black',
    value: '#434348'
  }, {
    displayName: 'Orange',
    name: 'orange',
    value: '#f7a35c'
  }, {
    displayName: 'Green',
    name: 'green',
    value: '#85E992'
  }, {
    displayName: 'Brown',
    name: 'brown',
    value: '#800a0a'
  }, {
    displayName: 'Yellow',
    name: 'yellow',
    value: '#E4D314'
  }];

  return {
    getAvailableUnits: function(topic) {
      var result = units[topic];
      if (!result || result.length == 0) {
        result = [];
        for ( var topic in units) {
          result = result.concat(units[topic]);
        }
      }
      return result;
    },
    getAvailableColors: function() {
      return colors;
    }
  };
}]);

app.factory('DateHelper', ['Restangular', function DateHelper(Restangular) {
  var timeZoneOffset = new Date().getTimezoneOffset() * 60 * 1000;

  var minuteMillis = 60000;
  var hourMillis = 60 * minuteMillis;
  var dayMillis = 24 * hourMillis;
  var weekMillis = 7 * dayMillis;

  return {
    getTimestamp: function(dataTs) {
      var dataTimestamp;
      if (typeof dataTs == 'string' || dataTs instanceof String) {
        dataTimestamp = moment(dataTs);
      } else {
        dataTimestamp = parseInt(dataTs);
      }
      return dataTimestamp;
    },
    getTimeZoneOffset: function() {
      return timeZoneOffset;
    },
    getDateFromUnix: function(unix, offset) {
      var a = moment(new Date(unix));
      return a.format("ddd, MMM Do YYYY, HH:mm:ss");
    },
    getShortDateFromUnix: function(unix) {
      var a = moment(new Date(unix));
      return a.format("MM-DD-YYYY HH:mm:ss");
    },
    isActive: function(from, to) {
      var current = new Date().getTime();
      if (current >= from && current <= to) { return true; }
    },
    getMillis: function(unit) {
      switch (unit) {
      case 'm':
        return minuteMillis;
      case 'h':
        return hourMillis;
      case 'd':
        return dayMillis;
      case 'w':
        return weekMillis;
      default:
        return 0;
      }
    }
  };
}]);

app.factory('FileService', ['Restangular', 'DeviceService',
    function FileUploadService(Restangular, DeviceService) {
      return {
        downloadFile: function(deviceUuid, fileName, version) {
          DeviceService.getAttachment(deviceUuid, version).then(function(result) {
            var blob = new Blob([result], {
              type: 'text/plain'
            });
            var url = window.URL || window.webkitURL;
            var link = document.createElementNS("http://www.w3.org/1999/xhtml", "a");
            link.href = url.createObjectURL(blob);
            link.download = fileName;

            var event = document.createEvent("MouseEvents");
            event.initEvent("click", true, false);
            link.dispatchEvent(event);
          });
        },
        exportResources: function(resources, filename) {
            var zip = new JSZip();
        	  var json = JSON.stringify(resources);
        	  zip.file(filename, json);
        	  var content = zip.generate();
        	  var link = document.createElement('a');
        	  link.download = "notifications.mer";
        	  link.href="data:application/zip;base64,"+content;
        	  link.click();
          }
      };
    }]);

app.factory('GrowlService', ['growl', function GrowlService(growl) {
  return {
    checkNotNullNorBlank: function(input, err, msg) {
      if (!input || input == null || input == '') {
        growl.warning(err, {
          title: msg
        });
        return true;
      } else {
        return false;
      }
    }
  };
}]);

app.factory('LocationService', [function LocationService() {
  return {
    formatLocation: function(lat, lng) {
      var convertLat = Math.abs(lat);
      var LatDeg = Math.floor(convertLat);
      var LatSec = ((convertLat - LatDeg) * 60).toFixed(3);
      var LatCardinal = ((lat > 0) ? "N" : "S");

      var convertLng = Math.abs(lng);
      var LngDeg = Math.floor(convertLng);
      var LngSec = ((convertLng - LngDeg) * 60).toFixed(3);
      var LngCardinal = ((lng > 0) ? "E" : "W");

      return LatDeg + "° " + LatSec + "' " + LatCardinal + ", " + LngDeg + "° " + LngSec + "' "
          + LngCardinal;
    }
  };
}]);

app.factory('UUIDGenerator', [
    'Restangular',
    function UUIDGenerator(Restangular) {
      return {
        guid: function() {
          var self = this;
          return self.s4() + self.s4() + '-' + self.s4() + '-' + self.s4() + '-' + self.s4() + '-'
              + self.s4() + self.s4() + self.s4();
        },
        s4: function() {
          return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
        }
      };
    }]);
