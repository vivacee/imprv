app.controller('multiSelectController', ['$scope', '$parse', '$filter', '$attrs', function MultiSelectController($scope, $parse, $filter, $attrs) {
    var propertyName = $attrs.multiSelectTable;
    var displayGetter = $parse(propertyName);
    var lastSelected;

    function copyRefs(src) {
        return src ? [].concat(src) : [];
    }

    function wipeSelection(rows) {
        rows.forEach(function(row) {
            row.isSelected = false;
        })
    }

    function areMultipleRowsSelected(rows) {
        return $.grep(rows, function(row) {
            return row.isSelected;
        }).length > 1;
    }

    this.selectSingle = function select(row) {
        var rows = copyRefs(displayGetter($scope));
        var index = rows.indexOf(row);
        if (index !== -1) {
            var selection = areMultipleRowsSelected(rows) ? true : row.isSelected !== true;
            wipeSelection(rows);
            row.isSelected = selection;
            lastSelected = row;
        }
    };

    this.ctrlSelect = function select(row) {
        var rows = copyRefs(displayGetter($scope));
        var index = rows.indexOf(row);
        if (index !== -1) {
            row.isSelected = row.isSelected !== true;
            lastSelected = row;
        }
    };

    this.shiftSelect = function select(row, ctrl) {
        var rows = copyRefs(displayGetter($scope));
        var index = rows.indexOf(row);
        lastSelected = lastSelected ? lastSelected : rows[0];
        var anchorIndex = rows.indexOf(lastSelected);

        if (index !== -1 && anchorIndex !== -1) {
            if (!ctrl)
                wipeSelection(rows);
            var min = index >= anchorIndex ? anchorIndex : index;
            var max = index >= anchorIndex ? index : anchorIndex;

            for (var i = min; i <= max; i++) {
                rows[i].isSelected = true;
            }
        }
    };
}]);


app.directive('multiSelectTable', [function() {
    return {
        controller: 'multiSelectController',
    }
}]);

app.directive('multiSelectRow', [function() {
    return {
        restrict: 'A',
        require: '^multiSelectTable',
        scope: {
            row: '=multiSelectRow'
        },
        link: function(scope, element, attr, ctrl) {
            element.bind('click', function(event) {
                if (event.shiftKey) {
                    scope.$apply(function() {
                        ctrl.shiftSelect(scope.row, event.ctrlKey);
                    });
                }
                else if (event.ctrlKey) {
                    scope.$apply(function() {
                        ctrl.ctrlSelect(scope.row);
                    });
                }
                else {
                    scope.$apply(function() {
                        ctrl.selectSingle(scope.row);
                    });
                }
            });

            scope.$watch('row.isSelected', function(newValue) {
                if (newValue === true) {
                    element.addClass('st-selected');
                } else {
                    element.removeClass('st-selected');
                }
            });
        }
    };
}]);