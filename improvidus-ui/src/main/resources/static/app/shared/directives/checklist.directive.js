app.directive('checkList', [function() {
    return {
        scope: {
            list: '=checkList',
            value: '@'
        },
        link: function(scope, elem, attrs) {
            var handler = function() {
                var checked = elem.prop('checked');
                var index = scope.list.indexOf(scope.value);
                if (index == -1)
                    index = scope.list.indexOf(parseInt(scope.value));

                if (checked && index == -1) {
                    scope.list.push(parseInt(scope.value));
                } else if (!checked && index != -1) {
                    scope.list.splice(index, 1);
                }
            };

            var changeHandler = handler.bind(null);

            elem.bind('change', function() {
                scope.$apply(changeHandler);
            });
            scope.$watch('list', changeHandler, true);
        }
    };
}]);