/**
 * Created by MED1IMB on 2/1/2017.
 */
app.directive("profilePicture", [function() {
    return {
        link: function(scope, element) {
            element.bind("change", function(changeEvent) {
                scope.fileread = changeEvent.target.files[0];
            });
        }
    }
}]);