app.directive("fileread", [function() {
    return {
        scope: {
            fileread: "="
        },
        link: function(scope, element) {
            element.bind("change", function(changeEvent) {
                scope.fileread = changeEvent.target.files[0];
            });
        }
    }
}]);