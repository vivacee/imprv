app.filter('customFilter', ['$parse', function($parse) {
  return function(items, filters) {
    var itemsLeft = items.slice();

    Object.keys(filters).forEach(function(model) {
      var value = filters[model], getter = $parse(model);

      itemsLeft = itemsLeft.filter(function(item) {
        return getter(item).toLowerCase().indexOf(value.toLowerCase()) === 0;
      });
    });

    return itemsLeft;
  };
}])

app.filter('splitcharacters', function() {
  return function(input, chars) {
    if (chars.length < 1) return input;
    if (chars[0] <= 0 || chars[0] <= 0) return '';
    if (input && input.length > (chars[0] + chars[1])) {
      var prefix = input.substring(0, chars[0] / 2);
      var postfix = input.substring(input.length - chars[1], input.length);
      return prefix + '...' + postfix;
    }
    return input;
  };
});

app.filter('words', function() {
  return function(input, words) {
    if (isNaN(words)) return input;
    if (words <= 0) return '';
    if (input) {
      var inputWords = input.split(/\s+/);
      if (inputWords.length > words) {
        input = inputWords.slice(0, words).join(' ') + '…';
      }
    }
    return input;
  };
});

app.filter('split', function() {
  return function(input, splitChar, splitIndex) {
    return input.split(splitChar)[splitIndex];
  };
});

app.filter('fieldArray', function() {
  return function(input, field) {
    var newArray = [];
    input.forEach(function(obj) {
      newArray.push(obj[field]);
    });
    return newArray;
  };
});

app.filter('keysSet', function() {
  return function(input) {
    return Object.keys(input);
  };
});

app.filter('lastPathRoute', function() {
  return function(input) {
    return input ? input.substr(input.lastIndexOf("/") + 1) : "";
  };
});

app.filter('characters', function() {
  return function(input, chars, breakOnWord) {
    if (isNaN(chars)) return input;
    if (chars <= 0) return '';
    if (input && input.length > chars) {
      input = input.substring(0, chars);

      if (!breakOnWord) {
        var lastspace = input.lastIndexOf(' ');
        // get last space
        if (lastspace !== -1) {
          input = input.substr(0, lastspace);
        }
      } else {
        while (input.charAt(input.length - 1) === ' ') {
          input = input.substr(0, input.length - 1);
        }
      }
      return input + '…';
    }
    return input;
  };
});

app.directive('stBooleanFilter', function() {
  return {
    restrict: 'E',
    require: '^stTable',
    template: '<input type="checkbox" ng-model="checked">',
    scope: true,
    link: function(scope, element, attr, ctrl) {
      var tableState = ctrl.tableState();
      scope.$watch('checked', function(value) {
        ctrl.search(value || false, 'active');
      })
    }
  };
});

app.filter('reverse', function() {
  return function(items) {
    return items.slice().reverse();
  };
});

app.filter('sliceLast', function() {
  return function(items) {
    return items.splice(0, items.length - 1);
  };
});

app.filter('unique', function() {
  return function(collection, keyname) {
    var output = [], keys = [];

    angular.forEach(collection, function(item) {
      var key = item[keyname];
      if (keys.indexOf(key) === -1) {
        keys.push(key);
        output.push(item);
      }
    });

    return output;
  };
});

app.filter('multipleSelectionFilter', function() {
  return function(allSelected) {
    var DGLength = allSelected.deviceGroups.length;
    var DLength = allSelected.devices.length;
    if (DGLength == 0 && DLength == 0) return "No Current Selection";
    if (DGLength == 1 && DLength == 0) return allSelected.deviceGroups[0].displayName;
    if (DGLength == 0 && DLength == 1) return allSelected.devices[0].displayName;
    if (DGLength > 0 && DLength > 0)
      return DGLength + " Device Groups + " + DLength + " Devices Selected";
    if (DGLength > 1 && DLength == 0) return DGLength + " Device Groups Selected";
    if (DGLength == 0 && DLength > 1) return DLength + " Devices Selected";
  };
});

app.filter('bytes', function() {
  return function(bytes, precision) {
    if (isNaN(parseFloat(bytes)) || !isFinite(bytes)) return '-';
    if (typeof precision === 'undefined') precision = 1;
    var units = ['bytes', 'kB', 'MB', 'GB', 'TB', 'PB'], number = Math.floor(Math.log(bytes)
        / Math.log(1024));
    return (bytes / Math.pow(1024, Math.floor(number))).toFixed(precision) + ' ' + units[number];
  }
});
