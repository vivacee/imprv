app.constant('Configuration', (function() {
    return {
        notificationsDefaultPeriod: {
            value: '14',
            units: 'days'
        }
    }
})());