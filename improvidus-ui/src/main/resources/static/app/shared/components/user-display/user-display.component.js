shared.component('userDisplay', {
    bindings: {
        userUuid: '='
    },
    template: '<user-media class="user-media" user="$ctrl.user"/>',
    controller: ['$rootScope', 'AdminUsersService', function($rootScope, AdminUsersService) {
        var that = this;
        AdminUsersService.getUser($rootScope.currentUser.organization.uuid, that.userUuid)
            .then(function(user) {
                that.user = user;
            });

    }]
});