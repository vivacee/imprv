/*******************************************************************************
 * Copyright (c) 2015 Bosch Software Innovations GmbH. All rights reserved.
 ******************************************************************************/
angular.module('app.controllers.links', []).controller(
    "LinksCtrl",
    [
        "$filter",
        "$location",
        "$rootScope",
        "$scope",
        "$state",
        "$stateParams",
        "$translate",
        "$http",
        "$interval",
        "$window",
        "growl",
        "Configuration",
        "Idle",
        "PermissionsService",
        "TranslationService",
        function($filter, $location, $rootScope, $scope, $state, $stateParams, $translate, $http,
                 $interval, $window, growl, Configuration, Idle,
                 PermissionsService, TranslationService) {

            /**
             * Links functionality
             */
            $scope.selectState = function(state, param1, param2, param3, param4, param5, param6) {
                if (!validate(state)) return;
                if (param5) {
                    $location.url($location.path());
                    $location.path(state).search(param1, param2).search(param3, param4).search(param5, param6);
                } else if (param3) {
                    $location.url($location.path());
                    $location.path(state).search(param1, param2).search(param3, param4);
                } else if (param1) {
                    $location.url($location.path());
                    $location.path(state).search(param1, param2);
                } else {
                    $location.url(state);
                }
            };

            var validate = function(state) {
                if (state === '/dashboards') {
                    if ($scope.dashboardEditMode) {
                        growl.warning("Please, save or discard the changes before changing dashboards", {
                            title: 'Invalid Action'
                        });
                        return false;
                    }
                } else {
                    $scope.dashboardEditMode = false;
                }
                return true;
            }

            $scope.$on('urlChange', function(event, msg) {
                $scope.selectState(msg.path, msg.key1, msg.value1, msg.key2, msg.value2);
            });

            $scope.getState = function() {
                return $location.$$path;
            }

            $scope.getUuid = function() {
                return $location.$$search.uuid;
            }

            $scope.getType = function() {
                return $location.$$search.type;
            }

            $scope.getClass = function() {
                return $location.$$search.clazz;
            }

            /**
             * User display
             */
            $scope.isAdmin = false;
            $scope.$watch('currentUser', function() {
                if ($rootScope.currentUser) {
                    $scope.username = $rootScope.currentUser.username;
                    $scope.isAdmin = $.inArray("ROLE_ADMIN", $rootScope.currentUser.authorities) > -1;
                }
            });

            $scope.$on('profilePictureLoaded', function() {
                $scope.$apply();
            })

            /**
             * Automatic logout
             */
            $scope.$on('IdleTimeout', function() {
                console.log("IdleTimeout");
                $window.location.href = 'logout';
            });

            /**
             * Language Options
             */
            var availableLanguages = TranslationService.getAvailableLocales();

            $scope.getLanguages = function() {
                return $.grep(availableLanguages, function(language) {
                    return language.name != $scope.activeLanguage.name;
                })
            }

            $scope.selectLanguage = function(language) {
                var previousLanguage = angular.copy($scope.activeLanguage);
                $scope.activeLanguage = language;
                $translate.use(language.name).then(function() {
                    if (previousLanguage) {
                        $window.sessionStorage.locale = language.name;
                        // $window.location.reload();
                    }
                });
            }

            var language = $window.navigator.userLanguage || $window.navigator.language;

            if ($window.sessionStorage.locale) language = $window.sessionStorage.locale;

            var languages = $.grep(availableLanguages, function(lang) {
                return lang.name.indexOf(language) === 0;
            })

            if (languages.length > 0) {
                $scope.selectLanguage(languages[0]);
            } else {
                $scope.selectLanguage(availableLanguages[0]);
            }

        }]);
