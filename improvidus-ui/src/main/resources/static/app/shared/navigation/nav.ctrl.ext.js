/*******************************************************************************
 * Copyright (c) 2015 Bosch Software Innovations GmbH. All rights reserved.
 ******************************************************************************/
angular.module('app.controllers.links').controller(
    "LinksExtCtrl",
    [
        "$rootScope",
        "$controller",
        "$filter",
        "$log",
        "$interval",
        "$scope",
        "$window",
        "$state",
        "growl",
        "ConfigurationService",
        "DatasourceService",
        "AuthService",
        "AdminUsersService",
        "AdminOrganizationsService",
        function($rootScope, $controller, $filter, $log, $interval, $scope, $window, $state, growl,
                 ConfigurationService, DatasourceService, AuthService, AdminUsersService, AdminOrganizationsService) {

            $scope.data = {};

            $rootScope.selections = {
                datasource: {}
            }

            $scope.logout = function() {
                AuthService.revokeToken();
                $rootScope.currentUser = null;
                $scope.selectState('/public');
            }

            $scope.$watch('user.configuration.content.selection', function(newObj, oldObj) {
                if (!newObj) return;
                $rootScope.selections.datasource = newObj;
            })

            $scope.$watch('selections.datasource', function(newObj, oldObj) {
                if (!newObj) return;
                $rootScope.$broadcast('datasource.selected', newObj);
                if (newObj.uuid && newObj.uuid !== oldObj.uuid) {
                    if (!$rootScope.user.configuration) $rootScope.user.configuration = {};
                    if (!$rootScope.user.configuration.content)
                        $rootScope.user.configuration.content = {};
                    $rootScope.user.configuration.content.selection = newObj;
                    ConfigurationService.updateConfiguration($scope.user.configuration,
                        $scope.currentUser.uuid).then(function(configuration) {
                        $rootScope.user.configuration = configuration;
                    });
                }
            })

            angular.extend(this, $controller('LinksCtrl', {
                $scope: $scope
            }));

            $scope.init = function() {
                // DatasourceService.getDatasources().then(function(datasources) {
                //     $scope.data.datasources = datasources;
                // })
            }

            $scope.$on('datasource.added', function(event, datasource) {
                $scope.data.datasources.push(datasource);
            })

            $scope.$on('datasource.removed', function(event, datasource) {
                $scope.data.datasources = $.grep($scope.data.datasources, function(item) {
                    return item.uuid !== datasource.uuid;
                })
                if (datasource.uuid === $rootScope.selections.datasource.uuid) {
                    $rootScope.selections.datasource = {};
                    $rootScope.user.configuration.content.selection = {};
                    ConfigurationService.updateConfiguration($scope.user.configuration,
                        $scope.currentUser.uuid).then(function(configuration) {
                        $rootScope.user.configuration = configuration;
                    });
                }
            })


            $scope.getBadgeClass = function(role) {
                if (!role) return;
                switch (role.name) {
                    case 'SuperAdmin':
                        return 'label-inverse';
                        break;
                    case 'Admin':
                        return 'label-default';
                        break;
                    case 'General Manager':
                    case 'GeneralManager':
                        return 'label-success';
                        break;
                    case 'Plant Manager':
                    case 'PlantManager':
                        return 'label-primary';
                        break;
                    case 'Production Manager':
                    case 'ProductionManager':
                        return 'label-warning';
                        break;
                    case 'Operator':
                        return 'label-info';
                        break;
                    default:
                        break;
                }
            };

            function getOrganizationConfiguration(uuid) {
                AdminOrganizationsService.getOrganizationConfiguration(uuid).then(function(orgConfiguration) {
                    if (orgConfiguration && orgConfiguration.conf.numShifts) {
                        $rootScope.org.configuration = orgConfiguration.plain();
                    } else {
                        $rootScope.org.configuration = {
                            organizationUuid: $rootScope.currentUser.organization.uuid,
                            conf: {
                                numShifts: 3
                            }
                        }
                    }
                })
            }

            $scope.selectOrganization = function(organization) {
                if (organization.uuid !== $rootScope.currentUser.organization.uuid) {
                    AdminUsersService.updateUserOrganization($rootScope.currentUser, organization.uuid).then(function(user) {
                        $rootScope.currentUser = user.plain();
                        getOrganizationConfiguration(user.organization.uuid)
                        if ($scope.getState().indexOf('/admin/plants/') > -1)
                            $scope.selectState('/admin/plants')
                        else if ($scope.getState().indexOf('/admin/schedules/') > -1)
                            $scope.selectState('/admin/schedules')
                        else if ($scope.getState().indexOf('/admin/organizations/') > -1)
                            $scope.selectState('/admin/organizations/' + $rootScope.currentUser.organization.uuid)
                        else
                            $state.reload(true);
                    });
                }
            };

            $scope.init();

        }]);
