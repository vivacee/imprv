'use strict';
angular.module('angular-international', []).value('countries', [{
  "name": "France",
  "alpha2": "FR",
  "alpha3": "FRA",
  "numeric": "250"
}, {
  "name": "Germany",
  "alpha2": "DE",
  "alpha3": "DEU",
  "numeric": "276"
}, {
  "name": "Italy",
  "alpha2": "IT",
  "alpha3": "ITA",
  "numeric": "380"
}, {
  "name": "Japan",
  "alpha2": "JP",
  "alpha3": "JPN",
  "numeric": "392"
}, {
  "name": "Spain",
  "alpha2": "ES",
  "alpha3": "ESP",
  "numeric": "724"
}, {
  "name": "United States of America",
  "alpha2": "US",
  "alpha3": "USA",
  "numeric": "840"
}, ]).value('languages', [{
  "code": "de",
  "displayName": "Deutsch"
}, {
  "code": "en",
  "displayName": "English"
}, {
  "code": "es",
  "displayName": "Español"
}, {
  "code": "fr",
  "displayName": "Français"
}, {
  "code": "it",
  "displayName": "Italiano"
}]);