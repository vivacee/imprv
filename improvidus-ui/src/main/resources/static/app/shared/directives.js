/*******************************************************************************
 * Copyright (c) 2015 Bosch Software Innovations GmbH. All rights reserved.
 ******************************************************************************/
app.directive('dashboardDraggable', function() {
  return {
    restrict: 'A',
    link: function(scope, element, attrs) {
      element.draggable({
        cursor: "move",
        handle: "div",
        stop: function(event, ui) {
          scope[attrs.xpos] = ui.position.left;
          scope[attrs.ypos] = ui.position.top;
          scope.$apply();
        }
      });
    }
  };
});

app
    .directive(
        'dropdownMultiselect',
        function() {
          return {
            restrict: 'E',
            scope: {
              model: '=',
              options: '=',
              pre_selected: '=preSelected'
            },
            template: "<div class='btn-group columns-selector' data-ng-class='{open: open}'>"
                + "<button class='btn btn-xs btn-default no-background'>Select Columns</button>"
                + "<button class='btn btn-xs btn-default no-background dropdown-toggle' data-toggle='dropdown'><span class='caret'></span></button>"
                + "<ul class='dropdown-menu' aria-labelledby='dropdownMenu'>"
                + "<li><a data-ng-click='selectAll()'><i class='icon-ok-sign'></i>  Check All</a></li>"
                + "<li><a data-ng-click='deselectAll();'><i class='icon-remove-sign'></i>  Uncheck All</a></li>"
                + "<li class='divider'></li>"
                + "<li data-ng-repeat='option in options'> <a data-ng-click='setSelectedItem()'>{{option.title}}<span data-ng-class='isChecked(option.field)'></span></a></li>"
                + "</ul>" + "</div>",
            controller: ["$scope", function($scope) {

              $scope.openDropdown = function() {
                $scope.selected_items = [];
                for (var i = 0; i < $scope.pre_selected.length; i++) {
                  $scope.selected_items.push($scope.pre_selected[i].field2);
                }
              };

              $scope.selectAll = function() {
                $scope.model = _.pluck($scope.options, 'field');
              };

              $scope.selectAll();

              $scope.deselectAll = function() {
                $scope.model = [];
              };

              $scope.setSelectedItem = function() {
                var field = this.option.field;
                if (_.contains($scope.model, field)) {
                  $scope.model = _.without($scope.model, field);
                } else {
                  $scope.model.push(field);
                }
                return false;
              };
              $scope.isChecked = function(field) {
                if (_.contains($scope.model, field)) { return 'glyphicon glyphicon-ok pull-right'; }
                return false;
              };
            }]
          };
        });

app.directive('fileModel', ['$parse', function($parse) {
  return {
    restrict: 'A',
    link: function(scope, element, attrs) {
      var model = $parse(attrs.fileModel);
      var modelSetter = model.assign;

      element.bind('change', function() {
        scope.$apply(function() {
          modelSetter(scope, element[0].files[0]);
        });
      });
    }
  };
}]);

app.service('fileUpload', ['$http', function($http) {
  this.uploadFileToUrl = function(file, uploadUrl) {
    var fd = new FormData();
    fd.append('file', file);
    $http.post(uploadUrl, fd, {
      transformRequest: angular.identity,
      headers: {
        'Content-Type': undefined
      }
    }).success(function() {
    }).error(function() {
    });
  }
}]);

app.directive('file', function() {
  return {
    scope: {
      file: '='
    },
    link: function(scope, el, attrs) {
      el.bind('change', function(event) {
        var files = event.target.files;
        var file = files[0];
        scope.file = file ? file : undefined;
        scope.$apply();
      });
    }
  };
});

app
    .directive(
        'pageSelect',
        function() {
          return {
            restrict: 'E',
            template: '<input type="text" class="select-page input-xs" ng-model="inputPage" ng-change="selectPage(inputPage)">',
            link: function(scope, element, attrs) {
              scope.$watch('currentPage', function(c) {
                scope.inputPage = c;
              });
            }
          }
        });

app.directive('dashboardDraggable', function() {
  return {
    restrict: 'A',
    link: function(scope, element, attrs) {
      element.draggable({
        cursor: "move",
        handle: "div",
        stop: function(event, ui) {
          scope[attrs.xpos] = ui.position.left;
          scope[attrs.ypos] = ui.position.top;
          scope.$apply();
        }
      });
    }
  };
});

app.directive('csSelect', function() {
  return {
    require: '^stTable',
    template: '<input type="checkbox"/>',
    scope: {
      row: '=csSelect'
    },
    link: function(scope, element, attr, ctrl) {

      element.bind('change', function(evt) {
        scope.$apply(function() {
          ctrl.select(scope.row, 'multiple');
        });
      });

      scope.$watch('row.isSelected', function(newValue, oldValue) {
        if (newValue === true) {
          element.parent().addClass('st-selected');
        } else {
          element.parent().removeClass('st-selected');
        }
      });
    }
  };
});

app.directive('stSelectAll', function() {
  return {
    restrict: 'E',
    template: '<input type="checkbox" ng-model="isAllSelected" />',
    scope: {
      all: '='
    },
    link: function(scope, element, attr) {
      scope.$watch('isAllSelected', function() {
        if (!scope.all) return;
        scope.all.forEach(function(val) {
          val.isSelected = scope.isAllSelected;
        });
      });
      scope.$watch('all', function(newVal, oldVal) {
        if (oldVal) {
          oldVal.forEach(function(val) {
            val.isSelected = false;
          });
        }
        scope.isAllSelected = false;
      });
    }
  }
});

app.directive('stSelection', function() {
  return {
    scope: {
      selection: '=stSelection',
      collection: '=stTable'
    },
    link: function(scope, element, attr) {
      scope.$watch('collection', function() {
        scope.selection = $.grep(scope.collection || [], function(item) {
          return item.isSelected;
        })
      }, true);
    }
  }
});

app.directive('compile', ['$compile', '$state', function($compile, $state) {
  return function(scope, element, attrs) {
    scope.loggMsg = function(uuid) {
      $state.go('Notifications', {
        uuid: uuid
      });
    };
    scope.$watch(function(scope) {
      // console.log(scope.$eval(attrs.compile).toString());
      return scope.$eval(attrs.compile).toString();
    }, function(value) {
      element.html(value);
      $compile(element.contents())(scope);
    })
  };
}]);

app.directive('bindHtmlCompile', ['$compile', function($compile) {
  return {
    restrict: 'A',
    link: function(scope, element, attrs) {
      scope.$watch(function() {
        return scope.$eval(attrs.bindHtmlCompile);
      }, function(value) {
        // In case value is a TrustedValueHolderType, sometimes it
        // needs to be explicitly called into a string in order to
        // get the HTML string.
        element.html(value && value.toString());
        // If scope is provided use it, otherwise use parent scope
        var compileScope = scope;
        if (attrs.bindHtmlScope) {
          compileScope = scope.$eval(attrs.bindHtmlScope);
        }
        $compile(element.contents())(compileScope);
      });

      scope.select = function() {
        console.log("Hello")
      }

    }
  };
}]);

app.directive('idWatch', [function() {
  return {
    scope: {
      id: '@',
    },
    controller: function($scope, $element, $attrs) {
      $attrs.$observe('id', function(id) {
        if ($scope.$parent.onElementIdChange) $scope.$parent.onElementIdChange($element);
      });
    }
  }
}]);

app.directive('tooltip', [function() {
  return {
    compile: function(scope, elem, attrs) {
      $(document).ready(function() {
        $(elem).tooltip();
      });
    }
  }
}]);

app.directive('ionslider', ['$timeout', function($timeout) {
  return {
    restrict: 'EA',
    require: '^ngModel',
    scope: {
      min: '=',
      max: '=',
      type: '@',
      prefix: '@',
      maxPostfix: '@',
      prettify: '=',
      grid: '@',
      gridMargin: '@',
      postfix: '@',
      step: '@',
      hideMinMax: '@',
      hideFromTo: '@',
      from: '=',
      disable: '=',
      onChange: '=',
      onFinish: '=',
      ngModel: '='
    },
    template: '<div></div>',
    replace: true,
    link: function(scope, element, attrs, ngModel) {
      (function init() {
        $(element).ionRangeSlider({
          min: scope.min,
          max: scope.max,
          type: scope.type,
          prefix: scope.prefix,
          maxPostfix: scope.maxPostfix,
          prettify: scope.prettify,
          grid: scope.grid,
          gridMargin: scope.gridMargin,
          postfix: scope.postfix,
          step: scope.step,
          hideMinMax: scope.hideMinMax,
          hideFromTo: scope.hideFromTo,
          from: scope.from,
          to: scope.to,
          disable: scope.disable,
          onChange: scope.onChange,
          onFinish: setSalaryRange
        });
      })();

      function setSalaryRange() {
        scope.$evalAsync(function() {
          var values = $(element).prop('value');
          ngModel.$setViewValue(values);
        });
      }

      scope.$watch('min', function(value) {
        $timeout(function() {
          $(element).data("ionRangeSlider").update({
            min: value
          });
        });
      }, true);
      scope.$watch('max', function(value) {
        $timeout(function() {
          $(element).data("ionRangeSlider").update({
            max: value
          });
        });
      });
      scope.$watch('from', function(value) {
        $timeout(function() {
          $(element).data("ionRangeSlider").update({
            from: value
          });
        });
      });
      scope.$watch('to', function(value) {
        $timeout(function() {
          $(element).data("ionRangeSlider").update({
            to: value
          });
        });
      });
      scope.$watch('disable', function(value) {
        $timeout(function() {
          $(element).data("ionRangeSlider").update({
            disable: value
          });
        });
      });
      ngModel.$render = function() {
        var options = {
          from: ngModel.$viewValue
        };
        $(element).data("ionRangeSlider").update(options);
      };
    }
  };
}]);

app.directive('ionslider2', [
    '$timeout',
    function($timeout) {
      return {
        restrict: 'EA',
        require: '^ngModel',
        scope: {
          min: '=',
          max: '=',
          type: '@',
          to: '=',
          prefix: '@',
          maxPostfix: '@',
          prettify: '=',
          grid: '@',
          gridMargin: '@',
          postfix: '@',
          step: '@',
          hideMinMax: '@',
          hideFromTo: '@',
          from: '=',
          disable: '=',
          onChange: '=',
          onFinish: '=',
          value: '=ngModel'

        },
        template: '<div></div>',
        replace: true,
        link: function(scope, element, attrs, modelCtrl) {
          var $slider;

          (function init() {
            $(element).ionRangeSlider({
              min: scope.min,
              max: scope.max,
              type: scope.type,
              prefix: scope.prefix,
              maxPostfix: scope.maxPostfix,
              prettify: scope.prettify,
              grid: scope.grid,
              gridMargin: scope.gridMargin,
              postfix: scope.postfix,
              step: scope.step,
              hideMinMax: scope.hideMinMax,
              hideFromTo: scope.hideFromTo,
              to: scope.to,
              from: scope.from,
              disable: scope.disable,
              onChange: scope.onChange,
              onFinish: setSalaryRange
            });
          })();

          function setSalaryRange() {
            scope.$evalAsync(function() {
              var values = (attrs.type == 'single') ? $(element).prop('value') : $(element).prop(
                  'value').split(';');
              modelCtrl.$setViewValue(values);
            });
          }

          scope.$watch('value', function(value) {
            if (attrs.type == 'single') {
              $(element).ionRangeSlider("update", {
                from: value
              });
            } else { // double
              if (value[0] == scope.max && value[0] > 0) {
                // avoid getting stuck with both from & to == max
                value[0]--;
              }
              $(element).ionRangeSlider("update", {
                from: value[0],
                to: value[1]
              });
            }
          }, true);

          scope.$watch('min', function(value) {
            $(element).ionRangeSlider("update", {
              min: value
            });
          }, true);
          scope.$watch('max', function(value) {
            $timeout(function() {
              $(element).ionRangeSlider("update", {
                max: value
              });
            });
          });
          scope.$watch('disable', function(value) {
            $timeout(function() {
              $(element).ionRangeSlider("update", {
                disable: value
              });
            });
          });
        }
      };
    }]);

app.directive('uiSwitch', ['$window', '$timeout', '$log', '$parse',
    function($window, $timeout, $log, $parse) {

      function linkSwitchery(scope, elem, attrs, ngModel) {
        if (!ngModel) return false;
        var options = {};
        try {
          options = $parse(attrs.uiSwitch)(scope);
        } catch (e) {
        }

        var switcher;

        attrs.$observe('disabled', function(value) {
          if (!switcher) { return; }

          if (value) {
            switcher.disable();
          } else {
            switcher.enable();
          }
        });

        function initializeSwitch() {
          $timeout(function() {
            // Remove any old switcher
            if (switcher) {
              angular.element(switcher.switcher).remove();
            }
            // (re)create switcher to reflect latest state of the checkbox
            // element
            switcher = new $window.Switchery(elem[0], options);
            var element = switcher.element;
            element.checked = scope.initValue;
            if (attrs.disabled) {
              switcher.disable();
            }

            switcher.setPosition(false);
            element.addEventListener('change', function(evt) {
              scope.$apply(function() {
                ngModel.$setViewValue(element.checked);
              })
            })

            scope.$watch("initValue", function(newValue) {
              ngModel.$setViewValue(newValue);
            });
          }, 0);
        }
        initializeSwitch();
      }

      return {
        require: 'ngModel',
        restrict: 'AE',
        scope: {
          initValue: '=ngModel'
        },
        link: linkSwitchery
      }
    }]);

app.directive('switch', function() {
  return {
    restrict: 'AE',
    replace: true,
    transclude: true,
    template: function(element, attrs) {
      var html = '';
      html += '<span';
      html += ' class="switch' + (attrs.class ? ' ' + attrs.class : '') + '"';
      html += attrs.ngModel ? ' ng-click="' + attrs.disabled + ' ? ' + attrs.ngModel + ' : '
          + attrs.ngModel + '=!' + attrs.ngModel
          + (attrs.ngChange ? '; ' + attrs.ngChange + '()"' : '"') : '';
      html += ' ng-class="{ checked:' + attrs.ngModel + ', disabled:' + attrs.disabled + ' }"';
      html += '>';
      html += '<small></small>';
      html += '<input type="checkbox"';
      html += attrs.id ? ' id="' + attrs.id + '"' : '';
      html += attrs.name ? ' name="' + attrs.name + '"' : '';
      html += attrs.ngModel ? ' ng-model="' + attrs.ngModel + '"' : '';
      html += ' style="display:none" />';
      html += '<span class="switch-text">';
      html += attrs.on ? '<span class="on">' + attrs.on + '</span>' : '';
      html += attrs.off ? '<span class="off">' + attrs.off + '</span>' : ' ';
      html += '</span>';
      return html;
    }
  }
});

app.directive('ngEnter', function() {
  return function(scope, element, attrs) {
    element.bind("keydown keypress", function(event) {
      if (attrs.validated) {
        if (event.which === 13 && scope.$eval(attrs.validated)) {
          scope.$apply(function() {
            scope.$eval(attrs.ngEnter);
          });

          event.preventDefault();
        }
      } else {
        if (event.which === 13) {
          scope.$apply(function() {
            scope.$eval(attrs.ngEnter);
          });

          event.preventDefault();
        }
      }
    });
  };
});

app.directive('ngLeft', function() {
  return function(scope, element, attrs) {
    element.bind("keydown keypress", function(event) {
      if (attrs.validated) {
        if (event.which === 37 && scope.$eval(attrs.validated)) {
          scope.$apply(function() {
            scope.$eval(attrs.ngLeft);
          });

          event.preventDefault();
        }
      } else {
        if (event.which === 37) {
          scope.$apply(function() {
            scope.$eval(attrs.ngLeft);
          });

          event.preventDefault();
        }
      }
    });
  };
});

app.directive('ngRight', function() {
  return function(scope, element, attrs) {
    element.bind("keydown keypress", function(event) {
      if (attrs.validated) {
        if (event.which === 39 && scope.$eval(attrs.validated)) {
          scope.$apply(function() {
            scope.$eval(attrs.ngRight);
          });

          event.preventDefault();
        }
      } else {
        if (event.which === 39) {
          scope.$apply(function() {
            scope.$eval(attrs.ngRight);
          });

          event.preventDefault();
        }
      }
    });
  };
});

app.directive('userImage', ["$http", 'Restangular', function($http, Restangular) {
    return {
        restrict: 'A',
        scope: {
            user: '='
        },
        link: function(scope, element, attrs) {
            function getUserPicture() {
                Restangular.one('media').withHttpConfig({responseType: 'blob', cache: true}).get({
                    'name': 'user-profile',
                    'ownerUuid': scope.user.uuid
                }).then(function(data) {
                    var fr = new FileReader();
                    fr.onload = function(e) {
                        if (!e.target.result || !e.target.result.split(',')[1])
                            element.attr('src', 'images/user.png');
                        else
                            element.attr('src', 'data:image/jpeg;base64,' + e.target.result.split(',')[1])
                    };
                    fr.readAsDataURL(data);
                }, function(error) {
                    element.attr('src', 'images/user.png');
                });
            }

            scope.$watch('user', function(newObj) {
                if (scope.user && scope.user.uuid) getUserPicture();
            })

        }
    };
}]);

app.directive('userMedia', [function() {
  return {
    restrict: 'E',
    scope: {
      user: "="
    },
    template: '<div class="media media-user media-xs"><a class="media-left" href="javascript:;">'
    + '<img user-image src="" user="user" alt="" class="media-object"></a>'
    + '<div class="media-body"><p class="media-heading">{{user.displayName}}</p>'
    + '</div></div>'
  }}]);

app.directive('dndMatchHeight', function() {
  function link(scope, element, attrs) {
    scope.$watch(function() {
      var maxItems = -1;
      var longestList = null;

      for (var i = 0; i < scope.models.length; ++i) {
        if (scope.models[i].items.length > maxItems) {
          maxItems = scope.models[i].items.length;
          longestList = scope.models[i];
        }
      }

      if (longestList.listName == element[0].attributes['dnd-match-height'].value) {
        for (var i = 0; i < scope.models.length; ++i) {
          if (scope.models[i].listName == longestList.listName)
            scope.models[i].style.height = null;
          else
            scope.models[i].style.height = element[0].offsetHeight;
        }
      }
    });
  }
  return {
    restrict: 'AE',
    link: link
  };
});

/*
 * ! ngclipboard - v1.1.1 - 2016-02-26
 * https://github.com/sachinchoolur/ngclipboard Copyright (c) 2016 Sachin;
 * Licensed MIT
 */
app.directive('ngclipboard', function() {
  return {
    restrict: 'A',
    scope: {
      ngclipboardSuccess: '&',
      ngclipboardError: '&'
    },
    link: function(scope, element) {
      var clipboard = new Clipboard(element[0]);

      clipboard.on('success', function(e) {
        scope.$apply(function() {
          scope.ngclipboardSuccess({
            e: e
          });
        });
      });

      clipboard.on('error', function(e) {
        scope.$apply(function() {
          scope.ngclipboardError({
            e: e
          });
        });
      });

    }
  };
});

app.directive('dragOverZone', function() {
  return {
    restrict: 'A',
    link: function(scope, element, attrs) {
      var mousex, mousey = [];
      var dropZoneCoordinates = {};

      var continueDragging = function(e) {
        if (mousex >= dropZoneCoordinates.left && mousex <= dropZoneCoordinates.right
            && mousey >= dropZoneCoordinates.top && mousey <= dropZoneCoordinates.bottom) {
          dropZoneCoordinates.dom.addClass(attrs.dropZoneClass);
        } else {
          dropZoneCoordinates.dom.removeClass(attrs.dropZoneClass);
        }

        mousex = e.pageX;
        mousey = e.pageY;
      }

      var endDragging = function(e) {
        $(document).unbind("mousemove", continueDragging);
        $(document).unbind("mouseup", endDragging);
        mousex = 0;
        mousey = 0;
        dropZoneCoordinates = {};
      }

      var startDragging = function(e) {
        var lefttop = $("#" + attrs.dragOverZone).offset();
        dropZoneCoordinates = {
          dom: $("#" + attrs.dragOverZone),
          left: lefttop.left,
          top: lefttop.top,
          right: lefttop.left + $("#" + attrs.dragOverZone).width(),
          bottom: lefttop.top + $("#" + attrs.dragOverZone).height()
        };

        if (e.type == "mousedown") {
          $(document).bind("mousemove", continueDragging);
          $(document).bind("mouseup", endDragging);
        }
      }

      element.bind("mousedown", startDragging);
    }
  }
});

app.directive('topMenuItem', function() {
  return {
    restrict: 'A',
    scope: true,
    link: function($scope, $element) {
      $scope.toggleDashboard = function(uuid) {
        if ($scope.getUuid() == uuid)
          $scope.selectState('/dashboards', 'uuid', $scope.dashboards[0].uuid);
        else
          $scope.selectState('/dashboards', 'uuid', uuid);
      }
      $scope.toggleState = function(defaultState, newState, param1, param2, param3, param4) {
        if ($scope.getState() == newState)
          $scope.selectState(defaultState, param1, param2, param3, param4);
        else
          $scope.selectState(newState, param1, param2, param3, param4);
      }
    }
  }
});