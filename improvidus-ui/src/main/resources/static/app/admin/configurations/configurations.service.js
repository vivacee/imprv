app.factory('ConfigurationService', [
    'RestangularV2',
    function ConfigurationService(RestangularV2) {
      return {
        getConfiguration: function(uuid) {
          return RestangularV2.one('configurations', uuid).get();
        },
        getConfigurations: function(options) {
          return RestangularV2.one('configurations').get(options);
        },
        addConfiguration: function(configuration) {
          return RestangularV2.one('configurations').customPOST(configuration, '', {}, {});
        },
        updateConfiguration: function(configuration, resourceUuid) {
          if (configuration.uuid) {
            return RestangularV2.one('configurations', configuration.uuid).customPUT(configuration,
                '', {}, {});
          } else {
            configuration.resourceUuid = resourceUuid;
            return this.addConfiguration(configuration);
          }
        },
        deleteConfiguration: function(uuid) {
          return RestangularV2.one('configurations', uuid).remove();
        },
        deleteConfiguration2: function(uuid) {
          return RestangularV2.one('configurations', uuid).remove();
        }
      };
    }]);