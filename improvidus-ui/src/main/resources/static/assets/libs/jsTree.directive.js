/*
 * jstree.directive [http://www.jstree.com]
 * http://arvindr21.github.io/jsTree-Angular-Directive
 * 
 * Copyright (c) 2014 Arvind Ravulavaru Licensed under the MIT license.
 */

var ngJSTree = angular.module('jsTree.directive', []);
ngJSTree.directive('jsTree', [
    '$http',
    'DeviceService',
    'DeviceGroupService',
    function($http, DeviceService, DeviceGroupService) {

      var typesConfig = {
        "deviceGroup": {
          "icon": "fa fa-folder text-primary fa-lg"
        },
        "device": {
          "icon": "fa fa-map-marker text-primary fa-lg"
        },
        "dataSet": {
          "icon": "fa fa-files-o text-success fa-lg"
        }
      }

      var sort = function(a, b) {
        var nodeA = this.get_node(a);
        var nodeB = this.get_node(b);
        var aType = nodeA.type;
        var bType = nodeB.type;
        var aName = nodeA.displayName;
        var bName = nodeB.displayName;
        if (aType == bType) {
          return (aName < bName) ? -1 : (aName > bName) ? 1 : 0;
        } else {
          return (aType < bType) ? 1 : -1;
        }
      }

      var treeDir = {
        restrict: 'EA',
        fetchResource: function(url, cb) {
          return $http.get(url).then(function(data) {
            if (cb) cb(data.data);
          });
        },

        managePlugins: function(s, e, a, config) {
          if (a.treePlugins) {
            config.plugins = a.treePlugins.split(',');
            config.core = config.core || {};
            config.core.check_callback = config.core.check_callback || true;

            if (config.plugins.indexOf('state') >= 0) {
              config.state = config.state || {};
              config.state.key = a.treeStateKey;
            }
            
/*USE CUSTOM SEARCH BAR*/
//            if (config.plugins.indexOf('search') >= 0) {
//              var to = false;
//              if (e.next().attr('class') !== 'ng-tree-search') {
//                e.after('<input type="text" placeholder="Search Tree" class="ng-tree-search"/>')
//                    .next().on('keyup', function(ev) {
//                      if (to) {
//                        clearTimeout(to);
//                      }
//                      to = setTimeout(function() {
//                        treeDir.tree.jstree(true).search(ev.target.value);
//                      }, 250);
//                    });
//              }
//            }

            if (config.plugins.indexOf('checkbox') >= 0) {
              config.checkbox = config.checkbox || {};
              config.checkbox.keep_selected_style = false;
            }

            if (config.plugins.indexOf('contextmenu') >= 0) {
              if (a.treeContextmenu) {
                config.contextmenu = s[a.treeContextmenu];
              }
            }

            if (config.plugins.indexOf('types') >= 0) {
              if (a.treeTypes) {
                config.types = s[a.treeTypes] ? s[a.treeTypes] : typesConfig;
              }
            }

            if (config.plugins.indexOf('dnd') >= 0) {
              if (a.treeDnd) {
                config.dnd = s[a.treeDnd];
              }
            }

            if (config.plugins.indexOf('sort') >= 0) {
              if (a.treeSort) {
                config.sort = s[a.treeSort] ? s[a.treeSort].sort : sort;
              }
            }

          }
          return config;
        },
        manageEvents: function(s, e, a) {
          if (a.treeEvents) {
            var evMap = a.treeEvents.split(';');
            for (var i = 0; i < evMap.length; i++) {
              if (evMap[i].length > 0) {
                // plugins could have events with suffixes other than '.jstree'
                var evt = evMap[i].split(':')[0];
                if (evt.indexOf('.') < 0) {
                  evt = evt + '.jstree';
                }
                var cb = evMap[i].split(':')[1];
                treeDir.tree.on(evt, s[cb]);
              }
            }
          }
        },
        link: function(s, e, a) { // scope, element, attribute \O/
          $(function() {
            var config = {};

            // users can define 'core'
            config.core = {};
            if (a.treeCore) {
              config.core = $.extend(config.core, s[a.treeCore]);
            }

            var deviceGroup2Node = function(deviceGroup) {
              deviceGroup.id = deviceGroup.uuid;
              deviceGroup.parent = deviceGroup.parentUuid == '-1' ? '#' : deviceGroup.parentUuid;
              deviceGroup.text = deviceGroup.displayName;
              deviceGroup.type = "deviceGroup";
              deviceGroup.children = true;
              return deviceGroup;
            }

            var device2Node = function(device, parentUuid) {
              device.id = device.uuid;
              device.parent = parentUuid;
              device.text = device.displayName;
              device.type = "device";
              return device;
            }

            // clean Case
            a.treeData = a.treeData ? a.treeData.toLowerCase() : '';
            a.treeSrc = a.treeSrc ? a.treeSrc.toLowerCase() : '';

            if (a.treeData == 'html') {
              treeDir.fetchResource(a.treeSrc, function(data) {
                e.html(data);
                treeDir.init(s, e, a, config);
              });
            } else if (a.treeData == 'json') {
              treeDir.fetchResource(a.treeSrc, function(data) {
                config.core.data = data;
                treeDir.init(s, e, a, config);
              });
            } else if (a.treeData == 'scope') {
              s.$watch(a.treeModel, function(n, o) {
                if (n) {
                  config.core.data = s[a.treeModel];
                  $(e).jstree('destroy');
                  treeDir.init(s, e, a, config);
                }
              }, true);
              // Trigger it initally
              // Fix issue #13
              config.core.data = s[a.treeModel];
              treeDir.init(s, e, a, config);
            } else if (a.treeAjax) {
              config.core.data = function(node, cb) {
                var uuid = node.id === "#" ? '-1' : node.id;
                var nodes = [];
                DeviceGroupService.getDeviceGroups(uuid).then(function(deviceGroups) {
                  for (var i = 0; i < deviceGroups.length; i++) {
                    nodes.push(deviceGroup2Node(deviceGroups[i]));
                  }
                  if (uuid === '-1') {
                    cb(nodes);
                  } else {
                    return DeviceService.getDevices(uuid);
                  }
                }).then(function(devices) {
                  if (devices) {
                    for (var i = 0; i < devices.length; i++) {
                      nodes.push(device2Node(devices[i], uuid));
                    }
                  }
                  cb(nodes);
                });
              };
              treeDir.init(s, e, a, config);
            }
          });

        },
        init: function(s, e, a, config) {
          treeDir.managePlugins(s, e, a, config);
          this.tree = $(e).jstree(config);
          s.$parent[a.treeInstance] = this.tree;
          treeDir.manageEvents(s, e, a);
        }
      };

      return treeDir;

    }]);
